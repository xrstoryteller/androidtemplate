﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionManager : MonoBehaviour
{
    [SerializeField] private string selectableTag = "CueBall1";
    public Material highlightMaterial;
    public Material defaultMaterial;

    private Transform _selection;

    void Update()
    {
        if (_selection != null)
        {
          var selectionRenderer = _selection.GetComponent<Renderer>();
          selectionRenderer.material = defaultMaterial;
          _selection = null;
        }

        var ray = Camera.main.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
          var selection = hit.transform;

          //if it's a selectable object
          if (selection.CompareTag(selectableTag))
          {
            var selectionRenderer = selection.GetComponent<Renderer>();
            if (selectionRenderer != null )
            {
              selectionRenderer.material = highlightMaterial;
            }
            _selection = selection;
          }
        }
    }
}
