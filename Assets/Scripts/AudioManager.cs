using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;


// if you don't hear anything, it's probably because you need to turn up the volume
// slider for that audio entry in the AudioManager
public class AudioManager : MonoBehaviour {
    public Sound[] sounds;      // store all our sounds
    public Sound[] ambient;    // store ambient background track
    public Sound[] playlist;    // store all our music
    public GameObject Canvas;
    public string mainMenu;


    private int currentPlayingIndex = 999; // set high to signify no song playing

    // a play music flag so we can stop playing music during cutscenes etc
    private bool shouldPlayMusic = false;
    private bool shouldPlayAmbient = false;

    public static AudioManager instance; // will hold a reference to the first AudioManager created

    private float mvol; // Global music volume
    private float evol; // Global effects volume

    private Sound currentSound;

    private void Start() {
        //start the music
        //PlayMusic();
        PlayAmbient();
    }


    private void Awake() {
/*
        if (instance == null) {     // if the instance var is null this is first AudioManager
            instance = this;        //save this AudioManager in instance
        } else {
            Destroy(gameObject);    // this isnt the first so destroy it
            return;                 // since this isn't the first return so no other code is run
        }

        DontDestroyOnLoad(gameObject); // do not destroy me when a new scene loads
*/
        instance = this;
        
        // get preferences
        mvol = PlayerPrefs.GetFloat("MusicVolume", 0.75f);
        evol = PlayerPrefs.GetFloat("EffectsVolume", 0.75f);

        createAudioSources(sounds, evol);     // create sources for effects
        createAudioSources(ambient, mvol);   // create sources for ambient
        createAudioSources(playlist, mvol);   // create sources for music

    }

    // create sources
    private void createAudioSources(Sound[] sounds, float volume) {
        foreach (Sound s in sounds) {   // loop through each music/effect
            s.source = gameObject.AddComponent<AudioSource>(); // create anew audio source(where the sound splays from in the world)
            s.source.clip = s.clip;     // the actual music/effect clip
            s.source.volume = s.volume * volume; // set volume based on parameter
            s.source.pitch = s.pitch;   // set the pitch
            s.source.loop = s.loop;     // should it loop
        }
    }

    public void PlaySound(string name) {
        // here we get the Sound from our array with the name passed in the methods parameters
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null) {
            Debug.LogError("Unable to play sound " + name);
            return;
        }
        s.source.Play(); // play the sound
    }

    public void PlayMusic(string name) {
        if (shouldPlayMusic == false && playlist.Length != 0) {
            Debug.Log("I should play music");
            shouldPlayMusic = true;
            if (name != "")
            {
              Debug.Log("I'm searching for a name...");
              Sound s = Array.Find(playlist, sound => sound.name == name);
              if (s == null) {
                  Debug.LogError("Unable to play music " + name);
                  return;
              }
              s.source.Play(); // play the music
            }
            // pick a random song from our playlist
            else
            {
              Debug.Log("I should play a random song");
              currentPlayingIndex = UnityEngine.Random.Range(0, playlist.Length - 1);
              playlist[currentPlayingIndex].source.volume = playlist[0].volume * mvol; // set the volume
              playlist[currentPlayingIndex].source.Play(); // play it
            }
        }
    }

    // play background wind noise
    public void PlayAmbient() {
      if (shouldPlayAmbient == false && playlist.Length != 0) {
          shouldPlayAmbient = true;
          ambient[0].source.volume = ambient[0].volume * mvol; // set the volume
          ambient[0].source.Play(); // play it
      }
    }

    // stop music
    public void StopMusic() {
        if (shouldPlayMusic == true) {
            shouldPlayMusic = false;
            playlist[currentPlayingIndex].source.Stop(); // stop it
            Debug.Log("Stopping music");
            //currentPlayingIndex = 999; // reset playlist counter
        }
    }

    public IEnumerator WaitForSound(string name) {
      Sound s = Array.Find(sounds, sound => sound.name == name);
      yield return new WaitUntil(() => s.source.isPlaying == false);
      // or yield return new WaitWhile(() => audiosource.isPlaying == true);

      Debug.Log("Ready to end The Creeps Demo...");
      Canvas.GetComponent<FadeIn>().fadeOut();
      yield return new WaitForSeconds(5);
      SceneManager.LoadScene(mainMenu);
    }

    void Update() {
        // if we are playing a track from the playlist && it has stopped playing
        if (shouldPlayMusic && playlist.Length != 0 && currentPlayingIndex != 999 && !playlist[currentPlayingIndex].source.isPlaying) {
            currentPlayingIndex++; // set next index
            if (currentPlayingIndex >= playlist.Length) { //have we went too high
                currentPlayingIndex = 0; // reset list when max reached
            }
            playlist[currentPlayingIndex].source.Play(); // play that funky music
        }
    }

    // get the song name
    public String getSongName() {
        return playlist[currentPlayingIndex].name;
    }

    // if the music volume change update all the audio sources
    public void musicVolumeChanged() {
        foreach (Sound m in playlist) {
            mvol = PlayerPrefs.GetFloat("MusicVolume", 0.75f);
            m.source.volume = playlist[0].volume * mvol;
        }
    }

    //if the effects volume changed update the audio sources
    public void effectVolumeChanged() {
        evol = PlayerPrefs.GetFloat("EffectsVolume", 0.75f);
        foreach (Sound s in sounds) {
            s.source.volume = s.volume * evol;
        }
        sounds[0].source.Play(); // play an effect so user can her effect volume
    }
}
