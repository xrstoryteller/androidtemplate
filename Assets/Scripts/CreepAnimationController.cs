﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

// create functions to disable all animations except the one being called to animate
// for use by buttons on a controller, or in my case gaze colliders and animation events
// put this on your character
public class CreepAnimationController : MonoBehaviour
{
    public GameObject EyeBall;
    public GameObject BallAttachment;
    public GameObject Creep;
    public GameObject Creep2;
    public float moveSpeed = 5f;
    public static bool LookForTouch = false;

    private Rigidbody BallRb;
//    private Rigidbody CreepRb;
    private Vector2 movementVec; //should be a Vector3?
    private float angle2Ball;
    public static bool searching = false; // need to check in BallController
    private bool idling = false;
    private float idleTimer = 0.0f;
    private float waitTime = 2.0f;
    private bool ready2Return = false;
    private bool justStartedSearching = true;

    public static float ComeTime = 50f;
    private Vector3 Velocity = Vector3.zero;
    private Vector3 StartingPos;
    private Quaternion StartingRot;

    private UnityAction PeekOnlyListener;

    #region Attributes

    private Animator animator;

    private const string IDLE0_ANIMATION_BOOL = "Idle0";
    private const string INTROPEEK_ANIMATION_BOOL = "IntroPeek";
    private const string IDLE1_ANIMATION_BOOL = "Idle1"; //default pose
    private const string CATCH1_ANIMATION_BOOL = "Catch1"; //start to smile
    private const string IDLE2_ANIMATION_BOOL = "Idle2"; //smile
    private const string CATCH2_ANIMATION_BOOL = "Catch2"; //smile and bouncing
    private const string IDLE3_ANIMATION_BOOL = "Idle3"; //ready to catch (also after Catch3)
    private const string CATCH3_ANIMATION_BOOL = "Catch3"; //bouncing and look at Creep2
    private const string HEADPOPOFF_ANIMATION_BOOL = "HeadPopOff";
    private const string SEARCHHEAD_ANIMATION_BOOL = "SearchHead";
    private const string WALK_ANIMATION_BOOL = "Walk";
    private const string PICKUP_ANIMATION_BOOL = "PickUp";
    private const string WALKWITHBALL_ANIMATION_BOOL = "WalkWithBall";//still need procs
    private const string PICKUPSMILE_ANIMATION_BOOL = "PickUpSmile";//still need procs

    #endregion

    #region Animate Functions

    public void AnimateIdle0()
    {
      Animate(IDLE0_ANIMATION_BOOL);
      Debug.Log("Starting Idle 0");
    }

    //made this one a coroutine so it could run asynchronously because I want to delay it and musicbox music
    public IEnumerator AnimateIntroPeek()
    {
      yield return new WaitForSeconds(2f);
      Animate(INTROPEEK_ANIMATION_BOOL);
      Debug.Log("Starting IntroPeek");
      yield return new WaitForSeconds(2f);
      AudioManager.instance.PlayMusic("");
    }

    public void AnimateIdle1()
    {
      Animate(IDLE1_ANIMATION_BOOL);
      Debug.Log("Starting Idle 1");
      idling = true;
      BallController.Swiped = false; //shouldn't need, but it got set to true somehow????
      LookForTouch = true;
    }

    public void AnimateCatch1()
    {
      LookForTouch = false;
      Animate(CATCH1_ANIMATION_BOOL);
      Debug.Log("Starting Catch1");
      idling = false;
      BallController.Swiped = false;
    }

    public void AnimateIdle2()
    {
      Animate(IDLE2_ANIMATION_BOOL);
      Debug.Log("Starting Idle 2 Smile");
      idling = true;
      LookForTouch = true;
    }

    public void AnimateCatch2()
    {
      LookForTouch = false;
      Animate(CATCH2_ANIMATION_BOOL);
      Debug.Log("Starting Catch2 Bouncing");
      idling = false;
      BallController.Swiped = false;
    }

    public void AnimateIdle3()
    {
      Animate(IDLE3_ANIMATION_BOOL);
      Debug.Log("Starting Idle 3 Ready2Catch");
      idling = true;
      LookForTouch = true;
    }

    public void AnimateCatch3()
    {
      LookForTouch = false;
      Animate(CATCH3_ANIMATION_BOOL);
      Debug.Log("Starting Catch3 Look at Creep2");
      idling = false;
      BallController.Swiped = false;
    }

    //go to Idle 3 again before Head Pop Off

    public void AnimateHeadPopOff()
    {
      Animate(HEADPOPOFF_ANIMATION_BOOL);
      Debug.Log("Starting HeadPopOff");
      idling = false;
      BallController.Swiped = false;
      AudioManager.instance.PlaySound("DrunkSquirrel");
      AudioManager.instance.StopMusic();
      AudioManager.instance.PlaySound("RecordScratch");
      //wait 10 seconds before letting Creep1 rise from the dead
      StartCoroutine(AnimateSearchHead());
      //listen for it to stop playing, so can end experience
      StartCoroutine(AudioManager.instance.WaitForSound("DrunkSquirrel"));
    }

    public IEnumerator AnimateSearchHead()
    {
      Debug.Log("Starting Coroutine AnimateSearchHead");
      LookForTouch = false;
      yield return new WaitForSeconds(10f);
      Animate(SEARCHHEAD_ANIMATION_BOOL);
      Debug.Log("Starting Searching for Head");
      idling = false;
      BallController.Swiped = false;
    }

    public void AnimateWalk()
    {
      Animate(WALK_ANIMATION_BOOL);
      Debug.Log("Starting Walk");
      //do NOT mess with idling or Swiped since in one case (searching) we want to keep running the initial smoothDamp in update
    }

    public void AnimateWalkWithBall()
    {
      Animate(WALKWITHBALL_ANIMATION_BOOL);
      Debug.Log("Starting Walk With Ball");
      //do NOT mess with idling or Swiped since in one case (searching) we want to keep running the initial smoothDamp in update
    }

    public void AnimateWalkCatch()
    {
      Animate(PICKUP_ANIMATION_BOOL); //clip needs anim event to call CreepReadyToReturn when finishes
      Debug.Log("Starting WalkCatch");

      //we can break out of searching mode in teh update function now
      idling = false;
      BallController.Swiped = false;
    }

    #endregion


    // Start is called before the first frame update
    void Start()
    {
      animator = GetComponent<Animator>();
      AnimateIdle0();
      StartingPos = transform.position;
      StartingRot = transform.rotation;
      //CreepRb = this.GetComponent<Rigidbody>();
    }

    //why am I using FixedUpdate again? For physics?
    void FixedUpdate()
    {
      //only start searching when we are idling and a swipe has occured
      if (idling && BallController.Swiped)
      {
        //Debug.Log("CreepAnimationController: BallController.Swiped is " + BallController.Swiped);
        idleTimer += Time.deltaTime;

        //if we've waited long enough
        if (idleTimer > waitTime)
        {
          //if (justStartedSearching)
          //{
          AnimateWalk();
        //    justStartedSearching = false;
          //}

          //if ComeTime is too big, Creep Pushes the ball...not good - but may be something else like collider related
          transform.position = Vector3.SmoothDamp(transform.position, EyeBall.transform.position, ref Velocity, ComeTime * Time.deltaTime);
//          Debug.Log ("Eyeball is at " + EyeBall.transform.position);
//          Debug.Log ("Creep is at " + transform.position);
          //Debug.Log ("Distance is " + Vector3.Distance(transform.position, EyeBall.transform.position));
          searching = true; // need to check which catch to use in BallController


          //for later...I don't like how inaccurate/poppy this is, should I base it on colliders?
          //may not be necessary at all since BallController is checking for collisions and triggering what to do- remove
/*          if (Vector3.Distance(transform.position, EyeBall.transform.position) <= 0.225) //.23 does not work, .225 has twice
          {
            Debug.Log ("Creep found the ball!");
            //make a specific walkCatch animation, have that triggered instead of Catch1
            //do the walk animation (with arms out so no pop)
            //searching = false;
//            idleTimer = idleTimer - waitTime;  //subtracting 2 is more accurate over time than resetting to zero???
            //make it go back to original position and face the user.
            //once that works do the walk animation (with arms out so no pop), figure out how to trigger it
            //catch1 could be getting triggered by animation event in idle1, so if we're in
            //walk anim, put an animation event in there - no, anim could go too long -
            //trigger the walkCatch anim, then trigger walk again once starts to move back
            //THEN when arrive trigger the smile one, or pick up where left off before added locomotion
          }*/
        }
      }
      //may need to use root motion to get anim to play while SmoothDamp is running?
      else if (ready2Return)
      {
        //Debug.Log("In ready2Return of Update...");
        //doesn't bounce or fly off until I uncomment below

        if (justStartedSearching)
        {
          AnimateWalkWithBall();
          justStartedSearching = false;
          Creep.GetComponent<Collider>().enabled = false;
        }

//        Debug.Log ("Distance is " + Vector3.Distance(transform.position, StartingPos));
        transform.position = Vector3.SmoothDamp(transform.position, StartingPos, ref Velocity, ComeTime * Time.deltaTime);

        //*********still need to reset rotation even if don't turn him around - it get's off sometimes

        //check if we made it back to where we started
        if (Vector3.Distance(transform.position, StartingPos) <= 0.132)
        {
          Debug.Log ("Made it back to Starting Position! Checking CatchCount...");
          ready2Return = false;

          //Trigger acting depending on which catch this is
          //same code as in BallController, but have to have it here too in case had to go searching
          int CatchCount = BallController.CatchCount;
          Debug.Log("Catchcount (in AnimController) is " + CatchCount);
          if (CatchCount == 0)
          {
            AnimateCatch1(); //this is just acting part after catch
            ComeTime = 40f; //next time
            //change user's swipe ball velocity in other script
          }
          else if (CatchCount == 1)
          {
            AnimateCatch2();
            ComeTime = 30f;
            Creep2.GetComponent<Creep2AnimationController>().AnimateWalkUp();
            //change user's swipe ball velocity in other script
          }
          else if (CatchCount == 2)
          {
            AnimateCatch3();
            ComeTime = 20f;
            //Creep2.GetComponent<Creep2AnimationController>().AnimateWatchBall());
            //change user's swipe ball velocity in other script
          }
          else if (CatchCount == 3)
          {
            AnimateHeadPopOff();
            //add sound effect
            //stop music
            //play drunk squirrel
            Creep2.GetComponent<Creep2AnimationController>().AnimateShock2Laugh();
          }
          BallController.CatchCount = CatchCount + 1;
          Debug.Log("Catchcount (in AnimController) now is " + BallController.CatchCount);

    //      whatTouched.gameObject.SetActive (false); //turn off for now just to test
          //CreepCount = CreepCount + 1;
        //  SetCountText ();
        }

      }
    }

    //called by animation event at the end of the WalkCatch animation (Creep_Anim_Pickup)
    public void CreepReadyToReturn()
    {
      Debug.Log("In CreepReadyToReturn");
      ready2Return = true;
      justStartedSearching = true;
      idleTimer = 0;
    }

/*
    void Update()
    {
      //only start searching when we are idling and a swipe has occured
      if (idling && BallController.Swiped)
      {
        idleTimer += Time.deltaTime;

        //if we've waited long enough
        if (idleTimer > waitTime)
        {
          //temporary, while I try to get search-and-grab working
          //then we should make an invisible wall (collider) that surrounds the scene and stops when hit it?
          //or does it work well with a timer on the ball? Wish could make slow to a stop.
          Debug.Log("I'm done waiting...making ball stop...");
          BallController.parentRb.isKinematic = true;
          BallController.parentRb.detectCollisions = false;

          Vector3 ballDirection = EyeBall.transform.position - transform.position;
//          Debug.Log ("ballDirection is " + ballDirection);
  //        Debug.Log ("z and x are " + ballDirection.z + " " + ballDirection.x);
    //      angle2Ball = Mathf.Atan2(ballDirection.y, ballDirection.x) + Mathf.Rad2Deg;
          //CreepRb.rotation = (Quaternion)angle2Ball; // this won't work b/c Creep is Rb3D

          //angleBetween = Vector3.Angle(transform.forward,ballDirection);

          Debug.Log("ballDirection is " + ballDirection);

          ballDirection.Normalize(); //make it between -1 and 1
          Debug.Log("ballDirection is " + ballDirection);
          //movementVec = ballDirection;
          movementVec = new Vector2(ballDirection.x,ballDirection.z);
          Debug.Log("movementVec is " + movementVec);


          searching = true;
        }
      }
    }

    // has the frequency of the physics system, called every fixed frame-rate frame
    // i.e. if app is running at 25fps, this will be called approximately twice per frame
    private void FixedUpdate()
    {
      //if we're searching and we haven't caught up with the ball yet...
      if ((searching) && (EyeBall.transform.position != transform.position))
      {
        Debug.Log ("Eyeball is at " + EyeBall.transform.position);
        Debug.Log ("Creep is at " + transform.position);

        moveCreep(movementVec);
      }
      else if ((searching) && (EyeBall.transform.position == transform.position))
      {
        Debug.Log ("Creep found the ball!");
        searching = false;
        idling = false;
        idleTimer = idleTimer - waitTime;  //subtracting 2 is more accurate over time than resetting to zero???
    }
    }

    void moveCreep(Vector2 direction2Go)
    {
//      Vector3 eulerAngleVelocity = new Vector3 (0, angle2Ball, 0);
//      Debug.Log("Rotating at angle " + eulerAngleVelocity);
//      Quaternion deltaRotation = Quaternion.Euler(eulerAngleVelocity * Time.deltaTime);
//      Debug.Log("deltaRotation is " + deltaRotation);
      Debug.Log("direction2Go (movementVec) is " + direction2Go);
      //CreepRb.MoveRotation(CreepRb.rotation * deltaRotation);  //let's just get translation to work first
      //should this be a Vector3? He's floating off into space which implies a vector 3, correct? Unless
      //he's moving in a non horizontal/vertical plane
      CreepRb.MovePosition(transform.position + (Vector3)(direction2Go * moveSpeed * Time.deltaTime));
    }
*/
    private void Animate(string boolName)
    {
      DisableOtherAnimations(animator, boolName);

      animator.SetBool(boolName, true);
    }

    private void DisableOtherAnimations(Animator animator, string animation)
    {
      foreach (AnimatorControllerParameter parameter in animator.parameters)
      {
        if (parameter.name != animation)
        {
          animator.SetBool(parameter.name, false);
        }
      }
    }

    // called by an animation event on IntroPeek animation
    public void PopSound()
    {
      AudioManager.instance.PlaySound("EyePop");
      AttachBall2CreepHand();
    }
    // called by an animation event on IntroPeek animation
    public void AttachBall2CreepHand()
    {
      //disable physics
      BallRb = EyeBall.GetComponent<Rigidbody>();
      BallRb.isKinematic = true;
      BallRb.detectCollisions = false;

      //parent Ball to the Ball_Attachment_Point on the Creep's left hand
      EyeBall.transform.SetParent(BallAttachment.transform);
      EyeBall.GetComponent<Renderer>().enabled = true;
      EyeBall.transform.localPosition = new Vector3(0, 0, 0);
    }

    // called by an animation event on IntroPeek animation
    public void ReleaseBallFromCreephand()
    {
      AudioManager.instance.PlaySound("BallRoll1Sec");

      //unparent Ball from the Ball_Attachment_Point
      EyeBall.transform.SetParent(null);

      //enable physics
      BallRb = EyeBall.GetComponent<Rigidbody>();
      BallRb.isKinematic = false;
      BallRb.detectCollisions = true;

      //apply a velocity vector on the Ball towards the main camera (user)
      BallRb.velocity =  new Vector3(-0.5F, 0, -1);
    }

}
