﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Lives on Canvas in MainMenu Scene
public class MainMenu : MonoBehaviour
{

    public string newGameScene;

    void Start()
    {
      // Enable screen dimming
      Screen.sleepTimeout = SleepTimeout.SystemSetting;
    }

    public void NewGame()
    {
          SceneManager.LoadScene(newGameScene);
    }

    public void QuitGame()
    {
          Debug.Log("Application.Quit won't work in the editor...");
          Application.Quit();
    }
}
