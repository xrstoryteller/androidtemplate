﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//this goes on the Main Camera
public class Gaze : MonoBehaviour
{
  public Camera mainCam;
  public GameObject ball;     //this is the main way you access functions on other game objects with scripts
  public GameObject FramedPictureCollider;
  public GameObject RadioCollider;
  public GameObject DresserCollider;
  public GameObject CueBall1;
  public GameObject CueBall2;
  public GameObject HideBall2;
  public GameObject Creep;

  private Quaternion initialRotation;
  private Quaternion offsetQuaternion;
  private Quaternion initialGyroRotation;  //(0.0, 0.0, 0.0, 1.0) will look at books ?
                                           //(-0.2, 0.6, 0.7, -0.3) will look where 1st ball comes in
  private int ballCount = 0;
  private UnityAction FadeInFinishedListener;

  void GyroModifyCamera()
  {
      //Apply offset
      transform.rotation = offsetQuaternion * GyroToUnity(Input.gyro.attitude);
  }


  void Awake ()
  {
    AudioManager.instance.PlaySound("QuickRun");

    offsetQuaternion = new Quaternion();
    offsetQuaternion.Set(0.2F, -0.6F, -0.7F, 0.3F);
    initialGyroRotation = Input.gyro.attitude;
    initialRotation = transform.rotation;

    Debug.Log("initialRotation: " + initialRotation.ToString()); // always (0.1, 0.0, 0.0, 1.0)
    Debug.Log("transform.rotation: " + transform.rotation.ToString()); // always (0.1, 0.0, 0.0, 1.0)
    Debug.Log("initialGyroRotation: " + initialGyroRotation.ToString());

//    GyroModifyCamera(); // this flips it upside down right now
    FadeInFinishedListener = new UnityAction (DoThingsAfterFadeIn);
  }

  void Start ()
  {
    ball.SetActive (false);
    //hide the collision object for the second ball until it is needed
    CueBall2.SetActive (false);
    HideBall2.SetActive (false);
  }

  void OnEnable ()
  {
    EventManager.StartListening ("FadeInFinished", FadeInFinishedListener);
  }

  void OnDisable ()
  {
    EventManager.StopListening ("FadeInFinished", FadeInFinishedListener);
  }

  private static Quaternion GyroToUnity(Quaternion q)
  {
      return new Quaternion(q.x, q.y, -q.z, -q.w);
  }

  // Update is called once per frame
  void Update()
  {
    //GyroModifyCamera(); // this flips it upside down right now
    //later put in a check to see if you need to keep shooting rays OPTIMIZATION!!!!

    RaycastHit hit;
    Ray ray = mainCam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));

    if (Physics.Raycast(ray, out hit)) {
      //Debug.DrawRay(ray.origin, ray.direction * 100, Color.red, 5.0F);

      GameObject WhatWasHit = hit.collider.gameObject;
      //print(WhatWasHit.name);

      if (WhatWasHit.CompareTag("FramedPicture")) {
        Debug.Log("I'm looking at the Framed Picture");
        EventManager.TriggerEvent("FramedPictureCreep");
        EventManager.RemoveEvent("FramedPictureCreep"); //this one is experimental, but seems to work to get it to execute once...
      }
      else if (WhatWasHit.CompareTag("Radio")) {
        Debug.Log("I'm looking at the Radio");
        EventManager.TriggerEvent("RadioCreep");
        EventManager.RemoveEvent("RadioCreep"); //this one is experimental, but seems to work to get it to execute once...
      }
      else if (WhatWasHit.CompareTag("Dresser")) {
        Debug.Log("I'm looking at the Dresser");
        EventManager.TriggerEvent("DresserCreep");
        EventManager.RemoveEvent("DresserCreep"); //this one is experimental, but seems to work to get it to execute once...
      }
      else if (WhatWasHit.CompareTag("CueBall1") && ballCount == 0) {
  		  Debug.Log("I'm looking at CueBall1");
        ball.transform.position =  new Vector3(2.0F, 0.1F, -0.9F); //set ball back to starting point
        ball.SetActive (true); //turn the ball on
        ball.GetComponent<Rigidbody>().velocity =  new Vector3(-3, 0, 0); //send it right to left

        ballCount = 1; //keep track of where we are in the story (and only do this part once)
        CueBall1.SetActive(false); //hide this Cueball trigger box
        DresserCollider.SetActive (true);
        CueBall2.SetActive (true);
  	  }
      else if (WhatWasHit.CompareTag("CueBall2") && ballCount == 1) {
        AudioManager.instance.PlaySound("QuickRun");
  		  Debug.Log("I'm looking at CueBall2");
        ball.transform.position =  new Vector3(-0.9F, 0.1F, -2.0F); //set ball to starting point by books
        ball.GetComponent<Renderer>().enabled = true;
        ball.SetActive (false);  //probably inefficient, but need to toggle on/off for ball audio to play again
        ball.SetActive (true);
        ball.GetComponent<Rigidbody>().velocity =  new Vector3(0, 0, 3); //send it right to left
        //trigger ball rolling audio
        ballCount = 2;
        FramedPictureCollider.SetActive (true);
        HideBall2.SetActive (true);
        CueBall2.SetActive(false);
  	  }
      else if (WhatWasHit.CompareTag("HideBall2") && ballCount == 2) {
        Debug.Log("I'm looking at HideBall2");
        //hide the ball
        ball.GetComponent<Renderer>().enabled = false;
        ball.GetComponent<Rigidbody>().isKinematic = true;  //stop physics calculations, but keep active so audio can finish
        //ball.SetActive (false);
        ballCount = 4;
        HideBall2.SetActive (false);

        //Cue the Creep Peeking, which has an animation event at the end of the clip to trigger idle1
        StartCoroutine(Creep.GetComponent<CreepAnimationController>().AnimateIntroPeek());

        //trigger audio question then scared - delayed after he peeks but timed to hide anim
        //Trigger Event? Trigger mechanim?
        //https://www.youtube.com/watch?v=JS4k_lwmZHk
        //CueBall3.SetActive (true);
      }
    }
  }

  //this way you can't trigger things before the fade in finishes
  void DoThingsAfterFadeIn()
  {
    RadioCollider.SetActive (true);
    CueBall1.SetActive (true);
  }
}
