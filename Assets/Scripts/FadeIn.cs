﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

// Lives on Canvas in Level1 scene
public class FadeIn : MonoBehaviour
{
    public Image whiteFade;
    bool fadeInFinished = false;

    // Start is called before the first frame update
    void Start()
    {
        whiteFade.canvasRenderer.SetAlpha(1.0f);

        Invoke("fadeIn",3);

        // Disable screen dimming
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    void Update()
    {
      if ((whiteFade.canvasRenderer.GetAlpha() == 0.0f) && !fadeInFinished)
      {
        Debug.Log("Finished Fading In!");
        EventManager.TriggerEvent("FadeInFinished");
        fadeInFinished = true;
      }
    }

    // Update is called once per frame
    void fadeIn()
    {
        whiteFade.CrossFadeAlpha(0,3,false);
    }

    public void fadeOut()
    {
      whiteFade.CrossFadeAlpha(1,3,false);
    }
}
