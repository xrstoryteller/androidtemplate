﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallTrigger : MonoBehaviour
{
  public Text countText;
  public Text winText;
  public GameObject Creep;
  public GameObject MainCam;

  public static Rigidbody parentRb;
  public static int CatchCount;//static makes this var a member of the class, not a particular instance
  private int CreepCount;

  //redundant bit of code for now - also in BallController
  void SetCountText ()
  {
    countText.text = "Count: " + CreepCount.ToString ();
//    if (CreepCount >= 1)
//      winText.text = "You Win!";
  }

  //called before script is active (often first frame of game)
  void Start ()
  {
    parentRb = this.transform.parent.GetComponent<Rigidbody>(); //get from this gameObject - used to not be parent GO
    CatchCount = 0;
    CreepCount = 0;
    SetCountText ();
    winText.text = "";
  }

  //when the ball enters the camera collider (user) stop the ball
  private void OnTriggerEnter (Collider whatTouched)
  {
    Debug.Log("Ball collided with " + whatTouched);

/*    if (whatTouched.gameObject.name == "Ground")
    {
      //let it keep going
      parentRb.isKinematic = false;
      parentRb.detectCollisions = true;
    }
    else*/
    if (whatTouched.gameObject.name == "Main Camera")
    {
      //make the ball stop
      parentRb.isKinematic = true;
      parentRb.detectCollisions = false;

      //disable Main Camera collider so the ball doesn't accidentally hit it when you swipe
      MainCam = whatTouched.gameObject;
      Camera.main.GetComponent<Collider>().enabled = false;
    }
    //if Creep (this is key if want to gameify later with a bunch of creeps)
    else if (whatTouched.gameObject.CompareTag("Creep"))
    {
      Debug.Log("OnTriggerEnter for BallTrigger - CREEP collided with ball - Checking CatchCount...");

      //make it stop
      parentRb.isKinematic = true;
      parentRb.detectCollisions = false;

      //enable Main Camera collider since if it got to the Creep, we've already swiped
      MainCam.GetComponent<Collider>().enabled = true;

//      Debug.Log("Catchcount (in BallTrigger) is " + CatchCount);

      //run catch (or splat) animation, set var to do it, GetComponent
      if (CreepAnimationController.searching)
      {
        Debug.Log ("Creep caught up to the ball!");
        Creep.GetComponent<CreepAnimationController>().AnimateWalkCatch();//needs to trigger CreepReadyToReturn in CreepAnimationController when finished
        //change user's swipe ball velocity in other script
        //sound effect?
      }
      else if (CatchCount == 0)
      {
        Creep.GetComponent<CreepAnimationController>().AnimateCatch1();
        CreepAnimationController.ComeTime = 40f; //next time
        //change user's swipe ball velocity in other script
      }
      else if (CatchCount == 1)
      {
        Creep.GetComponent<CreepAnimationController>().AnimateCatch2();
        CreepAnimationController.ComeTime = 30f; //next time
        //Creep2.GetComponent<Creep2AnimationController>().AnimateWalkUp());
        //change user's swipe ball velocity in other script
      }
      else if (CatchCount == 2)
      {
        Creep.GetComponent<CreepAnimationController>().AnimateCatch3();
        CreepAnimationController.ComeTime = 20f; //next time
        //Creep2.GetComponent<Creep2AnimationController>().AnimateWatchBall());
        //change user's swipe ball velocity in other script
      }
      else if (CatchCount == 3)
      {
        Creep.GetComponent<CreepAnimationController>().AnimateHeadPopOff();
        //add sound effect
        //stop music
        //Creep2.GetComponent<Creep2AnimationController>().AnimateShock2Laugh());
      }
//      CatchCount = CatchCount + 1;
//      Debug.Log("Catchcount (in BallTrigger) now is " + CatchCount);

//      whatTouched.gameObject.SetActive (false); //turn off for now just to test
      CreepCount = CreepCount + 1;
      SetCountText ();

      //delete Creep you splatted AFTER ANIM FINISHES....still need to do check finishes code
      //Destroy(whatTouched.gameObject);
    }
    //else, the user swiped it away from the Creep and it hit something else
    //put a hidden collider/wall around perimeter
/*    else
    {
      //make it stop
      parentRb.isKinematic = true;
      parentRb.detectCollisions = false;

      //send the creep after it and come back to idle1
      //this is dynamic...

    }*/
  }
}
