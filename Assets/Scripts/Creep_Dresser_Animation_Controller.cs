﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Creep_Dresser_Animation_Controller : MonoBehaviour
{
  private Animator animator;
  private UnityAction DresserListener;

  private const string PEEKONLY_ANIMATION_BOOL = "PeekOnly";

  //this should only be triggered at the beginning, before we do any catching
  public void AnimateDresserPeek()
  {
    Animate(PEEKONLY_ANIMATION_BOOL);
    Debug.Log("Starting Dresser Creep Peeking");
  }

  void Awake()
  {
    DresserListener = new UnityAction (AnimateDresserPeek);  //***May need to put AnimatePeekOnly BELOW this
  }

  // Start is called before the first frame update
  void Start()
  {
    animator = GetComponent<Animator>();
//      AnimateIdle0();
  }

  void OnEnable ()
  {
    EventManager.StartListening ("DresserCreep", DresserListener);
  }

  void OnDisable ()
  {
    EventManager.StopListening ("DresserCreep", DresserListener);
  }

  private void Animate(string boolName)
  {
    DisableOtherAnimations(animator, boolName);

    animator.SetBool(boolName, true);
  }

  private void DisableOtherAnimations(Animator animator, string animation)
  {
    foreach (AnimatorControllerParameter parameter in animator.parameters)
    {
      if (parameter.name != animation)
      {
        animator.SetBool(parameter.name, false);
      }
    }
  }
}
