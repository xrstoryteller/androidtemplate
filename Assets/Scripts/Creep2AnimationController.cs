﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Creep2AnimationController : MonoBehaviour
{
  #region Attributes

  private Animator animator;
  private const string IDLE0_ANIMATION_BOOL = "Creep2-Idle0";
  private const string WALKUP_ANIMATION_BOOL = "Creep2-WalkUp";
  private const string IDLE1_ANIMATION_BOOL = "Creep2-Idle1";
  private const string SHOCK2LAUGH_ANIMATION_BOOL = "Creep2-Shock2Laugh";

  #endregion

  #region Animate Functions

  public void AnimateIdle0()
  {
    Animate(IDLE0_ANIMATION_BOOL);
    Debug.Log("Starting Creep2 Idle 0");
  }

  public void AnimateWalkUp()
  {
    Animate(WALKUP_ANIMATION_BOOL);
    Debug.Log("Starting Creep2 WalkUp");
  }

  public void AnimateIdle1()
  {
    Animate(IDLE1_ANIMATION_BOOL);
    Debug.Log("Starting Creep2 Idle 1");
  }

  public void AnimateShock2Laugh()
  {
    Animate(SHOCK2LAUGH_ANIMATION_BOOL);
    Debug.Log("Starting Creep2 Shock2Laugh");
  }
  #endregion

  // Start is called before the first frame update
  void Start()
  {
      animator = GetComponent<Animator>();
      AnimateIdle0();
  }

  private void Animate(string boolName)
  {
    DisableOtherAnimations(animator, boolName);

    animator.SetBool(boolName, true);
  }

  private void DisableOtherAnimations(Animator animator, string animation)
  {
    foreach (AnimatorControllerParameter parameter in animator.parameters)
    {
      if (parameter.name != animation)
      {
        animator.SetBool(parameter.name, false);
      }
    }
  }
}
