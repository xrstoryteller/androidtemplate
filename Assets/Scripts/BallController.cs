﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//
// This is taken from the rollABall game and I was thinking of using it for both the
// eventual "creep game idea" - SwipeSplat - as well as the simple roll/catch interaction
//
public class BallController : MonoBehaviour
{
  public float speed; //public will show up in inspector
  public Text countText;
  public Text winText;
  public GameObject Creep;
  public GameObject Creep2;
  public GameObject MainCam;

  private Rigidbody rb;
  private Vector2 touchStart, touchEnd;
//  private int SwipeCount;
  private float ModifiedSpeed;
  //public GameObject ballTriggerGO; // don't need?
  public static int CatchCount; //public so others can see it!
  private int CreepCount;
  public static bool Swiped = false;   //set to false when caught

  //called before script is active (often first frame of game)
  void Start ()
  {
    //might need to set radius to 0.5 (what the child collider used to be) instead of 0.48
    rb = GetComponent<Rigidbody>(); //get from this gameObject
//    rb.sleepThreshold = 0.01f;
//    SwipeCount = 0;
    //CatchCount = BallController.CatchCount;
    CatchCount = 0;
    Debug.Log("Initial CatchCount is " + CatchCount);

    CreepCount = 0;
    SetCountText ();
    winText.text = "";
  }

  //called before rendering a frame
  //looks for touch events and input/output positions of swipe
  void Update() {
     //stop checking for touch input after Creep is hit and head pops off
     if ((CatchCount < 4) && CreepAnimationController.LookForTouch)
     {
       rb.drag = 0.5f;
       // Swipe start
       if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began) {
         Debug.Log("I detected a touch!");
         Debug.Log("Camera collider is " + Camera.main.GetComponent<Collider>().enabled);
         touchStart = Input.GetTouch(0).position;
         Debug.Log("touchStart is " + touchStart);
       }
       // Swipe end
       if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended) {
         touchEnd = Input.GetTouch(0).position;
         Debug.Log("touchEnd is " + touchEnd);
         float cameraFacing = Camera.main.transform.eulerAngles.y;
         Debug.Log("cameraFacing is " + cameraFacing);
         Vector2 swipeVector = touchEnd - touchStart;
         Debug.Log("swipeVector is " + swipeVector);
         Vector3 inputVector = new Vector3(swipeVector.x, 0.0f, swipeVector.y);
         Debug.Log("inputVector is " + inputVector);
         Vector3 movementVector = Quaternion.Euler(0.0f, cameraFacing, 0.0f) * Vector3.Normalize(inputVector);
         Debug.Log("movementVector is " + movementVector);
         Debug.Log("movementVector.x is " + movementVector.z);
         movementVector.z = Mathf.Abs(movementVector.z); //make sure never goes backward
         rb.isKinematic = false;
         rb.detectCollisions = true;
  //       SwipeCount = SwipeCount + 1;
  //       CatchCount = BallTrigger.CatchCount; //NO LONGER NEED - IT'S LOCAL!
         Debug.Log("Swiped! BallController CatchCount is " + CatchCount);

         //send the ball on rails right at the Creep on the last swipe (his head is going to pop off)
         if (CatchCount == 3 || touchStart == touchEnd)
         {
           rb.drag = 0f; //I want the ball to roll away from the body
//           Vector3 CreepVector = Creep.transform.position - MainCam.transform.position;
           Vector3 CreepVector = new Vector3 (0.1f, 0.2f, 0.5f); //added 0.2 to y to see if it would go at head
           if (CatchCount == 3)
              ModifiedSpeed = speed * 10;
           else
              ModifiedSpeed = speed * (0.5F * (CatchCount+1)); //add multiplier to speed for 2nd, 3rd, and 4th swipe
           rb.velocity = CreepVector * ModifiedSpeed;
           Debug.Log("Last Swipe or Start&End equal: rb.velocity is " + rb.velocity);
           if (CatchCount == 3)
           {
             Debug.Log("Ready for Head to pop off - touchStart doesn't equal touchEnd");
             CreepAnimationController.searching = false; // keeps us from catching so head can pop off
           }
         }
         else if (CatchCount < 3)
         {
           ModifiedSpeed = speed * (0.5F * (CatchCount+1)); //add multiplier to speed for 2nd, 3rd, and 4th swipe
           rb.velocity = movementVector * ModifiedSpeed;
           Debug.Log("MobifiedSpeed is " + ModifiedSpeed);
           Debug.Log("rb.velocity is " + rb.velocity);
         }
         Debug.Log("Swipe End - swipe value is " + Swiped);
         Swiped = true;     }
       //Debug.Log(rb.velocity);
     }
   }

// MAY NOT NEED BELOW
  //called before performing physics calculations
/*  void FixedUpdate ()
  {
    float moveHorizontal = Input.GetAxis ("Horizontal");
    float moveVertical = Input.GetAxis ("Vertical");

    Vector3 movementVector = new Vector3 (moveHorizontal, 0.0f, moveVertical);
    rb.AddForce (movementVector * speed);
  }
*/
    void SetCountText ()
    {
      countText.text = "Count: " + CreepCount.ToString ();
    //    if (CreepCount >= 1)
    //      winText.text = "You Win!";
    }

    // if something collided with the ball
    void OnCollisionEnter(Collision collision)
    {
      Collider whatTouched = collision.collider;
      Debug.Log("OnCollisionEnter for BallController - Ball collided with " + whatTouched);

  /*    if (whatTouched.gameObject.name == "Ground")
      {
        //let it keep going
        rb.isKinematic = false;
        rb.detectCollisions = true;
      }
      else*/
      if (whatTouched.gameObject.name == "Main Camera")
      {
        //make the ball stop
        Debug.Log("making the ball stop rolling");
        rb.isKinematic = true;
        rb.detectCollisions = false;

        //disable Main Camera collider so the ball doesn't accidentally hit it when you swipe
        MainCam = whatTouched.gameObject;
        Camera.main.GetComponent<Collider>().enabled = false;

        //if it hit the camera, it's okay to reenable the Creep collider
        Creep.GetComponent<Collider>().enabled = true;

      }
      else if (whatTouched.gameObject.CompareTag("BallTrap"))
      {
        AudioManager.instance.PlaySound("Giggle");
        Debug.Log("Ball hit the BallTrap collider");
        this.GetComponent<Renderer>().enabled = false; //using this to hide so audio finishes
        //ballCount = 2;
        //HideBall1.SetActive (false);
        //CueBall2.SetActive (true);
        //HideBall2.SetActive (true);
      }
      //if Creep (this is key if want to gameify later with a bunch of creeps)
      else if (whatTouched.gameObject.CompareTag("Creep"))
      {
        Debug.Log("OnCollisionEnter for BallController - CREEP collided with ball - Checking CatchCount...");

        //make the ballDirection stop IF not the one where the head pops Off
        if (CatchCount < 3)
        {
          Debug.Log("making ball stop rolling");
          rb.isKinematic = true;
          rb.detectCollisions = false;
        }

        //disable the creep collider to avoid the bug where he throws it to himself (never leaves his hand)
        Creep.GetComponent<Collider>().enabled = false;

        //enable Main Camera collider since if it got to the Creep, we've already swiped
        MainCam.GetComponent<Collider>().enabled = true;

  //      Debug.Log("Catchcount (in BallTrigger) is " + CatchCount);

        //if the creep was walking after the ball
        if (CreepAnimationController.searching)
        {
          Debug.Log ("Creep caught up to the ball!");
          Creep.GetComponent<CreepAnimationController>().AnimateWalkCatch();//needs to trigger CreepReadyToReturn in CreepAnimationController when finished
          //change user's swipe ball velocity in other script
          //sound effect?
        }
        //otherwise run a catch or headPopOff animation (or splat animation)
        else if (CatchCount == 0)
        {
          Creep.GetComponent<CreepAnimationController>().AnimateCatch1();
          CreepAnimationController.ComeTime = 40f; //next time
          //change user's swipe ball velocity in other script
          CatchCount = CatchCount + 1;
        }
        else if (CatchCount == 1)
        {
          Creep.GetComponent<CreepAnimationController>().AnimateCatch2();
          CreepAnimationController.ComeTime = 30f; //next time
          Creep2.GetComponent<Creep2AnimationController>().AnimateWalkUp();
          Debug.Log("Just passed AnimateWalkUp code");
          //change user's swipe ball velocity in other script
          CatchCount = CatchCount + 1;
        }
        else if (CatchCount == 2)
        {
          Creep.GetComponent<CreepAnimationController>().AnimateCatch3();
          CreepAnimationController.ComeTime = 20f; //next time
          //Creep2.GetComponent<Creep2AnimationController>().AnimateWatchBall());
          //change user's swipe ball velocity in other script
          CatchCount = CatchCount + 1;
        }
        else if (CatchCount == 3)
        {
          Creep.GetComponent<CreepAnimationController>().AnimateHeadPopOff();
          //add sound effect
          //stop music
          //start drunk squirrel
          Creep2.GetComponent<Creep2AnimationController>().AnimateShock2Laugh();
          CatchCount = CatchCount + 1;
        }
        //uncommenting fixes catchcount getting to 4, but also doubles too fast and DisableOtherAnimations
        // are missed!
        Debug.Log("Catchcount (in BallTrigger) now is " + CatchCount);

  //      whatTouched.gameObject.SetActive (false); //turn off for now just to test
        CreepCount = CreepCount + 1;
        SetCountText ();

        //delete Creep you splatted AFTER ANIM FINISHES....still need to do check finishes code
        //Destroy(whatTouched.gameObject);
      }
      //else, the user swiped it away from the Creep and it hit something else
      //put a hidden collider/wall around perimeter
  /*    else
      {
        //make it stop
        rb.isKinematic = true;
        rb.detectCollisions = false;

        //send the creep after it and come back to idle1
        //this is dynamic...

      }*/
    }
}
