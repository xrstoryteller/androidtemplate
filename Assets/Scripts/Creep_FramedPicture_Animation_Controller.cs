﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Creep_FramedPicture_Animation_Controller : MonoBehaviour
{
    private Animator animator;
    private UnityAction PeekOnlyReverseListener;

    private const string PEEKONLYREVERSE_ANIMATION_BOOL = "PeekOnlyReverse";

    //this should only be triggered at the beginning, before we do any catching
    public void AnimatePeekOnlyReverse()
    {
      Animate(PEEKONLYREVERSE_ANIMATION_BOOL);
      Debug.Log("Starting FramedPicture Creep Peeking in Reverse");
    }

    void Awake()
    {
      PeekOnlyReverseListener = new UnityAction (AnimatePeekOnlyReverse);  //***May need to put AnimatePeekOnly BELOW this
    }

    // Start is called before the first frame update
    void Start()
    {
      animator = GetComponent<Animator>();
//      AnimateIdle0();
    }

    void OnEnable ()
    {
      EventManager.StartListening ("FramedPictureCreep", PeekOnlyReverseListener);
    }

    void OnDisable ()
    {
      EventManager.StopListening ("FramedPictureCreep", PeekOnlyReverseListener);
    }

    private void Animate(string boolName)
    {
      DisableOtherAnimations(animator, boolName);

      animator.SetBool(boolName, true);
    }

    private void DisableOtherAnimations(Animator animator, string animation)
    {
      foreach (AnimatorControllerParameter parameter in animator.parameters)
      {
        if (parameter.name != animation)
        {
          animator.SetBool(parameter.name, false);
        }
      }
    }
}
