﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroControls : MonoBehaviour
{
    private bool gyroEnabled;
    private Gyroscope gyro;

    private GameObject cameraContainer;
    private Quaternion rot;

    // Start is called before the first frame update
    private void Start()
    {
      //makes empty game object and renames it
      cameraContainer = new GameObject("Camera Container");
      cameraContainer.transform.position = transform.position;
      transform.SetParent(cameraContainer.transform);

      gyroEnabled = EnableGyro();
    }

    private bool EnableGyro()
    {
      if (SystemInfo.supportsGyroscope)
      {
        gyro = Input.gyro;
        gyro.enabled = true;

        cameraContainer.transform.rotation = Quaternion.Euler(90f, 90f, 0f);
        //point straight in front of us
        rot = new Quaternion(0, 0, 1, 0);

        return true;
      }
      return false;
    }

    // Update is called once per frame
    private void Update()
    {
      if (gyroEnabled)
      {
        transform.localRotation = gyro.attitude * rot;
      }
    }
}
