//Maya ASCII 2019 scene
//Name: Creep2_Anim_Shock2Laugh.7.ma
//Last modified: Sat, Aug 22, 2020 09:46:39 AM
//Codeset: 1252
file -rdi 1 -ns "rig" -rfn "rigRN" -op "v=0;" -typ "mayaAscii" "C:/Users/davnkrit/Documents/maya/projects/default//scenes/Creep_Rig.ma";
file -r -ns "rig" -dr 1 -rfn "rigRN" -op "v=0;" -typ "mayaAscii" "C:/Users/davnkrit/Documents/maya/projects/default//scenes/Creep_Rig.ma";
requires maya "2019";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" -nodeType "aiViewRegion"
		 "mtoa" "3.1.2";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2019";
fileInfo "version" "2019";
fileInfo "cutIdentifier" "201812112215-434d8d9c04";
fileInfo "osv" "Microsoft Windows 10 Technical Preview  (Build 18362)\n";
fileInfo "license" "student";
fileInfo "AnimSchoolPickerData" "AAA4/njapVvJjhxFFMxh3zGbzWIjY1nGjCyEEbIQcMBGBiyqZBzYwtmX0Rg3MIw3zRgsSwg4cOQAn4AQ4ivAEnDjyImP4AIfYF5lV09NducS0UxPqrd4r15mvBeZlV3lnLvNObfkhr8d7oy75K66NWsX3Nidv2l/9vnT/fe/GfygPT87NZh8Hf6OuL2ZxxvupHsr++0kgt1uw476kXvFtW6lt1hxH1gcGxbJBPOrtR+sfT179D7KpRDpdj9vWx9W3fnI0y0GfNDaj1E/wn/GU2P2H7rL9rgaebrVgA9YW7fXu2hP6zbCY3ts99Tx8KWBf4rH1uV7h0xMt1v7wsCXo5hqnlIx3WHtDwP/GY/TzW3pkhhxJGK604BdBv1bGfG90TidcVfcUfvkYuTrLoO2sq/GYpr3dbdBP5d9vR/ebc708R5r63O+5sd9bzTu6T7ea+1V2Ve6j/dZu9/aP5KvdB/Nz9In1vZHvr6aG6/90XidcO+ESly1HIsrsaudI72/XQV/BxL+jpuHc9bja5FHq2v3jBkdoCNEMcId1s72/rgIUYnwIWs/W3sk8njjRl1Fj7vrVqGKio6DxfajP2ztb2t7Ukff1p/d0YjP++mi/07yg6SfR/vReC0zGjv73rU2jp+a1cdRb5ej3l7cwnSvV+2bS9GxHrP2l7VTlZjjat/udfC109ovkq8266uz/17yhayvxw16wp6f69s0X1/vX95hCtHYI8yA1Tm3PpfW58j63Fef0+pzVX0OYuYWZs5g5gJG4xntZjSZ1VpeQ1lt5DWvpko1tampiKYDTH0zdcvU43KyslbCczeim/Zu1d6NnTf0oUydrbjPAvNrNrZr9r5bnV8v4uf9nxXxXsSPQkbk8BuhHodIOKSnkaOQtznkpr1eDeczZ0mcJ3GjMEpp7cuzlsPnWOPxXsRPWMvh51ljkJ5GTljLIWdZq+M8iSsf96p9MnanDXvFcAezuOln74bZ6EJx5KefvWfKuGZ5Me6VthbDWohhXxbXsbke+jQ9+vzsWM7DFL6Uhxzei/ghD1P4dB7WkJ5GDvmQQqbysIzzJG6iHhDVA6J6QFQPiOoBWj1Aqwdo9QCpHiDVA6R6gFQPCOoBUT1Aqgco9YCoHhDVA6J6QFQP0OoBWj1AqwdI9QCpHiDVI32+U14xQlAPHu9F/LBiBKUeDNLTyGHFCEI96jhP4sqsdXPG9DzwTTuj6s6qSngk8c9nz3FzPGsWXrYYba2k0hazbLNYL2CHNUYaG3POIT2NHG2NWepcv8wLa+Fli4GXtEWalzrWC9iBlzQ2xUsN6WnkwEtqf6XMC2vhZYtR2Du4EnYvNtyxEGm3/3LY0N1+BiJeDgtoL6FHAX2gEHuKnRrS08huHF4sIM9tRX8yfD8Ov14eDuMNWQMhayBkDYSggRA0EIIGgtZA0BoIWgMhayBkDYSsgRA0EIIGQtBA0BoIWgNBayBkDYSsgRA18GDBIs1LHesF7MALp36g1Q+S+uV24XPc6FZ+Iath/zNvNcuTgvcifjJvlvExZzzaS+gukpcqv7Xk2FvEzi9oN6x/SnazHGoWXraYzau0RcykgvciflKJWKgSsVAlYqFKhFiJECsRYiVCqkRIlQipErFgJWLBSsSClQi5EiFXIuRKhFiJECsRUiUuJ39Hzu155dBpxnm0l9DDftM8epZTBudJ3CjsAKdxMWt1lKdQpZ5Od0o6ds5XcN2qtYYD6Q8z/g4Vj7sWtOR02G3vVmd5PDL4ZcL/sf7MNY9GBt1I2d9I2d9I2d9I2d+Q2d+Q2d+Q2d9Q2d9Q2d8Q2b+cvE4lzxAkhiAxBIkhkAyBZAgkQ6AYAsUQCIZeJq4UyvHF2abZW9TW/w/bydqVsZ3lWbfyC1l1Eb5AWcUZodr4BWyG86PUFV65HClZlHaUWQsvWww7/WmL9E5/HesF7LDTn8am9pJrSE8jJ0y2MpOtzGQrM9nKTLYCk63AZCsw2dJMtjSTLc0kZCYhMwmZSchMQmASApMQmATNJGgmQTI5e5V5d9XKZnQV7b7E9cHTdXHuSugJ6lg4zoXC9c153L4Ebv6oT1j73bmlU/3dDPxdcZOelu/nqI/Nk/39M99k7qBwyXty8r1+yuBH7flaxR/HyW5rZ6x9S0dX5mRPGFFnFc5Gl+XuP7ARRl0=";
createNode transform -s -n "persp";
	rename -uid "0838AA64-4A98-62B6-4548-5295AE2968E3";
	setAttr ".t" -type "double3" 24.510687144490614 75.708186615554851 71.152809880171105 ;
	setAttr ".r" -type "double3" -33.938352730705674 312.19999999948971 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "1B9FD628-4E67-6D5E-14AC-D0BFCBCE335B";
	setAttr -k off ".v";
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 27.125362338499013;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 67.219579484112089 41.225991525926055 22.968637191990545 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
	setAttr ".ai_translator" -type "string" "perspective";
createNode transform -n "imagePlane1" -p "perspShape";
	rename -uid "8D99D401-41ED-19F1-64DE-1EA625A9B145";
createNode imagePlane -n "imagePlaneShape1" -p "imagePlane1";
	rename -uid "68B50CB4-45E3-9148-EAFA-D9BF2B7B7B4F";
	setAttr -k off ".v";
	setAttr ".t" 2;
	setAttr ".fc" 204;
	setAttr ".imn" -type "string" "C:/Users/davnkrit/Downloads/drunk-squirrel-ringtone.mp4";
	setAttr ".ufe" yes;
	setAttr ".fo" -170;
	setAttr ".fin" 0;
	setAttr ".fot" 900;
	setAttr ".cov" -type "short2" 1920 1080 ;
	setAttr ".d" 1000;
	setAttr ".w" 19.2;
	setAttr ".h" 10.799999999999999;
	setAttr ".cs" -type "string" "sRGB";
createNode transform -s -n "top";
	rename -uid "5BF529D9-45F8-8295-820C-149DAE7B0E13";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "12F9B855-45F0-C9FA-D344-B4BCF39B9520";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" 67.219579484112089 41.225991525926055 22.968637191990545 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "5371EF8A-4BE7-3684-BF5B-CC8B57D19970";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -38.868650064943253 26.260786598424609 1001.5843060432711 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "3657F098-4306-D727-4C2B-82818298EF6D";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 999.67085869811376;
	setAttr ".ow" 130.72849960601386;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".tp" -type "double3" 67.219579484112089 41.225991525926055 22.968637191990545 ;
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "5FD2E9D0-4D12-4155-24E3-1C8431E02111";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1000.1 0 0 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "6594DDCB-494A-2E2D-8958-63AB7188FC90";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" 67.219579484112089 41.225991525926055 22.968637191990545 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "pCube1";
	rename -uid "31C877EE-42B9-EC4D-3C59-DAA14BEDE16F";
	setAttr ".t" -type "double3" -154.44652310577362 40.818580097392015 66.837598786461655 ;
	setAttr ".s" -type "double3" 81.651797226505408 81.651797226505408 81.651797226505408 ;
createNode mesh -n "pCubeShape1" -p "pCube1";
	rename -uid "DA4357C3-45AC-86AC-6920-50B5AECDED6B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".ai_translator" -type "string" "polymesh";
createNode lightLinker -s -n "lightLinker1";
	rename -uid "050AACAA-41D8-A1C8-9932-2C97051F194F";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "FD3E0BDC-4FCF-8D7E-152E-729714D88237";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "DEA572DC-4CD8-9A4A-D4DF-DE8794003319";
createNode displayLayerManager -n "layerManager";
	rename -uid "5EAD071B-4780-BDE8-89C8-C3A0F13F97EE";
createNode displayLayer -n "defaultLayer";
	rename -uid "034C61A9-4C67-1837-3F40-B39D6891558E";
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "21B117AA-42B0-97B8-670D-5C93F0E36491";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "95829084-40BA-3CBA-2109-34A682FE060F";
	setAttr ".g" yes;
createNode timeEditor -s -n "timeEditor";
	rename -uid "49CF4BA3-4B11-6B51-1EB2-71B0DF9201A6";
	setAttr ".ac" 0;
createNode mute -n "aTools_StoreNode";
	rename -uid "20EE98FA-4CF0-5F76-C83C-29A670839C52";
createNode mute -n "scene";
	rename -uid "A84184DA-4737-C63A-FFC2-F7AF31F22152";
	addAttr -ci true -sn "id" -ln "id" -dt "string";
	setAttr ".id" -type "string" "1591814882.06";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "BA2CEF39-411A-D554-E7FF-1F8AB03F50A0";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n"
		+ "            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n"
		+ "            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n"
		+ "            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n"
		+ "            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n"
		+ "            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n"
		+ "            -wireframeOnShaded 1\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -rendererOverrideName \"arnoldViewOverride\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n"
		+ "            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 0\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n"
		+ "            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 656\n            -height 414\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n"
		+ "            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n"
		+ "            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n"
		+ "            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n"
		+ "                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -isSet 0\n                -isSetMember 0\n                -displayMode \"DAG\" \n"
		+ "                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                -selectionOrder \"display\" \n                -expandAttribute 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n"
		+ "                -displayInfinities 0\n                -displayValues 0\n                -autoFit 1\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -showCurveNames 0\n                -showActiveCurveNames 0\n                -clipTime \"on\" \n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                -valueLinesToggle 1\n                -outliner \"graphEditor1OutlineEd\" \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 1\n"
		+ "                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n"
		+ "                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n"
		+ "                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -displayValues 0\n                -autoFit 0\n                -autoFitTime 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 1 \n                $editorName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n"
		+ "                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n"
		+ "                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n"
		+ "                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n"
		+ "\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -editorChanged \"updateModelPanelBar\" \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n"
		+ "                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n"
		+ "                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n"
		+ "                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n"
		+ "            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n"
		+ "            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n"
		+ "            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -rendererOverrideName \\\"arnoldViewOverride\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 0\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 656\\n    -height 414\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 1\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -rendererOverrideName \\\"arnoldViewOverride\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 0\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 656\\n    -height 414\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ttimeControl -e -displaySound 1 -sound drunk_squirrel_ringtone $gPlayBackSlider;\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "DB0C130B-4165-FBA5-6062-7E82F924DF9B";
	setAttr ".b" -type "string" "playbackOptions -min 730 -max 856 -ast 1 -aet 888 ";
	setAttr ".st" 6;
createNode reference -n "rigRN";
	rename -uid "4E7AB6AC-42F0-CAA0-64E0-20B905472388";
	setAttr -s 320 ".phl";
	setAttr ".phl[120]" 0;
	setAttr ".phl[121]" 0;
	setAttr ".phl[122]" 0;
	setAttr ".phl[123]" 0;
	setAttr ".phl[124]" 0;
	setAttr ".phl[125]" 0;
	setAttr ".phl[126]" 0;
	setAttr ".phl[127]" 0;
	setAttr ".phl[128]" 0;
	setAttr ".phl[129]" 0;
	setAttr ".phl[130]" 0;
	setAttr ".phl[131]" 0;
	setAttr ".phl[132]" 0;
	setAttr ".phl[133]" 0;
	setAttr ".phl[134]" 0;
	setAttr ".phl[135]" 0;
	setAttr ".phl[136]" 0;
	setAttr ".phl[137]" 0;
	setAttr ".phl[138]" 0;
	setAttr ".phl[139]" 0;
	setAttr ".phl[140]" 0;
	setAttr ".phl[141]" 0;
	setAttr ".phl[142]" 0;
	setAttr ".phl[143]" 0;
	setAttr ".phl[144]" 0;
	setAttr ".phl[145]" 0;
	setAttr ".phl[146]" 0;
	setAttr ".phl[147]" 0;
	setAttr ".phl[148]" 0;
	setAttr ".phl[149]" 0;
	setAttr ".phl[150]" 0;
	setAttr ".phl[151]" 0;
	setAttr ".phl[152]" 0;
	setAttr ".phl[153]" 0;
	setAttr ".phl[154]" 0;
	setAttr ".phl[155]" 0;
	setAttr ".phl[156]" 0;
	setAttr ".phl[157]" 0;
	setAttr ".phl[158]" 0;
	setAttr ".phl[159]" 0;
	setAttr ".phl[160]" 0;
	setAttr ".phl[161]" 0;
	setAttr ".phl[162]" 0;
	setAttr ".phl[163]" 0;
	setAttr ".phl[164]" 0;
	setAttr ".phl[165]" 0;
	setAttr ".phl[166]" 0;
	setAttr ".phl[167]" 0;
	setAttr ".phl[168]" 0;
	setAttr ".phl[169]" 0;
	setAttr ".phl[170]" 0;
	setAttr ".phl[171]" 0;
	setAttr ".phl[172]" 0;
	setAttr ".phl[173]" 0;
	setAttr ".phl[174]" 0;
	setAttr ".phl[175]" 0;
	setAttr ".phl[176]" 0;
	setAttr ".phl[177]" 0;
	setAttr ".phl[178]" 0;
	setAttr ".phl[179]" 0;
	setAttr ".phl[180]" 0;
	setAttr ".phl[181]" 0;
	setAttr ".phl[182]" 0;
	setAttr ".phl[183]" 0;
	setAttr ".phl[184]" 0;
	setAttr ".phl[185]" 0;
	setAttr ".phl[186]" 0;
	setAttr ".phl[187]" 0;
	setAttr ".phl[188]" 0;
	setAttr ".phl[189]" 0;
	setAttr ".phl[190]" 0;
	setAttr ".phl[191]" 0;
	setAttr ".phl[192]" 0;
	setAttr ".phl[193]" 0;
	setAttr ".phl[194]" 0;
	setAttr ".phl[195]" 0;
	setAttr ".phl[196]" 0;
	setAttr ".phl[197]" 0;
	setAttr ".phl[198]" 0;
	setAttr ".phl[199]" 0;
	setAttr ".phl[200]" 0;
	setAttr ".phl[201]" 0;
	setAttr ".phl[202]" 0;
	setAttr ".phl[203]" 0;
	setAttr ".phl[204]" 0;
	setAttr ".phl[205]" 0;
	setAttr ".phl[206]" 0;
	setAttr ".phl[207]" 0;
	setAttr ".phl[208]" 0;
	setAttr ".phl[209]" 0;
	setAttr ".phl[210]" 0;
	setAttr ".phl[211]" 0;
	setAttr ".phl[212]" 0;
	setAttr ".phl[213]" 0;
	setAttr ".phl[214]" 0;
	setAttr ".phl[215]" 0;
	setAttr ".phl[216]" 0;
	setAttr ".phl[217]" 0;
	setAttr ".phl[218]" 0;
	setAttr ".phl[219]" 0;
	setAttr ".phl[220]" 0;
	setAttr ".phl[221]" 0;
	setAttr ".phl[222]" 0;
	setAttr ".phl[223]" 0;
	setAttr ".phl[224]" 0;
	setAttr ".phl[225]" 0;
	setAttr ".phl[226]" 0;
	setAttr ".phl[227]" 0;
	setAttr ".phl[228]" 0;
	setAttr ".phl[229]" 0;
	setAttr ".phl[230]" 0;
	setAttr ".phl[231]" 0;
	setAttr ".phl[232]" 0;
	setAttr ".phl[233]" 0;
	setAttr ".phl[234]" 0;
	setAttr ".phl[235]" 0;
	setAttr ".phl[236]" 0;
	setAttr ".phl[237]" 0;
	setAttr ".phl[238]" 0;
	setAttr ".phl[239]" 0;
	setAttr ".phl[240]" 0;
	setAttr ".phl[241]" 0;
	setAttr ".phl[242]" 0;
	setAttr ".phl[243]" 0;
	setAttr ".phl[244]" 0;
	setAttr ".phl[245]" 0;
	setAttr ".phl[246]" 0;
	setAttr ".phl[247]" 0;
	setAttr ".phl[248]" 0;
	setAttr ".phl[249]" 0;
	setAttr ".phl[250]" 0;
	setAttr ".phl[251]" 0;
	setAttr ".phl[252]" 0;
	setAttr ".phl[253]" 0;
	setAttr ".phl[254]" 0;
	setAttr ".phl[255]" 0;
	setAttr ".phl[256]" 0;
	setAttr ".phl[257]" 0;
	setAttr ".phl[258]" 0;
	setAttr ".phl[259]" 0;
	setAttr ".phl[260]" 0;
	setAttr ".phl[261]" 0;
	setAttr ".phl[262]" 0;
	setAttr ".phl[263]" 0;
	setAttr ".phl[264]" 0;
	setAttr ".phl[265]" 0;
	setAttr ".phl[266]" 0;
	setAttr ".phl[267]" 0;
	setAttr ".phl[268]" 0;
	setAttr ".phl[269]" 0;
	setAttr ".phl[270]" 0;
	setAttr ".phl[271]" 0;
	setAttr ".phl[272]" 0;
	setAttr ".phl[273]" 0;
	setAttr ".phl[274]" 0;
	setAttr ".phl[275]" 0;
	setAttr ".phl[276]" 0;
	setAttr ".phl[277]" 0;
	setAttr ".phl[278]" 0;
	setAttr ".phl[279]" 0;
	setAttr ".phl[280]" 0;
	setAttr ".phl[281]" 0;
	setAttr ".phl[282]" 0;
	setAttr ".phl[283]" 0;
	setAttr ".phl[284]" 0;
	setAttr ".phl[285]" 0;
	setAttr ".phl[286]" 0;
	setAttr ".phl[287]" 0;
	setAttr ".phl[288]" 0;
	setAttr ".phl[289]" 0;
	setAttr ".phl[290]" 0;
	setAttr ".phl[291]" 0;
	setAttr ".phl[292]" 0;
	setAttr ".phl[293]" 0;
	setAttr ".phl[294]" 0;
	setAttr ".phl[295]" 0;
	setAttr ".phl[296]" 0;
	setAttr ".phl[297]" 0;
	setAttr ".phl[298]" 0;
	setAttr ".phl[299]" 0;
	setAttr ".phl[300]" 0;
	setAttr ".phl[301]" 0;
	setAttr ".phl[302]" 0;
	setAttr ".phl[303]" 0;
	setAttr ".phl[304]" 0;
	setAttr ".phl[305]" 0;
	setAttr ".phl[306]" 0;
	setAttr ".phl[307]" 0;
	setAttr ".phl[308]" 0;
	setAttr ".phl[309]" 0;
	setAttr ".phl[310]" 0;
	setAttr ".phl[311]" 0;
	setAttr ".phl[312]" 0;
	setAttr ".phl[313]" 0;
	setAttr ".phl[314]" 0;
	setAttr ".phl[315]" 0;
	setAttr ".phl[316]" 0;
	setAttr ".phl[317]" 0;
	setAttr ".phl[318]" 0;
	setAttr ".phl[319]" 0;
	setAttr ".phl[320]" 0;
	setAttr ".phl[321]" 0;
	setAttr ".phl[322]" 0;
	setAttr ".phl[323]" 0;
	setAttr ".phl[324]" 0;
	setAttr ".phl[325]" 0;
	setAttr ".phl[326]" 0;
	setAttr ".phl[327]" 0;
	setAttr ".phl[328]" 0;
	setAttr ".phl[329]" 0;
	setAttr ".phl[330]" 0;
	setAttr ".phl[331]" 0;
	setAttr ".phl[332]" 0;
	setAttr ".phl[333]" 0;
	setAttr ".phl[334]" 0;
	setAttr ".phl[335]" 0;
	setAttr ".phl[336]" 0;
	setAttr ".phl[337]" 0;
	setAttr ".phl[338]" 0;
	setAttr ".phl[339]" 0;
	setAttr ".phl[340]" 0;
	setAttr ".phl[341]" 0;
	setAttr ".phl[342]" 0;
	setAttr ".phl[343]" 0;
	setAttr ".phl[344]" 0;
	setAttr ".phl[345]" 0;
	setAttr ".phl[346]" 0;
	setAttr ".phl[347]" 0;
	setAttr ".phl[348]" 0;
	setAttr ".phl[349]" 0;
	setAttr ".phl[350]" 0;
	setAttr ".phl[351]" 0;
	setAttr ".phl[352]" 0;
	setAttr ".phl[353]" 0;
	setAttr ".phl[354]" 0;
	setAttr ".phl[355]" 0;
	setAttr ".phl[356]" 0;
	setAttr ".phl[357]" 0;
	setAttr ".phl[358]" 0;
	setAttr ".phl[359]" 0;
	setAttr ".phl[360]" 0;
	setAttr ".phl[361]" 0;
	setAttr ".phl[362]" 0;
	setAttr ".phl[363]" 0;
	setAttr ".phl[364]" 0;
	setAttr ".phl[365]" 0;
	setAttr ".phl[366]" 0;
	setAttr ".phl[367]" 0;
	setAttr ".phl[368]" 0;
	setAttr ".phl[369]" 0;
	setAttr ".phl[370]" 0;
	setAttr ".phl[371]" 0;
	setAttr ".phl[372]" 0;
	setAttr ".phl[373]" 0;
	setAttr ".phl[374]" 0;
	setAttr ".phl[375]" 0;
	setAttr ".phl[376]" 0;
	setAttr ".phl[377]" 0;
	setAttr ".phl[378]" 0;
	setAttr ".phl[379]" 0;
	setAttr ".phl[380]" 0;
	setAttr ".phl[381]" 0;
	setAttr ".phl[382]" 0;
	setAttr ".phl[383]" 0;
	setAttr ".phl[384]" 0;
	setAttr ".phl[385]" 0;
	setAttr ".phl[386]" 0;
	setAttr ".phl[387]" 0;
	setAttr ".phl[388]" 0;
	setAttr ".phl[389]" 0;
	setAttr ".phl[390]" 0;
	setAttr ".phl[391]" 0;
	setAttr ".phl[392]" 0;
	setAttr ".phl[393]" 0;
	setAttr ".phl[394]" 0;
	setAttr ".phl[395]" 0;
	setAttr ".phl[396]" 0;
	setAttr ".phl[397]" 0;
	setAttr ".phl[398]" 0;
	setAttr ".phl[399]" 0;
	setAttr ".phl[400]" 0;
	setAttr ".phl[401]" 0;
	setAttr ".phl[402]" 0;
	setAttr ".phl[403]" 0;
	setAttr ".phl[404]" 0;
	setAttr ".phl[405]" 0;
	setAttr ".phl[406]" 0;
	setAttr ".phl[407]" 0;
	setAttr ".phl[408]" 0;
	setAttr ".phl[409]" 0;
	setAttr ".phl[410]" 0;
	setAttr ".phl[411]" 0;
	setAttr ".phl[412]" 0;
	setAttr ".phl[413]" 0;
	setAttr ".phl[414]" 0;
	setAttr ".phl[415]" 0;
	setAttr ".phl[416]" 0;
	setAttr ".phl[417]" 0;
	setAttr ".phl[418]" 0;
	setAttr ".phl[419]" 0;
	setAttr ".phl[420]" 0;
	setAttr ".phl[421]" 0;
	setAttr ".phl[422]" 0;
	setAttr ".phl[423]" 0;
	setAttr ".phl[424]" 0;
	setAttr ".phl[425]" 0;
	setAttr ".phl[426]" 0;
	setAttr ".phl[427]" 0;
	setAttr ".phl[428]" 0;
	setAttr ".phl[429]" 0;
	setAttr ".phl[430]" 0;
	setAttr ".phl[431]" 0;
	setAttr ".phl[432]" 0;
	setAttr ".phl[433]" 0;
	setAttr ".phl[434]" 0;
	setAttr ".phl[435]" 0;
	setAttr ".phl[436]" 0;
	setAttr ".phl[437]" 0;
	setAttr ".phl[438]" 0;
	setAttr ".phl[439]" 0;
	setAttr ".ed" -type "dataReferenceEdits" 
		"rigRN"
		"rigRN" 7
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl|rig:L_Wrist_ctrl_orientConstraint1" 
		"L_FK_Hand_jntW0" " -k 1 0"
		3 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl|rig:L_Wrist_ctrl_orientConstraint1.constraintRotateX" 
		"|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl.rotateX" 
		""
		3 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl|rig:L_Wrist_ctrl_orientConstraint1.constraintRotateY" 
		"|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl.rotateY" 
		""
		3 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl|rig:L_Wrist_ctrl_orientConstraint1.constraintRotateZ" 
		"|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl.rotateZ" 
		""
		5 3 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl|rig:L_Wrist_ctrl_orientConstraint1.constraintRotateY" 
		"rigRN.placeHolderList[120]" "rig:L_Wrist_ctrl.ry"
		5 3 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl|rig:L_Wrist_ctrl_orientConstraint1.constraintRotateX" 
		"rigRN.placeHolderList[121]" "rig:L_Wrist_ctrl.rx"
		5 3 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl|rig:L_Wrist_ctrl_orientConstraint1.constraintRotateZ" 
		"rigRN.placeHolderList[122]" "rig:L_Wrist_ctrl.rz"
		"rigRN" 746
		1 |rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl 
		"blendOrient1" "blendOrient1" " -ci 1 -k 1 -dv 1 -smn 0 -smx 1 -at \"double\""
		2 "|rig:left|rig:leftShape" "tumblePivot" " -type \"double3\" 67.21957948411208861 41.2259915259260552 22.96863719199054543"
		
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:L_Eye_jnt" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:L_Eye_jnt" 
		"translate" " -type \"double3\" 16.27944639431007801 10.12720041532355353 8.077056884765625"
		
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:L_Eye_jnt" 
		"translateX" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:L_Eye_jnt" 
		"translateY" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:L_Eye_jnt" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:L_Eye_jnt" 
		"rotate" " -type \"double3\" 7.74578144713195993 11.70908170058925535 -22.3954418992972748"
		
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:L_Eye_jnt" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:L_Eye_jnt" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:L_Eye_jnt" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:L_Eye_jnt" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:L_Eye_jnt" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:L_Eye_jnt" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:L_Eye_jnt" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:L_Eye_jnt" 
		"blendParent1" " -av -k 1 0"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:R_Eye_jnt" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:R_Eye_jnt" 
		"translate" " -type \"double3\" 14.65087367090094972 10.23418844958548313 -12.35089111328125"
		
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:R_Eye_jnt" 
		"translateX" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:R_Eye_jnt" 
		"translateY" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:R_Eye_jnt" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:R_Eye_jnt" 
		"rotate" " -type \"double3\" 7.50439487115724546 12.72819035406805455 -23.9735654982861135"
		
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:R_Eye_jnt" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:R_Eye_jnt" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:R_Eye_jnt" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:R_Eye_jnt" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:R_Eye_jnt" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:R_Eye_jnt" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:R_Eye_jnt" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:M_Body_jnt|rig:M_Chest_jnt|rig:M_Head_jnt|rig:R_Eye_jnt" 
		"blendParent1" " -av -k 1 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl" "visibility" " -av 1"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl" "translate" " -type \"double3\" 144.90778263183347008 0 -33.29978391755764733"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl" "translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl" "translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl" "translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl" "rotate" " -type \"double3\" 0 -62.20315894580102878 0"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl" "rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl" "rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl" "rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl" "scale" " -type \"double3\" 1 1 1"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl" "scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl" "scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl" "scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl" "L_Arm_FKIK" " -av -k 1 0"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl" "R_Arm_FKIK" " -av -k 1 0"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"translate" " -type \"double3\" 21.464352863630765 0 92.69931999522327715"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"rotate" " -type \"double3\" -6.06558277724949058 60.46675590170355719 -6.6832367316788055"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"toeTap" " -av -k 1 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"footPeel" " -av -k 1 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"footSwivel" " -av -k 1 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"toeTip" " -av -k 1 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl" 
		"ankle" " -av -k 1 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl" 
		"translate" " -type \"double3\" -5.50024674495077637 0 12.44570070982003429"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl" 
		"scale" " -type \"double3\" 1.00000000000000022 0.99999999999999989 1.00000000000000022"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"translate" " -type \"double3\" 23.75250489851432789 0 91.34002575768064958"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"rotate" " -type \"double3\" 0 -32.80029683949018704 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"scale" " -type \"double3\" 0.99999999999999967 0.99999999999999967 0.99999999999999944"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"toeTap" " -av -k 1 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"footPeel" " -av -k 1 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"footSwivel" " -av -k 1 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"toeTip" " -av -k 1 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl" 
		"ankle" " -av -k 1 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl" 
		"scale" " -type \"double3\" 1.00000000000000022 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl" 
		"translate" " -type \"double3\" 22.6717714108303916 -1.15755387540422472 91.15099593703672554"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl" 
		"rotate" " -type \"double3\" -10.17790386925628177 -1.22660967483278149 1.79371304644747331"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl" 
		"translate" " -type \"double3\" -13.88142939517161878 -6.95024368782038326 1.03824352897549943"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl" 
		"translate" " -type \"double3\" -23.6585263017672851 -18.67979314645187117 12.20981974031785278"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl" 
		"scale" " -type \"double3\" 0.99999999999999989 1 0.99999999999999989"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl" 
		"rotate" " -type \"double3\" -61.76213062164057988 18.80319269088100853 -68.93572377593395117"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl" 
		"scale" " -type \"double3\" 0.99999999999999944 0.99999999999999944 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl" 
		"rotate" " -type \"double3\" 0 123.13579368890715671 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl" 
		"rotate" " -type \"double3\" -86.15532157146078873 20.55480719512417664 9.71086236539281877"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl" 
		"blendOrient1" " -av -k 1 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl" 
		"rotate" " -type \"double3\" -62.04051593305369749 69.14783890530638644 -118.02401675767467282"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl" 
		"rotate" " -type \"double3\" 0 40.80912820791403561 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl" 
		"scale" " -type \"double3\" 0.99999999999999989 0.99999999999999978 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl" 
		"rotate" " -type \"double3\" -188.54602961645022674 165.58320096687651812 -159.43485031372364347"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl" 
		"scale" " -type \"double3\" 0.99999999999999989 0.99999999999999978 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl" 
		"translate" " -type \"double3\" 0 0.35580644838680442 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"translate" " -type \"double3\" 3.79198028848014923 0 7.08184681841025565"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"L_Uplid" " -av -k 1 100"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"L_Lolid" " -av -k 1 -76.50000000000001421"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"R_Uplid" " -av -k 1 100"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"R_Lolid" " -av -k 1 -78.50000000000001421"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"L_Lid_Twist" " -av -k 1 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"R_Lid_Twist" " -av -k 1 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"L_Lid_Bend" " -av -k 1 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl" 
		"R_Lid_Bend" " -av -k 1 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:M_eye_ctrlShape" 
		"uvSet[0].uvSetName" " -type \"string\" \"map1\""
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:M_eye_ctrlShape" 
		"uvst[0].uvsp[0:13]" " -s 14 -type \"float2\" 0.375 0 0.625 0 0.375 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl|rig:L_eye_ctrlShape" 
		"uvSet[0].uvSetName" " -type \"string\" \"map1\""
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl|rig:L_eye_ctrlShape" 
		"uvst[0].uvsp[0:13]" " -s 14 -type \"float2\" 0.375 0 0.625 0 0.375 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl|rig:R_eye_ctrlShape" 
		"uvSet[0].uvSetName" " -type \"string\" \"map1\""
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl|rig:R_eye_ctrlShape" 
		"uvst[0].uvsp[0:13]" " -s 14 -type \"float2\" 0.375 0 0.625 0 0.375 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0 0.875 0.25 0.125 0 0.125 0.25"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl" 
		"translate" " -type \"double3\" -0.88182559546315209 -21.56376989066798089 105.52216679433892921"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl" 
		"rotate" " -type \"double3\" -0.064715742131998585 -316.815073 -0.025610098002168038"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl" 
		"translate" " -type \"double3\" -31.37203685973145539 -21.44869974981296679 -93.43115482892126522"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl" 
		"rotate" " -type \"double3\" -0.014968310620553218 -43.39388434961613683 -0.046256124349695497"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl" 
		"scale" " -type \"double3\" 1 1.00000000000000022 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl" 
		"rotate" " -type \"double3\" -25.92996357293301202 14.83845511838175035 -7.90064283228776265"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl" 
		"scale" " -type \"double3\" 1.00000000000000044 1.00000000000000022 1.00000000000000067"
		
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl" 
		"rotate" " -type \"double3\" 0 0 2.54776611119121998"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl" 
		"scale" " -type \"double3\" 2.34271804183265164 4.27415496392898131 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl" 
		"translate" " -type \"double3\" -2.35094647243713739 2.37912358419132142 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl" 
		"rotate" " -type \"double3\" 0 0 -30.25673017209843607"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl" 
		"translate" " -type \"double3\" 1.78213134463295408 2.72107703147541535 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl" 
		"rotate" " -type \"double3\" 0 0 39.49827244765612733"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl" 
		"scale" " -type \"double3\" 1 1 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl" 
		"translate" " -type \"double3\" 0 0.1580368632388599 0.3"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl" 
		"scale" " -type \"double3\" 0.99999999999999989 1 0.99999999999999989"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl" 
		"scaleZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl" 
		"visibility" " -av 1"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl" 
		"translate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl" 
		"translateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl" 
		"translateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl" 
		"translateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl" 
		"rotate" " -type \"double3\" 0 0 0"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl" 
		"rotateX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl" 
		"rotateY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl" 
		"rotateZ" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl" 
		"scale" " -type \"double3\" 0.99999999999999989 1 0.99999999999999989"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl" 
		"scaleX" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl" 
		"scaleY" " -av"
		2 "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl" 
		"scaleZ" " -av"
		2 "rig:Model" "visibility" " 1"
		2 "rig:Controls" "visibility" " 0"
		2 "rig:Rig" "displayType" " 1"
		2 "rig:Rig" "visibility" " 0"
		2 "rig:Creep_Rig_HelperRigStuff:HelperRiggingStuff:Rig" "visibility" " 0"
		
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl.L_Arm_FKIK" 
		"rigRN.placeHolderList[123]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl.R_Arm_FKIK" 
		"rigRN.placeHolderList[124]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl.visibility" 
		"rigRN.placeHolderList[125]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl.translateX" 
		"rigRN.placeHolderList[126]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl.translateY" 
		"rigRN.placeHolderList[127]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl.translateZ" 
		"rigRN.placeHolderList[128]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl.rotateX" 
		"rigRN.placeHolderList[129]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl.rotateY" 
		"rigRN.placeHolderList[130]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl.rotateZ" 
		"rigRN.placeHolderList[131]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl.scaleX" 
		"rigRN.placeHolderList[132]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl.scaleY" 
		"rigRN.placeHolderList[133]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl.scaleZ" 
		"rigRN.placeHolderList[134]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl.toeTap" 
		"rigRN.placeHolderList[135]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl.footPeel" 
		"rigRN.placeHolderList[136]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl.footSwivel" 
		"rigRN.placeHolderList[137]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl.toeTip" 
		"rigRN.placeHolderList[138]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl.ankle" 
		"rigRN.placeHolderList[139]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl.visibility" 
		"rigRN.placeHolderList[140]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl.translateX" 
		"rigRN.placeHolderList[141]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl.translateY" 
		"rigRN.placeHolderList[142]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl.translateZ" 
		"rigRN.placeHolderList[143]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl.rotateX" 
		"rigRN.placeHolderList[144]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl.rotateY" 
		"rigRN.placeHolderList[145]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl.rotateZ" 
		"rigRN.placeHolderList[146]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl.scaleX" 
		"rigRN.placeHolderList[147]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl.scaleY" 
		"rigRN.placeHolderList[148]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl.scaleZ" 
		"rigRN.placeHolderList[149]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl.translateX" 
		"rigRN.placeHolderList[150]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl.translateY" 
		"rigRN.placeHolderList[151]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl.translateZ" 
		"rigRN.placeHolderList[152]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl.visibility" 
		"rigRN.placeHolderList[153]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl.rotateX" 
		"rigRN.placeHolderList[154]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl.rotateY" 
		"rigRN.placeHolderList[155]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl.rotateZ" 
		"rigRN.placeHolderList[156]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl.scaleX" 
		"rigRN.placeHolderList[157]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl.scaleY" 
		"rigRN.placeHolderList[158]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_foot_offset|rig:L_foot_ctrl|rig:L_knee_offset|rig:L_knee_ctrl.scaleZ" 
		"rigRN.placeHolderList[159]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl.toeTap" 
		"rigRN.placeHolderList[160]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl.footPeel" 
		"rigRN.placeHolderList[161]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl.footSwivel" 
		"rigRN.placeHolderList[162]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl.toeTip" 
		"rigRN.placeHolderList[163]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl.ankle" 
		"rigRN.placeHolderList[164]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl.visibility" 
		"rigRN.placeHolderList[165]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl.translateX" 
		"rigRN.placeHolderList[166]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl.translateY" 
		"rigRN.placeHolderList[167]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl.translateZ" 
		"rigRN.placeHolderList[168]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl.rotateX" 
		"rigRN.placeHolderList[169]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl.rotateY" 
		"rigRN.placeHolderList[170]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl.rotateZ" 
		"rigRN.placeHolderList[171]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl.scaleX" 
		"rigRN.placeHolderList[172]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl.scaleY" 
		"rigRN.placeHolderList[173]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl.scaleZ" 
		"rigRN.placeHolderList[174]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl.translateX" 
		"rigRN.placeHolderList[175]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl.translateY" 
		"rigRN.placeHolderList[176]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl.translateZ" 
		"rigRN.placeHolderList[177]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl.visibility" 
		"rigRN.placeHolderList[178]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl.rotateX" 
		"rigRN.placeHolderList[179]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl.rotateY" 
		"rigRN.placeHolderList[180]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl.rotateZ" 
		"rigRN.placeHolderList[181]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl.scaleX" 
		"rigRN.placeHolderList[182]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl.scaleY" 
		"rigRN.placeHolderList[183]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_foot_offset|rig:R_foot_ctrl|rig:R_knee_offset|rig:R_knee_ctrl.scaleZ" 
		"rigRN.placeHolderList[184]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl.translateX" 
		"rigRN.placeHolderList[185]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl.translateY" 
		"rigRN.placeHolderList[186]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl.translateZ" 
		"rigRN.placeHolderList[187]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl.rotateX" 
		"rigRN.placeHolderList[188]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl.rotateY" 
		"rigRN.placeHolderList[189]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl.rotateZ" 
		"rigRN.placeHolderList[190]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl.scaleX" 
		"rigRN.placeHolderList[191]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl.scaleY" 
		"rigRN.placeHolderList[192]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl.scaleZ" 
		"rigRN.placeHolderList[193]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl.visibility" 
		"rigRN.placeHolderList[194]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl.translateX" 
		"rigRN.placeHolderList[195]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl.translateY" 
		"rigRN.placeHolderList[196]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl.translateZ" 
		"rigRN.placeHolderList[197]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl.rotateX" 
		"rigRN.placeHolderList[198]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl.rotateY" 
		"rigRN.placeHolderList[199]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl.rotateZ" 
		"rigRN.placeHolderList[200]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl.scaleX" 
		"rigRN.placeHolderList[201]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl.scaleY" 
		"rigRN.placeHolderList[202]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_Elbow_offset|rig:L_IK_Elbow_ctrl.scaleZ" 
		"rigRN.placeHolderList[203]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl.translateX" 
		"rigRN.placeHolderList[204]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl.translateY" 
		"rigRN.placeHolderList[205]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl.translateZ" 
		"rigRN.placeHolderList[206]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl.rotateX" 
		"rigRN.placeHolderList[207]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl.rotateY" 
		"rigRN.placeHolderList[208]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl.rotateZ" 
		"rigRN.placeHolderList[209]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl.scaleX" 
		"rigRN.placeHolderList[210]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl.scaleY" 
		"rigRN.placeHolderList[211]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_Elbow_offset|rig:R_IK_Elbow_ctrl.scaleZ" 
		"rigRN.placeHolderList[212]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl.rotateX" 
		"rigRN.placeHolderList[213]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl.rotateY" 
		"rigRN.placeHolderList[214]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl.rotateZ" 
		"rigRN.placeHolderList[215]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl.translateX" 
		"rigRN.placeHolderList[216]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl.translateY" 
		"rigRN.placeHolderList[217]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl.translateZ" 
		"rigRN.placeHolderList[218]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl.scaleX" 
		"rigRN.placeHolderList[219]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl.scaleY" 
		"rigRN.placeHolderList[220]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl.scaleZ" 
		"rigRN.placeHolderList[221]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl.rotateX" 
		"rigRN.placeHolderList[222]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl.rotateY" 
		"rigRN.placeHolderList[223]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl.rotateZ" 
		"rigRN.placeHolderList[224]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl.translateX" 
		"rigRN.placeHolderList[225]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl.translateY" 
		"rigRN.placeHolderList[226]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl.translateZ" 
		"rigRN.placeHolderList[227]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl.scaleX" 
		"rigRN.placeHolderList[228]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl.scaleY" 
		"rigRN.placeHolderList[229]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl.scaleZ" 
		"rigRN.placeHolderList[230]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl.rotateX" 
		"rigRN.placeHolderList[231]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl.rotateY" 
		"rigRN.placeHolderList[232]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl.rotateZ" 
		"rigRN.placeHolderList[233]" ""
		5 3 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl.blendOrient1" 
		"rigRN.placeHolderList[234]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl.blendOrient1" 
		"rigRN.placeHolderList[235]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl.translateX" 
		"rigRN.placeHolderList[236]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl.translateY" 
		"rigRN.placeHolderList[237]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl.translateZ" 
		"rigRN.placeHolderList[238]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl.scaleX" 
		"rigRN.placeHolderList[239]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl.scaleY" 
		"rigRN.placeHolderList[240]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:L_UpArm_ctrl_offset|rig:L_UpArm_ctrl|rig:L_LoArm_ctrl_offset|rig:L_LoArm_ctrl|rig:L_Wrist_ctrl_offset|rig:L_Wrist_ctrl.scaleZ" 
		"rigRN.placeHolderList[241]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl.rotateX" 
		"rigRN.placeHolderList[242]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl.rotateY" 
		"rigRN.placeHolderList[243]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl.rotateZ" 
		"rigRN.placeHolderList[244]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl.translateX" 
		"rigRN.placeHolderList[245]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl.translateY" 
		"rigRN.placeHolderList[246]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl.translateZ" 
		"rigRN.placeHolderList[247]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl.scaleX" 
		"rigRN.placeHolderList[248]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl.scaleY" 
		"rigRN.placeHolderList[249]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl.scaleZ" 
		"rigRN.placeHolderList[250]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl.rotateX" 
		"rigRN.placeHolderList[251]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl.rotateY" 
		"rigRN.placeHolderList[252]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl.rotateZ" 
		"rigRN.placeHolderList[253]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl.translateX" 
		"rigRN.placeHolderList[254]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl.translateY" 
		"rigRN.placeHolderList[255]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl.translateZ" 
		"rigRN.placeHolderList[256]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl.scaleX" 
		"rigRN.placeHolderList[257]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl.scaleY" 
		"rigRN.placeHolderList[258]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_LoArm_ctrl.scaleZ" 
		"rigRN.placeHolderList[259]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset.rotateX" 
		"rigRN.placeHolderList[260]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset.rotateY" 
		"rigRN.placeHolderList[261]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset.rotateZ" 
		"rigRN.placeHolderList[262]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset.visibility" 
		"rigRN.placeHolderList[263]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset.translateX" 
		"rigRN.placeHolderList[264]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset.translateY" 
		"rigRN.placeHolderList[265]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset.translateZ" 
		"rigRN.placeHolderList[266]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset.scaleX" 
		"rigRN.placeHolderList[267]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset.scaleY" 
		"rigRN.placeHolderList[268]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset.scaleZ" 
		"rigRN.placeHolderList[269]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl.rotateX" 
		"rigRN.placeHolderList[270]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl.rotateY" 
		"rigRN.placeHolderList[271]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl.rotateZ" 
		"rigRN.placeHolderList[272]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl.translateX" 
		"rigRN.placeHolderList[273]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl.translateY" 
		"rigRN.placeHolderList[274]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl.translateZ" 
		"rigRN.placeHolderList[275]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl.scaleX" 
		"rigRN.placeHolderList[276]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl.scaleY" 
		"rigRN.placeHolderList[277]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:R_UpArm_ctrl_offset|rig:R_UpArm_ctrl|rig:R_LoArm_ctrl_offset|rig:R_Wrist_ctrl_offset|rig:R_Wrist_ctrl.scaleZ" 
		"rigRN.placeHolderList[278]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl.rotateX" 
		"rigRN.placeHolderList[279]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl.rotateY" 
		"rigRN.placeHolderList[280]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl.rotateZ" 
		"rigRN.placeHolderList[281]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl.translateX" 
		"rigRN.placeHolderList[282]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl.translateY" 
		"rigRN.placeHolderList[283]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl.translateZ" 
		"rigRN.placeHolderList[284]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl.visibility" 
		"rigRN.placeHolderList[285]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl.scaleX" 
		"rigRN.placeHolderList[286]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl.scaleY" 
		"rigRN.placeHolderList[287]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_COG_offset|rig:M_COG_ctrl|rig:M_Hips_offset|rig:M_Hips_ctrl.scaleZ" 
		"rigRN.placeHolderList[288]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.L_Uplid" 
		"rigRN.placeHolderList[289]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.L_Lolid" 
		"rigRN.placeHolderList[290]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.R_Uplid" 
		"rigRN.placeHolderList[291]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.R_Lolid" 
		"rigRN.placeHolderList[292]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.L_Lid_Twist" 
		"rigRN.placeHolderList[293]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.R_Lid_Twist" 
		"rigRN.placeHolderList[294]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.L_Lid_Bend" 
		"rigRN.placeHolderList[295]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.R_Lid_Bend" 
		"rigRN.placeHolderList[296]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.visibility" 
		"rigRN.placeHolderList[297]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.translateX" 
		"rigRN.placeHolderList[298]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.translateY" 
		"rigRN.placeHolderList[299]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.translateZ" 
		"rigRN.placeHolderList[300]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.rotateX" 
		"rigRN.placeHolderList[301]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.rotateY" 
		"rigRN.placeHolderList[302]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.rotateZ" 
		"rigRN.placeHolderList[303]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.scaleX" 
		"rigRN.placeHolderList[304]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.scaleY" 
		"rigRN.placeHolderList[305]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl.scaleZ" 
		"rigRN.placeHolderList[306]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl.translateX" 
		"rigRN.placeHolderList[307]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl.translateY" 
		"rigRN.placeHolderList[308]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl.translateZ" 
		"rigRN.placeHolderList[309]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl.visibility" 
		"rigRN.placeHolderList[310]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl.rotateX" 
		"rigRN.placeHolderList[311]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl.rotateY" 
		"rigRN.placeHolderList[312]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl.rotateZ" 
		"rigRN.placeHolderList[313]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl.scaleX" 
		"rigRN.placeHolderList[314]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl.scaleY" 
		"rigRN.placeHolderList[315]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:L_eye_grp|rig:L_eye_ctrl.scaleZ" 
		"rigRN.placeHolderList[316]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl.translateX" 
		"rigRN.placeHolderList[317]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl.translateY" 
		"rigRN.placeHolderList[318]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl.translateZ" 
		"rigRN.placeHolderList[319]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl.visibility" 
		"rigRN.placeHolderList[320]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl.rotateX" 
		"rigRN.placeHolderList[321]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl.rotateY" 
		"rigRN.placeHolderList[322]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl.rotateZ" 
		"rigRN.placeHolderList[323]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl.scaleX" 
		"rigRN.placeHolderList[324]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl.scaleY" 
		"rigRN.placeHolderList[325]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Eye_offset|rig:M_eye_ctrl|rig:R_eye_grp|rig:R_eye_ctrl.scaleZ" 
		"rigRN.placeHolderList[326]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl.translateX" 
		"rigRN.placeHolderList[327]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl.translateY" 
		"rigRN.placeHolderList[328]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl.translateZ" 
		"rigRN.placeHolderList[329]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl.rotateX" 
		"rigRN.placeHolderList[330]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl.rotateY" 
		"rigRN.placeHolderList[331]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl.rotateZ" 
		"rigRN.placeHolderList[332]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl.scaleX" 
		"rigRN.placeHolderList[333]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl.scaleY" 
		"rigRN.placeHolderList[334]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:L_Hand_offset|rig:L_Hand_AxisFix_offset|rig:L_IK_Hand_ctrl.scaleZ" 
		"rigRN.placeHolderList[335]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl.translateX" 
		"rigRN.placeHolderList[336]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl.translateY" 
		"rigRN.placeHolderList[337]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl.translateZ" 
		"rigRN.placeHolderList[338]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl.rotateX" 
		"rigRN.placeHolderList[339]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl.rotateY" 
		"rigRN.placeHolderList[340]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl.rotateZ" 
		"rigRN.placeHolderList[341]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl.scaleX" 
		"rigRN.placeHolderList[342]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl.scaleY" 
		"rigRN.placeHolderList[343]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:R_Hand_offset|rig:R_IK_Hand_ctrl.scaleZ" 
		"rigRN.placeHolderList[344]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl.translateX" 
		"rigRN.placeHolderList[345]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl.translateY" 
		"rigRN.placeHolderList[346]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl.translateZ" 
		"rigRN.placeHolderList[347]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl.rotateX" 
		"rigRN.placeHolderList[348]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl.rotateY" 
		"rigRN.placeHolderList[349]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl.rotateZ" 
		"rigRN.placeHolderList[350]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl.scaleX" 
		"rigRN.placeHolderList[351]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl.scaleY" 
		"rigRN.placeHolderList[352]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl.scaleZ" 
		"rigRN.placeHolderList[353]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl.visibility" 
		"rigRN.placeHolderList[354]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl.translateX" 
		"rigRN.placeHolderList[355]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl.translateY" 
		"rigRN.placeHolderList[356]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl.translateZ" 
		"rigRN.placeHolderList[357]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl.rotateX" 
		"rigRN.placeHolderList[358]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl.rotateY" 
		"rigRN.placeHolderList[359]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl.rotateZ" 
		"rigRN.placeHolderList[360]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl.scaleX" 
		"rigRN.placeHolderList[361]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl.scaleY" 
		"rigRN.placeHolderList[362]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl.scaleZ" 
		"rigRN.placeHolderList[363]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl.visibility" 
		"rigRN.placeHolderList[364]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl.translateX" 
		"rigRN.placeHolderList[365]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl.translateY" 
		"rigRN.placeHolderList[366]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl.translateZ" 
		"rigRN.placeHolderList[367]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl.rotateX" 
		"rigRN.placeHolderList[368]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl.rotateY" 
		"rigRN.placeHolderList[369]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl.rotateZ" 
		"rigRN.placeHolderList[370]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl.scaleX" 
		"rigRN.placeHolderList[371]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl.scaleY" 
		"rigRN.placeHolderList[372]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl.scaleZ" 
		"rigRN.placeHolderList[373]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:M_mouth_grp|rig:M_mouth_ctrl.visibility" 
		"rigRN.placeHolderList[374]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl.translateX" 
		"rigRN.placeHolderList[375]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl.translateY" 
		"rigRN.placeHolderList[376]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl.translateZ" 
		"rigRN.placeHolderList[377]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl.rotateX" 
		"rigRN.placeHolderList[378]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl.rotateY" 
		"rigRN.placeHolderList[379]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl.rotateZ" 
		"rigRN.placeHolderList[380]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl.scaleX" 
		"rigRN.placeHolderList[381]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl.scaleY" 
		"rigRN.placeHolderList[382]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl.scaleZ" 
		"rigRN.placeHolderList[383]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:R_mouth_grp|rig:R_mouth_ctrl.visibility" 
		"rigRN.placeHolderList[384]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl.translateX" 
		"rigRN.placeHolderList[385]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl.translateY" 
		"rigRN.placeHolderList[386]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl.translateZ" 
		"rigRN.placeHolderList[387]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl.rotateX" 
		"rigRN.placeHolderList[388]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl.rotateY" 
		"rigRN.placeHolderList[389]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl.rotateZ" 
		"rigRN.placeHolderList[390]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl.scaleX" 
		"rigRN.placeHolderList[391]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl.scaleY" 
		"rigRN.placeHolderList[392]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl.scaleZ" 
		"rigRN.placeHolderList[393]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:M_Mouth_offset|rig:M_mouth_main_offset|rig:M_mouth_main_ctrl|rig:L_mouth_grp|rig:L_mouth_ctrl.visibility" 
		"rigRN.placeHolderList[394]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl.translateX" 
		"rigRN.placeHolderList[395]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl.translateY" 
		"rigRN.placeHolderList[396]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl.translateZ" 
		"rigRN.placeHolderList[397]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl.rotateX" 
		"rigRN.placeHolderList[398]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl.rotateY" 
		"rigRN.placeHolderList[399]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl.rotateZ" 
		"rigRN.placeHolderList[400]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl.scaleX" 
		"rigRN.placeHolderList[401]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl.scaleY" 
		"rigRN.placeHolderList[402]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl.scaleZ" 
		"rigRN.placeHolderList[403]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeBall_offset|rig:L_eyeBall_ctrl.visibility" 
		"rigRN.placeHolderList[404]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl.translateX" 
		"rigRN.placeHolderList[405]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl.translateY" 
		"rigRN.placeHolderList[406]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl.translateZ" 
		"rigRN.placeHolderList[407]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl.rotateX" 
		"rigRN.placeHolderList[408]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl.rotateY" 
		"rigRN.placeHolderList[409]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl.rotateZ" 
		"rigRN.placeHolderList[410]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl.scaleX" 
		"rigRN.placeHolderList[411]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl.scaleY" 
		"rigRN.placeHolderList[412]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl.scaleZ" 
		"rigRN.placeHolderList[413]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeBall_offset|rig:R_eyeBall_ctrl.visibility" 
		"rigRN.placeHolderList[414]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl.rotateX" 
		"rigRN.placeHolderList[415]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl.rotateY" 
		"rigRN.placeHolderList[416]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl.rotateZ" 
		"rigRN.placeHolderList[417]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl.translateX" 
		"rigRN.placeHolderList[418]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl.translateY" 
		"rigRN.placeHolderList[419]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl.translateZ" 
		"rigRN.placeHolderList[420]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl.visibility" 
		"rigRN.placeHolderList[421]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl.scaleX" 
		"rigRN.placeHolderList[422]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl.scaleY" 
		"rigRN.placeHolderList[423]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:R_eyeLid_offset|rig:R_eyeLid_ctrl.scaleZ" 
		"rigRN.placeHolderList[424]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl.rotateX" 
		"rigRN.placeHolderList[425]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl.rotateY" 
		"rigRN.placeHolderList[426]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl.rotateZ" 
		"rigRN.placeHolderList[427]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl.translateX" 
		"rigRN.placeHolderList[428]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl.translateY" 
		"rigRN.placeHolderList[429]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl.translateZ" 
		"rigRN.placeHolderList[430]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl.visibility" 
		"rigRN.placeHolderList[431]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl.scaleX" 
		"rigRN.placeHolderList[432]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl.scaleY" 
		"rigRN.placeHolderList[433]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_ctrl|rig:L_eyeLid_offset|rig:L_eyeLid_ctrl.scaleZ" 
		"rigRN.placeHolderList[434]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_offset_pointConstraint1.nodeState" 
		"rigRN.placeHolderList[435]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_offset_pointConstraint1.M_Head_pointConstrain_offsetW0" 
		"rigRN.placeHolderList[436]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_offset_pointConstraint1.offsetX" 
		"rigRN.placeHolderList[437]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_offset_pointConstraint1.offsetY" 
		"rigRN.placeHolderList[438]" ""
		5 4 "rigRN" "|rig:Creep|rig:Creep_Control_rig|rig:M_Root_ctrl|rig:M_Head_offset|rig:M_Head_offset_pointConstraint1.offsetZ" 
		"rigRN.placeHolderList[439]" "";
	setAttr ".ptag" -type "string" "";
lockNode -l 1 ;
createNode polyCube -n "polyCube1";
	rename -uid "ECFF1C13-4506-8467-9070-EC921D7B52E8";
	setAttr ".cuv" 4;
createNode animCurveTL -n "M_Root_ctrl_translateX";
	rename -uid "F823AF8C-476D-943E-582D-A5A92598427C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 144.90778263183347 7 144.90778263183347
		 13 144.90778263183347 217 144.90778263183347 274 144.90778263183347 275 144.90778263183347
		 485 144.90778263183347 509 144.90778263183347 510 144.90778263183347 515 144.90778263183347
		 516 144.90778263183347 526 144.90778263183347 527 144.90778263183347 535 144.90778263183347
		 536 144.90778263183347 544 144.90778263183347 545 144.90778263183347 555 144.90778263183347
		 556 144.90778263183347 560 144.90778263183347 561 144.90778263183347 567 144.90778263183347
		 568 144.90778263183347 580 144.90778263183347 581 144.90778263183347 596 144.90778263183347
		 597 144.90778263183347 613 144.90778263183347 614 144.90778263183347 624 144.90778263183347
		 625 144.90778263183347 630 144.90778263183347 631 144.90778263183347 636 144.90778263183347
		 637 144.90778263183347 642 144.90778263183347 643 144.90778263183347 647 144.90778263183347
		 648 144.90778263183347 656 144.90778263183347 657 144.90778263183347 666 144.90778263183347
		 667 144.90778263183347 681 144.90778263183347 684 144.90778263183347 687 144.90778263183347
		 690 144.90778263183347 697 144.90778263183347 707 144.90778263183347 711 144.90778263183347
		 716 144.90778263183347 720 144.90778263183347 725 144.90778263183347 729 144.90778263183347
		 734 144.90778263183347 740 144.90778263183347 752 144.90778263183347 759 144.90778263183347
		 765 144.90778263183347 779 144.90778263183347 806 144.90778263183347 819 144.90778263183347
		 826 144.90778263183347 841 144.90778263183347 850 144.90778263183347 864 144.90778263183347
		 888 144.90778263183347;
	setAttr -s 67 ".kit[63:66]"  3 18 18 18;
	setAttr -s 67 ".kot[1:66]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
createNode animCurveTL -n "M_Root_ctrl_translateY";
	rename -uid "6322E06F-45FD-B207-B080-758D64D17369";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 0 7 0 13 0 217 0 274 0 275 0 485 0 509 0
		 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0
		 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0
		 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0
		 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0
		 850 0 864 0 888 0;
	setAttr -s 67 ".kit[63:66]"  3 18 18 18;
	setAttr -s 67 ".kot[1:66]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
createNode animCurveTL -n "M_Root_ctrl_translateZ";
	rename -uid "04CDFFAB-4C05-ADEA-ADAA-2F9B5509078D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 -33.299783917557647 7 -33.299783917557647
		 13 -33.299783917557647 217 -33.299783917557647 274 -33.299783917557647 275 -33.299783917557647
		 485 -33.299783917557647 509 -33.299783917557647 510 -33.299783917557647 515 -33.299783917557647
		 516 -33.299783917557647 526 -33.299783917557647 527 -33.299783917557647 535 -33.299783917557647
		 536 -33.299783917557647 544 -33.299783917557647 545 -33.299783917557647 555 -33.299783917557647
		 556 -33.299783917557647 560 -33.299783917557647 561 -33.299783917557647 567 -33.299783917557647
		 568 -33.299783917557647 580 -33.299783917557647 581 -33.299783917557647 596 -33.299783917557647
		 597 -33.299783917557647 613 -33.299783917557647 614 -33.299783917557647 624 -33.299783917557647
		 625 -33.299783917557647 630 -33.299783917557647 631 -33.299783917557647 636 -33.299783917557647
		 637 -33.299783917557647 642 -33.299783917557647 643 -33.299783917557647 647 -33.299783917557647
		 648 -33.299783917557647 656 -33.299783917557647 657 -33.299783917557647 666 -33.299783917557647
		 667 -33.299783917557647 681 -33.299783917557647 684 -33.299783917557647 687 -33.299783917557647
		 690 -33.299783917557647 697 -33.299783917557647 707 -33.299783917557647 711 -33.299783917557647
		 716 -33.299783917557647 720 -33.299783917557647 725 -33.299783917557647 729 -33.299783917557647
		 734 -33.299783917557647 740 -33.299783917557647 752 -33.299783917557647 759 -33.299783917557647
		 765 -33.299783917557647 779 -33.299783917557647 806 -33.299783917557647 819 -33.299783917557647
		 826 -33.299783917557647 841 -33.299783917557647 850 -33.299783917557647 864 -33.299783917557647
		 888 -33.299783917557647;
	setAttr -s 67 ".kit[63:66]"  3 18 18 18;
	setAttr -s 67 ".kot[1:66]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
createNode animCurveTL -n "L_foot_ctrl_translateX";
	rename -uid "76B5010C-4BE8-9D8E-634C-DC9C497B3468";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 101 ".ktv[0:100]"  1 14.882543669724036 7 14.882543669724036
		 13 14.882543669724036 17 14.882543669724036 21 14.277501167935666 103 14.277501167935666
		 217 14.277501167935666 275 14.277501167935666 286 14.277501167935666 287 18.992856043448594
		 311 18.992856043448594 312 15.999493568243965 322 15.999493568243965 323 15.999493568243965
		 346 15.999493568243965 347 19.11784725197332 368 19.11784725197332 369 19.11784725197332
		 388 19.11784725197332 389 19.11784725197332 397 19.11784725197332 398 18.53186107269287
		 408 18.53186107269287 409 18.53186107269287 435 18.53186107269287 436 18.53186107269287
		 447 18.53186107269287 448 19.567864934642387 471 19.567864934642387 472 19.567864934642387
		 484 19.567864934642387 485 21.464352863630765 509 21.464352863630765 510 21.464352863630765
		 515 21.464352863630765 516 21.464352863630765 526 21.464352863630765 527 19.86605440905948
		 535 19.86605440905948 536 19.86605440905948 544 19.86605440905948 545 19.967609731586844
		 555 19.967609731586844 556 19.967609731586844 560 19.967609731586844 561 20.735861663193585
		 567 20.735861663193585 568 20.735861663193585 580 20.735861663193585 581 18.552157572464601
		 596 18.552157572464601 597 18.552157572464601 613 18.552157572464601 614 16.982202096479071
		 624 16.982202096479071 625 16.982202096479071 630 16.982202096479071 631 14.074400006787336
		 636 14.074400006787336 637 14.074400006787336 642 14.074400006787336 643 14.008677004440015
		 647 14.008677004440015 648 17.553025551615313 656 17.553025551615313 657 21.464352863630765
		 666 21.464352863630765 667 21.464352863630765 681 21.464352863630765 684 21.464352863630765
		 687 21.464352863630765 690 21.464352863630765 697 21.464352863630765 707 21.464352863630765
		 711 21.464352863630765 716 21.464352863630765 720 24.528919053012643 725 24.528919053012643
		 729 24.528919053012643 734 21.464352863630765 740 21.464352863630765 752 24.508420140326724
		 759 24.508420140326724 765 16.052784177045584 773 16.052784177045584 779 10.391121483215045
		 786 10.391121483215045 793 1.6875706787057503 800 1.6875706787057503 806 -2.1496283604730606
		 813 -2.1496283604730606 819 -1.4209347285700022 826 -1.4209347285700022 833 3.9764278535951734
		 841 3.9764278535951734 850 5.8819657472138873 858 5.8819657472138873 864 11.07845912120894
		 871 11.07845912120894 879 15.817514800468702 888 15.817514800468702;
	setAttr -s 101 ".kit[0:100]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 1 1 1 1 18 3 1 1 
		3 1 18 3 18 1 3 1;
	setAttr -s 101 ".kot[0:100]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
	setAttr -s 101 ".kix[85:100]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 101 ".kiy[85:100]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "L_foot_ctrl_translateY";
	rename -uid "17D12695-437C-C5AF-11FE-B4960031AEE3";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0 7 0 13 0 17 0 21 0 103 0 217 0 275 0
		 286 0 311 0 312 0 322 0 323 0 346 0 347 0 368 0 369 0 388 0 389 0 397 0 398 0 408 0
		 409 0 435 0 436 0 447 0 448 -1.1102230246251565e-16 471 -1.1102230246251565e-16 472 -1.1102230246251565e-16
		 484 0 485 0 509 -4.4201442506164297e-16 510 -4.4408920985006262e-16 515 -4.4408920985006262e-16
		 516 -4.4408920985006262e-16 526 -4.4408920985006262e-16 527 -4.4408920985006262e-16
		 535 -4.4408920985006262e-16 536 -4.4408920985006262e-16 544 -4.4408920985006262e-16
		 545 -4.4408920985006262e-16 555 -4.4408920985006262e-16 556 -4.4408920985006262e-16
		 560 -4.4408920985006262e-16 561 -4.4408920985006262e-16 567 0 568 0 580 0 581 -6.068497664211581e-17
		 596 -6.068497664211581e-17 597 -6.068497664211581e-17 613 -6.068497664211581e-17
		 614 -6.068497664211581e-17 624 -6.068497664211581e-17 625 -6.068497664211581e-17
		 630 -6.068497664211581e-17 631 -6.068497664211581e-17 636 -6.068497664211581e-17
		 637 -6.068497664211581e-17 642 -6.068497664211581e-17 643 1.5069112418372075 647 1.5069112418372075
		 648 0 656 0 657 -4.4408920985006262e-16 666 0 667 0 681 0 684 -4.0748100101853799e-16
		 687 -4.0748100101853799e-16 690 -4.0748100101853799e-16 697 -4.0748100101853799e-16
		 707 -4.0748100101853799e-16 711 -4.0748100101853799e-16 716 -4.0748100101853799e-16
		 720 -4.0748100101853799e-16 725 -6.2952560594356935e-16 729 -6.2952560594356935e-16
		 734 -4.4408920985006262e-16 740 -4.4408920985006262e-16 752 -4.4408920985006262e-16
		 759 -4.4408920985006262e-16 765 -3.3306690738754696e-16 773 -3.3306690738754696e-16
		 779 -4.4408920985006262e-16 786 -4.4408920985006262e-16 793 -4.4408920985006262e-16
		 800 -4.4408920985006262e-16 806 -2.8959030526845694e-16 813 -2.8959030526845694e-16
		 819 -2.8959030526845724e-16 826 -2.8959030526845724e-16 833 -2.8959030526845724e-16
		 841 -2.8959030526845724e-16 850 -8.4826945118418558e-17 858 -8.4826945118418558e-17
		 864 0 871 0 879 1.1102230246251565e-16 888 1.1102230246251565e-16;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 3 1 1 1 1 18 3 1 1 3 
		1 18 3 18 1 3 1;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[84:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[84:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "L_foot_ctrl_translateZ";
	rename -uid "99A98A33-4C19-9BB9-78A0-989E0139FA60";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 101 ".ktv[0:100]"  1 100.79931035246381 7 100.79931035246381
		 13 100.79931035246381 17 100.79931035246381 21 101.94702894622274 103 101.94702894622274
		 217 101.94702894622274 275 101.94702894622274 286 101.94702894622274 287 93.002367288898199
		 311 93.002367288898199 312 98.680543219511378 322 98.680543219511378 323 98.680543219511378
		 346 98.680543219511378 347 105.05408391328724 368 105.05408391328724 369 105.05408391328724
		 388 105.05408391328724 389 105.05408391328724 397 105.05408391328724 398 98.141016315361085
		 408 98.141016315361085 409 98.141016315361085 435 98.141016315361085 436 98.141016315361085
		 447 98.141016315361085 448 96.17579752155568 471 96.17579752155568 472 96.17579752155568
		 484 96.17579752155568 485 92.699319995223277 509 92.699319995223277 510 92.699319995223277
		 515 92.699319995223277 516 92.699319995223277 526 92.699319995223277 527 87.528153661909371
		 535 87.528153661909371 536 87.528153661909371 544 87.528153661909371 545 80.432080442983278
		 555 80.432080442983278 556 80.432080442983278 560 80.432080442983278 561 84.97939649409102
		 567 84.97939649409102 568 84.97939649409102 580 84.97939649409102 581 77.638535564137186
		 596 77.638535564137186 597 77.638535564137186 613 77.638535564137186 614 80.616619063786175
		 624 80.616619063786175 625 80.616619063786175 630 80.616619063786175 631 73.92235203928243
		 636 73.92235203928243 637 73.92235203928243 642 73.92235203928243 643 77.244236987242942
		 647 77.244236987242942 648 88.751160858735147 656 88.751160858735147 657 92.699319995223277
		 666 92.699319995223277 667 92.699319995223277 681 92.699319995223277 684 92.699319995223277
		 687 92.699319995223277 690 92.699319995223277 697 92.699319995223277 707 92.699319995223277
		 711 92.699319995223277 716 92.699319995223277 720 92.864374841886004 725 92.864374841886004
		 729 92.864374841886004 734 92.699319995223277 740 92.699319995223277 752 86.924960997647972
		 759 86.924960997647972 765 72.4220817957886 773 72.4220817957886 779 62.502212923540384
		 786 62.502212923540384 793 47.504156689306313 800 47.504156689306313 806 33.138260429763143
		 813 33.138260429763143 819 17.466468504336692 826 17.466468504336692 833 4.3213630949508648
		 841 4.3213630949508648 850 -11.860880574697186 858 -11.860880574697186 864 -28.712660426612885
		 871 -28.712660426612885 879 -41.536410469219099 888 -41.536410469219099;
	setAttr -s 101 ".kit[0:100]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 1 1 1 1 18 3 1 1 
		3 1 18 3 18 1 3 1;
	setAttr -s 101 ".kot[0:100]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
	setAttr -s 101 ".kix[85:100]"  1 1 1 1 1 1 0.026081374615127852 0.026081374615127852 
		1 1 1 1 1 0.019149344167784219 1 1;
	setAttr -s 101 ".kiy[85:100]"  0 0 0 0 0 0 -0.9996598230889272 -0.9996598230889272 
		0 0 0 0 0 -0.99981663449751812 0 0;
createNode animCurveTL -n "L_knee_ctrl_translateX";
	rename -uid "2FCAEDFF-4445-E779-D72E-9888127DB2DD";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  1 0 7 0 13 0 217 0 275 0 311 -0.011835960262958353
		 312 -5.5002467449507764 485 -5.5002467449507764 509 -5.5002467449507764 510 -5.5002467449507764
		 515 -5.5002467449507764 516 -5.5002467449507764 526 -5.5002467449507764 527 -5.5002467449507764
		 535 -5.5002467449507764 536 -5.5002467449507764 544 -5.5002467449507764 545 -5.5002467449507764
		 555 -5.5002467449507764 556 -5.5002467449507764 560 -5.5002467449507764 561 -5.5002467449507764
		 567 -5.5002467449507764 568 -5.5002467449507764 580 -5.5002467449507764 581 -5.5002467449507764
		 596 -5.5002467449507764 597 -5.5002467449507764 613 -5.5002467449507764 614 -5.5002467449507764
		 624 -5.5002467449507764 625 -5.5002467449507764 630 -5.5002467449507764 631 -5.5002467449507764
		 636 -5.5002467449507764 637 -5.5002467449507764 642 -5.5002467449507764 643 -5.5002467449507764
		 647 -5.5002467449507764 648 -5.5002467449507764 656 -5.5002467449507764 657 -5.5002467449507764
		 666 -5.5002467449507764 667 -5.5002467449507764 681 -5.5002467449507764 684 2.9753001326287376
		 687 2.9753001326287376 690 2.9753001326287376 697 2.9753001326287376 707 2.9753001326287376
		 711 2.9753001326287376 716 2.9753001326287376 720 2.9753001326287376 725 2.9753001326287376
		 729 2.9753001326287376 734 -5.5002467449507764 740 -5.5002467449507764 752 -5.5002467449507764
		 759 -5.5002467449507764 765 -5.5002467449507764 773 -5.5002467449507764 779 -5.5002467449507764
		 786 -5.5002467449507764 793 -5.5002467449507764 806 -5.5002467449507764 813 -5.5002467449507764
		 819 -5.5002467449507764 826 -5.5002467449507764 841 -5.5002467449507764 850 -5.5002467449507764
		 864 0 888 0;
	setAttr -s 72 ".kit[0:71]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 3 1 3 3 18 3 18 18 3 18 18 18;
	setAttr -s 72 ".kot[0:71]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 72 ".kix[61:71]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 72 ".kiy[61:71]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "L_knee_ctrl_translateY";
	rename -uid "226DA11C-4730-3848-E664-D2A6CF3CAED6";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  1 0 7 0 13 0 217 0 275 0 311 3.8225355950215323e-18
		 312 1.7763568394002505e-15 485 0 509 1.7680577002465719e-15 510 1.7763568394002505e-15
		 515 1.7763568394002505e-15 516 1.7763568394002505e-15 526 1.7763568394002505e-15
		 527 1.7763568394002505e-15 535 1.7763568394002505e-15 536 1.7763568394002505e-15
		 544 1.7763568394002505e-15 545 1.7763568394002505e-15 555 1.7763568394002505e-15
		 556 1.7763568394002505e-15 560 1.7763568394002505e-15 561 1.7763568394002505e-15
		 567 0 568 0 580 0 581 2.4273990656846324e-16 596 2.4273990656846324e-16 597 2.4273990656846324e-16
		 613 2.4273990656846324e-16 614 2.4273990656846324e-16 624 2.4273990656846324e-16
		 625 2.4273990656846324e-16 630 2.4273990656846324e-16 631 2.4273990656846324e-16
		 636 2.4273990656846324e-16 637 2.4273990656846324e-16 642 2.4273990656846324e-16
		 643 2.4273990656846324e-16 647 2.4273990656846324e-16 648 0 656 0 657 1.7763568394002505e-15
		 666 0 667 0 681 0 684 -3.4364921050121157e-16 687 -3.4364921050121157e-16 690 -3.4364921050121157e-16
		 697 -3.4364921050121157e-16 707 -3.4364921050121157e-16 711 -3.4364921050121157e-16
		 716 -3.4364921050121157e-16 720 -3.4364921050121157e-16 725 -3.4364921050121157e-16
		 729 -3.4364921050121157e-16 734 1.7763568394002505e-15 740 1.7763568394002505e-15
		 752 1.7763568394002505e-15 759 1.7763568394002505e-15 765 1.7763568394002505e-15
		 773 1.7763568394002505e-15 779 1.7763568394002505e-15 786 1.7763568394002505e-15
		 793 1.7763568394002505e-15 806 1.7763568394002505e-15 813 1.7763568394002505e-15
		 819 1.7763568394002505e-15 826 1.7763568394002505e-15 841 1.7763568394002505e-15
		 850 1.7763568394002505e-15 864 0 888 0;
	setAttr -s 72 ".kit[0:71]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 3 1 3 3 18 3 18 18 3 18 18 18;
	setAttr -s 72 ".kot[0:71]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 72 ".kix[61:71]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 72 ".kiy[61:71]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "L_knee_ctrl_translateZ";
	rename -uid "A1862710-4F0A-F3EC-F234-5FB67D95F386";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  1 0 7 0 13 0 217 0 275 0 311 0.026781856501497881
		 312 12.445700709820034 485 12.445700709820034 509 12.445700709820034 510 12.445700709820034
		 515 12.445700709820034 516 12.445700709820034 526 12.445700709820034 527 12.445700709820034
		 535 12.445700709820034 536 12.445700709820034 544 12.445700709820034 545 12.445700709820034
		 555 12.445700709820034 556 12.445700709820034 560 12.445700709820034 561 12.445700709820034
		 567 12.445700709820034 568 12.445700709820034 580 12.445700709820034 581 12.445700709820034
		 596 12.445700709820034 597 12.445700709820034 613 12.445700709820034 614 12.445700709820034
		 624 12.445700709820034 625 12.445700709820034 630 12.445700709820034 631 12.445700709820034
		 636 12.445700709820034 637 12.445700709820034 642 12.445700709820034 643 12.445700709820034
		 647 12.445700709820034 648 12.445700709820034 656 12.445700709820034 657 12.445700709820034
		 666 12.445700709820034 667 12.445700709820034 681 12.445700709820034 684 12.445700709820027
		 687 12.445700709820027 690 12.445700709820027 697 12.445700709820027 707 12.445700709820027
		 711 12.445700709820027 716 12.445700709820027 720 12.445700709820027 725 12.445700709820027
		 729 12.445700709820027 734 12.445700709820034 740 12.445700709820034 752 12.445700709820034
		 759 12.445700709820034 765 12.445700709820034 773 12.445700709820034 779 12.445700709820034
		 786 12.445700709820034 793 12.445700709820034 806 12.445700709820034 813 12.445700709820034
		 819 12.445700709820034 826 12.445700709820034 841 12.445700709820034 850 12.445700709820034
		 864 0 888 0;
	setAttr -s 72 ".kit[0:71]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 3 1 3 3 18 3 18 18 3 18 18 18;
	setAttr -s 72 ".kot[0:71]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 72 ".kix[61:71]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 72 ".kiy[61:71]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "M_COG_ctrl_translateX";
	rename -uid "1A6602CF-44DE-4A5E-D4EB-4B821F58688E";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 115 ".ktv[0:114]"  1 17.218850487438754 7 17.218850487438754
		 13 17.218850487438754 17 17.830601525302292 21 17.829675256968663 102 17.829675256968663
		 103 17.829675256968663 201 17.829675256968663 203 18.456104277158964 216 18.456104277158964
		 217 18.456104277158964 253 18.456104277158964 254 18.456104277158964 274 18.456104277158964
		 275 17.660103309486349 280 17.660103309486349 281 18.865688971156473 286 18.865688971156473
		 287 19.541790178856445 295 19.541790178856445 296 23.195396204072704 311 23.195396204072704
		 312 20.482926309699128 322 20.482926309699128 323 19.853655716733172 346 19.853655716733172
		 347 19.853655716733172 368 19.853655716733172 369 20.313789915980099 388 20.503624304057318
		 389 20.503624304057318 397 20.503624304057318 398 20.320944985462582 408 20.320944985462582
		 409 20.320944985462582 435 20.320944985462582 436 22.496646403820524 447 22.496646403820524
		 448 23.10101934905591 471 23.10101934905591 472 21.973196995098736 484 21.973196995098736
		 485 21.973196995098736 509 21.973196995098736 510 23.001732684079293 515 23.001732684079293
		 516 23.001732684079293 526 23.001732684079286 527 23.587254365808104 535 23.587254365808104
		 536 24.659272764348728 544 24.659272764348728 545 23.467563373536684 555 23.467563373536684
		 556 21.955409416938444 560 21.955409416938444 561 22.197052486325351 567 22.197052486325351
		 568 21.727386951826777 580 21.727386951826777 581 22.406864576243521 596 22.406864576243521
		 597 20.374448958763598 613 20.374448958763598 614 19.788910214203874 624 19.788910214203874
		 625 21.569324692558144 630 21.569324692558144 631 23.595566536272262 636 23.595566536272262
		 637 23.789564136451173 640 23.912755842905121 642 23.912755842905121 643 22.560599503439992
		 647 22.560599503439992 648 21.958234848374445 653 22.221315817603951 656 22.221315817603951
		 657 21.973196995098736 666 21.973196995098736 667 21.533809738741546 681 21.533809738741546
		 684 22.959192610323896 687 22.810469226991138 690 22.959192610323896 697 23.00298064051897
		 703 23.010094837836196 707 22.810400425052563 711 23.029141762319835 716 22.809950986310032
		 720 23.052385513491423 725 22.837125336252249 729 23.063991819535588 734 22.671771410830392
		 740 21.973196995098736 752 23.541467111332345 759 24.559058587077054 765 24.000918326217494
		 772 21.828071327150631 779 19.901084759575394 786 17.385408620862368 793 13.91001042020142
		 800 10.841116215414438 806 8.5968276550111682 813 9.5550835926155688 819 10.327981396751287
		 826 12.294361591841287 833 14.056367012707929 841 15.807091125785321 850 18.254779356356941
		 858 20.817170760455383 864 23.340097275143044 871 24.357106573226233 879 27.609658104143737
		 888 33.842217222675039;
	setAttr -s 115 ".kit[0:114]"  18 18 18 18 18 3 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 3 18 18 18 18 
		3 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 18 18 3 3 18 
		3 18 3 3 18;
	setAttr -s 115 ".kot[0:114]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5;
createNode animCurveTL -n "M_COG_ctrl_translateY";
	rename -uid "A704F2A9-4305-09F8-1591-29B4074B7404";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 116 ".ktv[0:115]"  1 -1.1479321265361442 7 -1.1479321265361442
		 13 -1.1479321265361442 17 -1.1479321265361442 21 -1.1479321265361442 102 -1.1479321265361442
		 103 -1.1479321265361442 201 -1.1479321265361442 203 -2.7188563916146964 216 -2.7188563916146964
		 217 -2.7188563916146964 253 -2.7188563916146964 254 -2.7188563916146964 274 -2.7188563916146964
		 275 -2.7188563916146964 280 -2.7188563916146964 281 -0.72078625180818845 286 -0.72078625180818845
		 287 -2.2623314499036091 295 -2.2623314499036091 296 -1.0013105433739029 311 -2.4360775092783826
		 312 -2.9600605921879675 322 -2.9600605921879675 323 -2.5013985494317299 346 -2.7188563916146964
		 347 -2.7188563916146964 368 -2.7188563916146964 369 -3.5517126147305187 388 -2.7188563916146964
		 389 -2.7188563916146964 397 -2.7188563916146964 398 -2.7188563916146964 408 -2.7188563916146964
		 409 -2.7188563916146964 435 -2.7188563916146964 436 -3.8455495719752015 447 -2.7188563916146964
		 448 -2.7188563916146964 471 -2.7188563916146964 472 -4.1714256915145151 484 -4.1714256915145151
		 485 -2.7188563916146964 509 -2.7188563916146964 510 -2.7188563916146964 515 -2.7188563916146964
		 516 -3.8441446177087815 526 -3.8441446177087815 527 -2.7188563916146964 535 -2.7188563916146964
		 536 -3.7668032978534387 544 -3.7668032978534387 545 -2.927482711219564 555 -2.927482711219564
		 556 -3.7836593653911628 560 -3.7836593653911628 561 -2.5196768171232367 567 -2.5196768171232367
		 568 -3.3044194453884823 580 -3.3044194453884823 581 -4.2703192696571151 596 -4.2703192696571151
		 597 -4.2703192696571151 613 -4.2703192696571151 614 -4.2703192696571151 624 -4.2703192696571151
		 625 -2.1240621901003429 630 -2.1240621901003429 631 -1.0923004059160029 636 -1.0923004059160029
		 637 -1.3625033919555047 640 -2.743121290102799 642 -2.743121290102799 643 -1.0931886894345233
		 647 -1.0931886894345233 648 -1.8006387988619155 653 -2.5282378636406859 656 -2.0321743459613311
		 657 -2.7188563916146964 662 -2.0390772802364761 666 -2.7188563916146964 667 -2.7188563916146964
		 681 -3.8763130028911195 684 -3.1834677173503794 687 -0.18763955586160891 690 -3.1834677173503794
		 697 -4.5733075503807665 703 -4.799113433118313 707 -0.18625363536575254 711 -4.5924972507411459
		 716 -0.1772003095770841 720 -5.0607106733924763 725 -0.72459024250294046 729 -5.2945037576997702
		 734 -1.1575538754042247 740 -0.51235446916804817 752 -2.7188563916146964 759 -2.9462210983577499
		 765 -4.3434540202282683 772 -4.0363979011902664 779 -3.7640855729603357 786 -1.7721158838310807
		 793 -3.5340256506230943 800 -0.34868816610369002 806 -3.3299970657130924 813 -1.5455307827822176
		 819 -2.9136408352218228 826 -2.348591071555973 833 -3.3721997430341988 841 -0.72929207190269407
		 850 -3.5103884757858212 858 -2.496694033453871 864 -3.1859845286683797 871 -2.4491753502175833
		 879 -2.6170190060793859 888 -2.3720917634222936;
	setAttr -s 116 ".kit[0:115]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 3 18 18 18 18 
		3 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 3 18 18 3 3 
		18 3 18 3 3 18;
	setAttr -s 116 ".kot[0:115]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "M_COG_ctrl_translateZ";
	rename -uid "EC9F2D11-4F04-D0DD-7361-33BB2B9B5269";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 115 ".ktv[0:114]"  1 100.79931035246385 7 100.79931035246385
		 13 100.79931035246385 17 99.63886618995582 21 99.640623246275737 102 99.640623246275737
		 103 99.640623246275737 201 99.640623246275737 203 98.452336089559481 216 98.452336089559481
		 217 98.452336089559481 253 98.452336089559481 254 98.452336089559481 274 98.452336089559481
		 275 99.962288049211338 280 99.962288049211338 281 98.726704024197858 286 98.726704024197858
		 287 94.574509115667581 295 94.574509115667581 296 93.917114675231275 311 93.917114675231275
		 312 94.607612246009978 322 94.607612246009992 323 95.801289645396025 346 95.801289645396025
		 347 95.801289645396025 368 95.801289645396025 369 98.889852349424189 388 97.788837521167039
		 389 97.788837521167039 397 97.788837521167039 398 93.871936925228496 408 93.871936925228496
		 409 93.871936925228496 435 93.871936925228496 436 94.008229522704767 447 94.008229522704767
		 448 92.861781025679804 471 92.861781025679804 472 92.267227219266189 484 92.267227219266189
		 485 92.267227219266189 509 92.267227219266189 510 95.028287459581705 515 95.028287459581705
		 516 95.028287459581705 526 95.028287459581719 527 93.917598344052067 535 93.917598344052067
		 536 90.680914272596212 544 90.680914272596212 545 87.140661248728435 555 87.140661248728435
		 556 84.604317340230779 560 84.604317340230779 561 85.343075692546421 567 85.343075692546421
		 568 84.584366417400702 580 84.584366417400702 581 83.327533220551985 596 83.327533220551985
		 597 87.182867659922877 613 87.182867659922877 614 88.293589142316421 624 88.293589142316421
		 625 84.91628127491046 630 84.91628127491046 631 83.669314445944764 636 83.669314445944764
		 637 82.939705052158118 640 82.26008075632879 642 82.26008075632879 643 84.825016224310218
		 647 84.825016224310218 648 87.053040671892674 653 90.89386511052615 656 90.89386511052615
		 657 92.267227219266189 666 92.267227219266189 667 93.100710689421305 681 93.100710689421305
		 684 91.253474078712713 687 90.908075812535145 690 91.253474078712713 697 91.283643311295421
		 703 91.288544876233303 707 90.907916025487779 711 91.41592543826745 716 90.906872239642297
		 720 91.469907207470271 725 90.969982512850265 729 91.496861933045707 734 91.150995937036726
		 740 92.267227219266189 752 89.292340716057524 759 87.362048787621546 765 83.805161535429548
		 772 79.540139568954174 779 75.757710297634262 786 68.420924876675784 793 58.285180271776383
		 800 49.334974834860049 806 42.789671434046092 813 33.940818667335876 819 26.80362430816654
		 826 18.212257349501023 833 11.239627701584656 841 4.3616385131303392 850 -4.9240266715491501
		 858 -14.584104359067087 864 -24.095401537476349 871 -27.929471856592286 879 -37.017344070796256
		 888 -46.412837928683722;
	setAttr -s 115 ".kit[0:114]"  18 18 18 18 18 3 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 3 18 18 18 18 
		3 1 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 18 18 3 3 18 
		3 18 3 3 18;
	setAttr -s 115 ".kot[0:114]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5;
	setAttr -s 115 ".kix[77:114]"  1 1 1 1 1 0.096060652289424397 1 0.93231886749913628 
		0.99730805835151315 1 1 1 1 1 1 1 1 1 0.12805229673379215 0.07872685576275637 0.055315123803030247 
		0.057892117707058592 0.041932476479452772 0.026699070306813781 0.024443490202529065 
		0.027954163610014172 0.028138064124848362 1 0.027540279043807635 0.029970260693147739 
		1 1 0.029896603610831216 1 0.03245358902873631 1 1 1;
	setAttr -s 115 ".kiy[77:114]"  0 0 0 0 0 -0.99537548245962459 0 0.36163728970493048 
		0.073325553166239024 0 0 0 0 0 0 0 0 0 -0.99176741693866965 -0.99689622437930336 
		-0.99846894647688245 -0.99832284492913026 -0.9991204469013234 -0.99964351628205528 
		-0.99970121325660044 -0.99960920600846037 -0.99960404628398047 0 -0.99962069457879332 
		-0.99955079084255882 0 0 -0.99955299664026653 0 -0.99947324354339462 0 0 0;
createNode animCurveTL -n "L_IK_Elbow_ctrl_translateX";
	rename -uid "F4101BAF-4879-FF70-90A2-E5B093BCD678";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 -13.881429395171619 7 -13.881429395171619
		 13 -13.881429395171619 217 -13.881429395171619 275 -13.881429395171619 485 -13.881429395171619
		 509 -13.881429395171619 510 -13.881429395171619 515 -13.881429395171619 516 -13.881429395171619
		 526 -13.881429395171619 527 -13.881429395171619 535 -13.881429395171619 536 -13.881429395171619
		 544 -13.881429395171619 545 -13.881429395171619 555 -13.881429395171619 556 -13.881429395171619
		 560 -13.881429395171619 561 -13.881429395171619 567 -13.881429395171619 568 -13.881429395171619
		 580 -13.881429395171619 581 -13.881429395171619 596 -13.881429395171619 597 -13.881429395171619
		 613 -13.881429395171619 614 -13.881429395171619 624 -13.881429395171619 625 -13.881429395171619
		 630 -13.881429395171619 631 -13.881429395171619 636 -13.881429395171619 637 -13.881429395171619
		 642 -13.881429395171619 643 -13.881429395171619 647 -13.881429395171619 648 -13.881429395171619
		 656 -13.881429395171619 657 -13.881429395171619 666 -13.881429395171619 667 -13.881429395171619
		 681 -13.881429395171619 684 -13.881429395171619 687 -13.881429395171619 690 -13.881429395171619
		 697 -13.881429395171619 707 -13.881429395171619 711 -13.881429395171619 716 -13.881429395171619
		 720 -13.881429395171619 725 -13.881429395171619 729 -13.881429395171619 734 -13.881429395171619
		 740 -13.881429395171619 752 -13.881429395171619 759 -13.881429395171619 765 -13.881429395171619
		 779 -13.881429395171619 806 -13.881429395171619 819 -13.881429395171619 826 -13.881429395171619
		 841 -13.881429395171619 850 -13.881429395171619 864 -13.881429395171619 888 -13.881429395171619;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "L_IK_Elbow_ctrl_translateY";
	rename -uid "4FC80F93-4DA0-02D2-6457-B68319019C11";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 -6.9502436878203833 7 -6.9502436878203833
		 13 -6.9502436878203833 217 -6.9502436878203833 275 -6.9502436878203833 485 -6.9502436878203833
		 509 -6.9502436878203833 510 -6.9502436878203833 515 -6.9502436878203833 516 -6.9502436878203833
		 526 -6.9502436878203833 527 -6.9502436878203833 535 -6.9502436878203833 536 -6.9502436878203833
		 544 -6.9502436878203833 545 -6.9502436878203833 555 -6.9502436878203833 556 -6.9502436878203833
		 560 -6.9502436878203833 561 -6.9502436878203833 567 -6.9502436878203833 568 -6.9502436878203833
		 580 -6.9502436878203833 581 -6.9502436878203833 596 -6.9502436878203833 597 -6.9502436878203833
		 613 -6.9502436878203833 614 -6.9502436878203833 624 -6.9502436878203833 625 -6.9502436878203833
		 630 -6.9502436878203833 631 -6.9502436878203833 636 -6.9502436878203833 637 -6.9502436878203833
		 642 -6.9502436878203833 643 -6.9502436878203833 647 -6.9502436878203833 648 -6.9502436878203833
		 656 -6.9502436878203833 657 -6.9502436878203833 666 -6.9502436878203833 667 -6.9502436878203833
		 681 -6.9502436878203833 684 -6.9502436878203833 687 -6.9502436878203833 690 -6.9502436878203833
		 697 -6.9502436878203833 707 -6.9502436878203833 711 -6.9502436878203833 716 -6.9502436878203833
		 720 -6.9502436878203833 725 -6.9502436878203833 729 -6.9502436878203833 734 -6.9502436878203833
		 740 -6.9502436878203833 752 -6.9502436878203833 759 -6.9502436878203833 765 -6.9502436878203833
		 779 -6.9502436878203833 806 -6.9502436878203833 819 -6.9502436878203833 826 -6.9502436878203833
		 841 -6.9502436878203833 850 -6.9502436878203833 864 -6.9502436878203833 888 -6.9502436878203833;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "L_IK_Elbow_ctrl_translateZ";
	rename -uid "316162D1-4E3B-BAE2-7C72-C99A3FEDE49B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1.0382435289754994 7 1.0382435289754994
		 13 1.0382435289754994 217 1.0382435289754994 275 1.0382435289754994 485 1.0382435289754994
		 509 1.0382435289754994 510 1.0382435289754994 515 1.0382435289754994 516 1.0382435289754994
		 526 1.0382435289754994 527 1.0382435289754994 535 1.0382435289754994 536 1.0382435289754994
		 544 1.0382435289754994 545 1.0382435289754994 555 1.0382435289754994 556 1.0382435289754994
		 560 1.0382435289754994 561 1.0382435289754994 567 1.0382435289754994 568 1.0382435289754994
		 580 1.0382435289754994 581 1.0382435289754994 596 1.0382435289754994 597 1.0382435289754994
		 613 1.0382435289754994 614 1.0382435289754994 624 1.0382435289754994 625 1.0382435289754994
		 630 1.0382435289754994 631 1.0382435289754994 636 1.0382435289754994 637 1.0382435289754994
		 642 1.0382435289754994 643 1.0382435289754994 647 1.0382435289754994 648 1.0382435289754994
		 656 1.0382435289754994 657 1.0382435289754994 666 1.0382435289754994 667 1.0382435289754994
		 681 1.0382435289754994 684 1.0382435289754994 687 1.0382435289754994 690 1.0382435289754994
		 697 1.0382435289754994 707 1.0382435289754994 711 1.0382435289754994 716 1.0382435289754994
		 720 1.0382435289754994 725 1.0382435289754994 729 1.0382435289754994 734 1.0382435289754994
		 740 1.0382435289754994 752 1.0382435289754994 759 1.0382435289754994 765 1.0382435289754994
		 779 1.0382435289754994 806 1.0382435289754994 819 1.0382435289754994 826 1.0382435289754994
		 841 1.0382435289754994 850 1.0382435289754994 864 1.0382435289754994 888 1.0382435289754994;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "L_IK_Hand_ctrl_translateX";
	rename -uid "ABBB3EE4-4A78-AE94-C4F1-B087B2CDB6AD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  1 4.5721981515074006 7 4.5721981515074006
		 13 -1.0062189401012736 21 -0.88182559546315209 103 -0.88182559546315209 217 -0.88182559546315209
		 275 -0.88182559546315209 485 -0.88182559546315209 509 -0.88182559546315209 510 -0.88182559546315209
		 515 -0.88182559546315209 516 -0.88182559546315209 526 -0.88182559546315209 527 -0.88182559546315209
		 535 -0.88182559546315209 536 -0.88182559546315209 544 -0.88182559546315209 545 -0.88182559546315209
		 555 -0.88182559546315209 556 -0.88182559546315209 560 -0.88182559546315209 561 -0.88182559546315209
		 567 -0.88182559546315209 568 -0.88182559546315209 580 -0.88182559546315209 581 -0.88182559546315209
		 596 -0.88182559546315209 597 -0.88182559546315209 613 -0.88182559546315209 614 -0.88182559546315209
		 624 -0.88182559546315209 625 -0.88182559546315209 630 -0.88182559546315209 631 -0.88182559546315209
		 636 -0.88182559546315209 637 -0.88182559546315209 642 -0.88182559546315209 643 -0.88182559546315209
		 647 -0.88182559546315209 648 -0.88182559546315209 656 -0.88182559546315209 657 -0.88182559546315209
		 666 -0.88182559546315209 667 -0.88182559546315209 681 -0.88182559546315209 684 -0.88182559546315209
		 687 -0.88182559546315209 690 -0.88182559546315209 697 -0.88182559546315209 707 -0.88182559546315209
		 711 -0.88182559546315209 716 -0.88182559546315209 720 -0.88182559546315209 725 -0.88182559546315209
		 729 -0.88182559546315209 734 -0.88182559546315209 740 -0.88182559546315209 752 -0.88182559546315209
		 759 -0.88182559546315209 765 -0.88182559546315209 779 -0.88182559546315209 806 -0.88182559546315209
		 819 -0.88182559546315209 826 -0.88182559546315209 841 -0.88182559546315209 850 -0.88182559546315209
		 864 14.512344667689467 888 20.115360101109207;
	setAttr -s 68 ".kit[64:67]"  3 18 18 18;
	setAttr -s 68 ".kot[1:67]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
createNode animCurveTL -n "L_IK_Hand_ctrl_translateY";
	rename -uid "4D44ABAA-4F23-14F4-76ED-72ACE7CC6502";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  1 -21.55720662557821 7 -21.55720662557821
		 13 -21.56391958317651 21 -21.563769890667981 103 -21.563769890667981 217 -21.563769890667981
		 275 -21.563769890667981 485 -21.563769890667981 509 -21.563769890667981 510 -21.563769890667981
		 515 -21.563769890667981 516 -21.563769890667981 526 -21.563769890667981 527 -21.563769890667981
		 535 -21.563769890667981 536 -21.563769890667981 544 -21.563769890667981 545 -21.563769890667981
		 555 -21.563769890667981 556 -21.563769890667981 560 -21.563769890667981 561 -21.563769890667981
		 567 -21.563769890667981 568 -21.563769890667981 580 -21.563769890667981 581 -21.563769890667981
		 596 -21.563769890667981 597 -21.563769890667981 613 -21.563769890667981 614 -21.563769890667981
		 624 -21.563769890667981 625 -21.563769890667981 630 -21.563769890667981 631 -21.563769890667981
		 636 -21.563769890667981 637 -21.563769890667981 642 -21.563769890667981 643 -21.563769890667981
		 647 -21.563769890667981 648 -21.563769890667981 656 -21.563769890667981 657 -21.563769890667981
		 666 -21.563769890667981 667 -21.563769890667981 681 -21.563769890667981 684 -21.563769890667981
		 687 -21.563769890667981 690 -21.563769890667981 697 -21.563769890667981 707 -21.563769890667981
		 711 -21.563769890667981 716 -21.563769890667981 720 -21.563769890667981 725 -21.563769890667981
		 729 -21.563769890667981 734 -21.563769890667981 740 -21.563769890667981 752 -21.563769890667981
		 759 -21.563769890667981 765 -21.563769890667981 779 -21.563769890667981 806 -21.563769890667981
		 819 -21.563769890667981 826 -21.563769890667981 841 -21.563769890667981 850 -21.563769890667981
		 864 -21.545244848431018 888 -21.538502289671172;
	setAttr -s 68 ".kit[64:67]"  3 18 18 18;
	setAttr -s 68 ".kot[1:67]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
createNode animCurveTL -n "L_IK_Hand_ctrl_translateZ";
	rename -uid "D71E2D2A-4427-288B-14B6-E8922978B470";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  1 95.176300216209739 7 95.176300216209739
		 13 105.7581314704275 21 105.52216679433893 103 105.52216679433893 217 105.52216679433893
		 275 105.52216679433893 485 105.52216679433893 509 105.52216679433893 510 105.52216679433893
		 515 105.52216679433893 516 105.52216679433893 526 105.52216679433893 527 105.52216679433893
		 535 105.52216679433893 536 105.52216679433893 544 105.52216679433893 545 105.52216679433893
		 555 105.52216679433893 556 105.52216679433893 560 105.52216679433893 561 105.52216679433893
		 567 105.52216679433893 568 105.52216679433893 580 105.52216679433893 581 105.52216679433893
		 596 105.52216679433893 597 105.52216679433893 613 105.52216679433893 614 105.52216679433893
		 624 105.52216679433893 625 105.52216679433893 630 105.52216679433893 631 105.52216679433893
		 636 105.52216679433893 637 105.52216679433893 642 105.52216679433893 643 105.52216679433893
		 647 105.52216679433893 648 105.52216679433893 656 105.52216679433893 657 105.52216679433893
		 666 105.52216679433893 667 105.52216679433893 681 105.52216679433893 684 105.52216679433893
		 687 105.52216679433893 690 105.52216679433893 697 105.52216679433893 707 105.52216679433893
		 711 105.52216679433893 716 105.52216679433893 720 105.52216679433893 725 105.52216679433893
		 729 105.52216679433893 734 105.52216679433893 740 105.52216679433893 752 105.52216679433893
		 759 105.52216679433893 765 105.52216679433893 779 105.52216679433893 806 105.52216679433893
		 819 105.52216679433893 826 105.52216679433893 841 105.52216679433893 850 105.52216679433893
		 864 -40.931413717076637 888 -57.036448968781016;
	setAttr -s 68 ".kit[64:67]"  3 18 18 18;
	setAttr -s 68 ".kot[1:67]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
createNode animCurveTL -n "L_UpArm_ctrl_translateX";
	rename -uid "E4070740-499B-7335-4C25-2196A69D3E9E";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 786 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTL -n "L_UpArm_ctrl_translateY";
	rename -uid "E2950A78-43F4-676E-45CF-ECA7821372F6";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 786 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTL -n "L_UpArm_ctrl_translateZ";
	rename -uid "8E8B6637-49E2-E702-F7C6-27BAC4ACA3F7";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 786 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTL -n "L_LoArm_ctrl_translateX";
	rename -uid "46B0DB1D-407E-DF52-4FB7-BEB1F9B016E5";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 786 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTL -n "L_LoArm_ctrl_translateY";
	rename -uid "937E29C4-48F2-B580-9732-35969BA5404D";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 786 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTL -n "L_LoArm_ctrl_translateZ";
	rename -uid "BE3BACCD-4B52-DEA7-8026-5FB655613D7B";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 786 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTL -n "L_Wrist_ctrl_translateX";
	rename -uid "C05477EA-42EA-28DA-16A2-ADA343C14413";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 77 ".ktv[0:76]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 704 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 779 0 786 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 77 ".kit[0:76]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 
		18;
	setAttr -s 77 ".kot[0:76]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5;
createNode animCurveTL -n "L_Wrist_ctrl_translateY";
	rename -uid "B63DBD5D-4BD0-36DC-4803-8199371F0193";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 77 ".ktv[0:76]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 704 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 779 0 786 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 77 ".kit[0:76]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 
		18;
	setAttr -s 77 ".kot[0:76]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5;
createNode animCurveTL -n "L_Wrist_ctrl_translateZ";
	rename -uid "D939F631-4BA0-AFAF-F508-8687CB3A2AC4";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 77 ".ktv[0:76]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 704 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 779 0 786 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 77 ".kit[0:76]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 
		18;
	setAttr -s 77 ".kot[0:76]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5;
createNode animCurveTL -n "M_Head_ctrl_translateX";
	rename -uid "9BEF1E01-4ECF-8777-F9CC-17AB7F47F091";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 102 ".ktv[0:101]"  1 0 7 0 13 0 17 0 21 0 89 0 201 0 203 0
		 204 0 216 0 217 0 253 0 254 0 274 0 275 0 280 0 286 0 295 0 311 0 322 0 346 0 347 0
		 388 0 389 0 397 0 398 0 408 0 409 0 423 0 424 0 435 0 436 0 447 0 471 0 472 0 484 0
		 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0
		 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0
		 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0
		 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 772 0 779 0 786 0
		 793 0 800 0 806 0 813 0 819 0 826 0 833 0 841 0 850 0 864 0;
	setAttr -s 102 ".kit[0:101]"  18 18 18 3 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 1 18 18 18 18 18 18 18 18 18 18 18 3 18 3 3 
		3 18 3 18 18 3 3 18 18;
	setAttr -s 102 ".kix[77:101]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 102 ".kiy[77:101]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0;
createNode animCurveTL -n "M_Head_ctrl_translateY";
	rename -uid "824AD2F9-4329-44EC-724F-AD908B70DB4B";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 102 ".ktv[0:101]"  1 0 7 0 13 0 17 0 21 0 89 0 201 0 203 0
		 204 0 216 0 217 0 253 0 254 0 274 0 275 0 280 0 286 0 295 0 311 0 322 0 346 0 347 0
		 388 0 389 0 397 0 398 0 408 0 409 0 423 0 424 0 435 0 436 0 447 0 471 0 472 0 484 0
		 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0
		 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0
		 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0
		 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 772 0 779 0 786 0
		 793 0 800 0 806 0 813 0 819 0 826 0 833 0 841 0 850 0 864 0;
	setAttr -s 102 ".kit[0:101]"  18 18 18 3 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 1 18 18 18 18 18 18 18 18 18 18 18 3 18 3 3 
		3 18 3 18 18 3 3 18 18;
	setAttr -s 102 ".kix[77:101]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 102 ".kiy[77:101]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0;
createNode animCurveTL -n "M_Head_ctrl_translateZ";
	rename -uid "A9308C1F-4DD8-C3B8-B69A-7EACAE080FDE";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 102 ".ktv[0:101]"  1 0 7 0 13 0 17 0 21 0 89 0 201 0 203 0
		 204 0 216 0 217 0 253 0 254 0 274 0 275 0 280 0 286 0 295 0 311 0 322 0 346 0 347 0
		 388 0 389 0 397 0 398 0 408 0 409 0 423 0 424 0 435 0 436 0 447 0 471 0 472 0 484 0
		 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0
		 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0
		 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0
		 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 772 0 779 0 786 0
		 793 0 800 0 806 0 813 0 819 0 826 0 833 0 841 0 850 0 864 0;
	setAttr -s 102 ".kit[0:101]"  18 18 18 3 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 1 18 18 18 18 18 18 18 18 18 18 18 3 18 3 3 
		3 18 3 18 18 3 3 18 18;
	setAttr -s 102 ".kix[77:101]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 102 ".kiy[77:101]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0;
createNode animCurveTL -n "M_eye_ctrl_translateX";
	rename -uid "54383A04-4EB2-607A-A00A-0DADCE37BFA6";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 83 ".ktv[0:82]"  1 0.54471876986219769 7 -0.018508277395294037
		 13 -2.2261087855054451 21 -2.2261087855054451 89 -2.2261087855054451 93 3.7919802884801492
		 97 3.7919802884801492 158 3.7919802884801492 159 3.7919802884801492 160 3.7919802884801492
		 161 3.7919802884801492 196 3.7919802884801492 202 3.7919802884801492 215 3.7919802884801492
		 220 3.7919802884801492 253 3.7919802884801492 254 3.7919802884801492 274 3.7919802884801492
		 275 3.7919802884801492 388 3.7919802884801492 389 3.491980288480149 435 3.491980288480149
		 436 3.7919802884801492 485 3.7919802884801492 509 3.7919802884801492 510 3.7919802884801492
		 515 3.7919802884801492 516 3.7919802884801492 526 3.7919802884801492 527 3.7919802884801492
		 535 3.7919802884801492 536 3.7919802884801492 544 3.7919802884801492 545 3.7919802884801492
		 555 3.7919802884801492 556 3.7919802884801492 560 3.7919802884801492 561 3.7919802884801492
		 567 3.7919802884801492 568 3.7919802884801492 580 3.7919802884801492 581 3.7919802884801492
		 596 3.7919802884801492 597 3.7919802884801492 613 3.7919802884801492 614 3.7919802884801492
		 624 3.7919802884801492 625 3.1459054277014373 630 3.1459054277014373 631 6.0603866848473125
		 636 6.0603866848473125 637 6.0603866848473231 642 6.0603866848473231 643 6.0603866848473231
		 647 6.0603866848473231 648 4.7501551618208717 656 4.7501551618208717 657 3.7919802884801492
		 666 3.7919802884801492 667 3.7919802884801492 681 3.7919802884801492 684 3.7919802884801492
		 687 3.7919802884801492 690 3.7919802884801492 697 3.7919802884801492 707 3.7919802884801492
		 711 3.7919802884801492 716 3.7919802884801492 720 3.7919802884801492 725 3.7919802884801492
		 729 3.7919802884801492 734 3.7919802884801492 740 3.7919802884801492 752 3.7919802884801492
		 759 3.7919802884801492 765 3.7919802884801492 779 3.7919802884801492 806 3.7919802884801492
		 819 3.7919802884801492 826 3.7919802884801492 841 3.7919802884801492 850 3.7919802884801492
		 864 1.1708399843240334;
	setAttr -s 83 ".kit[0:82]"  18 18 18 18 18 18 18 18 
		18 18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 1 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 3 18 18;
	setAttr -s 83 ".kot[0:82]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 83 ".kix[64:82]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 83 ".kiy[64:82]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "M_eye_ctrl_translateY";
	rename -uid "594B141E-43D8-0E78-E1FC-A8BBA79698E7";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 83 ".ktv[0:82]"  1 -0.23134323089887174 7 -0.32584849018217504
		 13 -0.70360328752148205 21 -0.70360328752148205 89 -0.70360328752148205 93 0 97 0
		 158 0 159 0 160 0 161 0 196 0 202 0 215 0 220 0 253 0 254 0 274 0 275 0 388 0 389 0
		 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0
		 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0.36915867201177999
		 630 0.36915867201177999 631 3.4980012092803427 636 3.4980012092803427 637 3.4980012092803427
		 642 3.4980012092803427 643 3.4980012092803427 647 3.4980012092803427 648 1.4775557285571002
		 656 1.4775557285571002 657 0 666 0 667 0 681 0 684 2.6 687 2.6 690 2.6 697 0 707 2.6
		 711 2.6 716 2.6 720 2.6 725 2.6 729 2.6 734 0 740 0 752 0 759 0 765 0 779 0 806 0
		 819 0 826 0 841 0 850 0 864 -0.21809175610137099;
	setAttr -s 83 ".kit[0:82]"  18 18 18 18 18 18 18 18 
		18 18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 1 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 3 18 18;
	setAttr -s 83 ".kot[0:82]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 83 ".kix[64:82]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 83 ".kiy[64:82]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "M_eye_ctrl_translateZ";
	rename -uid "F6A4D790-406C-F15D-FFA1-AB8044F545CE";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 83 ".ktv[0:82]"  1 4.7806815300611731 7 4.4734786852198516
		 13 3.3086181131309718 21 3.3086181131309718 89 3.3086181131309718 93 7.0818468184102557
		 97 7.0818468184102557 158 7.0818468184102557 159 7.0818468184102557 160 7.0818468184102557
		 161 7.0818468184102557 196 7.0818468184102557 202 7.0818468184102557 215 7.0818468184102557
		 220 7.0818468184102557 253 7.0818468184102557 254 7.0818468184102557 274 7.0818468184102557
		 275 7.0818468184102557 388 7.0818468184102557 389 7.0818468184102557 435 7.0818468184102557
		 436 7.0818468184102557 485 7.0818468184102557 509 7.0818468184102557 510 7.0818468184102557
		 515 7.0818468184102557 516 7.0818468184102557 526 7.0818468184102557 527 7.0818468184102557
		 535 7.0818468184102557 536 7.0818468184102557 544 7.0818468184102557 545 7.0818468184102557
		 555 7.0818468184102557 556 7.0818468184102557 560 7.0818468184102557 561 7.0818468184102557
		 567 7.0818468184102557 568 7.0818468184102557 580 7.0818468184102557 581 7.0818468184102557
		 596 7.0818468184102557 597 7.0818468184102557 613 7.0818468184102557 614 7.0818468184102557
		 624 7.0818468184102557 625 3.8168377168072567 630 3.8168377168072567 631 3.8168377168072567
		 636 3.8168377168072567 637 3.8168377168072567 642 3.8168377168072567 643 3.8168377168072567
		 647 3.8168377168072567 648 5.7027069573188154 656 5.7027069573188154 657 7.0818468184102557
		 666 7.0818468184102557 667 7.0818468184102557 681 7.0818468184102557 684 7.0818468184102557
		 687 7.0818468184102557 690 7.0818468184102557 697 7.0818468184102557 707 7.0818468184102557
		 711 7.0818468184102557 716 7.0818468184102557 720 7.0818468184102557 725 7.0818468184102557
		 729 7.0818468184102557 734 7.0818468184102557 740 7.0818468184102557 752 7.0818468184102557
		 759 7.0818468184102557 765 7.0818468184102557 779 7.0818468184102557 806 7.0818468184102557
		 819 7.0818468184102557 826 7.0818468184102557 841 7.0818468184102557 850 7.0818468184102557
		 864 4.7767078155594263;
	setAttr -s 83 ".kit[0:82]"  18 18 18 18 18 18 18 18 
		18 18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 1 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 3 18 18;
	setAttr -s 83 ".kot[0:82]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 83 ".kix[64:82]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 83 ".kiy[64:82]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "L_eye_ctrl_translateX";
	rename -uid "132339B2-4F72-FFC2-1102-0FB8E935333E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 0 7 0 13 0 196 0 217 0 253 0 254 0 274 0
		 275 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0
		 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0
		 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 -3.6 687 -3.6
		 690 -3.6 697 0 707 -3.6 711 -3.6 716 -3.6 720 -3.6 725 -3.6 729 -3.6 734 0 740 0
		 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "L_eye_ctrl_translateY";
	rename -uid "DFCEC065-4C88-F198-C29D-CE98B005156E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 0 7 0 13 0 196 0 217 0 253 0 254 0 274 0
		 275 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0
		 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0
		 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0
		 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0
		 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "L_eye_ctrl_translateZ";
	rename -uid "ED6F20E1-4C15-98C7-8160-FA8E91B224EF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 0 7 0 13 0 196 0 217 0 253 0 254 0 274 0
		 275 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0
		 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0
		 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0
		 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0
		 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_eye_ctrl_translateX";
	rename -uid "869FC63B-4803-8FEB-EF4C-B7B8D89FD7CA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 0 7 0 13 0 196 0 217 0 253 0 254 0 274 0
		 275 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0
		 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0
		 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 2.6 687 2.6
		 690 2.6 697 0 707 2.6 711 2.6 716 2.6 720 2.6 725 2.6 729 2.6 734 0 740 0 752 0 759 0
		 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_eye_ctrl_translateY";
	rename -uid "6B7F2952-4062-63FC-5F26-35BADF74EC58";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 0 7 0 13 0 196 0 217 0 253 0 254 0 274 0
		 275 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0
		 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0
		 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0
		 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0
		 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_eye_ctrl_translateZ";
	rename -uid "D63C4BCC-4FD4-6756-BD5A-8EB9DAED0C11";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 0 7 0 13 0 196 0 217 0 253 0 254 0 274 0
		 275 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0
		 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0
		 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0
		 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0
		 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "M_mouth_main_ctrl_translateX";
	rename -uid "AB97676A-47DF-0AD8-EC17-028391BD61EB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "M_mouth_main_ctrl_translateY";
	rename -uid "3ABE8B62-4A74-96B1-F079-DA843B404AB3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 -0.88984477600236611 687 -0.88984477600236611 690 -0.88984477600236611
		 697 0 707 -0.88984477600236611 711 -0.88984477600236611 716 -0.88984477600236611
		 720 -0.88984477600236611 725 -0.88984477600236611 729 -0.88984477600236611 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "M_mouth_main_ctrl_translateZ";
	rename -uid "88A085F1-410B-BD9C-8B0E-E0826EB672C7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "L_mouth_ctrl_translateX";
	rename -uid "28B78532-4A86-83D7-2400-F78DE18A2AE3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 90 ".ktv[0:89]"  1 0 7 0 13 0.60000000000000009 93 0.60000000000000009
		 201 0.60000000000000009 202 1.7821313446329541 203 1.7821313446329541 216 1.7821313446329541
		 217 1.7821313446329541 275 1.7821313446329541 389 1.7821313446329541 397 1.7821313446329541
		 398 0 406 0 407 1.7821313446329541 408 1.7821313446329541 409 1.5204856776792821
		 410 1.5204856776792821 411 1.7821313446329541 412 1.7821313446329541 413 1.7821313446329541
		 414 1.7821313446329541 415 1.7821313446329541 416 1.7821313446329541 417 1.5204856776792821
		 419 1.5204856776792821 420 1.7821313446329541 421 1.7821313446329541 424 1.7821313446329541
		 425 1.7821313446329541 485 1.7821313446329541 509 1.7821313446329541 510 1.7821313446329541
		 515 1.7821313446329541 516 1.7821313446329541 526 1.7821313446329541 527 1.7821313446329541
		 535 1.7821313446329541 536 1.7821313446329541 544 1.7821313446329541 545 1.7821313446329541
		 555 1.7821313446329541 556 1.7821313446329541 560 1.7821313446329541 561 1.7821313446329541
		 567 1.7821313446329541 568 1.7821313446329541 580 1.7821313446329541 581 1.7821313446329541
		 596 1.7821313446329541 597 1.7821313446329541 613 1.7821313446329541 614 1.7821313446329541
		 624 1.7821313446329541 625 1.5204856776792821 630 1.5204856776792821 631 1.7821313446329541
		 636 1.7821313446329541 637 1.7821313446329541 642 1.7821313446329541 643 1.7821313446329541
		 647 1.7821313446329541 648 1.7821313446329541 656 1.7821313446329541 657 1.7821313446329541
		 666 1.7821313446329541 667 1.7821313446329541 681 1.7821313446329541 684 2.3842962849216773
		 687 2.3842962849216773 690 2.3842962849216773 697 1.7821313446329541 707 2.3842962849216773
		 711 2.3842962849216773 716 2.3842962849216773 720 2.3842962849216773 725 2.3842962849216773
		 729 2.3842962849216773 734 1.7821313446329541 740 1.7821313446329541 752 1.7821313446329541
		 759 1.7821313446329541 765 1.7821313446329541 779 1.7821313446329541 806 1.7821313446329541
		 819 1.7821313446329541 826 1.7821313446329541 841 1.7821313446329541 850 1.7821313446329541
		 864 1.7821313446329541;
	setAttr -s 90 ".kit[71:89]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 90 ".kot[1:89]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 90 ".kix[71:89]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 90 ".kiy[71:89]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "L_mouth_ctrl_translateY";
	rename -uid "FB0DB0AB-4B7D-03BA-7BBA-41809C81EF27";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 90 ".ktv[0:89]"  1 0 7 0 13 -0.5 93 -0.5 201 -0.5 202 2.7210770314754154
		 203 2.7210770314754154 216 2.7210770314754154 217 2.7210770314754154 275 2.7210770314754154
		 389 2.7210770314754154 397 2.7210770314754154 398 0.70000000000000007 406 0.70000000000000007
		 407 2.7210770314754154 408 2.7210770314754154 409 1.468882433240579 410 1.468882433240579
		 411 2.7210770314754154 412 2.7210770314754154 413 2.7210770314754154 414 2.7210770314754154
		 415 2.7210770314754154 416 2.7210770314754154 417 1.468882433240579 419 1.468882433240579
		 420 2.7210770314754154 421 2.7210770314754154 424 2.7210770314754154 425 2.7210770314754154
		 485 2.7210770314754154 509 2.7210770314754154 510 2.7210770314754154 515 2.7210770314754154
		 516 2.7210770314754154 526 2.7210770314754154 527 2.7210770314754154 535 2.7210770314754154
		 536 2.7210770314754154 544 2.7210770314754154 545 2.7210770314754154 555 2.7210770314754154
		 556 2.7210770314754154 560 2.7210770314754154 561 2.7210770314754154 567 2.7210770314754154
		 568 2.7210770314754154 580 2.7210770314754154 581 2.7210770314754154 596 2.7210770314754154
		 597 2.7210770314754154 613 2.7210770314754154 614 2.7210770314754154 624 2.7210770314754154
		 625 1.468882433240579 630 1.468882433240579 631 2.7210770314754154 636 2.7210770314754154
		 637 2.7210770314754154 642 2.7210770314754154 643 2.7210770314754154 647 2.7210770314754154
		 648 2.7210770314754154 656 2.7210770314754154 657 2.7210770314754154 666 2.7210770314754154
		 667 2.7210770314754154 681 2.7210770314754154 684 -2.135697419648019 687 -2.135697419648019
		 690 -2.135697419648019 697 2.7210770314754154 707 -2.135697419648019 711 -2.135697419648019
		 716 -2.135697419648019 720 -2.135697419648019 725 -2.135697419648019 729 -2.135697419648019
		 734 2.7210770314754154 740 2.7210770314754154 752 2.7210770314754154 759 2.7210770314754154
		 765 2.7210770314754154 779 2.7210770314754154 806 2.7210770314754154 819 2.7210770314754154
		 826 2.7210770314754154 841 2.7210770314754154 850 2.7210770314754154 864 2.7210770314754154;
	setAttr -s 90 ".kit[71:89]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 90 ".kot[1:89]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 90 ".kix[71:89]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 90 ".kiy[71:89]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "L_mouth_ctrl_translateZ";
	rename -uid "8A144FDA-495B-505C-2EED-72817B1B9BB8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "M_mouth_ctrl_translateX";
	rename -uid "9F9EBC14-47D7-3C20-D851-21819359834D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 0.0052795943814449818 687 0.0052795943814449818 690 0.0052795943814449818
		 697 0 707 0.0052795943814449818 711 0.0052795943814449818 716 0.0052795943814449818
		 720 0.0052795943814449818 725 0.0052795943814449818 729 0.0052795943814449818 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "M_mouth_ctrl_translateY";
	rename -uid "066E6B82-4E39-CACA-171E-02975BF67AD6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 -0.1186525994868794 687 -0.1186525994868794 690 -0.1186525994868794
		 697 0 707 -0.1186525994868794 711 -0.1186525994868794 716 -0.1186525994868794 720 -0.1186525994868794
		 725 -0.1186525994868794 729 -0.1186525994868794 734 0 740 0 752 0 759 0 765 0 779 0
		 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "M_mouth_ctrl_translateZ";
	rename -uid "D805C479-416D-B6CF-EE79-5BBEF851AA4E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_mouth_ctrl_translateX";
	rename -uid "5521F2E6-45C8-FB22-3425-A88251B354BF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 90 ".ktv[0:89]"  1 0 7 0 13 -0.5 93 -0.5 201 -0.5 202 -2.3509464724371374
		 203 -2.3509464724371374 216 -2.3509464724371374 217 -2.3509464724371374 275 -2.3509464724371374
		 389 -2.3509464724371374 397 -2.3509464724371374 398 0 406 0 407 -2.3509464724371374
		 408 -2.3509464724371374 409 -1.6178002849840116 410 -1.6178002849840116 411 -2.3509464724371374
		 412 -2.3509464724371374 413 -2.3509464724371374 414 -2.3509464724371374 415 -2.3509464724371374
		 416 -2.3509464724371374 417 -1.6178002849840116 419 -1.6178002849840116 420 -2.3509464724371374
		 421 -2.3509464724371374 424 -2.3509464724371374 425 -2.3509464724371374 485 -2.3509464724371374
		 509 -2.3509464724371374 510 -2.3509464724371374 515 -2.3509464724371374 516 -2.3509464724371374
		 526 -2.3509464724371374 527 -2.3509464724371374 535 -2.3509464724371374 536 -2.3509464724371374
		 544 -2.3509464724371374 545 -2.3509464724371374 555 -2.3509464724371374 556 -2.3509464724371374
		 560 -2.3509464724371374 561 -2.3509464724371374 567 -2.3509464724371374 568 -2.3509464724371374
		 580 -2.3509464724371374 581 -2.3509464724371374 596 -2.3509464724371374 597 -2.3509464724371374
		 613 -2.3509464724371374 614 -2.3509464724371374 624 -2.3509464724371374 625 -1.6178002849840116
		 630 -1.6178002849840116 631 -2.3509464724371374 636 -2.3509464724371374 637 -2.3509464724371374
		 642 -2.3509464724371374 643 -2.3509464724371374 647 -2.3509464724371374 648 -2.3509464724371374
		 656 -2.3509464724371374 657 -2.3509464724371374 666 -2.3509464724371374 667 -2.3509464724371374
		 681 -2.3509464724371374 684 -1.9637269125632559 687 -1.9637269125632559 690 -1.9637269125632559
		 697 -2.3509464724371374 707 -1.9637269125632559 711 -1.9637269125632559 716 -1.9637269125632559
		 720 -1.9637269125632559 725 -1.9637269125632559 729 -1.9637269125632559 734 -2.3509464724371374
		 740 -2.3509464724371374 752 -2.3509464724371374 759 -2.3509464724371374 765 -2.3509464724371374
		 779 -2.3509464724371374 806 -2.3509464724371374 819 -2.3509464724371374 826 -2.3509464724371374
		 841 -2.3509464724371374 850 -2.3509464724371374 864 -2.3509464724371374;
	setAttr -s 90 ".kit[71:89]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 90 ".kot[1:89]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 90 ".kix[71:89]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 90 ".kiy[71:89]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_mouth_ctrl_translateY";
	rename -uid "2B0D5FE3-4E4A-2638-5A68-7F9320A665AF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 90 ".ktv[0:89]"  1 0 7 0 13 -0.60000000000000009 93 -0.60000000000000009
		 201 -0.60000000000000009 202 2.3791235841913214 203 2.3791235841913214 216 2.3791235841913214
		 217 2.3791235841913214 275 2.3791235841913214 389 2.3791235841913214 397 2.3791235841913214
		 398 0.70000000000000007 406 0.70000000000000007 407 2.3791235841913214 408 2.3791235841913214
		 409 1.1103810438084984 410 1.1103810438084984 411 2.3791235841913214 412 2.3791235841913214
		 413 2.3791235841913214 414 2.3791235841913214 415 2.3791235841913214 416 2.3791235841913214
		 417 1.1103810438084984 419 1.1103810438084984 420 2.3791235841913214 421 2.3791235841913214
		 424 2.3791235841913214 425 2.3791235841913214 485 2.3791235841913214 509 2.3791235841913214
		 510 2.3791235841913214 515 2.3791235841913214 516 2.3791235841913214 526 2.3791235841913214
		 527 2.3791235841913214 535 2.3791235841913214 536 2.3791235841913214 544 2.3791235841913214
		 545 2.3791235841913214 555 2.3791235841913214 556 2.3791235841913214 560 2.3791235841913214
		 561 2.3791235841913214 567 2.3791235841913214 568 2.3791235841913214 580 2.3791235841913214
		 581 2.3791235841913214 596 2.3791235841913214 597 2.3791235841913214 613 2.3791235841913214
		 614 2.3791235841913214 624 2.3791235841913214 625 1.1103810438084984 630 1.1103810438084984
		 631 2.3791235841913214 636 2.3791235841913214 637 2.3791235841913214 642 2.3791235841913214
		 643 2.3791235841913214 647 2.3791235841913214 648 2.3791235841913214 656 2.3791235841913214
		 657 2.3791235841913214 666 2.3791235841913214 667 2.3791235841913214 681 2.3791235841913214
		 684 -2.2887840237108983 687 -2.2887840237108983 690 -2.2887840237108983 697 2.3791235841913214
		 707 -2.2887840237108983 711 -2.2887840237108983 716 -2.2887840237108983 720 -2.2887840237108983
		 725 -2.2887840237108983 729 -2.2887840237108983 734 2.3791235841913214 740 2.3791235841913214
		 752 2.3791235841913214 759 2.3791235841913214 765 2.3791235841913214 779 2.3791235841913214
		 806 2.3791235841913214 819 2.3791235841913214 826 2.3791235841913214 841 2.3791235841913214
		 850 2.3791235841913214 864 2.3791235841913214;
	setAttr -s 90 ".kit[71:89]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 90 ".kot[1:89]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 90 ".kix[71:89]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 90 ".kiy[71:89]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_mouth_ctrl_translateZ";
	rename -uid "FBCDD743-4413-9399-D5A6-97877815BDDA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_IK_Elbow_ctrl_translateX";
	rename -uid "5707B386-4EF3-4DE4-0316-C3B285D0B074";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 -23.658526301767285 7 -23.658526301767285
		 13 -23.658526301767285 217 -23.658526301767285 275 -23.658526301767285 485 -23.658526301767285
		 509 -23.658526301767285 510 -23.658526301767285 515 -23.658526301767285 516 -23.658526301767285
		 526 -23.658526301767285 527 -23.658526301767285 535 -23.658526301767285 536 -23.658526301767285
		 544 -23.658526301767285 545 -23.658526301767285 555 -23.658526301767285 556 -23.658526301767285
		 560 -23.658526301767285 561 -23.658526301767285 567 -23.658526301767285 568 -23.658526301767285
		 580 -23.658526301767285 581 -23.658526301767285 596 -23.658526301767285 597 -23.658526301767285
		 613 -23.658526301767285 614 -23.658526301767285 624 -23.658526301767285 625 -23.658526301767285
		 630 -23.658526301767285 631 -23.658526301767285 636 -23.658526301767285 637 -23.658526301767285
		 642 -23.658526301767285 643 -23.658526301767285 647 -23.658526301767285 648 -23.658526301767285
		 656 -23.658526301767285 657 -23.658526301767285 666 -23.658526301767285 667 -23.658526301767285
		 681 -23.658526301767285 684 -23.658526301767285 687 -23.658526301767285 690 -23.658526301767285
		 697 -23.658526301767285 707 -23.658526301767285 711 -23.658526301767285 716 -23.658526301767285
		 720 -23.658526301767285 725 -23.658526301767285 729 -23.658526301767285 734 -23.658526301767285
		 740 -23.658526301767285 752 -23.658526301767285 759 -23.658526301767285 765 -23.658526301767285
		 779 -23.658526301767285 806 -23.658526301767285 819 -23.658526301767285 826 -23.658526301767285
		 841 -23.658526301767285 850 -23.658526301767285 864 -23.658526301767285 888 -23.658526301767285;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "R_IK_Elbow_ctrl_translateY";
	rename -uid "3F23A485-4F7A-9F3C-7F71-8D95E7F31FE1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 -18.679793146451871 7 -18.679793146451871
		 13 -18.679793146451871 217 -18.679793146451871 275 -18.679793146451871 485 -18.679793146451871
		 509 -18.679793146451871 510 -18.679793146451871 515 -18.679793146451871 516 -18.679793146451871
		 526 -18.679793146451871 527 -18.679793146451871 535 -18.679793146451871 536 -18.679793146451871
		 544 -18.679793146451871 545 -18.679793146451871 555 -18.679793146451871 556 -18.679793146451871
		 560 -18.679793146451871 561 -18.679793146451871 567 -18.679793146451871 568 -18.679793146451871
		 580 -18.679793146451871 581 -18.679793146451871 596 -18.679793146451871 597 -18.679793146451871
		 613 -18.679793146451871 614 -18.679793146451871 624 -18.679793146451871 625 -18.679793146451871
		 630 -18.679793146451871 631 -18.679793146451871 636 -18.679793146451871 637 -18.679793146451871
		 642 -18.679793146451871 643 -18.679793146451871 647 -18.679793146451871 648 -18.679793146451871
		 656 -18.679793146451871 657 -18.679793146451871 666 -18.679793146451871 667 -18.679793146451871
		 681 -18.679793146451871 684 -18.679793146451871 687 -18.679793146451871 690 -18.679793146451871
		 697 -18.679793146451871 707 -18.679793146451871 711 -18.679793146451871 716 -18.679793146451871
		 720 -18.679793146451871 725 -18.679793146451871 729 -18.679793146451871 734 -18.679793146451871
		 740 -18.679793146451871 752 -18.679793146451871 759 -18.679793146451871 765 -18.679793146451871
		 779 -18.679793146451871 806 -18.679793146451871 819 -18.679793146451871 826 -18.679793146451871
		 841 -18.679793146451871 850 -18.679793146451871 864 -18.679793146451871 888 -18.679793146451871;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "R_IK_Elbow_ctrl_translateZ";
	rename -uid "DA318A48-473E-F732-72BD-AF8ADE72D798";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 12.209819740317853 7 12.209819740317853
		 13 12.209819740317853 217 12.209819740317853 275 12.209819740317853 485 12.209819740317853
		 509 12.209819740317853 510 12.209819740317853 515 12.209819740317853 516 12.209819740317853
		 526 12.209819740317853 527 12.209819740317853 535 12.209819740317853 536 12.209819740317853
		 544 12.209819740317853 545 12.209819740317853 555 12.209819740317853 556 12.209819740317853
		 560 12.209819740317853 561 12.209819740317853 567 12.209819740317853 568 12.209819740317853
		 580 12.209819740317853 581 12.209819740317853 596 12.209819740317853 597 12.209819740317853
		 613 12.209819740317853 614 12.209819740317853 624 12.209819740317853 625 12.209819740317853
		 630 12.209819740317853 631 12.209819740317853 636 12.209819740317853 637 12.209819740317853
		 642 12.209819740317853 643 12.209819740317853 647 12.209819740317853 648 12.209819740317853
		 656 12.209819740317853 657 12.209819740317853 666 12.209819740317853 667 12.209819740317853
		 681 12.209819740317853 684 12.209819740317853 687 12.209819740317853 690 12.209819740317853
		 697 12.209819740317853 707 12.209819740317853 711 12.209819740317853 716 12.209819740317853
		 720 12.209819740317853 725 12.209819740317853 729 12.209819740317853 734 12.209819740317853
		 740 12.209819740317853 752 12.209819740317853 759 12.209819740317853 765 12.209819740317853
		 779 12.209819740317853 806 12.209819740317853 819 12.209819740317853 826 12.209819740317853
		 841 12.209819740317853 850 12.209819740317853 864 12.209819740317853 888 12.209819740317853;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "R_IK_Hand_ctrl_translateX";
	rename -uid "70D91A64-426E-2812-A0EF-46ABEA7C2952";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 -30.896507515196973 7 -30.896507515196973
		 13 -30.896507515196973 17 -30.896507515196973 21 -31.372036859731455 103 -31.372036859731455
		 217 -31.372036859731455 275 -31.372036859731455 485 -31.372036859731455 509 -31.372036859731455
		 510 -31.372036859731455 515 -31.372036859731455 516 -31.372036859731455 526 -31.372036859731455
		 527 -31.372036859731455 535 -31.372036859731455 536 -31.372036859731455 544 -31.372036859731455
		 545 -31.372036859731455 555 -31.372036859731455 556 -31.372036859731455 560 -31.372036859731455
		 561 -31.372036859731455 567 -31.372036859731455 568 -31.372036859731455 580 -31.372036859731455
		 581 -31.372036859731455 596 -31.372036859731455 597 -31.372036859731455 613 -31.372036859731455
		 614 -31.372036859731455 624 -31.372036859731455 625 -31.372036859731455 630 -31.372036859731455
		 631 -31.372036859731455 636 -31.372036859731455 637 -31.372036859731455 642 -31.372036859731455
		 643 -31.372036859731455 647 -31.372036859731455 648 -31.372036859731455 656 -31.372036859731455
		 657 -31.372036859731455 666 -31.372036859731455 667 -31.372036859731455 681 -31.372036859731455
		 684 -31.372036859731455 687 -31.372036859731455 690 -31.372036859731455 697 -31.372036859731455
		 707 -31.372036859731455 711 -31.372036859731455 716 -31.372036859731455 720 -31.372036859731455
		 725 -31.372036859731455 729 -31.372036859731455 734 -31.372036859731455 740 -31.372036859731455
		 752 -31.372036859731455 759 -31.372036859731455 765 -31.372036859731455 779 -31.372036859731455
		 806 -31.372036859731455 819 -31.372036859731455 826 -31.372036859731455 841 -31.372036859731455
		 850 -31.372036859731455 864 -48.824042801266827 888 -46.439078106132342;
	setAttr -s 69 ".kit[65:68]"  3 18 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
createNode animCurveTL -n "R_IK_Hand_ctrl_translateY";
	rename -uid "CFF8F842-4797-831E-162B-FCBB5AA5435B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 -21.448014844393587 7 -21.448014844393587
		 13 -21.448014844393587 17 -21.448014844393587 21 -21.448699749812967 103 -21.448699749812967
		 217 -21.448699749812967 275 -21.448699749812967 485 -21.448699749812967 509 -21.448699749812967
		 510 -21.448699749812967 515 -21.448699749812967 516 -21.448699749812967 526 -21.448699749812967
		 527 -21.448699749812967 535 -21.448699749812967 536 -21.448699749812967 544 -21.448699749812967
		 545 -21.448699749812967 555 -21.448699749812967 556 -21.448699749812967 560 -21.448699749812967
		 561 -21.448699749812967 567 -21.448699749812967 568 -21.448699749812967 580 -21.448699749812967
		 581 -21.448699749812967 596 -21.448699749812967 597 -21.448699749812967 613 -21.448699749812967
		 614 -21.448699749812967 624 -21.448699749812967 625 -21.448699749812967 630 -21.448699749812967
		 631 -21.448699749812967 636 -21.448699749812967 637 -21.448699749812967 642 -21.448699749812967
		 643 -21.448699749812967 647 -21.448699749812967 648 -21.448699749812967 656 -21.448699749812967
		 657 -21.448699749812967 666 -21.448699749812967 667 -21.448699749812967 681 -21.448699749812967
		 684 -21.448699749812967 687 -21.448699749812967 690 -21.448699749812967 697 -21.448699749812967
		 707 -21.448699749812967 711 -21.448699749812967 716 -21.448699749812967 720 -21.448699749812967
		 725 -21.448699749812967 729 -21.448699749812967 734 -21.448699749812967 740 -21.448699749812967
		 752 -21.448699749812967 759 -21.448699749812967 765 -21.448699749812967 779 -21.448699749812967
		 806 -21.448699749812967 819 -21.448699749812967 826 -21.448699749812967 841 -21.448699749812967
		 850 -21.448699749812967 864 -21.591726688281945 888 -21.610942651981723;
	setAttr -s 69 ".kit[65:68]"  3 18 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
createNode animCurveTL -n "R_IK_Hand_ctrl_translateZ";
	rename -uid "EF64FB53-44E0-482E-E2EA-9199F8523DF5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 -94.333205676838688 7 -94.333205676838688
		 13 -94.333205676838688 17 -94.333205676838688 21 -93.431154828921265 103 -93.431154828921265
		 217 -93.431154828921265 275 -93.431154828921265 485 -93.431154828921265 509 -93.431154828921265
		 510 -93.431154828921265 515 -93.431154828921265 516 -93.431154828921265 526 -93.431154828921265
		 527 -93.431154828921265 535 -93.431154828921265 536 -93.431154828921265 544 -93.431154828921265
		 545 -93.431154828921265 555 -93.431154828921265 556 -93.431154828921265 560 -93.431154828921265
		 561 -93.431154828921265 567 -93.431154828921265 568 -93.431154828921265 580 -93.431154828921265
		 581 -93.431154828921265 596 -93.431154828921265 597 -93.431154828921265 613 -93.431154828921265
		 614 -93.431154828921265 624 -93.431154828921265 625 -93.431154828921265 630 -93.431154828921265
		 631 -93.431154828921265 636 -93.431154828921265 637 -93.431154828921265 642 -93.431154828921265
		 643 -93.431154828921265 647 -93.431154828921265 648 -93.431154828921265 656 -93.431154828921265
		 657 -93.431154828921265 666 -93.431154828921265 667 -93.431154828921265 681 -93.431154828921265
		 684 -93.431154828921265 687 -93.431154828921265 690 -93.431154828921265 697 -93.431154828921265
		 707 -93.431154828921265 711 -93.431154828921265 716 -93.431154828921265 720 -93.431154828921265
		 725 -93.431154828921265 729 -93.431154828921265 734 -93.431154828921265 740 -93.431154828921265
		 752 -93.431154828921265 759 -93.431154828921265 765 -93.431154828921265 779 -93.431154828921265
		 806 -93.431154828921265 819 -93.431154828921265 826 -93.431154828921265 841 -93.431154828921265
		 850 -93.431154828921265 864 42.623440864114812 888 57.879517844068104;
	setAttr -s 69 ".kit[65:68]"  3 18 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
createNode animCurveTL -n "R_UpArm_ctrl_translateX";
	rename -uid "B07DB40F-4617-3AF3-D281-059894805D09";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 772 0 779 0 786 0 793 0 800 0 806 0 819 0 826 0 841 0 850 0
		 864 0;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 1 18 18 1 1 1 18 3 3 3 18 18 18 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
	setAttr -s 79 ".kix[63:78]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 79 ".kiy[63:78]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_UpArm_ctrl_translateY";
	rename -uid "208A7169-4A3F-F8CC-47CA-FD941C052FC5";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 772 0 779 0 786 0 793 0 800 0 806 0 819 0 826 0 841 0 850 0
		 864 0;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 1 18 18 1 1 1 18 3 3 3 18 18 18 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
	setAttr -s 79 ".kix[63:78]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 79 ".kiy[63:78]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_UpArm_ctrl_translateZ";
	rename -uid "2C17FBE6-4CD8-1024-8E18-1380B87ABD44";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 772 0 779 0 786 0 793 0 800 0 806 0 819 0 826 0 841 0 850 0
		 864 0;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 1 18 18 1 1 1 18 3 3 3 18 18 18 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
	setAttr -s 79 ".kix[63:78]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 79 ".kiy[63:78]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_LoArm_ctrl_translateX";
	rename -uid "17625B2A-4B84-6F18-E118-B98381B9AAFC";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 786 0 793 0 800 0 806 0 819 0 826 0 833 0 841 0 850 0
		 864 0;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 3 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "R_LoArm_ctrl_translateY";
	rename -uid "87083827-4F5F-0796-26AB-82BE97859ED0";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 786 0 793 0 800 0 806 0 819 0 826 0 833 0 841 0 850 0
		 864 0;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 3 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "R_LoArm_ctrl_translateZ";
	rename -uid "5BEB15E8-493C-B0B7-A3E1-D2BF64B78EA4";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 786 0 793 0 800 0 806 0 819 0 826 0 833 0 841 0 850 0
		 864 0;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 3 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "R_Wrist_ctrl_translateX";
	rename -uid "D4C5EED5-4BF8-8673-D3E9-2489B466EF3D";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 703 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 779 0 786 0 793 0 800 0 806 0 819 0 826 0 841 0 850 0
		 864 0;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "R_Wrist_ctrl_translateY";
	rename -uid "2DCD2813-4467-C382-164C-25BC0434449D";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 703 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 779 0 786 0 793 0 800 0 806 0 819 0 826 0 841 0 850 0
		 864 0;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "R_Wrist_ctrl_translateZ";
	rename -uid "295FCF3B-4725-23D2-360F-04B688CFA3B0";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 703 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 779 0 786 0 793 0 800 0 806 0 819 0 826 0 841 0 850 0
		 864 0;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTL -n "R_foot_ctrl_translateX";
	rename -uid "B1AF5252-4433-FD88-5449-08AEB479D9D0";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 101 ".ktv[0:100]"  1 19.24394192541671 7 19.24394192541671
		 13 19.24394192541671 17 20.884002625466827 21 20.884002625466827 103 20.884002625466827
		 217 20.884002625466827 275 20.884002625466827 295 20.884002625466827 296 25.01954450036181
		 322 25.01954450036181 323 23.005199365466154 346 23.005199365466154 347 23.005199365466154
		 368 23.005199365466154 369 21.17536051945763 388 21.17536051945763 389 21.17536051945763
		 397 21.17536051945763 398 21.17536051945763 408 21.17536051945763 409 21.17536051945763
		 435 21.17536051945763 436 22.858769044427323 447 22.858769044427323 448 22.858769044427323
		 471 22.858769044427323 472 23.752504898514328 484 23.752504898514328 485 23.752504898514328
		 509 23.752504898514328 510 25.59822931841774 515 25.59822931841774 516 25.59822931841774
		 526 25.59822931841774 527 25.59822931841774 535 25.59822931841774 536 28.297649908437855
		 544 28.297649908437855 545 28.297649908437855 555 28.297649908437855 556 22.34098112118869
		 560 22.34098112118869 561 22.34098112118869 567 22.34098112118869 568 20.58290625516748
		 580 20.58290625516748 581 20.58290625516748 596 20.58290625516748 597 23.773917629902058
		 613 23.773917629902058 614 23.773917629902058 624 23.773917629902058 625 25.697753547481103
		 630 25.697753547481103 631 25.697753547481103 636 25.697753547481103 637 25.697753547481103
		 640 23.968341353590844 642 23.968341353590844 643 23.968341353590844 647 23.968341353590844
		 648 23.968341353590844 653 20.070879203586848 656 20.070879203586848 657 20.070879203586848
		 666 20.070879203586848 667 22.034840711239596 681 22.034840711239596 684 22.034840711239596
		 687 23.752504898514328 690 23.752504898514328 697 23.752504898514328 707 23.752504898514328
		 711 23.752504898514328 716 23.752504898514328 720 23.752504898514328 725 23.752504898514328
		 729 23.752504898514328 734 23.752504898514328 740 23.752504898514328 752 23.752504898514328
		 759 29.227269126709785 765 29.227269126709785 772 29.293108183032309 779 29.293108183032309
		 786 26.499606743855495 793 26.499606743855495 800 20.238345839176343 806 20.238345839176343
		 813 19.88320383536642 819 19.88320383536642 826 23.348731291334524 833 23.348731291334524
		 841 29.884887364196988 850 29.884887364196988 858 36.307994189565783 864 36.307994189565783
		 871 40.019131745226794 879 40.019131745226794 888 48.095198643097028;
	setAttr -s 101 ".kit[0:100]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 3 
		18 18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 1 18 3 3 3 1 3 1 18 
		3 3 1 3 1 3 1 18;
	setAttr -s 101 ".kot[0:100]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
	setAttr -s 101 ".kix[84:100]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 101 ".kiy[84:100]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_foot_ctrl_translateY";
	rename -uid "7455F3F3-4485-9B74-BCAF-00AAEEFD40EE";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 101 ".ktv[0:100]"  1 0 7 0 13 0 17 0 21 0 103 0 217 0 275 0
		 295 0 296 0 322 0 323 0 346 0 347 0 368 -2.2204460492503131e-16 369 -2.2204460492503131e-16
		 388 -2.2204460492503131e-16 389 -2.2204460492503131e-16 397 -2.2204460492503131e-16
		 398 -2.2204460492503131e-16 408 -2.2204460492503131e-16 409 -2.2204460492503131e-16
		 435 -2.2204460492503131e-16 436 -2.2204460492503131e-16 447 -2.2204460492503131e-16
		 448 -2.2204460492503131e-16 471 -2.2204460492503131e-16 472 -2.2204460492503131e-16
		 484 0 485 0 509 -2.2100721253082149e-16 510 -2.2204460492503131e-16 515 -2.2204460492503131e-16
		 516 -2.2204460492503131e-16 526 -2.2204460492503131e-16 527 -2.2204460492503131e-16
		 535 -2.2204460492503131e-16 536 -2.2204460492503131e-16 544 -2.2204460492503131e-16
		 545 -2.2204460492503131e-16 555 -2.2204460492503131e-16 556 -2.2204460492503131e-16
		 560 -7.3274719625260332e-15 561 -7.3274719625260332e-15 567 0 568 0 580 0 581 0 596 0
		 597 -1.0013021145949106e-15 613 -1.0013021145949106e-15 614 -1.0013021145949106e-15
		 624 -1.0013021145949106e-15 625 -1.0013021145949106e-15 630 -1.0013021145949106e-15
		 631 -1.0013021145949106e-15 636 -1.0013021145949106e-15 637 -1.0013021145949106e-15
		 640 0.12764575472859244 642 0.12764575472859244 643 0.12764575472859244 647 0.12764575472859244
		 648 0.12764575472859244 653 0.12764575472859233 656 0.12764575472859221 657 0.12764575472859221
		 666 0.12764575472859221 667 0 681 0 684 0 687 -2.0374050050926899e-16 690 -2.0374050050926899e-16
		 697 -2.0374050050926899e-16 707 -2.0374050050926899e-16 711 -2.0374050050926899e-16
		 716 -2.0374050050926899e-16 720 -2.0374050050926899e-16 725 -2.0374050050926899e-16
		 729 -2.0374050050926899e-16 734 -2.2204460492503131e-16 740 -2.2204460492503131e-16
		 752 -2.2204460492503131e-16 759 -2.2204460492503131e-16 765 -6.6613381477509392e-16
		 772 -6.6613381477509392e-16 779 -6.6613381477509392e-16 786 -6.6613381477509392e-16
		 793 -6.6613381477509392e-16 800 -6.6613381477509392e-16 806 -6.6613381477509392e-16
		 813 -6.6613381477509392e-16 819 -6.6613381477509392e-16 826 -7.2238936005031979e-15
		 833 -7.2238936005031979e-15 841 -7.6679828103532605e-15 850 -7.6679828103532605e-15
		 858 -8.0010497177408075e-15 864 -8.0010497177408075e-15 871 -2.2204460492503131e-16
		 879 -2.2204460492503131e-16 888 0;
	setAttr -s 101 ".kit[0:100]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 3 
		18 18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 1 18 3 3 3 1 3 1 18 
		3 3 1 3 1 3 1 3;
	setAttr -s 101 ".kot[0:100]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
	setAttr -s 101 ".kix[84:100]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 101 ".kiy[84:100]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_foot_ctrl_translateZ";
	rename -uid "27CAF55E-45AC-5058-430C-48A1EEF6896E";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 101 ".ktv[0:100]"  1 100.79931035246381 7 100.79931035246381
		 13 100.79931035246381 17 96.781351590635069 21 96.781351590635069 103 96.781351590635069
		 217 96.781351590635069 275 96.781351590635069 295 96.781351590635069 296 88.936550124529234
		 322 88.936550124529234 323 92.757606263121488 346 92.757606263121488 347 92.757606263121488
		 368 92.757606263121488 369 96.228668314033385 388 96.228668314033385 389 96.228668314033385
		 397 96.228668314033385 398 96.228668314033385 408 96.228668314033385 409 96.228668314033385
		 435 96.228668314033385 436 93.035373199575716 447 93.035373199575716 448 93.035373199575716
		 471 93.035373199575716 472 91.34002575768065 484 91.34002575768065 485 91.34002575768065
		 509 91.34002575768065 510 99.218445842316925 515 99.218445842316925 516 99.218445842316925
		 526 99.218445842316925 527 99.218445842316925 535 99.218445842316925 536 94.097854818737005
		 544 94.097854818737005 545 94.097854818737005 555 94.097854818737005 556 85.761326891798547
		 560 85.761326891798547 561 85.761326891798547 567 85.761326891798547 568 77.827610639428258
		 580 77.827610639428258 581 77.827610639428258 596 77.827610639428258 597 92.847658198755425
		 613 92.847658198755425 614 92.847658198755425 624 92.847658198755425 625 89.198291009805303
		 630 89.198291009805303 631 89.198291009805303 636 89.198291009805303 637 89.198291009805303
		 640 84.449198189439628 642 84.449198189439628 643 84.449198189439628 647 84.449198189439628
		 648 84.449198189439628 653 88.429826540704127 656 88.429826540704127 657 88.429826540704127
		 666 88.429826540704127 667 94.598301200714971 681 94.598301200714971 684 94.598301200714971
		 687 91.34002575768065 690 91.34002575768065 697 91.34002575768065 707 91.34002575768065
		 711 91.34002575768065 716 91.34002575768065 720 91.34002575768065 725 91.34002575768065
		 729 91.34002575768065 734 91.34002575768065 740 91.34002575768065 752 91.34002575768065
		 759 89.025658652841457 765 89.025658652841457 772 80.721365353419316 779 80.721365353419316
		 786 72.516991546448693 793 72.516991546448693 800 51.749837929087505 806 51.749837929087505
		 813 35.198464535459422 819 35.198464535459422 826 17.983534760975367 833 17.983534760975367
		 841 2.4844592312310669 850 2.4844592312310669 858 -17.969099878755415 864 -17.969099878755415
		 871 -33.216077536243709 879 -33.216077536243709 888 -47.95687937208136;
	setAttr -s 101 ".kit[0:100]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 3 
		18 18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 1 18 3 3 3 1 3 1 18 
		3 3 1 3 1 3 1 18;
	setAttr -s 101 ".kot[0:100]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
	setAttr -s 101 ".kix[84:100]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 101 ".kiy[84:100]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_knee_ctrl_translateX";
	rename -uid "CE55F4A8-4389-57A1-5E68-5A8946C4DF33";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 70 ".ktv[0:69]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 2.9774359144351759
		 580 2.9774359144351759 581 2.9774359144351759 596 2.9774359144351759 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 -8.4713312799787701
		 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0
		 729 0 734 0 740 0 752 0 759 0 765 0 772 0 779 0 786 -12.54156568856461 806 9.5190692960550241
		 813 9.5190692960550241 819 0 826 0 841 0 850 -1.8145479684504409 864 0 879 0.49945382072259292
		 888 0;
	setAttr -s 70 ".kit[0:69]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 
		18 3 18 3 18 18 3 18 18 3 18;
	setAttr -s 70 ".kot[0:69]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 70 ".kix[58:69]"  1 1 1 1 1 1 1 1 1 0.38546439020748335 1 
		1;
	setAttr -s 70 ".kiy[58:69]"  0 0 0 0 0 0 0 0 0 0.92272271234752479 0 
		0;
createNode animCurveTL -n "R_knee_ctrl_translateY";
	rename -uid "2CA429FD-4B73-19B5-A3E4-0495751DEA5E";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 70 ".ktv[0:69]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 -0.15810009874484091
		 580 -0.15810009874484091 581 -0.15810009874484091 596 -0.15810009874484091 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 -1.766761983575128
		 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0
		 729 0 734 0 740 0 752 0 759 0 765 0 772 0 779 0 786 -3.7034773742567992 806 -6.574404436359103
		 813 -6.574404436359103 819 0 826 0 841 0 850 0 864 0 879 8.8817841970012523e-16 888 0;
	setAttr -s 70 ".kit[0:69]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 
		18 3 18 3 18 18 3 18 18 3 18;
	setAttr -s 70 ".kot[0:69]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 70 ".kix[58:69]"  1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 70 ".kiy[58:69]"  0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTL -n "R_knee_ctrl_translateZ";
	rename -uid "0DF5783B-417B-177E-E0D3-50A2DD64A0A3";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 70 ".ktv[0:69]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 14.701807583147851
		 580 14.701807583147851 581 14.701807583147851 596 14.701807583147851 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 3.9253567693541274
		 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0
		 729 0 734 0 740 0 752 0 759 0 765 0 772 0 779 0 786 11.089833567163762 806 3.9879822606848836
		 813 3.9879822606848836 819 0 826 0 841 0 850 8.3673016443519952 864 0 879 6.8327243440643253
		 888 0;
	setAttr -s 70 ".kit[0:69]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 
		18 3 18 3 18 18 3 18 18 3 18;
	setAttr -s 70 ".kot[0:69]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 70 ".kix[58:69]"  1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 70 ".kiy[58:69]"  0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_COG_ctrl_visibility";
	rename -uid "1D20AB62-4F6F-19A9-4DC2-DA823A778D43";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 113 ".ktv[0:112]"  1 1 7 1 13 1 17 1 21 1 102 1 103 1 201 1
		 203 1 216 1 217 1 253 1 254 1 274 1 275 1 280 1 286 1 287 1 295 1 296 1 311 1 322 1
		 323 1 346 1 347 1 368 1 369 1 388 1 389 1 397 1 398 1 408 1 409 1 435 1 436 1 447 1
		 448 1 471 1 472 1 484 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1
		 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1
		 625 1 630 1 631 1 636 1 637 1 640 1 642 1 643 1 647 1 648 1 653 1 656 1 657 1 666 1
		 667 1 681 1 684 1 687 1 690 1 697 1 703 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 772 1 779 1 786 1 793 1 800 1 806 1 813 1 819 1 826 1 833 1
		 841 1 850 1 858 1 864 1 871 1 879 1 888 1;
	setAttr -s 113 ".kit[0:112]"  18 18 18 18 18 9 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 9 18 18 18 18 9 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 9 18 18 9 9 18 9 18 
		9 9 18;
	setAttr -s 113 ".kot[0:112]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "M_COG_ctrl_rotateX";
	rename -uid "E817347B-4D75-6401-CD31-7CAB040763A1";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 116 ".ktv[0:115]"  1 -1.5024644233087925 7 -1.5024644233087925
		 13 -1.7587577312214271 17 -1.9143979013553991 21 -1.9639084357976775 93 -2.0138478745147674
		 102 -2.0138478745147674 103 -2.0138478745147674 201 -2.0138478745147674 203 34.060956785651626
		 216 34.060956785651626 217 12.516695806194338 253 12.516695806194338 254 6.6791228006422365
		 274 6.6791228006422365 275 -3.9445328733368039 280 -3.9445328733368039 281 -15.636156333040738
		 286 -15.636156333040738 287 1.8273205885292514 295 1.8273205885292514 296 9.3304031368163258
		 311 9.3304031368163258 312 4.3875699611411898 322 4.3875699611411898 323 2.3718097820005379
		 346 2.3718097820005379 347 20.651994584253167 368 20.651994584253167 369 21.048683499658022
		 388 20.651994584253167 389 20.651994584253167 397 20.651994584253167 398 16.123079783487739
		 408 16.123079783487739 409 16.123079783487739 435 16.123079783487739 436 20.651994584253167
		 447 20.651994584253167 448 19.285623129750775 471 19.285623129750775 472 13.381774499477649
		 484 13.381774499477649 485 4.9058776234120414 509 4.9058776234120414 510 5.1331025342728456
		 515 5.1331025342728456 516 5.6300439151348423 526 5.6300439151348423 527 5.70135577386149
		 535 5.70135577386149 536 5.70135577386149 544 5.70135577386149 545 5.70135577386149
		 555 5.70135577386149 556 5.2051258862155096 560 5.2051258862155096 561 4.1774532863083351
		 567 4.1774532863083351 568 -20.56312366888174 580 -20.56312366888174 581 -12.836079909962708
		 596 -12.836079909962708 597 -14.725517825063669 613 -14.725517825063669 614 7.0236598874841976
		 624 7.0236598874841976 625 7.8474005512381551 630 7.8474005512381551 631 -12.619918843024747
		 636 -12.619918843024747 637 -12.529168205814125 640 -12.529168205814125 642 -12.529168205814125
		 643 -11.95106356916807 647 -11.95106356916807 648 -1.8226534595028525 653 -2.2144944219055556
		 656 -2.2144944219055556 657 4.9058776234120414 666 4.9058776234120414 667 7.3175617928889292
		 681 7.3175617928889292 684 -6.5688235101977828 687 -6.5688235101977828 690 -6.5688235101977828
		 697 -1.242933941750596 703 -1.242933941750596 707 -6.5562371376801023 711 -6.5688235101977828
		 716 -6.5161307582628991 720 -6.5688235101977828 725 -11.290408897264868 729 -6.5688235101977828
		 734 -10.177903869256282 740 -21.723865459320884 752 -16.907891773296598 759 -29.397399121842398
		 765 -52.262735917220404 772 -34.320417944282575 779 -18.408296158386069 786 -17.683843987525862
		 793 -16.683015728145651 800 -15.799250508661409 806 -15.152951210016008 813 -9.0789566854686932
		 819 -4.1798719565065481 826 -9.7779590756704344 833 -18.56260494694444 841 10.338704879813491
		 850 11.550831271589445 858 7.3688556875785975 864 3.1637048942834518 871 5.0509821656491463
		 879 4.9797156166419567 888 4.7322721121534697;
	setAttr -s 116 ".kit[0:115]"  18 18 18 18 18 3 3 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 3 18 18 18 
		18 3 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 3 18 18 3 3 
		3 3 18 3 3 3;
	setAttr -s 116 ".kot[0:115]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "M_COG_ctrl_rotateY";
	rename -uid "B6DA7F13-4D0D-202B-979C-02BBF79A231E";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 116 ".ktv[0:115]"  1 0 7 0 13 -31.316167643573866 17 -37.82521211938365
		 21 -39.933122146593639 93 -36.108880220774054 102 -36.108880220774054 103 -36.108880220774054
		 201 -36.108880220774054 203 -24.081419886053563 216 -24.081419886053563 217 -39.933122146593639
		 253 -39.933122146593639 254 -39.641363601321977 274 -39.641363601321977 275 -39.933122146593639
		 280 -39.933122146593639 281 -30.781722983496707 286 -30.781722983496707 287 -9.1677999610206964
		 295 -9.1677999610206964 296 -15.83742435216892 311 -15.83742435216892 312 -31.446107279877463
		 322 -31.446107279877463 323 -13.749504084546315 346 -13.749504084546315 347 -13.076301034787614
		 368 -13.076301034787614 369 -7.380578169893413 388 -13.076301034787614 389 -13.076301034787614
		 397 -13.076301034787614 398 -10.887082740337464 408 -10.887082740337464 409 -10.887082740337464
		 435 -10.887082740337464 436 -13.076301034787614 447 -13.076301034787614 448 -18.277107704430861
		 471 -18.277107704430861 472 -11.720186020991138 484 -11.720186020991138 485 -6.0121275285947036
		 509 -6.0121275285947036 510 18.086892419460177 515 18.086892419460177 516 29.897578287219012
		 526 29.897578287219012 527 31.115816816994226 535 31.115816816994226 536 31.115816816994226
		 544 31.115816816994226 545 31.115816816994226 555 31.115816816994226 556 20.366576349094675
		 560 20.366576349094675 561 12.373546172921236 567 12.373546172921236 568 -4.5608426199807486
		 580 -4.5608426199807486 581 -4.139378769165865 596 -4.139378769165865 597 22.423746221235596
		 613 22.423746221235596 614 58.956685631056189 624 58.956685631056189 625 62.493973802456075
		 630 62.493973802456075 631 30.169411634862193 636 30.169411634862193 637 55.538236045813321
		 640 55.538236045813321 642 55.538236045813321 643 35.145535552352797 647 35.145535552352797
		 648 5.3269777963046625 653 11.372869565728278 656 11.372869565728278 657 -6.0121275285947036
		 666 -6.0121275285947036 667 -3.5642261106719677 681 -3.5642261106719677 684 0.26926560235487318
		 687 0.26926560235487318 690 0.26926560235487318 697 -0.37842866588524454 703 -0.37842866588524454
		 707 -0.48828727226088364 711 0.26926560235487318 716 0.87462518589332916 720 0.26926560235487318
		 725 -0.18968399785858614 729 0.26926560235487318 734 -1.2266096748327815 740 -6.0121275285947036
		 752 7.979997733380519 759 38.417605194663544 765 62.614577959780839 772 73.092046900386279
		 779 82.383976917767569 786 86.756362239204933 793 92.796797987223584 800 98.130707098891634
		 806 102.03140565763469 813 113.0854465789635 819 122.00127336348653 826 133.06554808142053
		 833 142.97287387843278 841 148.99781832861888 850 154.30960466111355 858 158.20182348847797
		 864 162.26805604370773 871 181.04859294836004 879 163.94744600081137 888 179.86888803885819;
	setAttr -s 116 ".kit[0:115]"  18 18 18 18 18 3 3 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 3 18 18 18 
		18 3 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 3 18 18 3 3 
		3 3 18 3 3 3;
	setAttr -s 116 ".kot[0:115]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "M_COG_ctrl_rotateZ";
	rename -uid "E37D6C24-4F92-C59C-E2EB-5BA4CC0E1499";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 116 ".ktv[0:115]"  1 0 7 0 13 0.91434186927811856 17 1.176975563981953
		 21 1.2613948688070091 93 2.0038302656120175 102 2.0038302656120175 103 2.0038302656120175
		 201 2.0038302656120175 203 -8.2942848401421827 216 -8.2942848401421827 217 1.2613948688070091
		 253 1.2613948688070091 254 5.806882819339731 274 5.806882819339731 275 1.2613948688070091
		 280 1.2613948688070091 281 0.21983937776364221 286 0.21983937776364221 287 -13.729446809752437
		 295 -13.729446809752437 296 -14.077005565063146 311 -14.077005565063146 312 -15.665613518469355
		 322 -15.665613518469355 323 -11.790726789091128 346 -11.790726789091128 347 -10.186326659828492
		 368 -10.186326659828492 369 -9.0068150507880897 388 -10.186326659828492 389 -10.186326659828492
		 397 -10.186326659828492 398 -6.119135779753905 408 -6.119135779753905 409 -6.119135779753905
		 435 -6.119135779753905 436 -10.186326659828492 447 -10.186326659828492 448 -4.4917612632942951
		 471 -4.4917612632942951 472 -0.77565738330072442 484 -0.77565738330072442 485 -1.460806628106943
		 509 -1.460806628106943 510 0.65175646486379879 515 0.65175646486379879 516 1.8673815927929454
		 526 1.8673815927929454 527 2.0077366625205437 535 2.0077366625205437 536 2.0077366625205437
		 544 2.0077366625205437 545 2.0077366625205437 555 2.0077366625205437 556 0.87021123185442151
		 560 0.87021123185442151 561 0.24942473470889556 567 0.24942473470889556 568 7.1107506537290082
		 580 7.1107506537290082 581 5.3049512985604244 596 5.3049512985604244 597 -6.7150998616782616
		 613 -6.7150998616782616 614 3.7200656869110151 624 3.7200656869110151 625 4.6639582088601239
		 630 4.6639582088601239 631 4.0119348401198121 636 4.0119348401198121 637 6.4342927986300396
		 640 6.4342927986300396 642 6.4342927986300396 643 0.45981239960868769 647 0.45981239960868769
		 648 3.4864117981159772 653 -0.64953714105012395 656 -0.64953714105012395 657 -1.460806628106943
		 666 -1.460806628106943 667 -0.99160882394952676 681 -0.99160882394952676 684 2.8110231201022469
		 687 2.8110231201022469 690 2.8110231201022469 697 1.81276363590826 703 1.81276363590826
		 707 -3.7667597587568227 711 2.8110231201022469 716 8.0859109375996372 720 2.8110231201022469
		 725 -1.1720941155210005 729 2.8110231201022469 734 1.7937130464474733 740 -1.460806628106943
		 752 -12.99737802850365 759 -30.427764250783291 765 -57.244831507830298 772 -51.521117968072865
		 779 -46.445049941035933 786 -44.056469171476827 793 -40.756652799681845 800 -37.842803282607221
		 806 -35.711899216997359 813 -29.050433849089789 819 -23.677514409578393 826 -8.2478641976338132
		 833 -5.9818482540335625 841 14.859910111528814 850 8.3407265672747624 858 1.9362293478192834
		 864 -8.2611270295309644 871 -14.5673388160044 879 -0.913492359313129 888 -2.7450085255587622;
	setAttr -s 116 ".kit[0:115]"  18 18 18 18 18 3 3 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 3 18 18 18 
		18 3 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 3 18 18 3 3 
		3 3 18 3 3 3;
	setAttr -s 116 ".kot[0:115]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "M_COG_ctrl_scaleX";
	rename -uid "CC9DF7FF-4E9C-D5AA-6E0B-70A31F6E965A";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 113 ".ktv[0:112]"  1 1 7 1 13 1 17 1 21 1 102 1 103 1 201 1
		 203 1 216 1 217 1 253 1 254 1 274 1 275 1 280 1 286 1 287 1 295 1 296 1 311 1 322 1
		 323 1 346 1 347 1 368 1 369 1 388 1 389 1 397 1 398 1 408 1 409 1 435 1 436 1 447 1
		 448 1 471 1 472 1 484 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1
		 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1
		 625 1 630 1 631 1 636 1 637 1 640 1 642 1 643 1 647 1 648 1 653 1 656 1 657 1 666 1
		 667 1 681 1 684 1 687 1 690 1 697 1 703 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 772 1 779 1 786 1 793 1 800 1 806 1 813 1 819 1 826 1 833 1
		 841 1 850 1 858 1 864 1 871 1 879 1 888 1;
	setAttr -s 113 ".kit[0:112]"  18 18 18 18 18 3 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 18 3 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 3 3 18 3 18 
		3 3 18;
	setAttr -s 113 ".kot[0:112]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "M_COG_ctrl_scaleY";
	rename -uid "98F42F56-46A8-8A15-E1ED-E8874BC584EF";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 113 ".ktv[0:112]"  1 1 7 1 13 1 17 1 21 1 102 1 103 1 201 1
		 203 1 216 1 217 1 253 1 254 1 274 1 275 1 280 1 286 1 287 1 295 1 296 1 311 1 322 1
		 323 1 346 1 347 1 368 1 369 1 388 1 389 1 397 1 398 1 408 1 409 1 435 1 436 1 447 1
		 448 1 471 1 472 1 484 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1
		 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1
		 625 1 630 1 631 1 636 1 637 1 640 1 642 1 643 1 647 1 648 1 653 1 656 1 657 1 666 1
		 667 1 681 1 684 1 687 1 690 1 697 1 703 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 772 1 779 1 786 1 793 1 800 1 806 1 813 1 819 1 826 1 833 1
		 841 1 850 1 858 1 864 1 871 1 879 1 888 1;
	setAttr -s 113 ".kit[0:112]"  18 18 18 18 18 3 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 18 3 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 3 3 18 3 18 
		3 3 18;
	setAttr -s 113 ".kot[0:112]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "M_COG_ctrl_scaleZ";
	rename -uid "DD85441B-4DDA-D3C0-DDE7-779CED024014";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 113 ".ktv[0:112]"  1 1 7 1 13 1 17 1 21 1 102 1 103 1 201 1
		 203 1 216 1 217 1 253 1 254 1 274 1 275 1 280 1 286 1 287 1 295 1 296 1 311 1 322 1
		 323 1 346 1 347 1 368 1 369 1 388 1 389 1 397 1 398 1 408 1 409 1 435 1 436 1 447 1
		 448 1 471 1 472 1 484 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1
		 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1
		 625 1 630 1 631 1 636 1 637 1 640 1 642 1 643 1 647 1 648 1 653 1 656 1 657 1 666 1
		 667 1 681 1 684 1 687 1 690 1 697 1 703 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 772 1 779 1 786 1 793 1 800 1 806 1 813 1 819 1 826 1 833 1
		 841 1 850 1 858 1 864 1 871 1 879 1 888 1;
	setAttr -s 113 ".kit[0:112]"  18 18 18 18 18 3 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 18 3 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 3 3 18 3 18 
		3 3 18;
	setAttr -s 113 ".kot[0:112]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "M_Head_ctrl_visibility";
	rename -uid "93167514-478D-38AF-D649-B0B251FA6EC5";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 102 ".ktv[0:101]"  1 1 7 1 13 1 17 1 21 1 89 1 201 1 203 1
		 204 1 216 1 217 1 253 1 254 1 274 1 275 1 280 1 286 1 295 1 311 1 322 1 346 1 347 1
		 388 1 389 1 397 1 398 1 408 1 409 1 423 1 424 1 435 1 436 1 447 1 471 1 472 1 484 1
		 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1
		 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1
		 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1
		 707 1 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 772 1 779 1 786 1
		 793 1 800 1 806 1 813 1 819 1 826 1 833 1 841 1 850 1 864 1;
	setAttr -s 102 ".kit[0:101]"  18 18 18 9 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 1 18 18 18 18 18 18 18 18 18 18 18 9 18 9 9 
		9 18 9 18 18 9 9 18 18;
	setAttr -s 102 ".kix[77:101]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 102 ".kiy[77:101]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0;
createNode animCurveTA -n "M_Head_ctrl_rotateX";
	rename -uid "0FFAF0D6-47B6-8EA9-230A-D69714F62A30";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 113 ".ktv[0:112]"  1 2.9186075878817848 7 5.2266017074504552
		 13 14.01812536433015 17 18.135799168343745 21 19.239994732791171 89 19.239994732791171
		 93 11.856268508286931 201 11.856268508286931 202 6.6254260010358061 203 7.8827755970774458
		 204 7.8827755970774458 216 7.8827755970774458 217 26.981350195304895 253 26.981350195304895
		 254 20.862451099856152 274 20.862451099856152 275 22.411242084479113 280 22.411242084479113
		 281 -11.478008991070341 286 -11.478008991070341 287 -16.101400469109837 295 -16.101400469109837
		 296 -7.8204651398050693 311 -7.8204651398050693 312 -10.278677071351629 322 -10.278677071351629
		 323 9.929756548051996 346 9.929756548051996 347 30.936766909837857 388 30.936766909837857
		 389 26.095509571641983 397 26.095509571641983 398 21.44098480111883 408 21.44098480111883
		 409 21.44098480111883 423 21.44098480111883 424 13.853080152977324 435 13.853080152977324
		 436 30.936766909837857 447 30.936766909837857 448 29.385740025544063 471 29.385740025544063
		 472 3.8205526721285099 484 3.8205526721285099 485 -13.217702990815674 509 -13.217702990815674
		 510 3.0312274596406277 515 3.0312274596406277 516 20.825645316340424 526 20.825645316340424
		 527 -1.2048849474570349 535 -1.2048849474570349 536 -10.278919644538361 544 -10.278919644538361
		 545 -2.7631217451912566 555 -2.7631217451912566 556 -2.2250053582465092 560 -2.2250053582465092
		 561 -10.792612420134164 567 -10.792612420134164 568 -47.184627104075403 580 -47.184627104075403
		 581 -35.995943608145275 596 -35.995943608145275 597 -29.919855271537912 613 -29.919855271537912
		 614 -18.395504440018179 624 -18.395504440018179 625 0.21244136951716361 630 0.21244136951716361
		 631 -39.608028827898686 636 -39.608028827898686 637 -18.139092082162776 642 -18.139092082162776
		 643 -29.256967554818079 647 -29.256967554818079 648 -17.498324065679562 653 -13.784832336602687
		 656 -13.784832336602687 657 -8.8992051024606074 666 -8.8992051024606074 667 4.7657075790487617
		 681 12.556790192914937 684 -17.884363892837836 687 -17.884363892837836 690 -17.884363892837836
		 697 -2.9675758815208209 707 -19.888101277026376 711 -17.884363892837836 716 -16.078293524781781
		 720 -17.884363892837836 725 -23.850352523956293 729 -17.884363892837836 734 -25.929963572933012
		 740 -39.087345195989727 752 -40.258548103660168 759 -70.043037534178325 765 -78.833105408933449
		 772 -69.154624801023317 779 -64.578578603145374 786 -65.174589542784759 793 -51.286484534061643
		 800 -46.488347857779331 806 -32.876824976614955 813 -17.16348150699665 819 -7.2793787259553353
		 826 -45.518924054007179 833 -28.772399944936016 841 -11.105837085964819 850 14.309475944514807
		 858 13.547519681935944 864 21.621670032093263 871 20.90563197179436;
	setAttr -s 113 ".kit[0:112]"  18 18 18 3 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 3 18 18 18 18 18 18 18 18 1 18 18 18 18 18 18 
		18 18 18 18 18 3 18 3 3 3 18 3 18 18 3 3 3 
		3 18 3;
	setAttr -s 113 ".kix[86:112]"  1 1 0.97630612150604001 1 0.91118349027184142 
		1 1 0.70384064882241737 0.98845119440065554 0.98845119440065565 0.54122396435869535 
		1 1 1 1 1 1 0.64616702326532327 1 1 1 1 1 1 1 1 1;
	setAttr -s 113 ".kiy[86:112]"  0 0 0.21639398584487804 0 -0.4120007852614177 
		0 0 -0.71035789646152225 -0.151539553542688 -0.151539553542688 -0.84087848135379095 
		0 0 0 0 0 0 0.76319602858271685 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "M_Head_ctrl_rotateY";
	rename -uid "C58B4DD2-4517-93D1-9316-9D81D0EB24D3";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 113 ".ktv[0:112]"  1 16.125908634663723 7 3.8606271971803765
		 13 -42.859817415805864 17 -48.344970321068097 21 -49.815869133500215 89 -49.815869133500215
		 93 32.487150594383266 201 32.487150594383266 202 28.887831074862525 203 27.100862517352525
		 204 27.100862517352525 216 27.100862517352525 217 17.353205432224023 253 17.353205432224023
		 254 25.929491461042986 274 25.929491461042986 275 30.320009217605072 280 30.320009217605072
		 281 23.450641833062146 286 23.450641833062146 287 9.927179047444973 295 9.927179047444973
		 296 -2.0046501936352192 311 -2.0046501936352192 312 21.065751250865905 322 21.065751250865905
		 323 18.907937890697504 346 18.907937890697504 347 5.7013230535648569 388 5.7013230535648569
		 389 19.397156856072087 397 19.397156856072087 398 22.037290004473103 408 22.037290004473103
		 409 22.037290004473103 423 22.037290004473103 424 23.434753331003186 435 23.434753331003186
		 436 5.7013230535648569 447 5.7013230535648569 448 -13.241446986911713 471 -13.241446986911713
		 472 -3.5878818051275005 484 -3.5878818051275005 485 15.779350431525504 509 15.779350431525504
		 510 40.042224974186283 515 40.042224974186283 516 41.403150626550087 526 41.403150626550087
		 527 5.8750480390874458 535 5.8750480390874458 536 50.009051260833232 544 50.009051260833232
		 545 4.7107105727816769 555 4.7107105727816769 556 19.048159296290699 560 19.048159296290699
		 561 11.493538978415595 567 11.493538978415595 568 -2.1750597967945158 580 -2.1750597967945158
		 581 0.70942080991434442 596 0.70942080991434442 597 11.523858152310105 613 11.523858152310105
		 614 48.495669421191472 624 48.495669421191472 625 42.469856451346807 630 42.469856451346807
		 631 51.074277553471973 636 51.074277553471973 637 37.080566436911148 642 37.080566436911148
		 643 59.455970148101265 647 59.455970148101265 648 35.988601298307977 653 35.252860111643869
		 656 35.252860111643869 657 18.826868596783925 666 18.826868596783925 667 14.380354923612952
		 681 15.924908064294634 684 18.790038437754792 687 18.790038437754792 690 18.790038437754792
		 697 21.904057829244532 707 16.644355239368089 711 18.790038437754792 716 20.341859285223762
		 720 18.790038437754792 725 17.51674812335272 729 18.790038437754792 734 14.83845511838175
		 740 8.376228334102926 752 26.173507754757036 759 43.418219272497112 765 58.029171899144778
		 772 64.528701584511495 779 67.601719892820029 786 69.586931869061161 793 74.116935089319853
		 800 78.881525285231888 806 84.214684850280378 813 94.210510792690144 819 105.831952603483
		 826 123.44569146396127 833 163.25575623765491 841 168.28088850347837 850 174.75418518327055
		 858 172.99366867567417 864 165.17349310461697 871 164.15734943209401;
	setAttr -s 113 ".kit[0:112]"  18 18 18 3 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 3 18 18 18 18 18 18 18 18 1 18 18 18 18 18 18 
		18 18 18 18 18 3 18 3 3 3 18 3 18 18 3 3 3 
		3 18 3;
	setAttr -s 113 ".kix[86:112]"  1 1 0.977636593891827 1 0.98676080719786075 
		1 1 0.8959642910549892 1 0.71934250892745066 0.6147354697050833 0.76184032874098717 
		1 0.98257236675727544 1 1 1 0.85089085775304962 1 0.6473194147124075 0.4221107998718861 
		1 1 1 1 0.97497471266531666 1;
	setAttr -s 113 ".kiy[86:112]"  0 0 0.210301427202924 0 -0.16218233374269342 
		0 0 -0.44412609600690051 0 0.6946555656222444 0.78873335309626069 0.64776485973215969 
		0 0.18588045643640511 0 0 0 0.52534250560208762 0 0.7622188500269369 0.90654424747582874 
		0 0 0 0 -0.22231578815545949 0;
createNode animCurveTA -n "M_Head_ctrl_rotateZ";
	rename -uid "DF9997BE-4F84-AFF6-FE80-78A297C37064";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 113 ".ktv[0:112]"  1 1.0346323370290141e-16 7 -0.76319093535681481
		 13 -3.6703147547365376 17 -6.6517080862643274 21 -7.4511986358404716 89 -7.4511986358404716
		 93 13.633752541922352 201 13.633752541922352 202 10.273060897530454 203 8.4294130369890965
		 204 8.4294130369890965 216 8.4294130369890965 217 15.462627208547108 253 15.462627208547108
		 254 20.198960588264903 274 20.198960588264903 275 14.270747209441232 280 14.270747209441232
		 281 -2.9353503006368276 286 -2.9353503006368276 287 -9.5610116132773229 295 -9.5610116132773229
		 296 -20.260799149229488 311 -20.260799149229488 312 -36.854238375737943 322 -36.854238375737943
		 323 -23.918893916815893 346 -23.918893916815893 347 -9.7062210829515543 388 -9.7062210829515543
		 389 11.073321102690743 397 11.073321102690743 398 15.525435201755487 408 15.525435201755487
		 409 15.525435201755487 423 15.525435201755487 424 14.014554226067148 435 14.014554226067148
		 436 -9.7062210829515543 447 -9.7062210829515543 448 3.7115388102100266 471 3.7115388102100266
		 472 12.960320165023825 484 12.960320165023825 485 8.7640592201967973 509 8.7640592201967973
		 510 12.37671757468445 515 12.37671757468445 516 3.9638539432767299 526 3.9638539432767299
		 527 -5.2130095605685538 535 -5.2130095605685538 536 -9.6712907968749775 544 -9.6712907968749775
		 545 -5.9803632510414833 555 -5.9803632510414833 556 -6.4317966718419406 560 -6.4317966718419406
		 561 -2.3389828365944552 567 -2.3389828365944552 568 -1.6191169747132657 580 -1.6191169747132657
		 581 -7.4085133466532724 596 -7.4085133466532724 597 1.9236716594932799 613 1.9236716594932799
		 614 -3.8561831446985178 624 -3.8561831446985178 625 -6.1472975040175468 630 -6.1472975040175468
		 631 -24.576867986761293 636 -24.576867986761293 637 2.6767339533454351 642 2.6767339533454351
		 643 -11.904905849779748 647 -11.904905849779748 648 -18.492125144093592 653 -22.537426953360523
		 656 -22.537426953360523 657 -23.309371487257092 666 -23.309371487257092 667 -8.8256256663061841
		 681 -8.1055670107678424 684 -2.2181241532431804 687 -2.2181241532431804 690 -2.2181241532431804
		 697 -6.6939445691072859 707 -8.7947432870627811 711 -2.2181241532431804 716 3.1708508613377644
		 720 -2.2181241532431804 725 -6.2193684023590778 729 -2.2181241532431804 734 -7.9006428322877627
		 740 -17.193556948473713 752 -49.678912220636697 759 -70.685705156671389 765 -96.607463937159977
		 772 -107.95672742775703 779 -113.32273022807422 786 -108.52719698265386 793 -97.584395254374755
		 800 -86.07491870539539 806 -73.191990180177285 813 -45.639544236893627 819 -13.606258631512954
		 826 -28.04764053435046 833 -5.2010971743442047 841 18.65622998380384 850 12.747168984386859
		 858 5.6171207475252505 864 -4.9636820494537588 871 -7.6707047646036317;
	setAttr -s 113 ".kit[0:112]"  18 18 18 3 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 3 18 18 18 18 18 18 18 18 1 18 18 18 18 18 18 
		18 18 18 18 18 3 18 3 3 3 18 3 18 18 3 3 3 
		3 18 3;
	setAttr -s 113 ".kix[86:112]"  1 1 0.82072129862646759 1 0.87758318135661317 
		1 1 0.81429415512569636 0.63539699488012602 0.56138577593229422 0.46764707099163649 
		0.55440370344895784 1 1 1 1 1 0.52325434662915815 1 1 1 1 1 1 1 0.88167176853677953 
		1;
	setAttr -s 113 ".kiy[86:112]"  0 0 0.57132875823021967 0 -0.47942440467711434 
		0 0 -0.58045243468188534 -0.77218563758807701 -0.82755423422328989 -0.88391527704466288 
		-0.83224787990242433 0 0 0 0 0 0.85217655960117367 0 0 0 0 0 0 0 -0.47186321382920632 
		0;
createNode animCurveTU -n "M_Head_ctrl_scaleX";
	rename -uid "EAF047EC-4250-2E62-3A22-90A22C6CD4AF";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 102 ".ktv[0:101]"  1 1.0000000000000004 7 1.0000000000000004
		 13 1.0000000000000004 17 1.0000000000000004 21 1.0000000000000004 89 1.0000000000000004
		 201 1.0000000000000004 203 1.0000000000000004 204 1.0000000000000004 216 1.0000000000000004
		 217 1.0000000000000004 253 1.0000000000000004 254 1.0000000000000004 274 1.0000000000000004
		 275 1.0000000000000004 280 1.0000000000000004 286 1.0000000000000004 295 1.0000000000000004
		 311 1.0000000000000004 322 1.0000000000000004 346 1.0000000000000004 347 1.0000000000000004
		 388 1.0000000000000004 389 1.0000000000000004 397 1.0000000000000004 398 1.0000000000000004
		 408 1.0000000000000004 409 1.0000000000000004 423 1.0000000000000004 424 1.0000000000000004
		 435 1.0000000000000004 436 1.0000000000000004 447 1.0000000000000004 471 1.0000000000000004
		 472 1.0000000000000004 484 1.0000000000000004 485 1.0000000000000004 509 1.0000000000000004
		 510 1.0000000000000004 515 1.0000000000000004 516 1.0000000000000004 526 1.0000000000000004
		 527 1.0000000000000004 535 1.0000000000000004 536 1.0000000000000004 544 1.0000000000000004
		 545 1.0000000000000004 555 1.0000000000000004 556 1.0000000000000004 560 1.0000000000000004
		 561 1.0000000000000004 567 1.0000000000000004 568 1.0000000000000004 580 1.0000000000000004
		 581 1.0000000000000004 596 1.0000000000000004 597 1.0000000000000004 613 1.0000000000000004
		 614 1.0000000000000004 624 1.0000000000000004 625 1.0000000000000004 630 1.0000000000000004
		 631 1.0000000000000004 636 1.0000000000000004 637 1.0000000000000004 642 1.0000000000000004
		 643 1.0000000000000004 647 1.0000000000000004 648 1.0000000000000004 656 1.0000000000000004
		 657 1.0000000000000004 666 1.0000000000000004 667 1.0000000000000004 681 1.0000000000000004
		 684 1.0000000000000004 687 1.0000000000000004 690 1.0000000000000004 697 1.0000000000000004
		 707 1.0000000000000004 711 1.0000000000000004 716 1.0000000000000004 720 1.0000000000000004
		 725 1.0000000000000004 729 1.0000000000000004 734 1.0000000000000004 740 1.0000000000000004
		 752 1.0000000000000004 759 1.0000000000000004 765 1.0000000000000004 772 1.0000000000000004
		 779 1.0000000000000004 786 1.0000000000000004 793 1.0000000000000004 800 1.0000000000000004
		 806 1.0000000000000004 813 1.0000000000000004 819 1.0000000000000004 826 1.0000000000000004
		 833 1.0000000000000004 841 1.0000000000000004 850 1.0000000000000004 864 1.0000000000000004;
	setAttr -s 102 ".kit[0:101]"  18 18 18 3 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 1 18 18 18 18 18 18 18 18 18 18 18 3 18 3 3 
		3 18 3 18 18 3 3 18 18;
	setAttr -s 102 ".kix[77:101]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 102 ".kiy[77:101]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0;
createNode animCurveTU -n "M_Head_ctrl_scaleY";
	rename -uid "193FF2DC-4D80-352B-A7CF-13B4DDA7BEA2";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 102 ".ktv[0:101]"  1 1.0000000000000002 7 1.0000000000000002
		 13 1.0000000000000002 17 1.0000000000000002 21 1.0000000000000002 89 1.0000000000000002
		 201 1.0000000000000002 203 1.0000000000000002 204 1.0000000000000002 216 1.0000000000000002
		 217 1.0000000000000002 253 1.0000000000000002 254 1.0000000000000002 274 1.0000000000000002
		 275 1.0000000000000002 280 1.0000000000000002 286 1.0000000000000002 295 1.0000000000000002
		 311 1.0000000000000002 322 1.0000000000000002 346 1.0000000000000002 347 1.0000000000000002
		 388 1.0000000000000002 389 1.0000000000000002 397 1.0000000000000002 398 1.0000000000000002
		 408 1.0000000000000002 409 1.0000000000000002 423 1.0000000000000002 424 1.0000000000000002
		 435 1.0000000000000002 436 1.0000000000000002 447 1.0000000000000002 471 1.0000000000000002
		 472 1.0000000000000002 484 1.0000000000000002 485 1.0000000000000002 509 1.0000000000000002
		 510 1.0000000000000002 515 1.0000000000000002 516 1.0000000000000002 526 1.0000000000000002
		 527 1.0000000000000002 535 1.0000000000000002 536 1.0000000000000002 544 1.0000000000000002
		 545 1.0000000000000002 555 1.0000000000000002 556 1.0000000000000002 560 1.0000000000000002
		 561 1.0000000000000002 567 1.0000000000000002 568 1.0000000000000002 580 1.0000000000000002
		 581 1.0000000000000002 596 1.0000000000000002 597 1.0000000000000002 613 1.0000000000000002
		 614 1.0000000000000002 624 1.0000000000000002 625 1.0000000000000002 630 1.0000000000000002
		 631 1.0000000000000002 636 1.0000000000000002 637 1.0000000000000002 642 1.0000000000000002
		 643 1.0000000000000002 647 1.0000000000000002 648 1.0000000000000002 656 1.0000000000000002
		 657 1.0000000000000002 666 1.0000000000000002 667 1.0000000000000002 681 1.0000000000000002
		 684 1.0000000000000002 687 1.0000000000000002 690 1.0000000000000002 697 1.0000000000000002
		 707 1.0000000000000002 711 1.0000000000000002 716 1.0000000000000002 720 1.0000000000000002
		 725 1.0000000000000002 729 1.0000000000000002 734 1.0000000000000002 740 1.0000000000000002
		 752 1.0000000000000002 759 1.0000000000000002 765 1.0000000000000002 772 1.0000000000000002
		 779 1.0000000000000002 786 1.0000000000000002 793 1.0000000000000002 800 1.0000000000000002
		 806 1.0000000000000002 813 1.0000000000000002 819 1.0000000000000002 826 1.0000000000000002
		 833 1.0000000000000002 841 1.0000000000000002 850 1.0000000000000002 864 1.0000000000000002;
	setAttr -s 102 ".kit[0:101]"  18 18 18 3 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 1 18 18 18 18 18 18 18 18 18 18 18 3 18 3 3 
		3 18 3 18 18 3 3 18 18;
	setAttr -s 102 ".kix[77:101]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 102 ".kiy[77:101]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0;
createNode animCurveTU -n "M_Head_ctrl_scaleZ";
	rename -uid "42E6E786-4B76-5792-35C0-D6A0A2A7F688";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 102 ".ktv[0:101]"  1 1.0000000000000007 7 1.0000000000000007
		 13 1.0000000000000007 17 1.0000000000000007 21 1.0000000000000007 89 1.0000000000000007
		 201 1.0000000000000007 203 1.0000000000000007 204 1.0000000000000007 216 1.0000000000000007
		 217 1.0000000000000007 253 1.0000000000000007 254 1.0000000000000007 274 1.0000000000000007
		 275 1.0000000000000007 280 1.0000000000000007 286 1.0000000000000007 295 1.0000000000000007
		 311 1.0000000000000007 322 1.0000000000000007 346 1.0000000000000007 347 1.0000000000000007
		 388 1.0000000000000007 389 1.0000000000000007 397 1.0000000000000007 398 1.0000000000000007
		 408 1.0000000000000007 409 1.0000000000000007 423 1.0000000000000007 424 1.0000000000000007
		 435 1.0000000000000007 436 1.0000000000000007 447 1.0000000000000007 471 1.0000000000000007
		 472 1.0000000000000007 484 1.0000000000000007 485 1.0000000000000007 509 1.0000000000000007
		 510 1.0000000000000007 515 1.0000000000000007 516 1.0000000000000007 526 1.0000000000000007
		 527 1.0000000000000007 535 1.0000000000000007 536 1.0000000000000007 544 1.0000000000000007
		 545 1.0000000000000007 555 1.0000000000000007 556 1.0000000000000007 560 1.0000000000000007
		 561 1.0000000000000007 567 1.0000000000000007 568 1.0000000000000007 580 1.0000000000000007
		 581 1.0000000000000007 596 1.0000000000000007 597 1.0000000000000007 613 1.0000000000000007
		 614 1.0000000000000007 624 1.0000000000000007 625 1.0000000000000007 630 1.0000000000000007
		 631 1.0000000000000007 636 1.0000000000000007 637 1.0000000000000007 642 1.0000000000000007
		 643 1.0000000000000007 647 1.0000000000000007 648 1.0000000000000007 656 1.0000000000000007
		 657 1.0000000000000007 666 1.0000000000000007 667 1.0000000000000007 681 1.0000000000000007
		 684 1.0000000000000007 687 1.0000000000000007 690 1.0000000000000007 697 1.0000000000000007
		 707 1.0000000000000007 711 1.0000000000000007 716 1.0000000000000007 720 1.0000000000000007
		 725 1.0000000000000007 729 1.0000000000000007 734 1.0000000000000007 740 1.0000000000000007
		 752 1.0000000000000007 759 1.0000000000000007 765 1.0000000000000007 772 1.0000000000000007
		 779 1.0000000000000007 786 1.0000000000000007 793 1.0000000000000007 800 1.0000000000000007
		 806 1.0000000000000007 813 1.0000000000000007 819 1.0000000000000007 826 1.0000000000000007
		 833 1.0000000000000007 841 1.0000000000000007 850 1.0000000000000007 864 1.0000000000000007;
	setAttr -s 102 ".kit[0:101]"  18 18 18 3 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 1 18 18 18 18 18 18 18 18 18 18 18 3 18 3 3 
		3 18 3 18 18 3 3 18 18;
	setAttr -s 102 ".kix[77:101]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 102 ".kiy[77:101]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0;
createNode animCurveTU -n "L_foot_ctrl_visibility";
	rename -uid "575E5281-46B3-6C17-A078-D988ABB785A5";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 1 7 1 13 1 17 1 21 1 103 1 217 1 275 1
		 286 1 311 1 312 1 322 1 323 1 346 1 347 1 368 1 369 1 388 1 389 1 397 1 398 1 408 1
		 409 1 435 1 436 1 447 1 448 1 471 1 472 1 484 1 485 1 509 1 510 1 515 1 516 1 526 1
		 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1
		 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1
		 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1
		 734 1 740 1 752 1 759 1 765 1 773 1 779 1 786 1 793 1 800 1 806 1 813 1 819 1 826 1
		 833 1 841 1 850 1 858 1 864 1 871 1 879 1 888 1;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 9 1 1 1 1 18 9 1 1 9 
		1 18 9 18 1 9 1;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[84:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[84:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_foot_ctrl_rotateX";
	rename -uid "B29EDC40-4278-15F7-8EF5-00BD061C4966";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0 7 0 13 0 17 0 21 0 103 0 217 0 275 0
		 286 0 311 0 312 0 322 0 323 0 346 0 347 0 368 0 369 0 388 0 389 0 397 0 398 0 408 0
		 409 0 435 0 436 0 447 0 448 0 471 0 472 0 484 0 485 -6.0655827772494906 509 -6.0655827772494906
		 510 -6.0655827772494906 515 -6.0655827772494906 516 -6.0655827772494906 526 -6.0655827772494906
		 527 -12.072072995383763 535 -12.072072995383763 536 -12.072072995383763 544 -12.072072995383763
		 545 -12.072072995383763 555 -12.072072995383763 556 -12.072072995383763 560 -12.072072995383763
		 561 -12.072072995383763 567 -12.072072995383763 568 -12.072072995383763 580 -12.072072995383763
		 581 -4.7685093765933999 596 -4.7685093765933999 597 -4.7685093765933999 613 -4.7685093765933999
		 614 -60.474993493509231 624 -60.474993493509231 625 -60.474993493509231 630 -60.474993493509231
		 631 -60.474993493509231 636 -60.474993493509231 637 -60.474993493509231 642 -60.474993493509231
		 643 -60.474993493509231 647 -60.474993493509231 648 -29.048118139998895 656 -29.048118139998895
		 657 -6.0655827772494906 666 -6.0655827772494906 667 -6.0655827772494906 681 -6.0655827772494906
		 684 -6.0655827772494906 687 -6.0655827772494906 690 -6.0655827772494906 697 -6.0655827772494906
		 707 -6.0655827772494906 711 -6.0655827772494906 716 -6.0655827772494906 720 -6.0655827772494906
		 725 -6.0655827772494906 729 -6.0655827772494906 734 -6.0655827772494906 740 -6.0655827772494906
		 752 -6.0655827772494906 759 -6.0655827772494906 765 -6.0655827772494906 773 -6.0655827772494906
		 779 -6.0655827772494906 786 -6.0655827772494906 793 -6.0655827772494906 800 -6.0655827772494906
		 806 -3.8481689312797767 813 -3.8481689312797767 819 -1.2000769125674178 826 -1.2000769125674178
		 833 -1.2000769125674178 841 -1.2000769125674178 850 -0.70286534440499215 858 -0.70286534440499215
		 864 0 871 0 879 0 888 0;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 3 1 1 1 1 18 3 1 1 3 
		1 18 3 18 1 3 1;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[84:99]"  1 1 1 1 1 1 0.9967941866463883 0.9967941866463883 
		1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[84:99]"  0 0 0 0 0 0 0.080008433730232489 0.080008433730232489 
		0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_foot_ctrl_rotateY";
	rename -uid "C296DA14-4282-A7C7-A14B-30A14D0893C2";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 9.1058319895499444 7 9.1058319895499444
		 13 9.1058319895499444 17 9.1058319895499444 21 -1.8535503065216681 103 -1.8535503065216681
		 217 -1.8535503065216681 275 -1.8535503065216681 286 -1.8535503065216681 311 -1.8535503065216681
		 312 -1.8535503065216681 322 -1.8535503065216681 323 -1.8535503065216681 346 -1.8535503065216681
		 347 -1.8535503065216681 368 -1.8535503065216681 369 -1.8535503065216681 388 -1.8535503065216681
		 389 -1.8535503065216681 397 -1.8535503065216681 398 -1.8535503065216681 408 -1.8535503065216681
		 409 -1.8535503065216681 435 -1.8535503065216681 436 -1.8535503065216681 447 -1.8535503065216681
		 448 -1.8535503065216681 471 -1.8535503065216681 472 -1.8535503065216681 484 -1.8535503065216681
		 485 60.466755901703557 509 60.466755901703557 510 60.466755901703557 515 60.466755901703557
		 516 60.466755901703557 526 60.466755901703557 527 75.232800987117088 535 75.232800987117088
		 536 75.232800987117088 544 75.232800987117088 545 75.232800987117088 555 75.232800987117088
		 556 75.232800987117088 560 75.232800987117088 561 75.232800987117088 567 75.232800987117088
		 568 75.232800987117088 580 75.232800987117088 581 50.113603577091354 596 50.113603577091354
		 597 50.113603577091354 613 50.113603577091354 614 86.487608336751961 624 86.487608336751961
		 625 86.487608336751961 630 86.487608336751961 631 86.487608336751961 636 86.487608336751961
		 637 86.487608336751961 642 86.487608336751961 643 86.487608336751961 647 86.487608336751961
		 648 71.457964102358972 656 71.457964102358972 657 60.466755901703557 666 60.466755901703557
		 667 60.466755901703557 681 60.466755901703557 684 60.466755901703557 687 60.466755901703557
		 690 60.466755901703557 697 60.466755901703557 707 60.466755901703557 711 60.466755901703557
		 716 60.466755901703557 720 60.466755901703557 725 60.466755901703557 729 60.466755901703557
		 734 60.466755901703557 740 60.466755901703557 752 60.466755901703557 759 60.466755901703557
		 765 60.466755901703557 773 60.466755901703557 779 60.466755901703557 786 60.466755901703557
		 793 60.466755901703557 800 60.466755901703557 806 106.20083112085938 813 106.20083112085938
		 819 153.38679093736013 826 153.38679093736013 833 153.38679093736013 841 153.38679093736013
		 850 171.07259652931648 858 171.07259652931648 864 185.5691672025722 871 185.5691672025722
		 879 159.90455533530192 888 159.90455533530192;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 3 1 1 1 1 18 3 1 1 3 
		1 18 3 18 1 3 1;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[84:99]"  1 1 1 1 1 1 0.51704570088148161 0.51704570088148161 
		1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[84:99]"  0 0 0 0 0 0 0.85595779288465945 0.85595779288465945 
		0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_foot_ctrl_rotateZ";
	rename -uid "5885E02D-4098-8AD5-471E-13A359B496B8";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0 7 0 13 0 17 0 21 0 103 0 217 0 275 0
		 286 0 311 0 312 0 322 0 323 0 346 0 347 0 368 0 369 0 388 0 389 0 397 0 398 0 408 0
		 409 0 435 0 436 0 447 0 448 0 471 0 472 0 484 0 485 -6.6832367316788055 509 -6.6832367316788055
		 510 -6.6832367316788055 515 -6.6832367316788055 516 -6.6832367316788055 526 -6.6832367316788055
		 527 -13.093756878724006 535 -13.093756878724006 536 -13.093756878724006 544 -13.093756878724006
		 545 -13.093756878724006 555 -13.093756878724006 556 -13.093756878724006 560 -13.093756878724006
		 561 -13.093756878724006 567 -13.093756878724006 568 -13.093756878724006 580 -13.093756878724006
		 581 -5.0717236468512885 596 -5.0717236468512885 597 -5.0717236468512885 613 -5.0717236468512885
		 614 -61.838064986097542 624 -61.838064986097542 625 -61.838064986097542 630 -61.838064986097542
		 631 -61.838064986097542 636 -61.838064986097542 637 -61.838064986097542 642 -61.838064986097542
		 643 -61.838064986097542 647 -61.838064986097542 648 -29.980636466330555 656 -29.980636466330555
		 657 -6.6832367316788055 666 -6.6832367316788055 667 -6.6832367316788055 681 -6.6832367316788055
		 684 -6.6832367316788055 687 -6.6832367316788055 690 -6.6832367316788055 697 -6.6832367316788055
		 707 -6.6832367316788055 711 -6.6832367316788055 716 -6.6832367316788055 720 -6.6832367316788055
		 725 -6.6832367316788055 729 -6.6832367316788055 734 -6.6832367316788055 740 -6.6832367316788055
		 752 -6.6832367316788055 759 -6.6832367316788055 765 -6.6832367316788055 773 -6.6832367316788055
		 779 -6.6832367316788055 786 -6.6832367316788055 793 -6.6832367316788055 800 -6.6832367316788055
		 806 -4.2400252202800557 813 -4.2400252202800557 819 -1.0818917909719965 826 -1.0818917909719965
		 833 -1.0818917909719965 841 -1.0818917909719965 850 -0.7744376195425694 858 -0.7744376195425694
		 864 0 871 0 879 0 888 0;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 3 1 1 1 1 18 3 1 1 3 
		1 18 3 18 1 3 1;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[84:99]"  1 1 1 1 1 1 0.99611205030144923 0.99611205030144923 
		1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[84:99]"  0 0 0 0 0 0 0.088095307731132347 0.088095307731132347 
		0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_foot_ctrl_scaleX";
	rename -uid "D15AE436-41D0-8E38-118B-A8B97E864E0F";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 1 7 1 13 1 17 1 21 1 103 1 217 1 275 1
		 286 1 311 1 312 1 322 1 323 1 346 1 347 1 368 1 369 1 388 1 389 1 397 1 398 1 408 1
		 409 1 435 1 436 1 447 1 448 1 471 1 472 1 484 1 485 1 509 1 510 1 515 1 516 1 526 1
		 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1
		 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1
		 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1
		 734 1 740 1 752 1 759 1 765 1 773 1 779 1 786 1 793 1 800 1 806 1 813 1 819 1 826 1
		 833 1 841 1 850 1 858 1 864 1 871 1 879 1 888 1;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 3 1 1 1 1 18 3 1 1 3 
		1 18 3 18 1 3 1;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[84:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[84:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_foot_ctrl_scaleY";
	rename -uid "CCA60D5C-4309-FFE7-0EF1-73B156F30BC2";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 1 7 1 13 1 17 1 21 1 103 1 217 1 275 1
		 286 1 311 1 312 1 322 1 323 1 346 1 347 1 368 1 369 1 388 1 389 1 397 1 398 1 408 1
		 409 1 435 1 436 1 447 1 448 1 471 1 472 1 484 1 485 1 509 1 510 1 515 1 516 1 526 1
		 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1
		 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1
		 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1
		 734 1 740 1 752 1 759 1 765 1 773 1 779 1 786 1 793 1 800 1 806 1 813 1 819 1 826 1
		 833 1 841 1 850 1 858 1 864 1 871 1 879 1 888 1;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 3 1 1 1 1 18 3 1 1 3 
		1 18 3 18 1 3 1;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[84:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[84:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_foot_ctrl_scaleZ";
	rename -uid "C02AB871-46DD-E1DD-11E6-17BC21F1A5A7";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 1 7 1 13 1 17 1 21 1 103 1 217 1 275 1
		 286 1 311 1 312 1 322 1 323 1 346 1 347 1 368 1 369 1 388 1 389 1 397 1 398 1 408 1
		 409 1 435 1 436 1 447 1 448 1 471 1 472 1 484 1 485 1 509 1 510 1 515 1 516 1 526 1
		 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1
		 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1
		 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1
		 734 1 740 1 752 1 759 1 765 1 773 1 779 1 786 1 793 1 800 1 806 1 813 1 819 1 826 1
		 833 1 841 1 850 1 858 1 864 1 871 1 879 1 888 1;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 3 1 1 1 1 18 3 1 1 3 
		1 18 3 18 1 3 1;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[84:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[84:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_foot_ctrl_toeTap";
	rename -uid "64C95D50-4C7C-4B81-A93C-0797B3DE0000";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0 7 0 13 0 17 0 21 0 103 0 217 0 275 0
		 286 0 311 0 312 0 322 0 323 0 346 0 347 0 368 0 369 0 388 0 389 0 397 0 398 0 408 0
		 409 0 435 0 436 0 447 0 448 0 471 0 472 0 484 0 485 0 509 0 510 0 515 0 516 0 526 0
		 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0
		 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0
		 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 773 0 779 0 786 0 793 0 800 0 806 0 813 0 819 0 826 0
		 833 0 841 0 850 0 858 0 864 0 871 0 879 0 888 0;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 3 1 1 1 1 18 3 1 1 3 
		1 18 3 18 1 3 1;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[84:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[84:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_foot_ctrl_footPeel";
	rename -uid "9849CBE0-41AC-A142-3023-58BC25509E3B";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0 7 0 13 0 17 0 21 0 103 0 217 0 275 0
		 286 0 311 0 312 0 322 0 323 0 346 0 347 0 368 0 369 0 388 0 389 0 397 0 398 0 408 0
		 409 0 435 0 436 0 447 0 448 0 471 0 472 0 484 0 485 0 509 0 510 0 515 0 516 0 526 0
		 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0
		 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0
		 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 773 0 779 0 786 0 793 0 800 0 806 0 813 0 819 0 826 0
		 833 0 841 0 850 0 858 0 864 0 871 0 879 0 888 0;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 3 1 1 1 1 18 3 1 1 3 
		1 18 3 18 1 3 1;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[84:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[84:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_foot_ctrl_footSwivel";
	rename -uid "C40F9280-4391-7B46-592F-4B9F437AC332";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0 7 0 13 0 17 0 21 0 103 0 217 0 275 0
		 286 0 311 0 312 0 322 0 323 0 346 0 347 0 368 0 369 0 388 0 389 0 397 0 398 0 408 0
		 409 0 435 0 436 0 447 0 448 0 471 0 472 0 484 0 485 0 509 0 510 0 515 0 516 0 526 0
		 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0
		 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0
		 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 773 0 779 0 786 0 793 0 800 0 806 0 813 0 819 0 826 0
		 833 0 841 0 850 0 858 0 864 0 871 0 879 0 888 0;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 3 1 1 1 1 18 3 1 1 3 
		1 18 3 18 1 3 1;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[84:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[84:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_foot_ctrl_toeTip";
	rename -uid "4B9555AE-4207-612D-0B37-DDAFFF747365";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0 7 0 13 0 17 0 21 0 103 0 217 0 275 0
		 286 0 311 0 312 0 322 0 323 0 346 0 347 0 368 0 369 0 388 0 389 0 397 0 398 0 408 0
		 409 0 435 0 436 0 447 0 448 0 471 0 472 0 484 0 485 0 509 0 510 0 515 0 516 0 526 0
		 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0
		 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0
		 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 773 0 779 0 786 0 793 0 800 0 806 0 813 0 819 0 826 0
		 833 0 841 0 850 0 858 0 864 0 871 0 879 0 888 0;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 3 1 1 1 1 18 3 1 1 3 
		1 18 3 18 1 3 1;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[84:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[84:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_foot_ctrl_ankle";
	rename -uid "F1F1222E-4813-78E8-0BF3-5289C6912D26";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0 7 0 13 0 17 0 21 0 103 0 217 0 275 0
		 286 0 311 0 312 0 322 0 323 0 346 0 347 0 368 0 369 0 388 0 389 0 397 0 398 0 408 0
		 409 0 435 0 436 0 447 0 448 0 471 0 472 0 484 0 485 0 509 0 510 0 515 0 516 0 526 0
		 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0
		 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0
		 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 773 0 779 0 786 0 793 0 800 0 806 0 813 0 819 0 826 0
		 833 0 841 0 850 0 858 0 864 0 871 0 879 0 888 0;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 3 1 1 1 1 18 3 1 1 3 
		1 18 3 18 1 3 1;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[84:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[84:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_knee_ctrl_visibility";
	rename -uid "C9B6ED4D-4CC0-58C0-1C70-E1804766AF38";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  1 1 7 1 13 1 217 1 275 1 311 1 312 1 485 1
		 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1
		 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1
		 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1
		 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 773 1 779 1 786 1 793 1
		 806 1 813 1 819 1 826 1 841 1 850 1 864 1 888 1;
	setAttr -s 72 ".kit[0:71]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 9 1 9 9 18 9 18 18 9 18 18 18;
	setAttr -s 72 ".kot[0:71]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 72 ".kix[61:71]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 72 ".kiy[61:71]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_knee_ctrl_rotateX";
	rename -uid "A4BC5A78-4A9A-25EF-BCD5-589C9D29146C";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  1 0 7 0 13 0 217 0 275 0 311 0 312 0 485 0
		 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0
		 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0
		 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0
		 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 773 0 779 0 786 0 793 0
		 806 0 813 0 819 0 826 0 841 0 850 0 864 0 888 0;
	setAttr -s 72 ".kit[0:71]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 3 1 3 3 18 3 18 18 3 18 18 18;
	setAttr -s 72 ".kot[0:71]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 72 ".kix[61:71]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 72 ".kiy[61:71]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_knee_ctrl_rotateY";
	rename -uid "9001B0B0-41F1-789A-9A00-41AE6E7E41AF";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  1 0 7 0 13 0 217 0 275 0 311 0 312 0 485 0
		 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0
		 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0
		 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0
		 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 773 0 779 0 786 0 793 0
		 806 0 813 0 819 0 826 0 841 0 850 0 864 0 888 0;
	setAttr -s 72 ".kit[0:71]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 3 1 3 3 18 3 18 18 3 18 18 18;
	setAttr -s 72 ".kot[0:71]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 72 ".kix[61:71]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 72 ".kiy[61:71]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_knee_ctrl_rotateZ";
	rename -uid "B43A2BB2-4611-E00C-369A-40B539A4718D";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  1 0 7 0 13 0 217 0 275 0 311 0 312 0 485 0
		 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0
		 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0
		 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0
		 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 773 0 779 0 786 0 793 0
		 806 0 813 0 819 0 826 0 841 0 850 0 864 0 888 0;
	setAttr -s 72 ".kit[0:71]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 3 1 3 3 18 3 18 18 3 18 18 18;
	setAttr -s 72 ".kot[0:71]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 72 ".kix[61:71]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 72 ".kiy[61:71]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_knee_ctrl_scaleX";
	rename -uid "A7FC03DF-4B26-7284-5F69-778BA80B27DB";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  1 1.0000000000000002 7 1.0000000000000002
		 13 1.0000000000000002 217 1.0000000000000002 275 1.0000000000000002 311 1.0000000000000002
		 312 1.0000000000000002 485 1.0000000000000002 509 1.0000000000000002 510 1.0000000000000002
		 515 1.0000000000000002 516 1.0000000000000002 526 1.0000000000000002 527 1.0000000000000002
		 535 1.0000000000000002 536 1.0000000000000002 544 1.0000000000000002 545 1.0000000000000002
		 555 1.0000000000000002 556 1.0000000000000002 560 1.0000000000000002 561 1.0000000000000002
		 567 1.0000000000000002 568 1.0000000000000002 580 1.0000000000000002 581 1.0000000000000002
		 596 1.0000000000000002 597 1.0000000000000002 613 1.0000000000000002 614 1.0000000000000002
		 624 1.0000000000000002 625 1.0000000000000002 630 1.0000000000000002 631 1.0000000000000002
		 636 1.0000000000000002 637 1.0000000000000002 642 1.0000000000000002 643 1.0000000000000002
		 647 1.0000000000000002 648 1.0000000000000002 656 1.0000000000000002 657 1.0000000000000002
		 666 1.0000000000000002 667 1.0000000000000002 681 1.0000000000000002 684 1.0000000000000002
		 687 1.0000000000000002 690 1.0000000000000002 697 1.0000000000000002 707 1.0000000000000002
		 711 1.0000000000000002 716 1.0000000000000002 720 1.0000000000000002 725 1.0000000000000002
		 729 1.0000000000000002 734 1.0000000000000002 740 1.0000000000000002 752 1.0000000000000002
		 759 1.0000000000000002 765 1.0000000000000002 773 1.0000000000000002 779 1.0000000000000002
		 786 1.0000000000000002 793 1.0000000000000002 806 1.0000000000000002 813 1.0000000000000002
		 819 1.0000000000000002 826 1.0000000000000002 841 1.0000000000000002 850 1.0000000000000002
		 864 1.0000000000000002 888 1.0000000000000002;
	setAttr -s 72 ".kit[0:71]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 3 1 3 3 18 3 18 18 3 18 18 18;
	setAttr -s 72 ".kot[0:71]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 72 ".kix[61:71]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 72 ".kiy[61:71]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_knee_ctrl_scaleY";
	rename -uid "18549ECB-4FA6-68C2-6B00-ED9609AC571B";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  1 0.99999999999999989 7 0.99999999999999989
		 13 0.99999999999999989 217 0.99999999999999989 275 0.99999999999999989 311 0.99999999999999989
		 312 0.99999999999999989 485 0.99999999999999989 509 0.99999999999999989 510 0.99999999999999989
		 515 0.99999999999999989 516 0.99999999999999989 526 0.99999999999999989 527 0.99999999999999989
		 535 0.99999999999999989 536 0.99999999999999989 544 0.99999999999999989 545 0.99999999999999989
		 555 0.99999999999999989 556 0.99999999999999989 560 0.99999999999999989 561 0.99999999999999989
		 567 0.99999999999999989 568 0.99999999999999989 580 0.99999999999999989 581 0.99999999999999989
		 596 0.99999999999999989 597 0.99999999999999989 613 0.99999999999999989 614 0.99999999999999989
		 624 0.99999999999999989 625 0.99999999999999989 630 0.99999999999999989 631 0.99999999999999989
		 636 0.99999999999999989 637 0.99999999999999989 642 0.99999999999999989 643 0.99999999999999989
		 647 0.99999999999999989 648 0.99999999999999989 656 0.99999999999999989 657 0.99999999999999989
		 666 0.99999999999999989 667 0.99999999999999989 681 0.99999999999999989 684 0.99999999999999989
		 687 0.99999999999999989 690 0.99999999999999989 697 0.99999999999999989 707 0.99999999999999989
		 711 0.99999999999999989 716 0.99999999999999989 720 0.99999999999999989 725 0.99999999999999989
		 729 0.99999999999999989 734 0.99999999999999989 740 0.99999999999999989 752 0.99999999999999989
		 759 0.99999999999999989 765 0.99999999999999989 773 0.99999999999999989 779 0.99999999999999989
		 786 0.99999999999999989 793 0.99999999999999989 806 0.99999999999999989 813 0.99999999999999989
		 819 0.99999999999999989 826 0.99999999999999989 841 0.99999999999999989 850 0.99999999999999989
		 864 0.99999999999999989 888 0.99999999999999989;
	setAttr -s 72 ".kit[0:71]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 3 1 3 3 18 3 18 18 3 18 18 18;
	setAttr -s 72 ".kot[0:71]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 72 ".kix[61:71]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 72 ".kiy[61:71]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_knee_ctrl_scaleZ";
	rename -uid "2BAA85B9-4CAE-8F29-C0A5-C3AA887193D6";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 72 ".ktv[0:71]"  1 1.0000000000000002 7 1.0000000000000002
		 13 1.0000000000000002 217 1.0000000000000002 275 1.0000000000000002 311 1.0000000000000002
		 312 1.0000000000000002 485 1.0000000000000002 509 1.0000000000000002 510 1.0000000000000002
		 515 1.0000000000000002 516 1.0000000000000002 526 1.0000000000000002 527 1.0000000000000002
		 535 1.0000000000000002 536 1.0000000000000002 544 1.0000000000000002 545 1.0000000000000002
		 555 1.0000000000000002 556 1.0000000000000002 560 1.0000000000000002 561 1.0000000000000002
		 567 1.0000000000000002 568 1.0000000000000002 580 1.0000000000000002 581 1.0000000000000002
		 596 1.0000000000000002 597 1.0000000000000002 613 1.0000000000000002 614 1.0000000000000002
		 624 1.0000000000000002 625 1.0000000000000002 630 1.0000000000000002 631 1.0000000000000002
		 636 1.0000000000000002 637 1.0000000000000002 642 1.0000000000000002 643 1.0000000000000002
		 647 1.0000000000000002 648 1.0000000000000002 656 1.0000000000000002 657 1.0000000000000002
		 666 1.0000000000000002 667 1.0000000000000002 681 1.0000000000000002 684 1.0000000000000002
		 687 1.0000000000000002 690 1.0000000000000002 697 1.0000000000000002 707 1.0000000000000002
		 711 1.0000000000000002 716 1.0000000000000002 720 1.0000000000000002 725 1.0000000000000002
		 729 1.0000000000000002 734 1.0000000000000002 740 1.0000000000000002 752 1.0000000000000002
		 759 1.0000000000000002 765 1.0000000000000002 773 1.0000000000000002 779 1.0000000000000002
		 786 1.0000000000000002 793 1.0000000000000002 806 1.0000000000000002 813 1.0000000000000002
		 819 1.0000000000000002 826 1.0000000000000002 841 1.0000000000000002 850 1.0000000000000002
		 864 1.0000000000000002 888 1.0000000000000002;
	setAttr -s 72 ".kit[0:71]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 3 1 3 3 18 3 18 18 3 18 18 18;
	setAttr -s 72 ".kot[0:71]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 72 ".kix[61:71]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 72 ".kiy[61:71]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_foot_ctrl_visibility";
	rename -uid "8EF6CED4-40BD-BE5F-1F58-E989044E1D9B";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 1 7 1 13 1 17 1 21 1 103 1 217 1 275 1
		 295 1 296 1 322 1 323 1 346 1 347 1 368 1 369 1 388 1 389 1 397 1 398 1 408 1 409 1
		 435 1 436 1 447 1 448 1 471 1 472 1 484 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1
		 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1
		 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 653 1 656 1
		 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1
		 734 1 740 1 752 1 759 1 765 1 772 1 779 1 786 1 793 1 800 1 806 1 813 1 819 1 826 1
		 833 1 841 1 850 1 858 1 864 1 871 1 879 1 888 1;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 9 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 9 9 9 1 9 1 18 9 
		9 1 9 1 9 1 9;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[83:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[83:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "R_foot_ctrl_rotateX";
	rename -uid "F8E4EA0E-4381-C612-D3EA-DA9CB4626513";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0 7 0 13 0 17 0 21 0 103 0 217 0 275 0
		 295 0 296 0 322 0 323 0 346 0 347 0 368 0 369 0 388 0 389 0 397 0 398 0 408 0 409 0
		 435 0 436 0 447 0 448 0 471 0 472 0 484 0 485 0 509 0 510 0.022629055071909748 515 0.022629055071909748
		 516 0.022629055071909748 526 0.022629055071909748 527 0.022629055071909748 535 0.022629055071909748
		 536 0.022629055071909748 544 0.022629055071909748 545 0.022629055071909748 555 0.022629055071909748
		 556 0.022629055071909748 560 0.022629055071909748 561 0.022629055071909748 567 0.022629055071909748
		 568 0.022629055071909748 580 0.022629055071909748 581 0.022629055071909748 596 0.022629055071909748
		 597 0.021336099262549205 613 0.021336099262549205 614 0.021336099262549205 624 0.021336099262549205
		 625 0.021336099262549205 630 0.021336099262549205 631 0.021336099262549205 636 0.021336099262549205
		 637 0.021336099262549205 642 0.021336099262549205 643 0.021336099262549205 647 0.021336099262549205
		 648 0.021336099262549205 653 0.021336099262549205 656 0.021336099262549205 657 0.021336099262549205
		 666 0.021336099262549205 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0
		 725 0 729 0 734 0 740 0 752 0 759 0 765 0 772 0 779 0 786 0 793 0 800 0 806 0 813 0
		 819 0 826 0 833 0 841 0 850 0 858 0 864 0 871 0 879 0 888 0;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 3 3 3 1 3 1 18 3 
		3 1 3 1 3 1 3;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[83:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[83:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "R_foot_ctrl_rotateY";
	rename -uid "5D32EA0C-4CB7-AFF1-D932-81A61955008B";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 -17.721020036371055 7 -17.721020036371055
		 13 -17.721020036371055 17 -32.800296839490187 21 -32.800296839490187 103 -32.800296839490187
		 217 -32.800296839490187 275 -32.800296839490187 295 -32.800296839490187 296 -32.800296839490187
		 322 -32.800296839490187 323 -32.800296839490187 346 -32.800296839490187 347 -32.800296839490187
		 368 -32.800296839490187 369 -32.800296839490187 388 -32.800296839490187 389 -32.800296839490187
		 397 -32.800296839490187 398 -32.800296839490187 408 -32.800296839490187 409 -32.800296839490187
		 435 -32.800296839490187 436 -32.800296839490187 447 -32.800296839490187 448 -32.800296839490187
		 471 -32.800296839490187 472 -32.800296839490187 484 -32.800296839490187 485 -32.800296839490187
		 509 -32.800296839490187 510 -20.066558279027394 515 -20.066558279027394 516 -20.066558279027394
		 526 -20.066558279027394 527 -20.066558279027394 535 -20.066558279027394 536 -20.066558279027394
		 544 -20.066558279027394 545 -20.066558279027394 555 -20.066558279027394 556 -20.066558279027394
		 560 -20.066558279027394 561 -20.066558279027394 567 -20.066558279027394 568 -20.066558279027394
		 580 -20.066558279027394 581 -20.066558279027394 596 -20.066558279027394 597 4.9863655148168009
		 613 4.9863655148168009 614 4.9863655148168009 624 4.9863655148168009 625 4.9863655148168009
		 630 4.9863655148168009 631 4.9863655148168009 636 4.9863655148168009 637 4.9863655148168009
		 642 4.9863655148168009 643 4.9863655148168009 647 4.9863655148168009 648 4.9863655148168009
		 653 4.9863655148168009 656 4.9863655148168009 657 4.9863655148168009 666 4.9863655148168009
		 667 -32.800296839490187 681 -32.800296839490187 684 -32.800296839490187 687 -32.800296839490187
		 690 -32.800296839490187 697 -32.800296839490187 707 -32.800296839490187 711 -32.800296839490187
		 716 -32.800296839490187 720 -32.800296839490187 725 -32.800296839490187 729 -32.800296839490187
		 734 -32.800296839490187 740 -32.800296839490187 752 -32.800296839490187 759 75.827713162493453
		 765 75.827713162493453 772 119.60546536163166 779 119.60546536163166 786 119.60546536163166
		 793 119.60546536163166 800 119.60546536163166 806 119.60546536163166 813 119.60546536163166
		 819 119.60546536163166 826 156.6050546992837 833 156.6050546992837 841 156.6050546992837
		 850 156.6050546992837 858 156.6050546992837 864 156.6050546992837 871 179.88119377254793
		 879 148.02243168303255 888 134.29078980373495;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 3 3 3 1 3 1 18 3 
		3 1 3 1 3 1 3;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[83:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[83:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "R_foot_ctrl_rotateZ";
	rename -uid "4BD6330C-40BD-621F-6461-D98EE0E07C8A";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0 7 0 13 0 17 0 21 0 103 0 217 0 275 0
		 295 0 296 0 322 0 323 0 346 0 347 0 368 0 369 0 388 0 389 0 397 0 398 0 408 0 409 0
		 435 0 436 0 447 0 448 0 471 0 472 0 484 0 485 0 509 0 510 -4.1708294582929382 515 -4.1708294582929382
		 516 -4.1708294582929382 526 -4.1708294582929382 527 -4.1708294582929382 535 -4.1708294582929382
		 536 -4.1708294582929382 544 -4.1708294582929382 545 -4.1708294582929382 555 -4.1708294582929382
		 556 -4.1708294582929382 560 -4.1708294582929382 561 -4.1708294582929382 567 -4.1708294582929382
		 568 -4.1708294582929382 580 -4.1708294582929382 581 -4.1708294582929382 596 -4.1708294582929382
		 597 -4.1612106628785028 613 -4.1612106628785028 614 -4.1612106628785028 624 -4.1612106628785028
		 625 -4.1612106628785028 630 -4.1612106628785028 631 -4.1612106628785028 636 -4.1612106628785028
		 637 -4.1612106628785028 642 -4.1612106628785028 643 -4.1612106628785028 647 -4.1612106628785028
		 648 -4.1612106628785028 653 -4.1612106628785028 656 -4.1612106628785028 657 -4.1612106628785028
		 666 -4.1612106628785028 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0
		 725 0 729 0 734 0 740 0 752 0 759 0 765 0 772 0 779 0 786 0 793 0 800 0 806 0 813 0
		 819 0 826 0 833 0 841 0 850 0 858 0 864 0 871 0 879 0 888 0;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 3 3 3 1 3 1 18 3 
		3 1 3 1 3 1 3;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[83:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[83:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_foot_ctrl_scaleX";
	rename -uid "BD2E2F6A-4332-DE51-748D-0F8FB28B23D9";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0.99999999999999967 7 0.99999999999999967
		 13 0.99999999999999967 17 0.99999999999999967 21 0.99999999999999967 103 0.99999999999999967
		 217 0.99999999999999967 275 0.99999999999999967 295 0.99999999999999967 296 0.99999999999999967
		 322 0.99999999999999967 323 0.99999999999999967 346 0.99999999999999967 347 0.99999999999999967
		 368 0.99999999999999967 369 0.99999999999999967 388 0.99999999999999967 389 0.99999999999999967
		 397 0.99999999999999967 398 0.99999999999999967 408 0.99999999999999967 409 0.99999999999999967
		 435 0.99999999999999967 436 0.99999999999999967 447 0.99999999999999967 448 0.99999999999999967
		 471 0.99999999999999967 472 0.99999999999999967 484 0.99999999999999967 485 0.99999999999999967
		 509 0.99999999999999967 510 0.99999999999999967 515 0.99999999999999967 516 0.99999999999999967
		 526 0.99999999999999967 527 0.99999999999999967 535 0.99999999999999967 536 0.99999999999999967
		 544 0.99999999999999967 545 0.99999999999999967 555 0.99999999999999967 556 0.99999999999999967
		 560 0.99999999999999967 561 0.99999999999999967 567 0.99999999999999967 568 0.99999999999999967
		 580 0.99999999999999967 581 0.99999999999999967 596 0.99999999999999967 597 0.99999999999999967
		 613 0.99999999999999967 614 0.99999999999999967 624 0.99999999999999967 625 0.99999999999999967
		 630 0.99999999999999967 631 0.99999999999999967 636 0.99999999999999967 637 0.99999999999999967
		 642 0.99999999999999967 643 0.99999999999999967 647 0.99999999999999967 648 0.99999999999999967
		 653 0.99999999999999967 656 0.99999999999999967 657 0.99999999999999967 666 0.99999999999999967
		 667 0.99999999999999967 681 0.99999999999999967 684 0.99999999999999967 687 0.99999999999999967
		 690 0.99999999999999967 697 0.99999999999999967 707 0.99999999999999967 711 0.99999999999999967
		 716 0.99999999999999967 720 0.99999999999999967 725 0.99999999999999967 729 0.99999999999999967
		 734 0.99999999999999967 740 0.99999999999999967 752 0.99999999999999967 759 0.99999999999999967
		 765 0.99999999999999967 772 0.99999999999999967 779 0.99999999999999967 786 0.99999999999999967
		 793 0.99999999999999967 800 0.99999999999999967 806 0.99999999999999967 813 0.99999999999999967
		 819 0.99999999999999967 826 0.99999999999999967 833 0.99999999999999967 841 0.99999999999999967
		 850 0.99999999999999967 858 0.99999999999999967 864 0.99999999999999967 871 0.99999999999999967
		 879 0.99999999999999967 888 0.99999999999999967;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 3 3 3 1 3 1 18 3 
		3 1 3 1 3 1 3;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[83:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[83:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_foot_ctrl_scaleY";
	rename -uid "7FBDD5A9-4AFA-84FD-0F16-9BA493DFA49F";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0.99999999999999967 7 0.99999999999999967
		 13 0.99999999999999967 17 0.99999999999999967 21 0.99999999999999967 103 0.99999999999999967
		 217 0.99999999999999967 275 0.99999999999999967 295 0.99999999999999967 296 0.99999999999999967
		 322 0.99999999999999967 323 0.99999999999999967 346 0.99999999999999967 347 0.99999999999999967
		 368 0.99999999999999967 369 0.99999999999999967 388 0.99999999999999967 389 0.99999999999999967
		 397 0.99999999999999967 398 0.99999999999999967 408 0.99999999999999967 409 0.99999999999999967
		 435 0.99999999999999967 436 0.99999999999999967 447 0.99999999999999967 448 0.99999999999999967
		 471 0.99999999999999967 472 0.99999999999999967 484 0.99999999999999967 485 0.99999999999999967
		 509 0.99999999999999967 510 0.99999999999999967 515 0.99999999999999967 516 0.99999999999999967
		 526 0.99999999999999967 527 0.99999999999999967 535 0.99999999999999967 536 0.99999999999999967
		 544 0.99999999999999967 545 0.99999999999999967 555 0.99999999999999967 556 0.99999999999999967
		 560 0.99999999999999967 561 0.99999999999999967 567 0.99999999999999967 568 0.99999999999999967
		 580 0.99999999999999967 581 0.99999999999999967 596 0.99999999999999967 597 0.99999999999999967
		 613 0.99999999999999967 614 0.99999999999999967 624 0.99999999999999967 625 0.99999999999999967
		 630 0.99999999999999967 631 0.99999999999999967 636 0.99999999999999967 637 0.99999999999999967
		 642 0.99999999999999967 643 0.99999999999999967 647 0.99999999999999967 648 0.99999999999999967
		 653 0.99999999999999967 656 0.99999999999999967 657 0.99999999999999967 666 0.99999999999999967
		 667 0.99999999999999967 681 0.99999999999999967 684 0.99999999999999967 687 0.99999999999999967
		 690 0.99999999999999967 697 0.99999999999999967 707 0.99999999999999967 711 0.99999999999999967
		 716 0.99999999999999967 720 0.99999999999999967 725 0.99999999999999967 729 0.99999999999999967
		 734 0.99999999999999967 740 0.99999999999999967 752 0.99999999999999967 759 0.99999999999999967
		 765 0.99999999999999967 772 0.99999999999999967 779 0.99999999999999967 786 0.99999999999999967
		 793 0.99999999999999967 800 0.99999999999999967 806 0.99999999999999967 813 0.99999999999999967
		 819 0.99999999999999967 826 0.99999999999999967 833 0.99999999999999967 841 0.99999999999999967
		 850 0.99999999999999967 858 0.99999999999999967 864 0.99999999999999967 871 0.99999999999999967
		 879 0.99999999999999967 888 0.99999999999999967;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 3 3 3 1 3 1 18 3 
		3 1 3 1 3 1 3;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[83:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[83:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_foot_ctrl_scaleZ";
	rename -uid "6F00BFCC-4E36-17E6-97B1-9B819B81A9A9";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0.99999999999999944 7 0.99999999999999944
		 13 0.99999999999999944 17 0.99999999999999944 21 0.99999999999999944 103 0.99999999999999944
		 217 0.99999999999999944 275 0.99999999999999944 295 0.99999999999999944 296 0.99999999999999944
		 322 0.99999999999999944 323 0.99999999999999944 346 0.99999999999999944 347 0.99999999999999944
		 368 0.99999999999999944 369 0.99999999999999944 388 0.99999999999999944 389 0.99999999999999944
		 397 0.99999999999999944 398 0.99999999999999944 408 0.99999999999999944 409 0.99999999999999944
		 435 0.99999999999999944 436 0.99999999999999944 447 0.99999999999999944 448 0.99999999999999944
		 471 0.99999999999999944 472 0.99999999999999944 484 0.99999999999999944 485 0.99999999999999944
		 509 0.99999999999999944 510 0.99999999999999944 515 0.99999999999999944 516 0.99999999999999944
		 526 0.99999999999999944 527 0.99999999999999944 535 0.99999999999999944 536 0.99999999999999944
		 544 0.99999999999999944 545 0.99999999999999944 555 0.99999999999999944 556 0.99999999999999944
		 560 0.99999999999999944 561 0.99999999999999944 567 0.99999999999999944 568 0.99999999999999944
		 580 0.99999999999999944 581 0.99999999999999944 596 0.99999999999999944 597 0.99999999999999944
		 613 0.99999999999999944 614 0.99999999999999944 624 0.99999999999999944 625 0.99999999999999944
		 630 0.99999999999999944 631 0.99999999999999944 636 0.99999999999999944 637 0.99999999999999944
		 642 0.99999999999999944 643 0.99999999999999944 647 0.99999999999999944 648 0.99999999999999944
		 653 0.99999999999999944 656 0.99999999999999944 657 0.99999999999999944 666 0.99999999999999944
		 667 0.99999999999999944 681 0.99999999999999944 684 0.99999999999999944 687 0.99999999999999944
		 690 0.99999999999999944 697 0.99999999999999944 707 0.99999999999999944 711 0.99999999999999944
		 716 0.99999999999999944 720 0.99999999999999944 725 0.99999999999999944 729 0.99999999999999944
		 734 0.99999999999999944 740 0.99999999999999944 752 0.99999999999999944 759 0.99999999999999944
		 765 0.99999999999999944 772 0.99999999999999944 779 0.99999999999999944 786 0.99999999999999944
		 793 0.99999999999999944 800 0.99999999999999944 806 0.99999999999999944 813 0.99999999999999944
		 819 0.99999999999999944 826 0.99999999999999944 833 0.99999999999999944 841 0.99999999999999944
		 850 0.99999999999999944 858 0.99999999999999944 864 0.99999999999999944 871 0.99999999999999944
		 879 0.99999999999999944 888 0.99999999999999944;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 3 3 3 1 3 1 18 3 
		3 1 3 1 3 1 3;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[83:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[83:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_foot_ctrl_toeTap";
	rename -uid "FDF8D70C-46D9-0E92-1380-92AB053C553C";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0 7 0 13 0 17 0 21 0 103 0 217 0 275 0
		 295 0 296 0 322 0 323 0 346 0 347 0 368 0 369 0 388 0 389 0 397 0 398 0 408 0 409 0
		 435 0 436 0 447 0 448 0 471 0 472 0 484 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 653 0 656 0
		 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 772 0 779 0 786 0 793 0 800 0 806 0 813 0 819 0 826 0
		 833 0 841 0 850 0 858 0 864 0 871 0 879 0 888 0;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 3 3 3 1 3 1 18 3 
		3 1 3 1 3 1 3;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[83:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[83:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_foot_ctrl_footPeel";
	rename -uid "83C86F88-4EB7-5823-B61B-50964F233827";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0 7 0 13 0 17 0 21 0 103 0 217 0 275 0
		 295 0 296 0 322 0 323 0 346 0 347 0 368 0 369 0 388 0 389 0 397 0 398 0 408 0 409 0
		 435 0 436 0 447 0 448 0 471 0 472 0 484 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 653 0 656 0
		 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 772 0 779 0 786 0 793 0 800 0 806 0 813 0 819 0 826 0
		 833 0 841 0 850 0 858 0 864 0 871 0 879 0 888 0;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 3 3 3 1 3 1 18 3 
		3 1 3 1 3 1 3;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[83:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[83:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_foot_ctrl_footSwivel";
	rename -uid "EA5C15BE-4899-70EB-0AF2-818A8A69A3DB";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0 7 0 13 0 17 0 21 0 103 0 217 0 275 0
		 295 0 296 0 322 0 323 0 346 0 347 0 368 0 369 0 388 0 389 0 397 0 398 0 408 0 409 0
		 435 0 436 0 447 0 448 0 471 0 472 0 484 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 653 0 656 0
		 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 772 0 779 0 786 0 793 0 800 0 806 0 813 0 819 0 826 0
		 833 0 841 0 850 0 858 0 864 0 871 0 879 0 888 0;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 3 3 3 1 3 1 18 3 
		3 1 3 1 3 1 3;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[83:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[83:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_foot_ctrl_toeTip";
	rename -uid "44ACE322-4A4F-DBB8-7790-95A5722B69DE";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0 7 0 13 0 17 0 21 0 103 0 217 0 275 0
		 295 0 296 0 322 0 323 0 346 0 347 0 368 0 369 0 388 0 389 0 397 0 398 0 408 0 409 0
		 435 0 436 0 447 0 448 0 471 0 472 0 484 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 653 0 656 0
		 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 772 0 779 0 786 0 793 0 800 0 806 0 813 0 819 0 826 0
		 833 0 841 0 850 0 858 0 864 0 871 0 879 0 888 0;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 3 3 3 1 3 1 18 3 
		3 1 3 1 3 1 3;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[83:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[83:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_foot_ctrl_ankle";
	rename -uid "A266C49E-495A-D502-71D3-94B88FDBC5EA";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 100 ".ktv[0:99]"  1 0 7 0 13 0 17 0 21 0 103 0 217 0 275 0
		 295 0 296 0 322 0 323 0 346 0 347 0 368 0 369 0 388 0 389 0 397 0 398 0 408 0 409 0
		 435 0 436 0 447 0 448 0 471 0 472 0 484 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 653 0 656 0
		 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 772 0 779 0 786 0 793 0 800 0 806 0 813 0 819 0 826 0
		 833 0 841 0 850 0 858 0 864 0 871 0 879 0 888 0;
	setAttr -s 100 ".kit[0:99]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 3 3 3 1 3 1 18 3 
		3 1 3 1 3 1 3;
	setAttr -s 100 ".kot[0:99]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
	setAttr -s 100 ".kix[83:99]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 100 ".kiy[83:99]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_knee_ctrl_visibility";
	rename -uid "878CB823-4BC1-7B9A-FB00-04BA4F10388F";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 772 1 779 1 806 1 813 1 819 1 826 1
		 841 1 850 1 864 1 879 1 888 1;
	setAttr -s 69 ".kit[0:68]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 
		18 18 9 18 18 9 18 18 9 18;
	setAttr -s 69 ".kot[0:68]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[58:68]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[58:68]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "R_knee_ctrl_rotateX";
	rename -uid "32D39793-4167-1A0A-AE51-54867507F108";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 772 0 779 0 806 0 813 0 819 0 826 0
		 841 0 850 0 864 0 879 0 888 0;
	setAttr -s 69 ".kit[0:68]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 
		18 18 3 18 18 3 18 18 3 18;
	setAttr -s 69 ".kot[0:68]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[58:68]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[58:68]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "R_knee_ctrl_rotateY";
	rename -uid "E6B7D1CA-4C1C-128B-CED8-DDA6F9461F34";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 772 0 779 0 806 0 813 0 819 0 826 0
		 841 0 850 0 864 0 879 0 888 -45.590403968812772;
	setAttr -s 69 ".kit[0:68]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 
		18 18 3 18 18 3 18 18 3 18;
	setAttr -s 69 ".kot[0:68]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[58:68]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[58:68]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "R_knee_ctrl_rotateZ";
	rename -uid "D07B8117-4077-D633-8709-7C92732CB62F";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 772 0 779 0 806 0 813 0 819 0 826 0
		 841 0 850 0 864 0 879 0 888 0;
	setAttr -s 69 ".kit[0:68]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 
		18 18 3 18 18 3 18 18 3 18;
	setAttr -s 69 ".kot[0:68]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[58:68]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[58:68]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_knee_ctrl_scaleX";
	rename -uid "DABC6AB4-41FE-612C-47BB-4093F205C73D";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 1.0000000000000002 7 1.0000000000000002
		 13 1.0000000000000002 217 1.0000000000000002 275 1.0000000000000002 485 1.0000000000000002
		 509 1.0000000000000002 510 1.0000000000000002 515 1.0000000000000002 516 1.0000000000000002
		 526 1.0000000000000002 527 1.0000000000000002 535 1.0000000000000002 536 1.0000000000000002
		 544 1.0000000000000002 545 1.0000000000000002 555 1.0000000000000002 556 1.0000000000000002
		 560 1.0000000000000002 561 1.0000000000000002 567 1.0000000000000002 568 1.0000000000000002
		 580 1.0000000000000002 581 1.0000000000000002 596 1.0000000000000002 597 1.0000000000000002
		 613 1.0000000000000002 614 1.0000000000000002 624 1.0000000000000002 625 1.0000000000000002
		 630 1.0000000000000002 631 1.0000000000000002 636 1.0000000000000002 637 1.0000000000000002
		 642 1.0000000000000002 643 1.0000000000000002 647 1.0000000000000002 648 1.0000000000000002
		 656 1.0000000000000002 657 1.0000000000000002 666 1.0000000000000002 667 1.0000000000000002
		 681 1.0000000000000002 684 1.0000000000000002 687 1.0000000000000002 690 1.0000000000000002
		 697 1.0000000000000002 707 1.0000000000000002 711 1.0000000000000002 716 1.0000000000000002
		 720 1.0000000000000002 725 1.0000000000000002 729 1.0000000000000002 734 1.0000000000000002
		 740 1.0000000000000002 752 1.0000000000000002 759 1.0000000000000002 765 1.0000000000000002
		 772 1.0000000000000002 779 1.0000000000000002 806 1.0000000000000002 813 1.0000000000000002
		 819 1.0000000000000002 826 1.0000000000000002 841 1.0000000000000002 850 1.0000000000000002
		 864 1.0000000000000002 879 1.0000000000000002 888 1.0000000000000002;
	setAttr -s 69 ".kit[0:68]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 
		18 18 3 18 18 3 18 18 3 18;
	setAttr -s 69 ".kot[0:68]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[58:68]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[58:68]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_knee_ctrl_scaleY";
	rename -uid "FB328502-4500-9E82-F8CF-A6AA79919818";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 772 1 779 1 806 1 813 1 819 1 826 1
		 841 1 850 1 864 1 879 1 888 1;
	setAttr -s 69 ".kit[0:68]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 
		18 18 3 18 18 3 18 18 3 18;
	setAttr -s 69 ".kot[0:68]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[58:68]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[58:68]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_knee_ctrl_scaleZ";
	rename -uid "F3BA03B9-4A11-6119-8E6A-E89900DF1AF8";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 772 1 779 1 806 1 813 1 819 1 826 1
		 841 1 850 1 864 1 879 1 888 1;
	setAttr -s 69 ".kit[0:68]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 
		18 18 3 18 18 3 18 18 3 18;
	setAttr -s 69 ".kot[0:68]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[58:68]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[58:68]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_Root_ctrl_visibility";
	rename -uid "9E8D52AD-4EA3-13F2-A152-0DA05946CD4A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 1 7 1 13 1 217 1 274 1 275 1 485 1 509 1
		 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1
		 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1
		 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1
		 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1
		 850 1 864 1 888 1;
	setAttr -s 67 ".kit[63:66]"  9 18 18 18;
	setAttr -s 67 ".kot[1:66]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
createNode animCurveTA -n "M_Root_ctrl_rotateX";
	rename -uid "6EA81D9B-4BB8-FBFE-887F-9C9277B25B37";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 0 7 0 13 0 217 0 274 0 275 0 485 0 509 0
		 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0
		 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0
		 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0
		 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0
		 850 0 864 0 888 0;
	setAttr -s 67 ".kit[63:66]"  3 18 18 18;
	setAttr -s 67 ".kot[1:66]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
createNode animCurveTA -n "M_Root_ctrl_rotateY";
	rename -uid "16E213FD-4F9D-2FA4-1A3F-C984A8629B60";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 -62.203158945801029 7 -62.203158945801029
		 13 -62.203158945801029 217 -62.203158945801029 274 -62.203158945801029 275 -62.203158945801029
		 485 -62.203158945801029 509 -62.203158945801029 510 -62.203158945801029 515 -62.203158945801029
		 516 -62.203158945801029 526 -62.203158945801029 527 -62.203158945801029 535 -62.203158945801029
		 536 -62.203158945801029 544 -62.203158945801029 545 -62.203158945801029 555 -62.203158945801029
		 556 -62.203158945801029 560 -62.203158945801029 561 -62.203158945801029 567 -62.203158945801029
		 568 -62.203158945801029 580 -62.203158945801029 581 -62.203158945801029 596 -62.203158945801029
		 597 -62.203158945801029 613 -62.203158945801029 614 -62.203158945801029 624 -62.203158945801029
		 625 -62.203158945801029 630 -62.203158945801029 631 -62.203158945801029 636 -62.203158945801029
		 637 -62.203158945801029 642 -62.203158945801029 643 -62.203158945801029 647 -62.203158945801029
		 648 -62.203158945801029 656 -62.203158945801029 657 -62.203158945801029 666 -62.203158945801029
		 667 -62.203158945801029 681 -62.203158945801029 684 -62.203158945801029 687 -62.203158945801029
		 690 -62.203158945801029 697 -62.203158945801029 707 -62.203158945801029 711 -62.203158945801029
		 716 -62.203158945801029 720 -62.203158945801029 725 -62.203158945801029 729 -62.203158945801029
		 734 -62.203158945801029 740 -62.203158945801029 752 -62.203158945801029 759 -62.203158945801029
		 765 -62.203158945801029 779 -62.203158945801029 806 -62.203158945801029 819 -62.203158945801029
		 826 -62.203158945801029 841 -62.203158945801029 850 -62.203158945801029 864 -62.203158945801029
		 888 -62.203158945801029;
	setAttr -s 67 ".kit[63:66]"  3 18 18 18;
	setAttr -s 67 ".kot[1:66]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
createNode animCurveTA -n "M_Root_ctrl_rotateZ";
	rename -uid "5B48CCF3-4DBC-46A7-4C2F-78A3FC7DEE1D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 0 7 0 13 0 217 0 274 0 275 0 485 0 509 0
		 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0
		 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0
		 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0
		 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0
		 850 0 864 0 888 0;
	setAttr -s 67 ".kit[63:66]"  3 18 18 18;
	setAttr -s 67 ".kot[1:66]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
createNode animCurveTU -n "M_Root_ctrl_scaleX";
	rename -uid "6C0D44E2-42B1-0FEA-E699-3286D58504F7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 1 7 1 13 1 217 1 274 1 275 1 485 1 509 1
		 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1
		 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1
		 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1
		 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1
		 850 1 864 1 888 1;
	setAttr -s 67 ".kit[63:66]"  3 18 18 18;
	setAttr -s 67 ".kot[1:66]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
createNode animCurveTU -n "M_Root_ctrl_scaleY";
	rename -uid "1C78ADF0-4408-5734-0738-938B13C247E4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 1 7 1 13 1 217 1 274 1 275 1 485 1 509 1
		 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1
		 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1
		 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1
		 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1
		 850 1 864 1 888 1;
	setAttr -s 67 ".kit[63:66]"  3 18 18 18;
	setAttr -s 67 ".kot[1:66]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
createNode animCurveTU -n "M_Root_ctrl_scaleZ";
	rename -uid "F665EF61-4722-2625-227C-37A7145B8A77";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 1 7 1 13 1 217 1 274 1 275 1 485 1 509 1
		 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1
		 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1
		 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1
		 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1
		 850 1 864 1 888 1;
	setAttr -s 67 ".kit[63:66]"  3 18 18 18;
	setAttr -s 67 ".kot[1:66]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5;
createNode animCurveTU -n "M_Root_ctrl_L_Arm_FKIK";
	rename -uid "9F6DD8F1-431A-C897-3442-6B9C6274B3F0";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 65 ".ktv[0:64]"  1 1 7 1 13 1 217 1 274 1 275 0 485 0 509 0
		 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0
		 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0
		 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0
		 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0
		 850 0;
	setAttr -s 65 ".kit[0:64]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 1 18 18 1 1 
		18 18 18 18 3 18;
	setAttr -s 65 ".kot[0:64]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
	setAttr -s 65 ".kix[54:64]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 65 ".kiy[54:64]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_Root_ctrl_R_Arm_FKIK";
	rename -uid "6D3A179C-43A2-9B45-6F36-0297CDAF7473";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 65 ".ktv[0:64]"  1 1 7 1 13 1 217 1 274 1 275 0 485 0 509 0
		 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0
		 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0
		 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0
		 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0
		 850 0;
	setAttr -s 65 ".kit[0:64]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 1 18 18 1 1 
		18 18 18 18 3 18;
	setAttr -s 65 ".kot[0:64]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
	setAttr -s 65 ".kix[54:64]"  1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 65 ".kiy[54:64]"  0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_UpArm_ctrl_rotateX";
	rename -uid "EA1CC800-4E98-2525-7EF8-2F9FD0521A3D";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0 7 0 13 0 217 0 274 0 275 -61.76213062164058
		 388 -61.76213062164058 389 -61.76213062164058 397 -61.76213062164058 398 40.162229682862026
		 408 40.162229682862026 409 40.162229682862026 435 26.751349266386061 436 -61.76213062164058
		 485 -61.76213062164058 509 -61.76213062164058 510 -61.76213062164058 515 -61.76213062164058
		 516 -61.76213062164058 526 -61.76213062164058 527 -61.76213062164058 535 -61.76213062164058
		 536 -61.76213062164058 544 -61.76213062164058 545 -61.76213062164058 555 -61.76213062164058
		 556 -61.76213062164058 560 -61.76213062164058 561 -61.76213062164058 567 -61.76213062164058
		 568 31.945977963599574 580 31.945977963599574 581 31.945977963599574 596 31.945977963599574
		 597 31.945977963599574 613 31.945977963599574 614 -24.481151768785836 624 -24.481151768785836
		 625 -54.349311431534019 630 -54.349311431534019 631 23.879196617143819 636 23.879196617143819
		 637 42.200265456097952 642 42.200265456097952 643 42.200265456097952 647 42.200265456097952
		 648 66.429862494375584 656 66.429862494375584 657 84.149014159740958 666 84.149014159740958
		 667 84.149014159740958 677 84.149014159740958 681 36.906637204499752 684 2.3581874548856048
		 687 2.3581874548856048 690 2.3581874548856048 697 4.3876020353826988 707 2.3581874548856048
		 711 2.3581874548856048 716 2.3581874548856048 720 2.3581874548856048 725 2.3581874548856048
		 729 2.3581874548856048 734 -61.76213062164058 740 49.046781015250396 752 49.046781015250396
		 759 51.985588279820789 765 51.985588279820789 779 51.985588279820789 786 -61.76213062164058
		 806 -61.76213062164058 819 -61.76213062164058 826 -61.76213062164058 841 -61.76213062164058
		 850 -61.76213062164058 864 -61.76213062164058;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTA -n "L_UpArm_ctrl_rotateY";
	rename -uid "B7BA9C38-4415-88A6-ECCC-ECBA20ED9383";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0 7 0 13 0 217 0 274 0 275 18.803192690881009
		 388 18.803192690881009 389 18.803192690881009 397 18.803192690881009 398 20.395577176995786
		 408 20.395577176995786 409 20.395577176995786 435 35.426927652275261 436 18.803192690881009
		 485 18.803192690881009 509 18.803192690881009 510 18.803192690881009 515 18.803192690881009
		 516 18.803192690881009 526 18.803192690881009 527 18.803192690881009 535 18.803192690881009
		 536 18.803192690881009 544 18.803192690881009 545 18.803192690881009 555 18.803192690881009
		 556 18.803192690881009 560 18.803192690881009 561 18.803192690881009 567 18.803192690881009
		 568 -13.786160181412827 580 -13.786160181412827 581 -13.786160181412827 596 -13.786160181412827
		 597 -13.786160181412827 613 -13.786160181412827 614 42.06880083206412 624 42.06880083206412
		 625 41.443999342875053 630 41.443999342875053 631 38.21950419015532 636 38.21950419015532
		 637 29.847847319475953 642 29.847847319475953 643 29.847847319475953 647 29.847847319475953
		 648 16.518778395525924 656 16.518778395525924 657 6.7712042427876238 666 6.7712042427876238
		 667 6.7712042427876238 677 6.7712042427876238 681 45.630906914856567 684 74.049089309084579
		 687 74.049089309084579 690 74.049089309084579 697 27.271150145675463 707 74.049089309084579
		 711 74.049089309084579 716 74.049089309084579 720 74.049089309084579 725 74.049089309084579
		 729 74.049089309084579 734 18.803192690881009 740 13.670206493510651 752 13.670206493510651
		 759 18.49600274042659 765 18.49600274042659 779 18.49600274042659 786 18.803192690881009
		 806 18.803192690881009 819 18.803192690881009 826 18.803192690881009 841 18.803192690881009
		 850 18.803192690881009 864 18.803192690881009;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTA -n "L_UpArm_ctrl_rotateZ";
	rename -uid "94A16954-4183-7EEE-F31E-34A1CC9C16AA";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0 7 0 13 0 217 0 274 0 275 -68.935723775933951
		 388 -68.935723775933951 389 -68.935723775933951 397 -68.935723775933951 398 19.862380959976601
		 408 19.862380959976601 409 19.862380959976601 435 7.8085110739336292 436 -68.935723775933951
		 485 -68.935723775933951 509 -68.935723775933951 510 -68.935723775933951 515 -68.935723775933951
		 516 -68.935723775933951 526 -68.935723775933951 527 -68.935723775933951 535 -68.935723775933951
		 536 -68.935723775933951 544 -68.935723775933951 545 -68.935723775933951 555 -68.935723775933951
		 556 -68.935723775933951 560 -68.935723775933951 561 -68.935723775933951 567 -68.935723775933951
		 568 -73.557422416434065 580 -73.557422416434065 581 -73.557422416434065 596 -73.557422416434065
		 597 -73.557422416434065 613 -73.557422416434065 614 -86.578569982249491 624 -86.578569982249491
		 625 -102.57735299355012 630 -102.57735299355012 631 -66.048864447523727 636 -66.048864447523727
		 637 -51.33643564823398 642 -51.33643564823398 643 -51.33643564823398 647 -51.33643564823398
		 648 -69.041242948194665 656 -69.041242948194665 657 -81.988803126701555 666 -81.988803126701555
		 667 -81.988803126701555 677 -81.988803126701555 681 -98.836974568917157 684 -111.15807782322096
		 687 -111.15807782322096 690 -111.15807782322096 697 -65.519585976879952 707 -111.15807782322096
		 711 -111.15807782322096 716 -111.15807782322096 720 -111.15807782322096 725 -111.15807782322096
		 729 -111.15807782322096 734 -68.935723775933951 740 -55.053039985857666 752 -55.053039985857666
		 759 -40.86553233147464 765 -40.86553233147464 779 -40.86553233147464 786 -68.935723775933951
		 806 -68.935723775933951 819 -68.935723775933951 826 -68.935723775933951 841 -68.935723775933951
		 850 -68.935723775933951 864 -68.935723775933951;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTU -n "L_UpArm_ctrl_scaleX";
	rename -uid "6A7D81E1-4129-36E5-6F51-F7A78D9C6303";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0.99999999999999944 7 0.99999999999999944
		 13 0.99999999999999944 217 0.99999999999999944 274 0.99999999999999944 275 0.99999999999999944
		 388 0.99999999999999944 389 0.99999999999999944 397 0.99999999999999944 398 0.99999999999999944
		 408 0.99999999999999944 409 0.99999999999999944 435 0.99999999999999944 436 0.99999999999999944
		 485 0.99999999999999944 509 0.99999999999999944 510 0.99999999999999944 515 0.99999999999999944
		 516 0.99999999999999944 526 0.99999999999999944 527 0.99999999999999944 535 0.99999999999999944
		 536 0.99999999999999944 544 0.99999999999999944 545 0.99999999999999944 555 0.99999999999999944
		 556 0.99999999999999944 560 0.99999999999999944 561 0.99999999999999944 567 0.99999999999999944
		 568 0.99999999999999944 580 0.99999999999999944 581 0.99999999999999944 596 0.99999999999999944
		 597 0.99999999999999944 613 0.99999999999999944 614 0.99999999999999944 624 0.99999999999999944
		 625 0.99999999999999944 630 0.99999999999999944 631 0.99999999999999944 636 0.99999999999999944
		 637 0.99999999999999944 642 0.99999999999999944 643 0.99999999999999944 647 0.99999999999999944
		 648 0.99999999999999944 656 0.99999999999999944 657 0.99999999999999944 666 0.99999999999999944
		 667 0.99999999999999944 677 0.99999999999999944 681 0.99999999999999944 684 0.99999999999999944
		 687 0.99999999999999944 690 0.99999999999999944 697 0.99999999999999944 707 0.99999999999999944
		 711 0.99999999999999944 716 0.99999999999999944 720 0.99999999999999944 725 0.99999999999999944
		 729 0.99999999999999944 734 0.99999999999999944 740 0.99999999999999944 752 0.99999999999999944
		 759 0.99999999999999944 765 0.99999999999999944 779 0.99999999999999944 786 0.99999999999999944
		 806 0.99999999999999944 819 0.99999999999999944 826 0.99999999999999944 841 0.99999999999999944
		 850 0.99999999999999944 864 0.99999999999999944;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTU -n "L_UpArm_ctrl_scaleY";
	rename -uid "A5E3CB8B-4E51-D1B7-A7F1-DCAC558A9745";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0.99999999999999944 7 0.99999999999999944
		 13 0.99999999999999944 217 0.99999999999999944 274 0.99999999999999944 275 0.99999999999999944
		 388 0.99999999999999944 389 0.99999999999999944 397 0.99999999999999944 398 0.99999999999999944
		 408 0.99999999999999944 409 0.99999999999999944 435 0.99999999999999944 436 0.99999999999999944
		 485 0.99999999999999944 509 0.99999999999999944 510 0.99999999999999944 515 0.99999999999999944
		 516 0.99999999999999944 526 0.99999999999999944 527 0.99999999999999944 535 0.99999999999999944
		 536 0.99999999999999944 544 0.99999999999999944 545 0.99999999999999944 555 0.99999999999999944
		 556 0.99999999999999944 560 0.99999999999999944 561 0.99999999999999944 567 0.99999999999999944
		 568 0.99999999999999944 580 0.99999999999999944 581 0.99999999999999944 596 0.99999999999999944
		 597 0.99999999999999944 613 0.99999999999999944 614 0.99999999999999944 624 0.99999999999999944
		 625 0.99999999999999944 630 0.99999999999999944 631 0.99999999999999944 636 0.99999999999999944
		 637 0.99999999999999944 642 0.99999999999999944 643 0.99999999999999944 647 0.99999999999999944
		 648 0.99999999999999944 656 0.99999999999999944 657 0.99999999999999944 666 0.99999999999999944
		 667 0.99999999999999944 677 0.99999999999999944 681 0.99999999999999944 684 0.99999999999999944
		 687 0.99999999999999944 690 0.99999999999999944 697 0.99999999999999944 707 0.99999999999999944
		 711 0.99999999999999944 716 0.99999999999999944 720 0.99999999999999944 725 0.99999999999999944
		 729 0.99999999999999944 734 0.99999999999999944 740 0.99999999999999944 752 0.99999999999999944
		 759 0.99999999999999944 765 0.99999999999999944 779 0.99999999999999944 786 0.99999999999999944
		 806 0.99999999999999944 819 0.99999999999999944 826 0.99999999999999944 841 0.99999999999999944
		 850 0.99999999999999944 864 0.99999999999999944;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTU -n "L_UpArm_ctrl_scaleZ";
	rename -uid "1C4C6581-47AB-A0FA-7476-91A06E41928C";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 1 7 1 13 1 217 1 274 1 275 1 388 1 389 1
		 397 1 398 1 408 1 409 1 435 1 436 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1
		 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1
		 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1
		 667 1 677 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 786 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTA -n "L_LoArm_ctrl_rotateX";
	rename -uid "91DB973F-4703-23E2-AC70-4E8B0D3FD85E";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 786 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTA -n "L_LoArm_ctrl_rotateY";
	rename -uid "F3CA6006-4DEC-3E78-700A-FFB7D4C0D4D5";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0 7 0 13 0 217 0 274 0 275 123.13579368890716
		 388 123.13579368890716 389 123.13579368890716 397 123.13579368890716 398 32.085636527326102
		 408 32.085636527326102 409 32.085636527326102 435 32.085636527326102 436 123.13579368890716
		 485 123.13579368890716 509 123.13579368890716 510 123.13579368890716 515 123.13579368890716
		 516 123.13579368890716 526 123.13579368890716 527 123.13579368890716 535 123.13579368890716
		 536 123.13579368890716 544 123.13579368890716 545 123.13579368890716 555 123.13579368890716
		 556 123.13579368890716 560 123.13579368890716 561 123.13579368890716 567 123.13579368890716
		 568 64.749196962393711 580 64.749196962393711 581 64.749196962393711 596 64.749196962393711
		 597 64.749196962393711 613 64.749196962393711 614 64.749196962393711 624 64.749196962393711
		 625 64.749196962393711 630 64.749196962393711 631 64.749196962393711 636 64.749196962393711
		 637 40.166840306232217 642 40.166840306232217 643 40.166840306232217 647 40.166840306232217
		 648 54.995665402982858 656 54.995665402982858 657 65.840014175824834 666 65.840014175824834
		 667 65.840014175824834 677 65.840014175824834 681 84.967981105993658 684 98.956303601394751
		 687 98.956303601394751 690 98.956303601394751 697 98.956303601394751 707 98.956303601394751
		 711 98.956303601394751 716 98.956303601394751 720 98.956303601394751 725 98.956303601394751
		 729 98.956303601394751 734 123.13579368890716 740 28.024799697190492 752 39.668238045810249
		 759 46.976040466699899 765 57.181252944863544 779 28.024799697190492 786 123.13579368890716
		 806 123.13579368890716 819 123.13579368890716 826 123.13579368890716 841 123.13579368890716
		 850 123.13579368890716 864 123.13579368890716;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTA -n "L_LoArm_ctrl_rotateZ";
	rename -uid "A42B2375-4B17-9119-F808-36AB76906011";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 786 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTU -n "L_LoArm_ctrl_scaleX";
	rename -uid "18E8A2EC-4456-878C-BC83-BABD50E20867";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 1 7 1 13 1 217 1 274 1 275 1 388 1 389 1
		 397 1 398 1 408 1 409 1 435 1 436 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1
		 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1
		 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1
		 667 1 677 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 786 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTU -n "L_LoArm_ctrl_scaleY";
	rename -uid "E199A2F8-4E3B-9978-FB39-DD818FBB6DE3";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 1 7 1 13 1 217 1 274 1 275 1 388 1 389 1
		 397 1 398 1 408 1 409 1 435 1 436 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1
		 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1
		 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1
		 667 1 677 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 786 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTU -n "L_LoArm_ctrl_scaleZ";
	rename -uid "6B2D10DE-4117-30CE-0296-5AA74E4B25C1";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 1 7 1 13 1 217 1 274 1 275 1 388 1 389 1
		 397 1 398 1 408 1 409 1 435 1 436 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1
		 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1
		 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1
		 667 1 677 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 786 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 76 ".kit[0:75]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[0:75]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode pairBlend -n "pairBlend1";
	rename -uid "2085590C-4CF5-465A-AB96-73A86BD28B96";
	setAttr ".txm" 2;
	setAttr ".tym" 2;
	setAttr ".tzm" 2;
createNode animCurveTA -n "pairBlend1_inRotateX1";
	rename -uid "5A2E3880-469C-5DCE-FD97-2A941B502BC7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0 7 0 13 0 217 0 274 -20.691297886145069
		 275 -20.704599105548066 388 -20.704599105548066 389 -20.704599105548066 397 -20.704599105548066
		 398 -20.704599105548066 408 -20.704599105548066 409 -20.704599105548066 435 -20.704599105548066
		 436 -20.704599105548066 485 -20.704599105548066 509 -20.704599105548066 510 -20.704599105548066
		 515 -20.704599105548066 516 -20.704599105548066 526 -20.704599105548066 527 -20.704599105548066
		 535 -20.704599105548066 536 -20.704599105548066 544 -20.704599105548066 545 -20.704599105548066
		 555 -20.704599105548066 556 -20.704599105548066 560 -20.704599105548066 561 -20.704599105548066
		 567 -20.704599105548066 568 -20.704599105548066 580 -20.704599105548066 581 -20.704599105548066
		 596 -20.704599105548066 597 -20.704599105548066 613 -20.704599105548066 614 -20.704599105548066
		 624 -20.704599105548066 625 -20.704599105548066 630 -20.704599105548066 631 -20.704599105548066
		 636 -20.704599105548066 637 -20.704599105548066 642 -20.704599105548066 643 -20.704599105548066
		 647 -20.704599105548066 648 -20.704599105548066 656 -20.704599105548066 657 -20.704599105548066
		 666 -20.704599105548066 667 -20.704599105548066 681 -20.704599105548066 684 -20.704599105548066
		 687 -20.704599105548066 690 -20.704599105548066 697 -20.704599105548066 704 -20.704599105548066
		 707 -19.520972205940318 711 -20.704599105548066 716 -20.704599105548066 720 -20.704599105548066
		 725 -20.704599105548066 729 -20.704599105548066 734 -20.704599105548066 740 -20.704599105548066
		 752 -20.704599105548066 759 -20.704599105548066 765 -20.704599105548066 779 -20.704599105548066
		 786 -20.704599105548066 806 -20.704599105548066 819 -20.704599105548066 826 -20.704599105548066
		 841 -20.704599105548066 850 -20.704599105548066 864 -20.704599105548066;
	setAttr -s 76 ".kit[69:75]"  3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[1:75]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTA -n "pairBlend1_inRotateY1";
	rename -uid "4BFA3C13-464A-0B75-AFBF-798E9E183693";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 681 0 684 0 687 0 690 0 697 0 704 0 707 7.0388962767341514 711 0 716 0 720 0
		 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 786 0 806 0 819 0 826 0 841 0 850 0
		 864 0;
	setAttr -s 76 ".kit[69:75]"  3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[1:75]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTA -n "pairBlend1_inRotateZ1";
	rename -uid "F9AA5CC5-440D-3160-C90E-48B266EAD47A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 76 ".ktv[0:75]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 681 0 684 0 687 0 690 0 697 0 704 0 707 19.067655963000576 711 0 716 0 720 0
		 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 786 0 806 0 819 0 826 0 841 0 850 0
		 864 0;
	setAttr -s 76 ".kit[69:75]"  3 18 18 18 3 18 18;
	setAttr -s 76 ".kot[1:75]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
createNode animCurveTU -n "L_Wrist_ctrl_scaleX";
	rename -uid "F864BA9B-4C0B-EC17-BF27-EEBC709BFE96";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 77 ".ktv[0:76]"  1 1 7 1 13 1 217 1 274 1 275 1 388 1 389 1
		 397 1 398 1 408 1 409 1 435 1 436 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1
		 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1
		 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1
		 667 1 677 1 681 1 684 1 687 1 690 1 697 1 704 1 707 1 711 1 716 1 720 1 725 1 729 1
		 734 1 740 1 752 1 759 1 765 1 779 1 786 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 77 ".kit[0:76]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 
		18;
	setAttr -s 77 ".kot[0:76]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5;
createNode animCurveTU -n "L_Wrist_ctrl_scaleY";
	rename -uid "91EC40B1-48BA-0D95-AC21-D6A65B166E19";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 77 ".ktv[0:76]"  1 1 7 1 13 1 217 1 274 1 275 1 388 1 389 1
		 397 1 398 1 408 1 409 1 435 1 436 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1
		 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1
		 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1
		 667 1 677 1 681 1 684 1 687 1 690 1 697 1 704 1 707 1 711 1 716 1 720 1 725 1 729 1
		 734 1 740 1 752 1 759 1 765 1 779 1 786 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 77 ".kit[0:76]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 
		18;
	setAttr -s 77 ".kot[0:76]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5;
createNode animCurveTU -n "L_Wrist_ctrl_scaleZ";
	rename -uid "5E2C0761-44B5-9671-6C91-2D8E2B5D31E5";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 77 ".ktv[0:76]"  1 1 7 1 13 1 217 1 274 1 275 1 388 1 389 1
		 397 1 398 1 408 1 409 1 435 1 436 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1
		 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1
		 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1
		 667 1 677 1 681 1 684 1 687 1 690 1 697 1 704 1 707 1 711 1 716 1 720 1 725 1 729 1
		 734 1 740 1 752 1 759 1 765 1 779 1 786 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 77 ".kit[0:76]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 
		18;
	setAttr -s 77 ".kot[0:76]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5;
createNode animCurveTA -n "R_UpArm_ctrl_rotateX";
	rename -uid "66874E4A-493D-B6D7-FCF3-8EAB51841B20";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  1 0 7 0 13 0 217 0 274 0 275 -98.433589405868304
		 388 -98.433589405868304 389 -98.433589405868304 397 -98.433589405868304 398 -98.230154604107028
		 408 -98.230154604107028 409 -98.230154604107028 435 -98.230154604107028 436 -98.433589405868304
		 485 -98.433589405868304 509 -98.433589405868304 510 -98.433589405868304 515 -98.433589405868304
		 516 -98.433589405868304 526 -98.433589405868304 527 -147.65107384478489 535 -147.65107384478489
		 536 -143.82917487484491 544 -143.82917487484491 545 -169.72563237896037 555 -169.72563237896037
		 556 -198.15385224621193 560 -198.15385224621193 561 27.346828974371814 567 27.346828974371814
		 568 27.346828974371814 580 27.346828974371814 581 27.346828974371814 596 27.346828974371814
		 597 27.346828974371814 613 27.346828974371814 614 8.3092211764983333 624 8.3092211764983333
		 625 -5.0819313963053903 630 -5.0819313963053903 631 32.560104412238609 636 32.560104412238609
		 637 48.082288280914597 642 48.082288280914597 643 48.082288280914597 647 48.082288280914597
		 648 52.981465186524531 656 52.981465186524531 657 56.564242831931409 666 56.564242831931409
		 667 56.564242831931409 677 56.564242831931409 681 2.3647156786507706 684 -33.441876025796141
		 687 -33.441876025796141 690 -33.441876025796141 697 -17.44551607673683 707 -33.441876025796141
		 711 -33.441876025796141 716 -33.441876025796141 720 -33.441876025796141 725 -33.441876025796141
		 729 -33.441876025796141 734 -62.040515933053697 740 -70.738225440774002 752 -41.904473507903262
		 759 44.989130278763888 765 52.541851863423375 772 60.869517076337424 779 10.586237669005985
		 786 -8.9452588398861934 793 -9.2805761988899871 800 -11.128314267217702 806 44.612406512203478
		 819 44.612406512203478 826 15.056205855135895 833 -17.691913077597878 841 -98.433589405868304
		 850 -98.433589405868304 864 -98.433589405868304 871 -70.556907351242742;
	setAttr -s 81 ".kit[0:80]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 1 18 18 1 1 1 18 3 3 3 18 18 18 
		3 3 18 18 3;
	setAttr -s 81 ".kot[0:80]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5;
	setAttr -s 81 ".kix[63:80]"  1 1 0.29919524040559742 1 1 1 0.35765259391850168 
		1 1 1 1 1 0.39437006937518065 1 1 1 1 1;
	setAttr -s 81 ".kiy[63:80]"  0 0 0.95419191367284006 0 0 0 -0.93385471143179843 
		0 0 0 0 0 -0.91895171166988698 0 0 0 0 0;
createNode animCurveTA -n "R_UpArm_ctrl_rotateY";
	rename -uid "1EE49102-4E15-81A2-7D0D-3E870CFF1512";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  1 0 7 0 13 0 217 0 274 0 275 40.966316236664099
		 388 40.966316236664099 389 40.966316236664099 397 40.966316236664099 398 45.064138392652922
		 408 45.064138392652922 409 45.064138392652922 435 45.064138392652922 436 40.966316236664099
		 485 40.966316236664099 509 40.966316236664099 510 40.966316236664099 515 40.966316236664099
		 516 40.966316236664099 526 40.966316236664099 527 102.18556212349472 535 102.18556212349472
		 536 103.66543435016263 544 103.66543435016263 545 105.91878240433435 555 105.91878240433435
		 556 106.03442526717835 560 106.03442526717835 561 -8.6921507603120727 567 -8.6921507603120727
		 568 -8.6921507603120727 580 -8.6921507603120727 581 -8.6921507603120727 596 -8.6921507603120727
		 597 -8.6921507603120727 613 -8.6921507603120727 614 27.09644126434242 624 27.09644126434242
		 625 48.580340066970038 630 48.580340066970038 631 9.0514779782398112 636 9.0514779782398112
		 637 15.017857475489709 642 15.017857475489709 643 15.017857475489709 647 15.017857475489709
		 648 14.087307204294268 656 14.087307204294268 657 13.406793972418773 666 13.406793972418773
		 667 13.406793972418773 677 13.406793972418773 681 40.822632567608011 684 58.93474148023234
		 687 58.93474148023234 690 58.93474148023234 697 50.989671215125902 707 58.93474148023234
		 711 58.93474148023234 716 58.93474148023234 720 58.93474148023234 725 58.93474148023234
		 729 58.93474148023234 734 69.147838905306386 740 59.855092367801724 752 77.502132315109179
		 759 48.905584268393746 765 20.971011247311075 772 19.393119922585868 779 21.786768398528775
		 786 29.552546752897474 793 22.667519171033927 800 -15.271845502154722 806 -21.185385694552451
		 819 -21.185385694552451 826 -24.57222867211529 833 1.102626147901892 841 40.966316236664099
		 850 40.966316236664099 864 40.966316236664099 871 40.658306509882316;
	setAttr -s 81 ".kit[0:80]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 1 18 18 1 1 1 18 3 3 3 18 18 18 
		3 3 18 18 3;
	setAttr -s 81 ".kot[0:80]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5;
	setAttr -s 81 ".kix[63:80]"  1 1 1 1 1 1 0.9347954761808408 1 1 1 1 
		1 1 1 1 1 1 1;
	setAttr -s 81 ".kiy[63:80]"  0 0 0 0 0 0 0.35518645485411632 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTA -n "R_UpArm_ctrl_rotateZ";
	rename -uid "2B3F8312-4C70-08E1-35E8-72A774E08132";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 81 ".ktv[0:80]"  1 0 7 0 13 0 217 0 274 0 275 -96.767622580107258
		 388 -96.767622580107258 389 -96.767622580107258 397 -96.767622580107258 398 -101.54943625748778
		 408 -101.54943625748778 409 -101.54943625748778 435 -101.54943625748778 436 -96.767622580107258
		 485 -96.767622580107258 509 -96.767622580107258 510 -96.767622580107258 515 -96.767622580107258
		 516 -96.767622580107258 526 -96.767622580107258 527 -149.75429201152781 535 -149.75429201152781
		 536 -144.77528545110377 544 -144.77528545110377 545 -173.50653103697408 555 -173.50653103697408
		 556 -209.04434289102889 560 -209.04434289102889 561 -65.114488541390173 567 -65.114488541390173
		 568 -65.114488541390173 580 -65.114488541390173 581 -65.114488541390173 596 -65.114488541390173
		 597 -65.114488541390173 613 -65.114488541390173 614 -65.122080066412508 624 -65.122080066412508
		 625 -63.043228152567217 630 -63.043228152567217 631 -54.520238484661895 636 -54.520238484661895
		 637 -29.670603380180097 642 -29.670603380180097 643 -29.670603380180097 647 -29.670603380180097
		 648 -53.763257323650308 656 -53.763257323650308 657 -71.382262236158681 666 -71.382262236158681
		 667 -71.382262236158681 677 -71.382262236158681 681 -114.12949890282562 684 -142.37020376152213
		 687 -142.37020376152213 690 -142.37020376152213 697 -73.241805372062132 707 -142.37020376152213
		 711 -142.37020376152213 716 -142.37020376152213 720 -142.37020376152213 725 -142.37020376152213
		 729 -142.37020376152213 734 -118.02401675767467 740 -122.57257268434509 752 -108.63613021367136
		 759 -42.824394814219914 765 -24.966506721211264 772 -4.9404166053249945 779 -7.8532149903835267
		 786 -26.900738260496141 793 -23.746236007783288 800 -6.3636169979006016 806 7.2712616307031137
		 819 7.2712616307031137 826 -51.927594844127462 833 -61.355062594874084 841 -96.767622580107258
		 850 -96.767622580107258 864 -96.767622580107258 871 -82.247429802193082;
	setAttr -s 81 ".kit[0:80]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 1 18 18 1 1 1 18 3 3 3 18 18 18 
		3 3 18 18 3;
	setAttr -s 81 ".kot[0:80]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5;
	setAttr -s 81 ".kix[63:80]"  1 1 0.48068129320028768 0.70412888663625495 
		0.70412888663625495 1 0.83705231240213129 1 1 1 1 1 0.42735745707906553 1 1 1 1 1;
	setAttr -s 81 ".kiy[63:80]"  0 0 0.87689537253158034 0.71007218717845033 
		0.71007218717845033 0 -0.54712286216374162 0 0 0 0 0 -0.90408274172163838 0 0 0 0 
		0;
createNode animCurveTU -n "R_UpArm_ctrl_scaleX";
	rename -uid "9264E2EB-45AF-F574-BE88-85AE440E789C";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 1 7 1 13 1 217 1 274 1 275 1 388 1 389 1
		 397 1 398 1 408 1 409 1 435 1 436 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1
		 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1
		 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1
		 667 1 677 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 772 1 779 1 786 1 793 1 800 1 806 1 819 1 826 1 841 1 850 1
		 864 1;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 1 18 18 1 1 1 18 3 3 3 18 18 18 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
	setAttr -s 79 ".kix[63:78]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 79 ".kiy[63:78]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_UpArm_ctrl_scaleY";
	rename -uid "8DBFA20D-46DF-6F2C-7056-2A96549C826F";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 1 7 1 13 1 217 1 274 1 275 1 388 1 389 1
		 397 1 398 1 408 1 409 1 435 1 436 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1
		 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1
		 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1
		 667 1 677 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 772 1 779 1 786 1 793 1 800 1 806 1 819 1 826 1 841 1 850 1
		 864 1;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 1 18 18 1 1 1 18 3 3 3 18 18 18 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
	setAttr -s 79 ".kix[63:78]"  1 1 1 1 1 0.70412888663625495 1 1 1 1 1 
		1 1 1 1 1;
	setAttr -s 79 ".kiy[63:78]"  0 0 0 0 0 40.684139474948608 0 0 0 0 0 
		0 0 0 0 0;
createNode animCurveTU -n "R_UpArm_ctrl_scaleZ";
	rename -uid "5722816F-4493-D39D-9C4A-C990FE4CADC9";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 1 7 1 13 1 217 1 274 1 275 1 388 1 389 1
		 397 1 398 1 408 1 409 1 435 1 436 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1
		 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1
		 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1
		 667 1 677 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 772 1 779 1 786 1 793 1 800 1 806 1 819 1 826 1 841 1 850 1
		 864 1;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 1 18 18 1 1 1 18 3 3 3 18 18 18 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
	setAttr -s 79 ".kix[63:78]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 79 ".kiy[63:78]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "R_LoArm_ctrl_rotateX";
	rename -uid "EB25C273-47BD-08E7-9BA1-649D53FDCCC9";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 786 -20.400826158001081 793 -29.603362771711542 800 -80.313165797864642
		 806 -80.313165797864642 819 -80.313165797864642 826 0 833 0 841 0 850 0 864 0;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 3 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "R_LoArm_ctrl_rotateY";
	rename -uid "3492027F-4600-A9F1-6FED-FDB1E19E7D80";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 80 ".ktv[0:79]"  1 0 7 0 13 0 217 0 274 0 275 98.690448784438956
		 388 98.690448784438956 389 98.690448784438956 397 98.690448784438956 398 98.690448784438956
		 408 98.690448784438956 409 98.690448784438956 435 98.690448784438956 436 98.690448784438956
		 485 98.690448784438956 509 98.690448784438956 510 98.690448784438956 515 98.690448784438956
		 516 98.690448784438956 526 98.690448784438956 527 4.7253703829839457 535 4.7253703829839457
		 536 4.7253703829839457 544 4.7253703829839457 545 4.7253703829839457 555 4.7253703829839457
		 556 24.756080338989509 560 24.756080338989509 561 61.628044655507445 567 61.628044655507445
		 568 61.628044655507445 580 61.628044655507445 581 61.628044655507445 596 61.628044655507445
		 597 61.628044655507445 613 61.628044655507445 614 61.628044655507445 624 61.628044655507445
		 625 56.985644236547188 630 56.985644236547188 631 84.98551102897396 636 84.98551102897396
		 637 41.869174324324902 642 41.869174324324902 643 41.869174324324902 647 41.869174324324902
		 648 41.595100208139193 656 41.595100208139193 657 41.394669271356641 666 41.394669271356641
		 667 41.394669271356641 677 41.394669271356641 681 80.793662185764887 684 106.82236994299156
		 687 106.82236994299156 690 106.82236994299156 697 106.82236994299156 707 106.82236994299156
		 711 106.82236994299156 716 106.82236994299156 720 106.82236994299156 725 106.82236994299156
		 729 106.82236994299156 734 40.809128207914036 740 38.172539907454734 752 22.370170710460251
		 759 38.172539907454734 765 38.172539907454734 772 24.631530168757472 779 38.172539907454734
		 786 67.567458816298441 793 80.827108960887983 800 153.8932944326271 806 153.8932944326271
		 819 153.8932944326271 826 98.690448784438956 833 71.51204053226985 841 98.690448784438956
		 850 98.690448784438956 864 98.690448784438956;
	setAttr -s 80 ".kit[0:79]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 3 3 3 18 18 18 
		3 3 18 18;
	setAttr -s 80 ".kot[0:79]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5;
createNode animCurveTA -n "R_LoArm_ctrl_rotateZ";
	rename -uid "3AFBD7C9-4E9E-700E-4DA5-98852960B339";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 786 -21.77027227653862 793 -31.590547502829896 800 -85.704347131284109
		 806 -85.704347131284109 819 -85.704347131284109 826 0 833 0 841 0 850 0 864 0;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 3 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "R_LoArm_ctrl_scaleX";
	rename -uid "71234135-49CC-B6A9-E046-7E93EF798093";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0.99999999999999989 7 0.99999999999999989
		 13 0.99999999999999989 217 0.99999999999999989 274 0.99999999999999989 275 0.99999999999999989
		 388 0.99999999999999989 389 0.99999999999999989 397 0.99999999999999989 398 0.99999999999999989
		 408 0.99999999999999989 409 0.99999999999999989 435 0.99999999999999989 436 0.99999999999999989
		 485 0.99999999999999989 509 0.99999999999999989 510 0.99999999999999989 515 0.99999999999999989
		 516 0.99999999999999989 526 0.99999999999999989 527 0.99999999999999989 535 0.99999999999999989
		 536 0.99999999999999989 544 0.99999999999999989 545 0.99999999999999989 555 0.99999999999999989
		 556 0.99999999999999989 560 0.99999999999999989 561 0.99999999999999989 567 0.99999999999999989
		 568 0.99999999999999989 580 0.99999999999999989 581 0.99999999999999989 596 0.99999999999999989
		 597 0.99999999999999989 613 0.99999999999999989 614 0.99999999999999989 624 0.99999999999999989
		 625 0.99999999999999989 630 0.99999999999999989 631 0.99999999999999989 636 0.99999999999999989
		 637 0.99999999999999989 642 0.99999999999999989 643 0.99999999999999989 647 0.99999999999999989
		 648 0.99999999999999989 656 0.99999999999999989 657 0.99999999999999989 666 0.99999999999999989
		 667 0.99999999999999989 677 0.99999999999999989 681 0.99999999999999989 684 0.99999999999999989
		 687 0.99999999999999989 690 0.99999999999999989 697 0.99999999999999989 707 0.99999999999999989
		 711 0.99999999999999989 716 0.99999999999999989 720 0.99999999999999989 725 0.99999999999999989
		 729 0.99999999999999989 734 0.99999999999999989 740 0.99999999999999989 752 0.99999999999999989
		 759 0.99999999999999989 765 0.99999999999999989 779 0.99999999999999989 786 0.99999999999999989
		 793 0.99999999999999989 800 0.99999999999999989 806 0.99999999999999989 819 0.99999999999999989
		 826 0.99999999999999989 833 0.99999999999999989 841 0.99999999999999989 850 0.99999999999999989
		 864 0.99999999999999989;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 3 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "R_LoArm_ctrl_scaleY";
	rename -uid "F108FC59-4AC1-5ED0-294C-50950755D8C1";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0.99999999999999978 7 0.99999999999999978
		 13 0.99999999999999978 217 0.99999999999999978 274 0.99999999999999978 275 0.99999999999999978
		 388 0.99999999999999978 389 0.99999999999999978 397 0.99999999999999978 398 0.99999999999999978
		 408 0.99999999999999978 409 0.99999999999999978 435 0.99999999999999978 436 0.99999999999999978
		 485 0.99999999999999978 509 0.99999999999999978 510 0.99999999999999978 515 0.99999999999999978
		 516 0.99999999999999978 526 0.99999999999999978 527 0.99999999999999978 535 0.99999999999999978
		 536 0.99999999999999978 544 0.99999999999999978 545 0.99999999999999978 555 0.99999999999999978
		 556 0.99999999999999978 560 0.99999999999999978 561 0.99999999999999978 567 0.99999999999999978
		 568 0.99999999999999978 580 0.99999999999999978 581 0.99999999999999978 596 0.99999999999999978
		 597 0.99999999999999978 613 0.99999999999999978 614 0.99999999999999978 624 0.99999999999999978
		 625 0.99999999999999978 630 0.99999999999999978 631 0.99999999999999978 636 0.99999999999999978
		 637 0.99999999999999978 642 0.99999999999999978 643 0.99999999999999978 647 0.99999999999999978
		 648 0.99999999999999978 656 0.99999999999999978 657 0.99999999999999978 666 0.99999999999999978
		 667 0.99999999999999978 677 0.99999999999999978 681 0.99999999999999978 684 0.99999999999999978
		 687 0.99999999999999978 690 0.99999999999999978 697 0.99999999999999978 707 0.99999999999999978
		 711 0.99999999999999978 716 0.99999999999999978 720 0.99999999999999978 725 0.99999999999999978
		 729 0.99999999999999978 734 0.99999999999999978 740 0.99999999999999978 752 0.99999999999999978
		 759 0.99999999999999978 765 0.99999999999999978 779 0.99999999999999978 786 0.99999999999999978
		 793 0.99999999999999978 800 0.99999999999999978 806 0.99999999999999978 819 0.99999999999999978
		 826 0.99999999999999978 833 0.99999999999999978 841 0.99999999999999978 850 0.99999999999999978
		 864 0.99999999999999978;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 3 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "R_LoArm_ctrl_scaleZ";
	rename -uid "08A59875-48F4-61C9-7F47-908B1DC15A67";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 1 7 1 13 1 217 1 274 1 275 1 388 1 389 1
		 397 1 398 1 408 1 409 1 435 1 436 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1
		 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1
		 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1
		 667 1 677 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 786 1 793 1 800 1 806 1 819 1 826 1 833 1 841 1 850 1
		 864 1;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 3 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "R_Wrist_ctrl_rotateX";
	rename -uid "9CE7AAD9-41F0-4895-FD4A-1282BED0C840";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0 7 0 13 0 217 0 274 0 275 -293.4613966465576
		 388 -293.4613966465576 389 -293.4613966465576 397 -293.4613966465576 398 -293.4613966465576
		 408 -293.4613966465576 409 -293.4613966465576 435 -293.4613966465576 436 -293.4613966465576
		 485 -293.4613966465576 509 -293.4613966465576 510 -293.4613966465576 515 -293.4613966465576
		 516 -293.4613966465576 526 -293.4613966465576 527 26.779166617064714 535 26.779166617064714
		 536 26.779166617064714 544 26.779166617064714 545 26.779166617064714 555 26.779166617064714
		 556 7.6291866041934364 560 7.6291866041934364 561 -293.4613966465576 567 -293.4613966465576
		 568 -293.4613966465576 580 -293.4613966465576 581 -293.4613966465576 596 -293.4613966465576
		 597 -293.4613966465576 613 -293.4613966465576 614 -293.4613966465576 624 -293.4613966465576
		 625 -194.93475983532923 630 -194.93475983532923 631 -194.93475983532923 636 -194.93475983532923
		 637 -194.93475983532923 642 -194.93475983532923 643 -194.93475983532923 647 -194.93475983532923
		 648 -251.84374475733881 656 -251.84374475733881 657 -293.4613966465576 666 -293.4613966465576
		 667 -293.4613966465576 677 -293.4613966465576 681 -243.94859270873081 684 -211.2382565146487
		 687 -211.2382565146487 690 -211.2382565146487 697 -211.2382565146487 703 -211.2382565146487
		 707 -118.49659064477076 711 -211.2382565146487 716 -211.2382565146487 720 -211.2382565146487
		 725 -211.2382565146487 729 -211.2382565146487 734 -188.54602961645023 740 -214.22900436067962
		 752 -212.90484883207651 759 -214.22900436067962 765 -214.22900436067962 779 -214.22900436067962
		 786 -217.82469486479661 793 -219.44666219393494 800 -228.38437656856917 806 -228.38437656856917
		 819 -228.38437656856917 826 -293.4613966465576 841 -293.4613966465576 850 -293.4613966465576
		 864 -293.4613966465576;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "R_Wrist_ctrl_rotateY";
	rename -uid "905C9DF0-40CA-F28F-0EBD-DBAEDC9A959C";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0 7 0 13 0 217 0 274 0 275 136.89456063534092
		 388 136.89456063534092 389 136.89456063534092 397 136.89456063534092 398 136.89456063534092
		 408 136.89456063534092 409 136.89456063534092 435 136.89456063534092 436 136.89456063534092
		 485 136.89456063534092 509 136.89456063534092 510 136.89456063534092 515 136.89456063534092
		 516 136.89456063534092 526 136.89456063534092 527 -4.7442775975348903 535 -4.7442775975348903
		 536 -4.7442775975348903 544 -4.7442775975348903 545 -4.7442775975348903 555 -4.7442775975348903
		 556 10.704568290679795 560 10.704568290679795 561 136.89456063534092 567 136.89456063534092
		 568 136.89456063534092 580 136.89456063534092 581 136.89456063534092 596 136.89456063534092
		 597 136.89456063534092 613 136.89456063534092 614 136.89456063534092 624 136.89456063534092
		 625 179.14635305217703 630 179.14635305217703 631 179.14635305217703 636 179.14635305217703
		 637 179.14635305217703 642 179.14635305217703 643 179.14635305217703 647 179.14635305217703
		 648 154.74171796669748 656 154.74171796669748 657 136.89456063534092 666 136.89456063534092
		 667 136.89456063534092 677 136.89456063534092 681 118.35840189094334 684 106.11260007698259
		 687 106.11260007698259 690 106.11260007698259 697 106.11260007698259 703 106.11260007698259
		 707 119.82494509057331 711 106.11260007698259 716 106.11260007698259 720 106.11260007698259
		 725 106.11260007698259 729 106.11260007698259 734 165.58320096687652 740 172.0587091874909
		 752 166.53237616060187 759 172.0587091874909 765 172.0587091874909 779 172.0587091874909
		 786 180.77801171523865 793 184.7111710949703 800 206.38451462935049 806 206.38451462935049
		 819 206.38451462935049 826 136.89456063534092 841 136.89456063534092 850 136.89456063534092
		 864 136.89456063534092;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "R_Wrist_ctrl_rotateZ";
	rename -uid "46D58EEF-4D5A-4BD9-E7FD-32B72DA36F66";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0 7 0 13 0 217 0 274 0 275 -253.01554842151711
		 388 -253.01554842151711 389 -253.01554842151711 397 -253.01554842151711 398 -253.01554842151711
		 408 -253.01554842151711 409 -253.01554842151711 435 -253.01554842151711 436 -253.01554842151711
		 485 -253.01554842151711 509 -253.01554842151711 510 -253.01554842151711 515 -253.01554842151711
		 516 -253.01554842151711 526 -253.01554842151711 527 41.36265519563171 535 41.36265519563171
		 536 41.36265519563171 544 41.36265519563171 545 41.36265519563171 555 41.36265519563171
		 556 17.261920707676282 560 17.261920707676282 561 -253.01554842151711 567 -253.01554842151711
		 568 -253.01554842151711 580 -253.01554842151711 581 -253.01554842151711 596 -253.01554842151711
		 597 -253.01554842151711 613 -253.01554842151711 614 -253.01554842151711 624 -253.01554842151711
		 625 -163.94395126116058 630 -163.94395126116058 631 -163.94395126116058 636 -163.94395126116058
		 637 -163.94395126116058 642 -163.94395126116058 643 -163.94395126116058 647 -163.94395126116058
		 648 -215.39170532882378 656 -215.39170532882378 657 -253.01554842151711 666 -253.01554842151711
		 667 -253.01554842151711 677 -253.01554842151711 681 -213.57685429653057 684 -187.52191817326428
		 687 -187.52191817326428 690 -187.52191817326428 697 -187.52191817326428 703 -187.52191817326428
		 707 -90.475496273998502 711 -187.52191817326428 716 -187.52191817326428 720 -187.52191817326428
		 725 -187.52191817326428 729 -187.52191817326428 734 -159.43485031372364 740 -182.8437000995996
		 752 -182.90405124971969 759 -182.8437000995996 765 -182.8437000995996 779 -182.8437000995996
		 786 -162.82068089509343 793 -153.78856788558042 800 -104.01786977828024 806 -104.01786977828024
		 819 -104.01786977828024 826 -253.01554842151711 841 -253.01554842151711 850 -253.01554842151711
		 864 -253.01554842151711;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "R_Wrist_ctrl_scaleX";
	rename -uid "93B93CA8-4D4B-7D19-3013-BE8E823B8BD0";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0.99999999999999989 7 0.99999999999999989
		 13 0.99999999999999989 217 0.99999999999999989 274 0.99999999999999989 275 0.99999999999999989
		 388 0.99999999999999989 389 0.99999999999999989 397 0.99999999999999989 398 0.99999999999999989
		 408 0.99999999999999989 409 0.99999999999999989 435 0.99999999999999989 436 0.99999999999999989
		 485 0.99999999999999989 509 0.99999999999999989 510 0.99999999999999989 515 0.99999999999999989
		 516 0.99999999999999989 526 0.99999999999999989 527 0.99999999999999989 535 0.99999999999999989
		 536 0.99999999999999989 544 0.99999999999999989 545 0.99999999999999989 555 0.99999999999999989
		 556 0.99999999999999989 560 0.99999999999999989 561 0.99999999999999989 567 0.99999999999999989
		 568 0.99999999999999989 580 0.99999999999999989 581 0.99999999999999989 596 0.99999999999999989
		 597 0.99999999999999989 613 0.99999999999999989 614 0.99999999999999989 624 0.99999999999999989
		 625 0.99999999999999989 630 0.99999999999999989 631 0.99999999999999989 636 0.99999999999999989
		 637 0.99999999999999989 642 0.99999999999999989 643 0.99999999999999989 647 0.99999999999999989
		 648 0.99999999999999989 656 0.99999999999999989 657 0.99999999999999989 666 0.99999999999999989
		 667 0.99999999999999989 677 0.99999999999999989 681 0.99999999999999989 684 0.99999999999999989
		 687 0.99999999999999989 690 0.99999999999999989 697 0.99999999999999989 703 0.99999999999999989
		 707 0.99999999999999989 711 0.99999999999999989 716 0.99999999999999989 720 0.99999999999999989
		 725 0.99999999999999989 729 0.99999999999999989 734 0.99999999999999989 740 0.99999999999999989
		 752 0.99999999999999989 759 0.99999999999999989 765 0.99999999999999989 779 0.99999999999999989
		 786 0.99999999999999989 793 0.99999999999999989 800 0.99999999999999989 806 0.99999999999999989
		 819 0.99999999999999989 826 0.99999999999999989 841 0.99999999999999989 850 0.99999999999999989
		 864 0.99999999999999989;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "R_Wrist_ctrl_scaleY";
	rename -uid "EE37C0C3-4585-A31C-F347-319BB4C8CCA8";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 0.99999999999999978 7 0.99999999999999978
		 13 0.99999999999999978 217 0.99999999999999978 274 0.99999999999999978 275 0.99999999999999978
		 388 0.99999999999999978 389 0.99999999999999978 397 0.99999999999999978 398 0.99999999999999978
		 408 0.99999999999999978 409 0.99999999999999978 435 0.99999999999999978 436 0.99999999999999978
		 485 0.99999999999999978 509 0.99999999999999978 510 0.99999999999999978 515 0.99999999999999978
		 516 0.99999999999999978 526 0.99999999999999978 527 0.99999999999999978 535 0.99999999999999978
		 536 0.99999999999999978 544 0.99999999999999978 545 0.99999999999999978 555 0.99999999999999978
		 556 0.99999999999999978 560 0.99999999999999978 561 0.99999999999999978 567 0.99999999999999978
		 568 0.99999999999999978 580 0.99999999999999978 581 0.99999999999999978 596 0.99999999999999978
		 597 0.99999999999999978 613 0.99999999999999978 614 0.99999999999999978 624 0.99999999999999978
		 625 0.99999999999999978 630 0.99999999999999978 631 0.99999999999999978 636 0.99999999999999978
		 637 0.99999999999999978 642 0.99999999999999978 643 0.99999999999999978 647 0.99999999999999978
		 648 0.99999999999999978 656 0.99999999999999978 657 0.99999999999999978 666 0.99999999999999978
		 667 0.99999999999999978 677 0.99999999999999978 681 0.99999999999999978 684 0.99999999999999978
		 687 0.99999999999999978 690 0.99999999999999978 697 0.99999999999999978 703 0.99999999999999978
		 707 0.99999999999999978 711 0.99999999999999978 716 0.99999999999999978 720 0.99999999999999978
		 725 0.99999999999999978 729 0.99999999999999978 734 0.99999999999999978 740 0.99999999999999978
		 752 0.99999999999999978 759 0.99999999999999978 765 0.99999999999999978 779 0.99999999999999978
		 786 0.99999999999999978 793 0.99999999999999978 800 0.99999999999999978 806 0.99999999999999978
		 819 0.99999999999999978 826 0.99999999999999978 841 0.99999999999999978 850 0.99999999999999978
		 864 0.99999999999999978;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTU -n "R_Wrist_ctrl_scaleZ";
	rename -uid "00859316-44F8-96E0-7DA1-44B4F7D2A8C8";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 79 ".ktv[0:78]"  1 1 7 1 13 1 217 1 274 1 275 1 388 1 389 1
		 397 1 398 1 408 1 409 1 435 1 436 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1
		 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1
		 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1
		 667 1 677 1 681 1 684 1 687 1 690 1 697 1 703 1 707 1 711 1 716 1 720 1 725 1 729 1
		 734 1 740 1 752 1 759 1 765 1 779 1 786 1 793 1 800 1 806 1 819 1 826 1 841 1 850 1
		 864 1;
	setAttr -s 79 ".kit[0:78]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 3 3 18 18 18 
		3 18 18;
	setAttr -s 79 ".kot[0:78]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5;
createNode animCurveTA -n "L_IK_Hand_ctrl_rotateX";
	rename -uid "72132B80-493D-1E76-CBBF-0AAF9EF6A1A6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  1 -0.064715742131998585 7 -0.064715742131998585
		 13 -0.064715742131998585 21 -0.064715742131998585 103 -0.064715742131998585 217 -0.064715742131998585
		 275 -0.064715742131998585 485 -0.064715742131998585 509 -0.064715742131998585 510 -0.064715742131998585
		 515 -0.064715742131998585 516 -0.064715742131998585 526 -0.064715742131998585 527 -0.064715742131998585
		 535 -0.064715742131998585 536 -0.064715742131998585 544 -0.064715742131998585 545 -0.064715742131998585
		 555 -0.064715742131998585 556 -0.064715742131998585 560 -0.064715742131998585 561 -0.064715742131998585
		 567 -0.064715742131998585 568 -0.064715742131998585 580 -0.064715742131998585 581 -0.064715742131998585
		 596 -0.064715742131998585 597 -0.064715742131998585 613 -0.064715742131998585 614 -0.064715742131998585
		 624 -0.064715742131998585 625 -0.064715742131998585 630 -0.064715742131998585 631 -0.064715742131998585
		 636 -0.064715742131998585 637 -0.064715742131998585 642 -0.064715742131998585 643 -0.064715742131998585
		 647 -0.064715742131998585 648 -0.064715742131998585 656 -0.064715742131998585 657 -0.064715742131998585
		 666 -0.064715742131998585 667 -0.064715742131998585 681 -0.064715742131998585 684 -0.064715742131998585
		 687 -0.064715742131998585 690 -0.064715742131998585 697 -0.064715742131998585 707 -0.064715742131998585
		 711 -0.064715742131998585 716 -0.064715742131998585 720 -0.064715742131998585 725 -0.064715742131998585
		 729 -0.064715742131998585 734 -0.064715742131998585 740 -0.064715742131998585 752 -0.064715742131998585
		 759 -0.064715742131998585 765 -0.064715742131998585 779 -0.064715742131998585 806 -0.064715742131998585
		 819 -0.064715742131998585 826 -0.064715742131998585 841 -0.064715742131998585 850 -0.064715742131998585
		 864 -0.064715742131998585 888 -0.064715742131998585;
	setAttr -s 68 ".kit[64:67]"  3 18 18 18;
	setAttr -s 68 ".kot[1:67]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
createNode animCurveTA -n "L_IK_Hand_ctrl_rotateY";
	rename -uid "9CF59574-4A44-5AC8-791F-0FB91FD03C2C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  1 -316.815073 7 -316.815073 13 -316.815073
		 21 -316.815073 103 -316.815073 217 -316.815073 275 -316.815073 485 -316.815073 509 -316.815073
		 510 -316.815073 515 -316.815073 516 -316.815073 526 -316.815073 527 -316.815073 535 -316.815073
		 536 -316.815073 544 -316.815073 545 -316.815073 555 -316.815073 556 -316.815073 560 -316.815073
		 561 -316.815073 567 -316.815073 568 -316.815073 580 -316.815073 581 -316.815073 596 -316.815073
		 597 -316.815073 613 -316.815073 614 -316.815073 624 -316.815073 625 -316.815073 630 -316.815073
		 631 -316.815073 636 -316.815073 637 -316.815073 642 -316.815073 643 -316.815073 647 -316.815073
		 648 -316.815073 656 -316.815073 657 -316.815073 666 -316.815073 667 -316.815073 681 -316.815073
		 684 -316.815073 687 -316.815073 690 -316.815073 697 -316.815073 707 -316.815073 711 -316.815073
		 716 -316.815073 720 -316.815073 725 -316.815073 729 -316.815073 734 -316.815073 740 -316.815073
		 752 -316.815073 759 -316.815073 765 -316.815073 779 -316.815073 806 -316.815073 819 -316.815073
		 826 -316.815073 841 -316.815073 850 -316.815073 864 -316.815073 888 -316.815073;
	setAttr -s 68 ".kit[64:67]"  3 18 18 18;
	setAttr -s 68 ".kot[1:67]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
createNode animCurveTA -n "L_IK_Hand_ctrl_rotateZ";
	rename -uid "49B27A45-413D-B7E5-E5E7-609808B0B332";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  1 -0.025610098002168038 7 -0.025610098002168038
		 13 -0.025610098002168038 21 -0.025610098002168038 103 -0.025610098002168038 217 -0.025610098002168038
		 275 -0.025610098002168038 485 -0.025610098002168038 509 -0.025610098002168038 510 -0.025610098002168038
		 515 -0.025610098002168038 516 -0.025610098002168038 526 -0.025610098002168038 527 -0.025610098002168038
		 535 -0.025610098002168038 536 -0.025610098002168038 544 -0.025610098002168038 545 -0.025610098002168038
		 555 -0.025610098002168038 556 -0.025610098002168038 560 -0.025610098002168038 561 -0.025610098002168038
		 567 -0.025610098002168038 568 -0.025610098002168038 580 -0.025610098002168038 581 -0.025610098002168038
		 596 -0.025610098002168038 597 -0.025610098002168038 613 -0.025610098002168038 614 -0.025610098002168038
		 624 -0.025610098002168038 625 -0.025610098002168038 630 -0.025610098002168038 631 -0.025610098002168038
		 636 -0.025610098002168038 637 -0.025610098002168038 642 -0.025610098002168038 643 -0.025610098002168038
		 647 -0.025610098002168038 648 -0.025610098002168038 656 -0.025610098002168038 657 -0.025610098002168038
		 666 -0.025610098002168038 667 -0.025610098002168038 681 -0.025610098002168038 684 -0.025610098002168038
		 687 -0.025610098002168038 690 -0.025610098002168038 697 -0.025610098002168038 707 -0.025610098002168038
		 711 -0.025610098002168038 716 -0.025610098002168038 720 -0.025610098002168038 725 -0.025610098002168038
		 729 -0.025610098002168038 734 -0.025610098002168038 740 -0.025610098002168038 752 -0.025610098002168038
		 759 -0.025610098002168038 765 -0.025610098002168038 779 -0.025610098002168038 806 -0.025610098002168038
		 819 -0.025610098002168038 826 -0.025610098002168038 841 -0.025610098002168038 850 -0.025610098002168038
		 864 -0.025610098002168038 888 -0.025610098002168038;
	setAttr -s 68 ".kit[64:67]"  3 18 18 18;
	setAttr -s 68 ".kot[1:67]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
createNode animCurveTU -n "L_IK_Hand_ctrl_scaleX";
	rename -uid "2470654F-422E-C635-100A-B5A4E3E30BCD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  1 1 7 1 13 1 21 1 103 1 217 1 275 1 485 1
		 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1
		 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1
		 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1
		 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1
		 841 1 850 1 864 1 888 1;
	setAttr -s 68 ".kit[64:67]"  3 18 18 18;
	setAttr -s 68 ".kot[1:67]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
createNode animCurveTU -n "L_IK_Hand_ctrl_scaleY";
	rename -uid "5F4E9BBA-4E9B-8A31-4E85-C0B1A1FC8FEE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  1 1 7 1 13 1 21 1 103 1 217 1 275 1 485 1
		 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1
		 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1
		 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1
		 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1
		 841 1 850 1 864 1 888 1;
	setAttr -s 68 ".kit[64:67]"  3 18 18 18;
	setAttr -s 68 ".kot[1:67]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
createNode animCurveTU -n "L_IK_Hand_ctrl_scaleZ";
	rename -uid "466E3E43-4A8C-5485-F610-92A06AA96FD9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  1 1 7 1 13 1 21 1 103 1 217 1 275 1 485 1
		 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1
		 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1
		 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1
		 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1
		 841 1 850 1 864 1 888 1;
	setAttr -s 68 ".kit[64:67]"  3 18 18 18;
	setAttr -s 68 ".kot[1:67]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
createNode animCurveTA -n "L_IK_Elbow_ctrl_rotateX";
	rename -uid "3E87BC47-4D81-78D0-0FE6-F7B5BF905FBB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "L_IK_Elbow_ctrl_rotateY";
	rename -uid "5B6E9B8B-45AC-8C7E-BBF8-51AB84DCA922";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "L_IK_Elbow_ctrl_rotateZ";
	rename -uid "B2241308-47B5-0EB3-5A39-81B9C7992C1E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "L_IK_Elbow_ctrl_scaleX";
	rename -uid "32747922-4BCB-E74B-7D7A-96AA68D19D5D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "L_IK_Elbow_ctrl_scaleY";
	rename -uid "C2F5BF37-4506-513D-343F-D381606B5BAA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "L_IK_Elbow_ctrl_scaleZ";
	rename -uid "7457DBA4-49F5-4F72-CFF6-ACB27C8C4657";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "R_IK_Hand_ctrl_rotateX";
	rename -uid "C3CD6FD9-4FBA-0A30-E5A2-F19F3886C9E6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 -0.014968310620553218 7 -0.014968310620553218
		 13 -0.014968310620553218 17 -0.014968310620553218 21 -0.014968310620553218 103 -0.014968310620553218
		 217 -0.014968310620553218 275 -0.014968310620553218 485 -0.014968310620553218 509 -0.014968310620553218
		 510 -0.014968310620553218 515 -0.014968310620553218 516 -0.014968310620553218 526 -0.014968310620553218
		 527 -0.014968310620553218 535 -0.014968310620553218 536 -0.014968310620553218 544 -0.014968310620553218
		 545 -0.014968310620553218 555 -0.014968310620553218 556 -0.014968310620553218 560 -0.014968310620553218
		 561 -0.014968310620553218 567 -0.014968310620553218 568 -0.014968310620553218 580 -0.014968310620553218
		 581 -0.014968310620553218 596 -0.014968310620553218 597 -0.014968310620553218 613 -0.014968310620553218
		 614 -0.014968310620553218 624 -0.014968310620553218 625 -0.014968310620553218 630 -0.014968310620553218
		 631 -0.014968310620553218 636 -0.014968310620553218 637 -0.014968310620553218 642 -0.014968310620553218
		 643 -0.014968310620553218 647 -0.014968310620553218 648 -0.014968310620553218 656 -0.014968310620553218
		 657 -0.014968310620553218 666 -0.014968310620553218 667 -0.014968310620553218 681 -0.014968310620553218
		 684 -0.014968310620553218 687 -0.014968310620553218 690 -0.014968310620553218 697 -0.014968310620553218
		 707 -0.014968310620553218 711 -0.014968310620553218 716 -0.014968310620553218 720 -0.014968310620553218
		 725 -0.014968310620553218 729 -0.014968310620553218 734 -0.014968310620553218 740 -0.014968310620553218
		 752 -0.014968310620553218 759 -0.014968310620553218 765 -0.014968310620553218 779 -0.014968310620553218
		 806 -0.014968310620553218 819 -0.014968310620553218 826 -0.014968310620553218 841 -0.014968310620553218
		 850 -0.014968310620553218 864 -0.014968310620553218 888 -0.014968310620553218;
	setAttr -s 69 ".kit[65:68]"  3 18 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
createNode animCurveTA -n "R_IK_Hand_ctrl_rotateY";
	rename -uid "326C4E1A-4D5B-2629-1372-A19176B3A538";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 -43.393884349616137 7 -43.393884349616137
		 13 -43.393884349616137 17 -43.393884349616137 21 -43.393884349616137 103 -43.393884349616137
		 217 -43.393884349616137 275 -43.393884349616137 485 -43.393884349616137 509 -43.393884349616137
		 510 -43.393884349616137 515 -43.393884349616137 516 -43.393884349616137 526 -43.393884349616137
		 527 -43.393884349616137 535 -43.393884349616137 536 -43.393884349616137 544 -43.393884349616137
		 545 -43.393884349616137 555 -43.393884349616137 556 -43.393884349616137 560 -43.393884349616137
		 561 -43.393884349616137 567 -43.393884349616137 568 -43.393884349616137 580 -43.393884349616137
		 581 -43.393884349616137 596 -43.393884349616137 597 -43.393884349616137 613 -43.393884349616137
		 614 -43.393884349616137 624 -43.393884349616137 625 -43.393884349616137 630 -43.393884349616137
		 631 -43.393884349616137 636 -43.393884349616137 637 -43.393884349616137 642 -43.393884349616137
		 643 -43.393884349616137 647 -43.393884349616137 648 -43.393884349616137 656 -43.393884349616137
		 657 -43.393884349616137 666 -43.393884349616137 667 -43.393884349616137 681 -43.393884349616137
		 684 -43.393884349616137 687 -43.393884349616137 690 -43.393884349616137 697 -43.393884349616137
		 707 -43.393884349616137 711 -43.393884349616137 716 -43.393884349616137 720 -43.393884349616137
		 725 -43.393884349616137 729 -43.393884349616137 734 -43.393884349616137 740 -43.393884349616137
		 752 -43.393884349616137 759 -43.393884349616137 765 -43.393884349616137 779 -43.393884349616137
		 806 -43.393884349616137 819 -43.393884349616137 826 -43.393884349616137 841 -43.393884349616137
		 850 -43.393884349616137 864 -43.393884349616137 888 -43.393884349616137;
	setAttr -s 69 ".kit[65:68]"  3 18 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
createNode animCurveTA -n "R_IK_Hand_ctrl_rotateZ";
	rename -uid "B558032F-496C-4D52-DDD8-E5A44DABBD60";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 -0.046256124349695497 7 -0.046256124349695497
		 13 -0.046256124349695497 17 -0.046256124349695497 21 -0.046256124349695497 103 -0.046256124349695497
		 217 -0.046256124349695497 275 -0.046256124349695497 485 -0.046256124349695497 509 -0.046256124349695497
		 510 -0.046256124349695497 515 -0.046256124349695497 516 -0.046256124349695497 526 -0.046256124349695497
		 527 -0.046256124349695497 535 -0.046256124349695497 536 -0.046256124349695497 544 -0.046256124349695497
		 545 -0.046256124349695497 555 -0.046256124349695497 556 -0.046256124349695497 560 -0.046256124349695497
		 561 -0.046256124349695497 567 -0.046256124349695497 568 -0.046256124349695497 580 -0.046256124349695497
		 581 -0.046256124349695497 596 -0.046256124349695497 597 -0.046256124349695497 613 -0.046256124349695497
		 614 -0.046256124349695497 624 -0.046256124349695497 625 -0.046256124349695497 630 -0.046256124349695497
		 631 -0.046256124349695497 636 -0.046256124349695497 637 -0.046256124349695497 642 -0.046256124349695497
		 643 -0.046256124349695497 647 -0.046256124349695497 648 -0.046256124349695497 656 -0.046256124349695497
		 657 -0.046256124349695497 666 -0.046256124349695497 667 -0.046256124349695497 681 -0.046256124349695497
		 684 -0.046256124349695497 687 -0.046256124349695497 690 -0.046256124349695497 697 -0.046256124349695497
		 707 -0.046256124349695497 711 -0.046256124349695497 716 -0.046256124349695497 720 -0.046256124349695497
		 725 -0.046256124349695497 729 -0.046256124349695497 734 -0.046256124349695497 740 -0.046256124349695497
		 752 -0.046256124349695497 759 -0.046256124349695497 765 -0.046256124349695497 779 -0.046256124349695497
		 806 -0.046256124349695497 819 -0.046256124349695497 826 -0.046256124349695497 841 -0.046256124349695497
		 850 -0.046256124349695497 864 -0.046256124349695497 888 -0.046256124349695497;
	setAttr -s 69 ".kit[65:68]"  3 18 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
createNode animCurveTU -n "R_IK_Hand_ctrl_scaleX";
	rename -uid "128E2EC3-4D93-1BB0-2555-50A8C3FA31FE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 1 7 1 13 1 17 1 21 1 103 1 217 1 275 1
		 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1
		 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1
		 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1
		 707 1 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1
		 826 1 841 1 850 1 864 1 888 1;
	setAttr -s 69 ".kit[65:68]"  3 18 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
createNode animCurveTU -n "R_IK_Hand_ctrl_scaleY";
	rename -uid "F2A3C8CC-4C8F-14E2-626B-339A019DF420";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 1.0000000000000002 7 1.0000000000000002
		 13 1.0000000000000002 17 1.0000000000000002 21 1.0000000000000002 103 1.0000000000000002
		 217 1.0000000000000002 275 1.0000000000000002 485 1.0000000000000002 509 1.0000000000000002
		 510 1.0000000000000002 515 1.0000000000000002 516 1.0000000000000002 526 1.0000000000000002
		 527 1.0000000000000002 535 1.0000000000000002 536 1.0000000000000002 544 1.0000000000000002
		 545 1.0000000000000002 555 1.0000000000000002 556 1.0000000000000002 560 1.0000000000000002
		 561 1.0000000000000002 567 1.0000000000000002 568 1.0000000000000002 580 1.0000000000000002
		 581 1.0000000000000002 596 1.0000000000000002 597 1.0000000000000002 613 1.0000000000000002
		 614 1.0000000000000002 624 1.0000000000000002 625 1.0000000000000002 630 1.0000000000000002
		 631 1.0000000000000002 636 1.0000000000000002 637 1.0000000000000002 642 1.0000000000000002
		 643 1.0000000000000002 647 1.0000000000000002 648 1.0000000000000002 656 1.0000000000000002
		 657 1.0000000000000002 666 1.0000000000000002 667 1.0000000000000002 681 1.0000000000000002
		 684 1.0000000000000002 687 1.0000000000000002 690 1.0000000000000002 697 1.0000000000000002
		 707 1.0000000000000002 711 1.0000000000000002 716 1.0000000000000002 720 1.0000000000000002
		 725 1.0000000000000002 729 1.0000000000000002 734 1.0000000000000002 740 1.0000000000000002
		 752 1.0000000000000002 759 1.0000000000000002 765 1.0000000000000002 779 1.0000000000000002
		 806 1.0000000000000002 819 1.0000000000000002 826 1.0000000000000002 841 1.0000000000000002
		 850 1.0000000000000002 864 1.0000000000000002 888 1.0000000000000002;
	setAttr -s 69 ".kit[65:68]"  3 18 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
createNode animCurveTU -n "R_IK_Hand_ctrl_scaleZ";
	rename -uid "4C4001B3-4CED-0B11-2BE1-749515F971DB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 1 7 1 13 1 17 1 21 1 103 1 217 1 275 1
		 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1
		 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1
		 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1
		 707 1 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1
		 826 1 841 1 850 1 864 1 888 1;
	setAttr -s 69 ".kit[65:68]"  3 18 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
createNode animCurveTA -n "R_IK_Elbow_ctrl_rotateX";
	rename -uid "58DE93D6-46E9-0CFA-DFBE-D4A35EE4E03B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "R_IK_Elbow_ctrl_rotateY";
	rename -uid "1802D8BE-415F-3084-56D4-39A4AA17D193";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "R_IK_Elbow_ctrl_rotateZ";
	rename -uid "301DD58C-4EC7-24AC-6B11-8EA7D7CBBB2B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "R_IK_Elbow_ctrl_scaleX";
	rename -uid "03FB225F-4DC6-EE01-97A0-0DB8537FBD35";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0.99999999999999989 7 0.99999999999999989
		 13 0.99999999999999989 217 0.99999999999999989 275 0.99999999999999989 485 0.99999999999999989
		 509 0.99999999999999989 510 0.99999999999999989 515 0.99999999999999989 516 0.99999999999999989
		 526 0.99999999999999989 527 0.99999999999999989 535 0.99999999999999989 536 0.99999999999999989
		 544 0.99999999999999989 545 0.99999999999999989 555 0.99999999999999989 556 0.99999999999999989
		 560 0.99999999999999989 561 0.99999999999999989 567 0.99999999999999989 568 0.99999999999999989
		 580 0.99999999999999989 581 0.99999999999999989 596 0.99999999999999989 597 0.99999999999999989
		 613 0.99999999999999989 614 0.99999999999999989 624 0.99999999999999989 625 0.99999999999999989
		 630 0.99999999999999989 631 0.99999999999999989 636 0.99999999999999989 637 0.99999999999999989
		 642 0.99999999999999989 643 0.99999999999999989 647 0.99999999999999989 648 0.99999999999999989
		 656 0.99999999999999989 657 0.99999999999999989 666 0.99999999999999989 667 0.99999999999999989
		 681 0.99999999999999989 684 0.99999999999999989 687 0.99999999999999989 690 0.99999999999999989
		 697 0.99999999999999989 707 0.99999999999999989 711 0.99999999999999989 716 0.99999999999999989
		 720 0.99999999999999989 725 0.99999999999999989 729 0.99999999999999989 734 0.99999999999999989
		 740 0.99999999999999989 752 0.99999999999999989 759 0.99999999999999989 765 0.99999999999999989
		 779 0.99999999999999989 806 0.99999999999999989 819 0.99999999999999989 826 0.99999999999999989
		 841 0.99999999999999989 850 0.99999999999999989 864 0.99999999999999989 888 0.99999999999999989;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "R_IK_Elbow_ctrl_scaleY";
	rename -uid "63904F73-4E74-566C-C091-5B9D17D90FE1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "R_IK_Elbow_ctrl_scaleZ";
	rename -uid "11412B40-4CEE-24D4-3D98-72A273DD9F39";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0.99999999999999989 7 0.99999999999999989
		 13 0.99999999999999989 217 0.99999999999999989 275 0.99999999999999989 485 0.99999999999999989
		 509 0.99999999999999989 510 0.99999999999999989 515 0.99999999999999989 516 0.99999999999999989
		 526 0.99999999999999989 527 0.99999999999999989 535 0.99999999999999989 536 0.99999999999999989
		 544 0.99999999999999989 545 0.99999999999999989 555 0.99999999999999989 556 0.99999999999999989
		 560 0.99999999999999989 561 0.99999999999999989 567 0.99999999999999989 568 0.99999999999999989
		 580 0.99999999999999989 581 0.99999999999999989 596 0.99999999999999989 597 0.99999999999999989
		 613 0.99999999999999989 614 0.99999999999999989 624 0.99999999999999989 625 0.99999999999999989
		 630 0.99999999999999989 631 0.99999999999999989 636 0.99999999999999989 637 0.99999999999999989
		 642 0.99999999999999989 643 0.99999999999999989 647 0.99999999999999989 648 0.99999999999999989
		 656 0.99999999999999989 657 0.99999999999999989 666 0.99999999999999989 667 0.99999999999999989
		 681 0.99999999999999989 684 0.99999999999999989 687 0.99999999999999989 690 0.99999999999999989
		 697 0.99999999999999989 707 0.99999999999999989 711 0.99999999999999989 716 0.99999999999999989
		 720 0.99999999999999989 725 0.99999999999999989 729 0.99999999999999989 734 0.99999999999999989
		 740 0.99999999999999989 752 0.99999999999999989 759 0.99999999999999989 765 0.99999999999999989
		 779 0.99999999999999989 806 0.99999999999999989 819 0.99999999999999989 826 0.99999999999999989
		 841 0.99999999999999989 850 0.99999999999999989 864 0.99999999999999989 888 0.99999999999999989;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "M_eye_ctrl_visibility";
	rename -uid "ECC54907-493E-B2D4-EA2D-FE9029D106B5";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 84 ".ktv[0:83]"  1 1 7 1 13 1 21 1 89 1 97 1 153 1 155 1
		 158 1 159 1 160 1 161 1 196 1 202 1 215 1 220 1 253 1 254 1 274 1 275 1 388 1 389 1
		 435 1 436 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1
		 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1
		 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1
		 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1
		 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 84 ".kit[0:83]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 9 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 1 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 9 18 18;
	setAttr -s 84 ".kot[0:83]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
	setAttr -s 84 ".kix[65:83]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 84 ".kiy[65:83]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "M_eye_ctrl_rotateX";
	rename -uid "6F1B557F-4500-EFF9-1C5B-209B605B3B1E";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 85 ".ktv[0:84]"  1 0 7 0 13 0 21 0 89 0 93 0 97 0 153 0 155 0
		 158 0 159 0 160 0 161 0 196 0 202 0 215 0 220 0 253 0 254 0 274 0 275 0 388 0 389 0
		 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0
		 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0
		 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0
		 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0
		 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 85 ".kit[0:84]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 3 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 3 18 18;
	setAttr -s 85 ".kot[0:84]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 85 ".kix[66:84]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 85 ".kiy[66:84]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "M_eye_ctrl_rotateY";
	rename -uid "8ED96BD2-48AF-D147-EFFC-97963E64F081";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 85 ".ktv[0:84]"  1 0 7 0 13 0 21 0 89 0 93 0 97 0 153 0 155 0
		 158 0 159 0 160 0 161 0 196 0 202 0 215 0 220 0 253 0 254 0 274 0 275 0 388 0 389 0
		 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0
		 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0
		 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0
		 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0
		 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 85 ".kit[0:84]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 3 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 3 18 18;
	setAttr -s 85 ".kot[0:84]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 85 ".kix[66:84]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 85 ".kiy[66:84]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "M_eye_ctrl_rotateZ";
	rename -uid "B2A25562-4A36-6C70-DCE8-3CAAE420A4FC";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 85 ".ktv[0:84]"  1 0 7 0 13 0 21 0 89 0 93 0 97 0 153 0 155 0
		 158 0 159 0 160 0 161 0 196 0 202 0 215 0 220 0 253 0 254 0 274 0 275 0 388 0 389 0
		 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0
		 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0
		 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0
		 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0
		 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 85 ".kit[0:84]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 3 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 3 18 18;
	setAttr -s 85 ".kot[0:84]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 85 ".kix[66:84]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 85 ".kiy[66:84]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_eye_ctrl_scaleX";
	rename -uid "E7376718-49E1-BF5C-6E78-B1986834E111";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 84 ".ktv[0:83]"  1 1 7 1 13 1 21 1 89 1 97 1 153 1 155 1
		 158 1 159 1 160 1 161 1 196 1 202 1 215 1 220 1 253 1 254 1 274 1 275 1 388 1 389 1
		 435 1 436 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1
		 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1
		 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1
		 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1
		 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 84 ".kit[0:83]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 1 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 3 18 18;
	setAttr -s 84 ".kot[0:83]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
	setAttr -s 84 ".kix[65:83]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 84 ".kiy[65:83]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_eye_ctrl_scaleY";
	rename -uid "1592BA04-4140-4556-013D-EBA7E15EFDF0";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 84 ".ktv[0:83]"  1 1 7 1 13 1 21 1 89 1 97 1 153 1 155 1
		 158 1 159 1 160 1 161 1 196 1 202 1 215 1 220 1 253 1 254 1 274 1 275 1 388 1 389 1
		 435 1 436 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1
		 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1
		 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1
		 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1
		 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 84 ".kit[0:83]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 1 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 3 18 18;
	setAttr -s 84 ".kot[0:83]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
	setAttr -s 84 ".kix[65:83]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 84 ".kiy[65:83]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_eye_ctrl_scaleZ";
	rename -uid "DBA8A100-4A4D-0390-BCBF-9BB7D689D7F8";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 84 ".ktv[0:83]"  1 1 7 1 13 1 21 1 89 1 97 1 153 1 155 1
		 158 1 159 1 160 1 161 1 196 1 202 1 215 1 220 1 253 1 254 1 274 1 275 1 388 1 389 1
		 435 1 436 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1
		 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1
		 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1
		 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1
		 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 84 ".kit[0:83]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 3 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 1 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 3 18 18;
	setAttr -s 84 ".kot[0:83]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
	setAttr -s 84 ".kix[65:83]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 84 ".kiy[65:83]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_eye_ctrl_L_Uplid";
	rename -uid "39F343C1-4D42-2C5F-3490-F09DE9E25760";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 88 ".ktv[0:87]"  1 0 7 0 13 0 21 0 89 0 93 0 97 0 153 0 155 0
		 158 0 159 100 160 100 161 0 196 0 201 0 202 60.217599736977874 215 60.217599736977874
		 216 100 217 100 220 100 253 100 254 0 274 0 275 100 388 100 389 66.6 435 66.6 436 100
		 485 100 509 100 510 100 515 100 516 100 526 100 527 73.699999999999989 535 73.699999999999989
		 536 73.699999999999989 544 73.699999999999989 545 73.699999999999989 555 73.699999999999989
		 556 73.699999999999989 560 73.699999999999989 561 100 567 100 568 100 580 100 581 100
		 596 100 597 100 613 100 614 100 624 100 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 42.569119625872915 656 42.569119625872915 657 73.699999999999989 666 73.699999999999989
		 667 73.699999999999989 681 73.699999999999989 684 0 687 0 690 0 697 73.699999999999989
		 707 0 711 0 716 0 720 0 725 0 729 0 734 100 740 100 752 100 759 100 765 100 779 100
		 806 100 819 100 826 100 841 100 850 100 864 100;
	setAttr -s 88 ".kit[69:87]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 88 ".kot[1:87]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 88 ".kix[69:87]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 88 ".kiy[69:87]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_eye_ctrl_L_Lolid";
	rename -uid "C153D19A-46EA-C931-FEB7-FBA989D7AB5B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 88 ".ktv[0:87]"  1 0 7 0 13 0 21 0 89 0 93 0 97 0 153 0 155 0
		 158 0 159 -76.500000000000014 160 -76.500000000000014 161 0 196 0 201 0 202 -46.06646379878805
		 215 -46.06646379878805 216 -76.500000000000014 217 -76.500000000000014 220 -76.500000000000014
		 253 -76.500000000000014 254 0 274 0 275 -76.500000000000014 388 -76.500000000000014
		 389 -76.500000000000014 435 -76.500000000000014 436 -76.500000000000014 485 -76.500000000000014
		 509 -76.500000000000014 510 -76.500000000000014 515 -76.500000000000014 516 -76.500000000000014
		 526 -76.500000000000014 527 -61.700000000000017 535 -61.700000000000017 536 -61.700000000000017
		 544 -61.700000000000017 545 -61.700000000000017 555 -61.700000000000017 556 -61.700000000000017
		 560 -61.700000000000017 561 -76.500000000000014 567 -76.500000000000014 568 -76.500000000000014
		 580 -76.500000000000014 581 -76.500000000000014 596 -76.500000000000014 597 -76.500000000000014
		 613 -76.500000000000014 614 -76.500000000000014 624 -76.500000000000014 625 0 630 0
		 631 0 636 0 637 0 642 0 643 0 647 0 648 -30.555039731461019 656 -30.555039731461019
		 657 -52.90000000000002 666 -52.90000000000002 667 -52.90000000000002 681 -52.90000000000002
		 684 0 687 0 690 0 697 -52.90000000000002 707 0 711 0 716 0 720 0 725 0 729 0 734 -76.500000000000014
		 740 -76.500000000000014 752 -76.500000000000014 759 -76.500000000000014 765 -76.500000000000014
		 779 -76.500000000000014 806 -76.500000000000014 819 -76.500000000000014 826 -76.500000000000014
		 841 -76.500000000000014 850 -76.500000000000014 864 -76.7;
	setAttr -s 88 ".kit[69:87]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 88 ".kot[1:87]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 88 ".kix[69:87]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 88 ".kiy[69:87]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_eye_ctrl_R_Uplid";
	rename -uid "78D97E0E-41E5-667B-7D69-E4892CBF389F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 88 ".ktv[0:87]"  1 0 7 0 13 0 21 0 89 0 93 0 97 0 153 0 155 0
		 158 0 159 100 160 100 161 0 196 0 201 0 202 60.217599736977874 215 60.217599736977874
		 216 100 217 100 220 100 253 100 254 0 274 0 275 100 388 100 389 56.099999999999994
		 435 56.099999999999994 436 100 485 100 509 100 510 100 515 100 516 100 526 100 527 65.3
		 535 65.3 536 65.3 544 65.3 545 65.3 555 65.3 556 65.3 560 65.3 561 100 567 100 568 100
		 580 100 581 100 596 100 597 100 613 100 614 100 624 100 625 0 630 0 631 0 636 0 637 0
		 642 0 643 0 647 0 648 37.717279668514252 656 37.717279668514252 657 65.3 666 65.3
		 667 65.3 681 65.3 684 0 687 0 690 0 697 65.3 707 0 711 0 716 0 720 0 725 0 729 0
		 734 100 740 100 752 100 759 100 765 100 779 100 806 100 819 100 826 100 841 100 850 100
		 864 100;
	setAttr -s 88 ".kit[69:87]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 88 ".kot[1:87]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 88 ".kix[69:87]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 88 ".kiy[69:87]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_eye_ctrl_R_Lolid";
	rename -uid "5C265390-4118-BE02-FB86-49B2286175DA";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 88 ".ktv[0:87]"  1 0 7 0 13 0 21 0 89 0 93 0 97 0 153 0 155 0
		 158 0 159 -78.500000000000014 160 -78.500000000000014 161 0 196 0 201 0 202 -47.270815793527632
		 215 -47.270815793527632 216 -78.500000000000014 217 -78.500000000000014 220 -78.500000000000014
		 253 -78.500000000000014 254 0 274 0 275 -78.500000000000014 388 -78.500000000000014
		 389 -78.500000000000014 435 -78.500000000000014 436 -78.500000000000014 485 -78.500000000000014
		 509 -78.500000000000014 510 -78.500000000000014 515 -78.500000000000014 516 -78.500000000000014
		 526 -78.500000000000014 527 -63.700000000000017 535 -63.700000000000017 536 -63.700000000000017
		 544 -63.700000000000017 545 -63.700000000000017 555 -63.700000000000017 556 -63.700000000000017
		 560 -63.700000000000017 561 -78.500000000000014 567 -78.500000000000014 568 -78.500000000000014
		 580 -78.500000000000014 581 -78.500000000000014 596 -78.500000000000014 597 -78.500000000000014
		 613 -78.500000000000014 614 -78.500000000000014 624 -78.500000000000014 625 0 630 0
		 631 0 636 0 637 0 642 0 643 0 647 0 648 -31.710239721308316 656 -31.710239721308316
		 657 -54.90000000000002 666 -54.90000000000002 667 -54.90000000000002 681 -54.90000000000002
		 684 0 687 0 690 0 697 -54.90000000000002 707 0 711 0 716 0 720 0 725 0 729 0 734 -78.500000000000014
		 740 -78.500000000000014 752 -78.500000000000014 759 -78.500000000000014 765 -78.500000000000014
		 779 -78.500000000000014 806 -78.500000000000014 819 -78.500000000000014 826 -78.500000000000014
		 841 -78.500000000000014 850 -78.500000000000014 864 -81.300000000000011;
	setAttr -s 88 ".kit[69:87]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 88 ".kot[1:87]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 88 ".kix[69:87]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 88 ".kiy[69:87]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_eye_ctrl_L_Lid_Twist";
	rename -uid "4AB22941-47B6-794C-21DB-0695848DF17D";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 85 ".ktv[0:84]"  1 0 7 0 13 0 21 0 89 0 93 0 97 0 153 0 155 0
		 158 0 159 0 160 0 161 0 196 0 202 0 215 0 220 0 253 0 254 0 274 0 275 0 388 0 389 0
		 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0
		 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0
		 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0
		 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0
		 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 85 ".kit[0:84]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 3 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 3 18 18;
	setAttr -s 85 ".kot[0:84]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 85 ".kix[66:84]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 85 ".kiy[66:84]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_eye_ctrl_R_Lid_Twist";
	rename -uid "E0EC58DB-41F5-F2D0-6E98-7EB63275D839";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 85 ".ktv[0:84]"  1 0 7 0 13 0 21 0 89 0 93 0 97 0 153 0 155 0
		 158 0 159 0 160 0 161 0 196 0 202 0 215 0 220 0 253 0 254 0 274 0 275 0 388 0 389 0
		 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0
		 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0
		 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0
		 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0
		 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 85 ".kit[0:84]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 3 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 3 18 18;
	setAttr -s 85 ".kot[0:84]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 85 ".kix[66:84]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 85 ".kiy[66:84]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_eye_ctrl_L_Lid_Bend";
	rename -uid "66D297C4-49DB-5397-7BAC-A9BE31DA513B";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 85 ".ktv[0:84]"  1 0 7 0 13 0 21 0 89 0 93 0 97 0 153 0 155 0
		 158 0 159 0 160 0 161 0 196 0 202 0 215 0 220 0 253 0 254 0 274 0 275 0 388 0 389 0
		 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0
		 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0
		 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0
		 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0
		 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 85 ".kit[0:84]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 3 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 3 18 18;
	setAttr -s 85 ".kot[0:84]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 85 ".kix[66:84]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 85 ".kiy[66:84]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_eye_ctrl_R_Lid_Bend";
	rename -uid "47665A82-4463-40D3-DDF1-33BB52600608";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 85 ".ktv[0:84]"  1 0 7 0 13 0 21 0 89 0 93 0 97 0 153 0 155 0
		 158 0 159 0 160 0 161 0 196 0 202 0 215 0 220 0 253 0 254 0 274 0 275 0 388 0 389 0
		 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0
		 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0
		 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0
		 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0
		 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 85 ".kit[0:84]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 3 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 1 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 3 18 18;
	setAttr -s 85 ".kot[0:84]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 85 ".kix[66:84]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 85 ".kiy[66:84]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_eye_ctrl_visibility";
	rename -uid "EDB10E3E-4ACB-D4B8-645C-518EF9D0AAA0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 1 7 1 13 1 196 1 217 1 253 1 254 1 274 1
		 275 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1
		 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1
		 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1
		 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1
		 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 9 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_eye_ctrl_rotateX";
	rename -uid "5972E12D-4675-8528-87CA-A9A3E5755553";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 0 7 0 13 0 196 0 217 0 253 0 254 0 274 0
		 275 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0
		 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0
		 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0
		 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0
		 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_eye_ctrl_rotateY";
	rename -uid "17965552-4B54-0831-8D88-87B66D55D9EF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 0 7 0 13 0 196 0 217 0 253 0 254 0 274 0
		 275 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0
		 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0
		 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0
		 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0
		 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_eye_ctrl_rotateZ";
	rename -uid "770C3A2A-4AFD-94D5-77ED-84A77D479913";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 0 7 0 13 0 196 0 217 0 253 0 254 0 274 0
		 275 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0
		 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0
		 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0
		 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0
		 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_eye_ctrl_scaleX";
	rename -uid "842979B7-407E-7ED7-A726-4982A91E335E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 1 7 1 13 1 196 1 217 1 253 1 254 1 274 1
		 275 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1
		 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1
		 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1
		 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1
		 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_eye_ctrl_scaleY";
	rename -uid "A0E265D4-4843-7092-A132-19938F098207";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 1 7 1 13 1 196 1 217 1 253 1 254 1 274 1
		 275 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1
		 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1
		 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1
		 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1
		 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_eye_ctrl_scaleZ";
	rename -uid "DA4C7D65-4320-294A-9639-4986D68CC058";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 1 7 1 13 1 196 1 217 1 253 1 254 1 274 1
		 275 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1
		 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1
		 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1
		 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1
		 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_eye_ctrl_visibility";
	rename -uid "C910AE95-4D49-A788-C5BB-588A7C0D0C75";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 1 7 1 13 1 196 1 217 1 253 1 254 1 274 1
		 275 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1
		 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1
		 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1
		 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1
		 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 9 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "R_eye_ctrl_rotateX";
	rename -uid "5DC09DCD-4A0D-473E-429D-7D9DE38FDBA0";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 0 7 0 13 0 196 0 217 0 253 0 254 0 274 0
		 275 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0
		 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0
		 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0
		 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0
		 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "R_eye_ctrl_rotateY";
	rename -uid "3DBE8128-44E1-30A0-6D82-A39DDD038CEB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 0 7 0 13 0 196 0 217 0 253 0 254 0 274 0
		 275 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0
		 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0
		 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0
		 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0
		 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "R_eye_ctrl_rotateZ";
	rename -uid "4B6C8FDC-45DD-A754-0EC8-A3BC23682865";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 0 7 0 13 0 196 0 217 0 253 0 254 0 274 0
		 275 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0
		 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0
		 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0
		 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0
		 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_eye_ctrl_scaleX";
	rename -uid "5123B68D-47B8-1611-3071-A6AFA5AD0465";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 1 7 1 13 1 196 1 217 1 253 1 254 1 274 1
		 275 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1
		 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1
		 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1
		 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1
		 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_eye_ctrl_scaleY";
	rename -uid "68B51905-42C8-7C08-0BE4-8381A3AF76F3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 1 7 1 13 1 196 1 217 1 253 1 254 1 274 1
		 275 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1
		 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1
		 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1
		 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1
		 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_eye_ctrl_scaleZ";
	rename -uid "838B8F56-4841-C10A-DED2-CEBF363B270B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 69 ".ktv[0:68]"  1 1 7 1 13 1 196 1 217 1 253 1 254 1 274 1
		 275 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1
		 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1
		 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1
		 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1
		 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 69 ".kit[50:68]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 69 ".kot[1:68]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 69 ".kix[50:68]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 69 ".kiy[50:68]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_mouth_main_ctrl_visibility";
	rename -uid "F7EACB69-4242-848F-C678-BB8E96E99E79";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 1 7 1 13 1 201 1 202 1 203 1 216 1 217 1
		 275 1 389 1 397 1 398 1 406 1 407 1 408 1 409 1 410 1 411 1 412 1 413 1 414 1 415 1
		 416 1 417 1 419 1 420 1 421 1 424 1 425 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1
		 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1
		 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1
		 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 9 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "M_mouth_main_ctrl_rotateX";
	rename -uid "3F67104C-4767-5797-FE2F-E19998B2C3AC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "M_mouth_main_ctrl_rotateY";
	rename -uid "D343EF27-453C-1A57-8A90-C8B3F89545F5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "M_mouth_main_ctrl_rotateZ";
	rename -uid "9AEE9DA0-4BA2-5EDF-870A-F8A154B125A3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_mouth_main_ctrl_scaleX";
	rename -uid "1E10AE4D-4A03-D1A6-3D10-D8A3313332A5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 1 7 1 13 1 201 1 202 1 203 1 216 1 217 1
		 275 1 389 1 397 1 398 1 406 1 407 1 408 1 409 1 410 1 411 1 412 1 413 1 414 1 415 1
		 416 1 417 1 419 1 420 1 421 1 424 1 425 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1
		 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1
		 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1
		 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_mouth_main_ctrl_scaleY";
	rename -uid "D91C74F3-467E-8449-D401-D3B34088CA5B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 1 7 1 13 1 201 1 202 1 203 1 216 1 217 1
		 275 1 389 1 397 1 398 1 406 1 407 1 408 1 409 1 410 1 411 1 412 1 413 1 414 1 415 1
		 416 1 417 1 419 1 420 1 421 1 424 1 425 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1
		 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1
		 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1
		 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_mouth_main_ctrl_scaleZ";
	rename -uid "B75E007B-46FC-4F9C-A153-048121C979A9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 1 7 1 13 1 201 1 202 1 203 1 216 1 217 1
		 275 1 389 1 397 1 398 1 406 1 407 1 408 1 409 1 410 1 411 1 412 1 413 1 414 1 415 1
		 416 1 417 1 419 1 420 1 421 1 424 1 425 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1
		 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1
		 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1
		 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_mouth_ctrl_visibility";
	rename -uid "96EC8248-4416-201B-DB25-3D8FFECE1984";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 1 7 1 13 1 201 1 202 1 203 1 216 1 217 1
		 275 1 389 1 397 1 398 1 406 1 407 1 408 1 409 1 410 1 411 1 412 1 413 1 414 1 415 1
		 416 1 417 1 419 1 420 1 421 1 424 1 425 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1
		 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1
		 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1
		 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 9 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_mouth_ctrl_rotateX";
	rename -uid "AE8B5179-4E6D-D1AD-2414-59BA7FD8E6F5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_mouth_ctrl_rotateY";
	rename -uid "ABA6AABC-4A04-E13A-2279-3E939D31620A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "L_mouth_ctrl_rotateZ";
	rename -uid "A35584B5-46BE-F8C7-8FCF-60AC0B71D8A6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 90 ".ktv[0:89]"  1 0 7 0 13 -16.042818263663051 93 -16.042818263663051
		 201 -16.042818263663051 202 39.498272447656127 203 39.498272447656127 216 39.498272447656127
		 217 39.498272447656127 275 39.498272447656127 389 39.498272447656127 397 39.498272447656127
		 398 22.627583015290416 406 22.627583015290416 407 39.498272447656127 408 39.498272447656127
		 409 39.498272447656127 410 39.498272447656127 411 39.498272447656127 412 39.498272447656127
		 413 39.498272447656127 414 39.498272447656127 415 39.498272447656127 416 39.498272447656127
		 417 39.498272447656127 419 39.498272447656127 420 39.498272447656127 421 39.498272447656127
		 424 39.498272447656127 425 39.498272447656127 485 39.498272447656127 509 39.498272447656127
		 510 39.498272447656127 515 39.498272447656127 516 39.498272447656127 526 39.498272447656127
		 527 39.498272447656127 535 39.498272447656127 536 39.498272447656127 544 39.498272447656127
		 545 39.498272447656127 555 39.498272447656127 556 39.498272447656127 560 39.498272447656127
		 561 39.498272447656127 567 39.498272447656127 568 39.498272447656127 580 39.498272447656127
		 581 39.498272447656127 596 39.498272447656127 597 39.498272447656127 613 39.498272447656127
		 614 39.498272447656127 624 39.498272447656127 625 39.498272447656127 630 39.498272447656127
		 631 39.498272447656127 636 39.498272447656127 637 39.498272447656127 642 39.498272447656127
		 643 39.498272447656127 647 39.498272447656127 648 39.498272447656127 656 39.498272447656127
		 657 39.498272447656127 666 39.498272447656127 667 39.498272447656127 681 39.498272447656127
		 684 -42.585848387769254 687 -42.585848387769254 690 -42.585848387769254 697 39.498272447656127
		 707 -42.585848387769254 711 -42.585848387769254 716 -42.585848387769254 720 -42.585848387769254
		 725 -42.585848387769254 729 -42.585848387769254 734 39.498272447656127 740 39.498272447656127
		 752 39.498272447656127 759 39.498272447656127 765 39.498272447656127 779 39.498272447656127
		 806 39.498272447656127 819 39.498272447656127 826 39.498272447656127 841 39.498272447656127
		 850 39.498272447656127 864 39.498272447656127;
	setAttr -s 90 ".kit[71:89]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 90 ".kot[1:89]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 90 ".kix[71:89]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 90 ".kiy[71:89]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_mouth_ctrl_scaleX";
	rename -uid "E1EBF6D4-4546-7569-A93B-6E81950BF8FD";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 1 7 1 13 1 201 1 202 1 203 1 216 1 217 1
		 275 1 389 1 397 1 398 1 406 1 407 1 408 1 409 1 410 1 411 1 412 1 413 1 414 1 415 1
		 416 1 417 1 419 1 420 1 421 1 424 1 425 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1
		 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1
		 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1
		 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_mouth_ctrl_scaleY";
	rename -uid "4895C4BF-46DD-F118-1BC8-D0B8A6327F19";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 1 7 1 13 1 201 1 202 1 203 1 216 1 217 1
		 275 1 389 1 397 1 398 1 406 1 407 1 408 1 409 1 410 1 411 1 412 1 413 1 414 1 415 1
		 416 1 417 1 419 1 420 1 421 1 424 1 425 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1
		 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1
		 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1
		 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_mouth_ctrl_scaleZ";
	rename -uid "372AD62A-4794-5E59-38A4-6598CF235CAB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 1 7 1 13 1 201 1 202 1 203 1 216 1 217 1
		 275 1 389 1 397 1 398 1 406 1 407 1 408 1 409 1 410 1 411 1 412 1 413 1 414 1 415 1
		 416 1 417 1 419 1 420 1 421 1 424 1 425 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1
		 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1
		 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1
		 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_mouth_ctrl_visibility";
	rename -uid "40835850-45F5-13B0-D4CF-73B7031D7028";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 1 7 1 13 1 201 1 202 1 203 1 216 1 217 1
		 275 1 389 1 397 1 398 1 406 1 407 1 408 1 409 1 410 1 411 1 412 1 413 1 414 1 415 1
		 416 1 417 1 419 1 420 1 421 1 424 1 425 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1
		 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1
		 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1
		 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 9 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "M_mouth_ctrl_rotateX";
	rename -uid "EEF87520-4417-78E2-A299-78B25E90D9C1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "M_mouth_ctrl_rotateY";
	rename -uid "CB5BEC26-4434-F60D-A14C-CFB2F10BF616";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "M_mouth_ctrl_rotateZ";
	rename -uid "B8074A3B-46D8-67DE-08C7-B4B8E6DE603E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 2.54776611119122
		 203 2.54776611119122 216 2.54776611119122 217 2.54776611119122 275 2.54776611119122
		 389 2.54776611119122 397 2.54776611119122 398 2.54776611119122 406 2.54776611119122
		 407 2.54776611119122 408 2.54776611119122 409 0 410 0 411 2.54776611119122 412 2.54776611119122
		 413 2.54776611119122 414 2.54776611119122 415 2.54776611119122 416 2.54776611119122
		 417 0 419 0 420 2.54776611119122 421 2.54776611119122 424 2.54776611119122 425 2.54776611119122
		 485 2.54776611119122 509 2.54776611119122 510 2.54776611119122 515 2.54776611119122
		 516 2.54776611119122 526 2.54776611119122 527 2.54776611119122 535 2.54776611119122
		 536 2.54776611119122 544 2.54776611119122 545 2.54776611119122 555 2.54776611119122
		 556 2.54776611119122 560 2.54776611119122 561 2.54776611119122 567 2.54776611119122
		 568 2.54776611119122 580 2.54776611119122 581 2.54776611119122 596 2.54776611119122
		 597 2.54776611119122 613 2.54776611119122 614 2.54776611119122 624 2.54776611119122
		 625 0 630 0 631 2.54776611119122 636 2.54776611119122 637 2.54776611119122 642 2.54776611119122
		 643 2.54776611119122 647 2.54776611119122 648 2.54776611119122 656 2.54776611119122
		 657 2.54776611119122 666 2.54776611119122 667 2.54776611119122 681 2.54776611119122
		 684 2.54776611119122 687 2.54776611119122 690 2.54776611119122 697 2.54776611119122
		 707 2.54776611119122 711 2.54776611119122 716 2.54776611119122 720 2.54776611119122
		 725 2.54776611119122 729 2.54776611119122 734 2.54776611119122 740 2.54776611119122
		 752 2.54776611119122 759 2.54776611119122 765 2.54776611119122 779 2.54776611119122
		 806 2.54776611119122 819 2.54776611119122 826 2.54776611119122 841 2.54776611119122
		 850 2.54776611119122 864 2.54776611119122;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_mouth_ctrl_scaleX";
	rename -uid "7135C05C-4B20-3237-4A7B-9197BC8344F7";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 1 7 1 13 1 201 1 202 2.3427180418326516
		 203 2.3427180418326516 216 2.3427180418326516 217 2.3427180418326516 275 2.3427180418326516
		 389 2.3427180418326516 397 2.3427180418326516 398 1.7226797272207839 406 1.7226797272207839
		 407 2.3427180418326516 408 2.3427180418326516 409 1 410 1 411 2.3427180418326516
		 412 2.3427180418326516 413 2.3427180418326516 414 2.3427180418326516 415 2.3427180418326516
		 416 2.3427180418326516 417 1 419 1 420 2.3427180418326516 421 2.3427180418326516
		 424 2.3427180418326516 425 2.3427180418326516 485 2.3427180418326516 509 2.3427180418326516
		 510 2.3427180418326516 515 2.3427180418326516 516 2.3427180418326516 526 2.3427180418326516
		 527 2.3427180418326516 535 2.3427180418326516 536 2.3427180418326516 544 2.3427180418326516
		 545 2.3427180418326516 555 2.3427180418326516 556 2.3427180418326516 560 2.3427180418326516
		 561 2.3427180418326516 567 2.3427180418326516 568 2.3427180418326516 580 2.3427180418326516
		 581 2.3427180418326516 596 2.3427180418326516 597 2.3427180418326516 613 2.3427180418326516
		 614 2.3427180418326516 624 2.3427180418326516 625 1 630 1 631 2.3427180418326516
		 636 2.3427180418326516 637 2.3427180418326516 642 2.3427180418326516 643 2.3427180418326516
		 647 2.3427180418326516 648 2.3427180418326516 656 2.3427180418326516 657 2.3427180418326516
		 666 2.3427180418326516 667 2.3427180418326516 681 2.3427180418326516 684 2.3427180418326516
		 687 2.3427180418326516 690 2.3427180418326516 697 2.3427180418326516 707 2.3427180418326516
		 711 2.3427180418326516 716 2.3427180418326516 720 2.3427180418326516 725 2.3427180418326516
		 729 2.3427180418326516 734 2.3427180418326516 740 2.3427180418326516 752 2.3427180418326516
		 759 2.3427180418326516 765 2.3427180418326516 779 2.3427180418326516 806 2.3427180418326516
		 819 2.3427180418326516 826 2.3427180418326516 841 2.3427180418326516 850 2.3427180418326516
		 864 2.3427180418326516;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_mouth_ctrl_scaleY";
	rename -uid "CEE8C664-42AE-2B18-FEAA-14BA46008E9E";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 93 ".ktv[0:92]"  1 1 7 1 13 1 201 1 202 4.2741549639289813
		 203 4.2741549639289813 216 4.2741549639289813 217 4.2741549639289813 275 4.2741549639289813
		 389 4.2741549639289813 397 4.2741549639289813 398 4.2741549639289813 406 4.2741549639289813
		 407 4.2741549639289813 408 4.2741549639289813 409 1 410 1 411 4.2741549639289813
		 412 4.2741549639289813 413 4.2741549639289813 414 4.2741549639289813 415 4.2741549639289813
		 416 4.2741549639289813 417 1 419 1 420 4.2741549639289813 421 4.2741549639289813
		 424 4.2741549639289813 425 4.2741549639289813 485 4.2741549639289813 509 4.2741549639289813
		 510 4.2741549639289813 515 4.2741549639289813 516 4.2741549639289813 526 4.2741549639289813
		 527 4.2741549639289813 535 4.2741549639289813 536 4.2741549639289813 544 4.2741549639289813
		 545 4.2741549639289813 555 4.2741549639289813 556 4.2741549639289813 560 4.2741549639289813
		 561 4.2741549639289813 567 4.2741549639289813 568 4.2741549639289813 580 4.2741549639289813
		 581 4.2741549639289813 596 4.2741549639289813 597 4.2741549639289813 613 4.2741549639289813
		 614 4.2741549639289813 624 4.2741549639289813 625 1 630 1 631 4.2741549639289813
		 636 4.2741549639289813 637 4.2741549639289813 642 4.2741549639289813 643 4.2741549639289813
		 645 1.7823647109629794 647 4.2741549639289813 648 4.2741549639289813 656 4.2741549639289813
		 657 4.2741549639289813 666 4.2741549639289813 667 4.2741549639289813 681 4.2741549639289813
		 684 1.8385091509064404 687 4.2741549639289813 690 2.9625545747159965 693 1.7817001659357912
		 697 4.2741549639289813 703 1.7967759412206399 707 4.2741549639289813 711 4.2741549639289813
		 712 2.4579830320843774 716 4.2741549639289813 720 2.1145603850485393 725 4.2741549639289813
		 729 5.169680079250683 734 4.2741549639289813 740 4.2741549639289813 752 4.2741549639289813
		 759 4.2741549639289813 765 4.2741549639289813 779 4.2741549639289813 806 4.2741549639289813
		 819 4.2741549639289813 826 4.2741549639289813 841 4.2741549639289813 850 4.2741549639289813
		 864 4.2741549639289813;
	setAttr -s 93 ".kit[0:92]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 3 18 18 18 18 18 18 18 18 18 18 3 1 3 18 18 
		3 18 18 18 18 18 18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 93 ".kot[0:92]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 93 ".kix[72:92]"  1 1 1 1 1 1 1 0.097725799101387692 1 1 
		1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 93 ".kiy[72:92]"  0 0 0 0 0 0 0 0.99521337822096989 0 0 0 
		0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "M_mouth_ctrl_scaleZ";
	rename -uid "E2544D20-48AD-692B-1F5D-F7B1E6B14C53";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 1 7 1 13 1 201 1 202 1 203 1 216 1 217 1
		 275 1 389 1 397 1 398 1 406 1 407 1 408 1 409 1 410 1 411 1 412 1 413 1 414 1 415 1
		 416 1 417 1 419 1 420 1 421 1 424 1 425 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1
		 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1
		 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1
		 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_mouth_ctrl_visibility";
	rename -uid "D39F8FDA-4571-0CA8-31EC-259021D90D7D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 1 7 1 13 1 201 1 202 1 203 1 216 1 217 1
		 275 1 389 1 397 1 398 1 406 1 407 1 408 1 409 1 410 1 411 1 412 1 413 1 414 1 415 1
		 416 1 417 1 419 1 420 1 421 1 424 1 425 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1
		 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1
		 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1
		 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 9 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "R_mouth_ctrl_rotateX";
	rename -uid "B2DB7CC9-4006-D671-E55A-23905DA89B54";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "R_mouth_ctrl_rotateY";
	rename -uid "CF089E48-44A1-0B6B-A5F5-32A651B4576D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 0 7 0 13 0 201 0 202 0 203 0 216 0 217 0
		 275 0 389 0 397 0 398 0 406 0 407 0 408 0 409 0 410 0 411 0 412 0 413 0 414 0 415 0
		 416 0 417 0 419 0 420 0 421 0 424 0 425 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0
		 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0
		 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0
		 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTA -n "R_mouth_ctrl_rotateZ";
	rename -uid "B3E41D13-4630-B2D5-CF03-29B01AF3A416";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 90 ".ktv[0:89]"  1 0 7 0 13 25.210142985756221 93 25.210142985756221
		 201 25.210142985756221 202 -30.256730172098436 203 -30.256730172098436 216 -30.256730172098436
		 217 -30.256730172098436 275 -30.256730172098436 389 -30.256730172098436 397 -30.256730172098436
		 398 0 406 0 407 -30.256730172098436 408 -30.256730172098436 409 -30.256730172098436
		 410 -30.256730172098436 411 -30.256730172098436 412 -30.256730172098436 413 -30.256730172098436
		 414 -30.256730172098436 415 -30.256730172098436 416 -30.256730172098436 417 -30.256730172098436
		 419 -30.256730172098436 420 -30.256730172098436 421 -30.256730172098436 424 -30.256730172098436
		 425 -30.256730172098436 485 -30.256730172098436 509 -30.256730172098436 510 -30.256730172098436
		 515 -30.256730172098436 516 -30.256730172098436 526 -30.256730172098436 527 -30.256730172098436
		 535 -30.256730172098436 536 -30.256730172098436 544 -30.256730172098436 545 -30.256730172098436
		 555 -30.256730172098436 556 -30.256730172098436 560 -30.256730172098436 561 -30.256730172098436
		 567 -30.256730172098436 568 -30.256730172098436 580 -30.256730172098436 581 -30.256730172098436
		 596 -30.256730172098436 597 -30.256730172098436 613 -30.256730172098436 614 -30.256730172098436
		 624 -30.256730172098436 625 -30.256730172098436 630 -30.256730172098436 631 -30.256730172098436
		 636 -30.256730172098436 637 -30.256730172098436 642 -30.256730172098436 643 -30.256730172098436
		 647 -30.256730172098436 648 -30.256730172098436 656 -30.256730172098436 657 -30.256730172098436
		 666 -30.256730172098436 667 -30.256730172098436 681 -30.256730172098436 684 59.620576025425784
		 687 59.620576025425784 690 59.620576025425784 697 -30.256730172098436 707 59.620576025425784
		 711 59.620576025425784 716 59.620576025425784 720 59.620576025425784 725 59.620576025425784
		 729 59.620576025425784 734 -30.256730172098436 740 -30.256730172098436 752 -30.256730172098436
		 759 -30.256730172098436 765 -30.256730172098436 779 -30.256730172098436 806 -30.256730172098436
		 819 -30.256730172098436 826 -30.256730172098436 841 -30.256730172098436 850 -30.256730172098436
		 864 -30.256730172098436;
	setAttr -s 90 ".kit[71:89]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 90 ".kot[1:89]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 90 ".kix[71:89]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 90 ".kiy[71:89]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_mouth_ctrl_scaleX";
	rename -uid "D8845614-4780-CA8B-1DAE-E0A3C3B0F488";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 1 7 1 13 1 201 1 202 1 203 1 216 1 217 1
		 275 1 389 1 397 1 398 1 406 1 407 1 408 1 409 1 410 1 411 1 412 1 413 1 414 1 415 1
		 416 1 417 1 419 1 420 1 421 1 424 1 425 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1
		 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1
		 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1
		 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_mouth_ctrl_scaleY";
	rename -uid "DFEA2023-4CEA-5EF7-A41F-5E81F847B0C8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 1 7 1 13 1 201 1 202 1 203 1 216 1 217 1
		 275 1 389 1 397 1 398 1 406 1 407 1 408 1 409 1 410 1 411 1 412 1 413 1 414 1 415 1
		 416 1 417 1 419 1 420 1 421 1 424 1 425 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1
		 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1
		 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1
		 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "R_mouth_ctrl_scaleZ";
	rename -uid "9A17C030-4B58-22D6-B88E-6A937BECE59C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 89 ".ktv[0:88]"  1 1 7 1 13 1 201 1 202 1 203 1 216 1 217 1
		 275 1 389 1 397 1 398 1 406 1 407 1 408 1 409 1 410 1 411 1 412 1 413 1 414 1 415 1
		 416 1 417 1 419 1 420 1 421 1 424 1 425 1 485 1 509 1 510 1 515 1 516 1 526 1 527 1
		 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1 580 1 581 1 596 1 597 1
		 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1 647 1 648 1 656 1 657 1
		 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1 720 1 725 1 729 1 734 1
		 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1 864 1;
	setAttr -s 89 ".kit[70:88]"  1 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 3 18 18;
	setAttr -s 89 ".kot[1:88]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 89 ".kix[70:88]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1;
	setAttr -s 89 ".kiy[70:88]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0;
createNode animCurveTU -n "L_Wrist_ctrl_blendOrient1";
	rename -uid "3458E953-4776-C201-1DFE-83890C86937D";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 77 ".ktv[0:76]"  1 0 7 0 13 0 217 0 274 0 275 0 388 0 389 0
		 397 0 398 0 408 0 409 0 435 0 436 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0
		 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0
		 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0
		 667 0 677 0 681 0 684 0 687 0 690 0 697 0 704 0 707 0 711 0 716 0 720 0 725 0 729 0
		 734 0 740 0 752 0 759 0 765 0 779 0 786 0 806 0 819 0 826 0 841 0 850 0 864 0;
	setAttr -s 77 ".kit[0:76]"  18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 3 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 3 18 18 18 3 18 
		18;
	setAttr -s 77 ".kot[0:76]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5;
createNode animCurveTA -n "M_Hips_ctrl_rotateX";
	rename -uid "61CA0C6A-4A8A-531D-4960-B8B34AC03D95";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "M_Hips_ctrl_rotateY";
	rename -uid "0D41DE26-4AD8-BCED-306F-7FAFD2B908F9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "M_Hips_ctrl_rotateZ";
	rename -uid "6CE37320-41FA-5BC9-7765-DB9322C76164";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "M_Hips_ctrl_visibility";
	rename -uid "35DF509F-48A2-E83A-EAB7-CFB491E24471";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  9 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "M_Hips_ctrl_translateX";
	rename -uid "53252A0A-4079-4092-194F-37A6A0DEDF53";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "M_Hips_ctrl_translateY";
	rename -uid "E8430637-40AE-3292-CFE9-A9B9F2218C3A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0.35580644838680442 7 0.35580644838680442
		 13 0.35580644838680442 217 0.35580644838680442 275 0.35580644838680442 485 0.35580644838680442
		 509 0.35580644838680442 510 0.35580644838680442 515 0.35580644838680442 516 0.35580644838680442
		 526 0.35580644838680442 527 0.35580644838680442 535 0.35580644838680442 536 0.35580644838680442
		 544 0.35580644838680442 545 0.35580644838680442 555 0.35580644838680442 556 0.35580644838680442
		 560 0.35580644838680442 561 0.35580644838680442 567 0.35580644838680442 568 0.35580644838680442
		 580 0.35580644838680442 581 0.35580644838680442 596 0.35580644838680442 597 0.35580644838680442
		 613 0.35580644838680442 614 0.35580644838680442 624 0.35580644838680442 625 0.35580644838680442
		 630 0.35580644838680442 631 0.35580644838680442 636 0.35580644838680442 637 0.35580644838680442
		 642 0.35580644838680442 643 0.35580644838680442 647 0.35580644838680442 648 0.35580644838680442
		 656 0.35580644838680442 657 0.35580644838680442 666 0.35580644838680442 667 0.35580644838680442
		 681 0.35580644838680442 684 0.35580644838680442 687 0.35580644838680442 690 0.35580644838680442
		 697 0.35580644838680442 707 0.35580644838680442 711 0.35580644838680442 716 0.35580644838680442
		 720 0.35580644838680442 725 0.35580644838680442 729 0.35580644838680442 734 0.35580644838680442
		 740 0.35580644838680442 752 0.35580644838680442 759 0.35580644838680442 765 0.35580644838680442
		 779 0.35580644838680442 806 0.35580644838680442 819 0.35580644838680442 826 0.35580644838680442
		 841 0.35580644838680442 850 0.35580644838680442 864 0.35580644838680442 888 0.35580644838680442;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "M_Hips_ctrl_translateZ";
	rename -uid "24CBFF72-4A74-FD4E-A29F-A0A2C7D7AD62";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "M_Hips_ctrl_scaleX";
	rename -uid "17B80807-4580-5EDE-21CB-EA8CD2805E00";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "M_Hips_ctrl_scaleY";
	rename -uid "BEA3B11B-43EA-B800-BAA9-77BB01FEE5A4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "M_Hips_ctrl_scaleZ";
	rename -uid "DCD02418-4C0C-C4EE-9D75-B4A6954D7C0F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "L_eyeLid_ctrl_visibility";
	rename -uid "C96B0EC0-48FE-96D6-BCB0-1788406548F4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  9 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "L_eyeLid_ctrl_translateX";
	rename -uid "E37399CA-46FC-6645-9013-7CBE1374F519";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 3.9968028886505635e-15 7 0 13 0 217 0
		 275 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0
		 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0
		 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0
		 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0
		 819 0 826 0 841 0 850 0 864 3.9968028886505635e-15 888 3.9968028886505635e-15;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "L_eyeLid_ctrl_translateY";
	rename -uid "D0528A51-46B8-970C-7A8D-2DB23E00BF8E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 -3.4416913763379853e-15 7 0 13 0 217 0
		 275 0 485 0 509 0 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0
		 560 0 561 0 567 0 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0
		 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0
		 697 0 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0
		 819 0 826 0 841 0 850 0 864 -3.4416913763379853e-15 888 -3.4416913763379853e-15;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "L_eyeLid_ctrl_translateZ";
	rename -uid "C0F33D99-4507-38C4-9C11-4EA0E036D12E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "L_eyeLid_ctrl_rotateX";
	rename -uid "DABB0A77-4E90-30E7-CAEF-5F97DDE797CE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "L_eyeLid_ctrl_rotateY";
	rename -uid "0B59BC31-42FC-C888-62D1-C29FEE26DD77";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "L_eyeLid_ctrl_rotateZ";
	rename -uid "B13FF593-4565-0672-0CF6-D992CAAEC03B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 -8.041290498309527 666 0 667 0 681 0 684 0 687 0 690 0 697 0
		 707 0 711 0 716 0 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0
		 826 0 841 0 850 0 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "L_eyeLid_ctrl_scaleX";
	rename -uid "435147AC-4022-0F40-4AD4-F39BACAF2A0E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0.99999999999999989 7 0.99999999999999989
		 13 0.99999999999999989 217 0.99999999999999989 275 0.99999999999999989 485 0.99999999999999989
		 509 0.99999999999999989 510 0.99999999999999989 515 0.99999999999999989 516 0.99999999999999989
		 526 0.99999999999999989 527 0.99999999999999989 535 0.99999999999999989 536 0.99999999999999989
		 544 0.99999999999999989 545 0.99999999999999989 555 0.99999999999999989 556 0.99999999999999989
		 560 0.99999999999999989 561 0.99999999999999989 567 0.99999999999999989 568 0.99999999999999989
		 580 0.99999999999999989 581 0.99999999999999989 596 0.99999999999999989 597 0.99999999999999989
		 613 0.99999999999999989 614 0.99999999999999989 624 0.99999999999999989 625 0.99999999999999989
		 630 0.99999999999999989 631 0.99999999999999989 636 0.99999999999999989 637 0.99999999999999989
		 642 0.99999999999999989 643 0.99999999999999989 647 0.99999999999999989 648 0.99999999999999989
		 656 0.99999999999999989 657 0.99999999999999989 666 0.99999999999999989 667 0.99999999999999989
		 681 0.99999999999999989 684 0.99999999999999989 687 0.99999999999999989 690 0.99999999999999989
		 697 0.99999999999999989 707 0.99999999999999989 711 0.99999999999999989 716 0.99999999999999989
		 720 0.99999999999999989 725 0.99999999999999989 729 0.99999999999999989 734 0.99999999999999989
		 740 0.99999999999999989 752 0.99999999999999989 759 0.99999999999999989 765 0.99999999999999989
		 779 0.99999999999999989 806 0.99999999999999989 819 0.99999999999999989 826 0.99999999999999989
		 841 0.99999999999999989 850 0.99999999999999989 864 0.99999999999999989 888 0.99999999999999989;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "L_eyeLid_ctrl_scaleY";
	rename -uid "87874D5E-48E7-3214-F21E-C9A4891F0343";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "L_eyeLid_ctrl_scaleZ";
	rename -uid "D1757956-4BD5-1FAF-ACC2-5FAF2F6D6BBE";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0.99999999999999989 7 0.99999999999999989
		 13 0.99999999999999989 217 0.99999999999999989 275 0.99999999999999989 485 0.99999999999999989
		 509 0.99999999999999989 510 0.99999999999999989 515 0.99999999999999989 516 0.99999999999999989
		 526 0.99999999999999989 527 0.99999999999999989 535 0.99999999999999989 536 0.99999999999999989
		 544 0.99999999999999989 545 0.99999999999999989 555 0.99999999999999989 556 0.99999999999999989
		 560 0.99999999999999989 561 0.99999999999999989 567 0.99999999999999989 568 0.99999999999999989
		 580 0.99999999999999989 581 0.99999999999999989 596 0.99999999999999989 597 0.99999999999999989
		 613 0.99999999999999989 614 0.99999999999999989 624 0.99999999999999989 625 0.99999999999999989
		 630 0.99999999999999989 631 0.99999999999999989 636 0.99999999999999989 637 0.99999999999999989
		 642 0.99999999999999989 643 0.99999999999999989 647 0.99999999999999989 648 0.99999999999999989
		 656 0.99999999999999989 657 0.99999999999999989 666 0.99999999999999989 667 0.99999999999999989
		 681 0.99999999999999989 684 0.99999999999999989 687 0.99999999999999989 690 0.99999999999999989
		 697 0.99999999999999989 707 0.99999999999999989 711 0.99999999999999989 716 0.99999999999999989
		 720 0.99999999999999989 725 0.99999999999999989 729 0.99999999999999989 734 0.99999999999999989
		 740 0.99999999999999989 752 0.99999999999999989 759 0.99999999999999989 765 0.99999999999999989
		 779 0.99999999999999989 806 0.99999999999999989 819 0.99999999999999989 826 0.99999999999999989
		 841 0.99999999999999989 850 0.99999999999999989 864 0.99999999999999989 888 0.99999999999999989;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "L_eyeBall_ctrl_visibility";
	rename -uid "9AADE5BA-45FF-FEBD-FCD9-93BBD6006B21";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  9 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "L_eyeBall_ctrl_translateX";
	rename -uid "4FCFA2F5-425C-2F39-DBE2-75AF42AEA671";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "L_eyeBall_ctrl_translateY";
	rename -uid "EF5E0013-4F11-08F8-0DA1-CEA1F61F9C40";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "L_eyeBall_ctrl_translateZ";
	rename -uid "75AFD6F2-4F45-2DC8-32E5-19B401904B6D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "L_eyeBall_ctrl_rotateX";
	rename -uid "58F43944-4FE4-C912-FB25-2CBBF24B6A74";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "L_eyeBall_ctrl_rotateY";
	rename -uid "7858062F-427A-3714-26E3-90B2FC9C588A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "L_eyeBall_ctrl_rotateZ";
	rename -uid "A358951C-49F7-1DE3-5360-2E854204C5EB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "L_eyeBall_ctrl_scaleX";
	rename -uid "4E51BA8A-47F6-5FD3-1821-AE8730684C9B";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "L_eyeBall_ctrl_scaleY";
	rename -uid "0E3BD846-4251-20D4-F664-EFBAD5A7B9A6";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "L_eyeBall_ctrl_scaleZ";
	rename -uid "807E0313-4D53-D20D-947C-ACAAEE7EF16F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "R_eyeBall_ctrl_visibility";
	rename -uid "2ADB00BE-41B7-6A39-0463-0F8F641A066E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  9 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "R_eyeBall_ctrl_translateX";
	rename -uid "4C5E78A2-417D-1495-E53B-78855099B9B2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "R_eyeBall_ctrl_translateY";
	rename -uid "8F85EA2C-4C95-DE23-F43F-76A2BDD87F72";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTL -n "R_eyeBall_ctrl_translateZ";
	rename -uid "5E0EE8BF-461E-C2A3-FFA4-D9833B7B021F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "R_eyeBall_ctrl_rotateX";
	rename -uid "8D9D96D3-46CE-305A-14EA-F795EF6D77B4";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "R_eyeBall_ctrl_rotateY";
	rename -uid "F2112E98-4EEC-BD29-C013-498E01DBAE7A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTA -n "R_eyeBall_ctrl_rotateZ";
	rename -uid "90320038-41E9-7237-EC1D-CD8C41E6A30C";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 0 7 0 13 0 217 0 275 0 485 0 509 0 510 0
		 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0
		 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0
		 647 0 648 0 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0
		 720 0 725 0 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0
		 864 0 888 0;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "R_eyeBall_ctrl_scaleX";
	rename -uid "B58BFC5A-4D74-546E-9EEE-5AB745943FDF";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "R_eyeBall_ctrl_scaleY";
	rename -uid "BC9C34A2-49BB-F4DD-1AB7-2EA3891F91EC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "R_eyeBall_ctrl_scaleZ";
	rename -uid "1F17ED98-4EED-9835-4825-4E850D95A49D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 66 ".ktv[0:65]"  1 1 7 1 13 1 217 1 275 1 485 1 509 1 510 1
		 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1 568 1
		 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1 643 1
		 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1 716 1
		 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1 850 1
		 864 1 888 1;
	setAttr -s 66 ".kit[62:65]"  3 18 18 18;
	setAttr -s 66 ".kot[1:65]"  5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5;
createNode animCurveTU -n "R_eyeLid_ctrl_visibility";
	rename -uid "5209B943-4AA2-1D2D-9174-BA93B7C8835D";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 1 7 1 13 1 202 1 217 1 275 1 485 1 509 1
		 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1
		 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1
		 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1
		 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1
		 850 1 864 1 888 1;
	setAttr -s 67 ".kit[0:66]"  18 18 18 9 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 1 
		1 1 1 1 1 1 1 1 1 1 1 1 1 18 18 18 18 
		18 18 18 18 9 18 18 18;
	setAttr -s 67 ".kot[0:66]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
	setAttr -s 67 ".kix[40:66]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1;
	setAttr -s 67 ".kiy[40:66]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTL -n "R_eyeLid_ctrl_translateX";
	rename -uid "10DFBD2B-4094-A9DD-D352-F9987C6DE49C";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  1 0 7 0 13 0 202 -0.033747588723035536 217 -0.033747588723035536
		 254 0 275 -0.033747588723035536 485 -7.3552275381416621e-16 509 0 510 0 515 0 516 0
		 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0 568 0 580 0 581 0
		 596 0 597 0 613 0 614 0 624 0 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0
		 656 0 657 0 666 0 667 0 681 0 684 0 687 0 690 0 697 0 707 0 711 0 716 0 720 0 725 0
		 729 0 734 0 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0 888 0;
	setAttr -s 68 ".kit[0:67]"  18 18 18 3 18 3 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 
		1 1 1 1 1 1 1 1 1 1 1 1 1 1 18 18 18 
		18 18 18 18 18 3 18 18 18;
	setAttr -s 68 ".kot[0:67]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 68 ".kix[41:67]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1;
	setAttr -s 68 ".kiy[41:67]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTL -n "R_eyeLid_ctrl_translateY";
	rename -uid "0E2ABE41-4D9E-D135-D1EC-E0B13B51A09C";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 68 ".ktv[0:67]"  1 0 7 0 13 0 202 -0.2364822865856325 217 -0.2364822865856325
		 254 0 275 -0.2364822865856325 485 0.1580368632388599 509 0.1580368632388599 510 0.1580368632388599
		 515 0.1580368632388599 516 0.1580368632388599 526 0.1580368632388599 527 0.1580368632388599
		 535 0.1580368632388599 536 0.1580368632388599 544 0.1580368632388599 545 0.1580368632388599
		 555 0.1580368632388599 556 0.1580368632388599 560 0.1580368632388599 561 0.1580368632388599
		 567 0.1580368632388599 568 0.1580368632388599 580 0.1580368632388599 581 0.1580368632388599
		 596 0.1580368632388599 597 0.1580368632388599 613 0.1580368632388599 614 0.1580368632388599
		 624 0.1580368632388599 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0
		 657 0.1580368632388599 666 0.1580368632388599 667 0.1580368632388599 681 0.1580368632388599
		 684 0 687 0 690 0 697 0.1580368632388599 707 0 711 0 716 0 720 0 725 0 729 0 734 0.1580368632388599
		 740 0.1580368632388599 752 0.1580368632388599 759 0.1580368632388599 765 0.1580368632388599
		 779 0.1580368632388599 806 0.1580368632388599 819 0.1580368632388599 826 0.1580368632388599
		 841 0.1580368632388599 850 0.1580368632388599 864 0.1580368632388599 888 0.1580368632388599;
	setAttr -s 68 ".kit[0:67]"  18 18 18 3 18 3 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 
		1 1 1 1 1 1 1 1 1 1 1 1 1 1 18 18 18 
		18 18 18 18 18 3 18 18 18;
	setAttr -s 68 ".kot[0:67]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5;
	setAttr -s 68 ".kix[41:67]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1;
	setAttr -s 68 ".kiy[41:67]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTL -n "R_eyeLid_ctrl_translateZ";
	rename -uid "A9F48CFB-4EE9-5F2B-0BD0-6AAA4C9AFED5";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 70 ".ktv[0:69]"  1 0 7 0 13 0 159 0.29833136570519847 161 0
		 202 0.26205744274314519 217 0.26205744274314519 254 0 275 0.26205744274314519 485 0.29999999999998234
		 509 0.3 510 0.3 515 0.3 516 0.3 526 0.3 527 0.3 535 0.3 536 0.3 544 0.3 545 0.3 555 0.3
		 556 0.3 560 0.3 561 0.3 567 0.3 568 0.3 580 0.3 581 0.3 596 0.3 597 0.3 613 0.3 614 0.3
		 624 0.3 625 0 630 0 631 0 636 0 637 0 642 0 643 0 647 0 648 0 656 0 657 0.3 666 0.3
		 667 0.3 681 0.3 684 0 687 0 690 0 697 0.3 707 0 711 0 716 0 720 0 725 0 729 0 734 0.3
		 740 0.3 752 0.3 759 0.3 765 0.3 779 0.3 806 0.3 819 0.3 826 0.3 841 0.3 850 0.3 864 0.3
		 888 0.3;
	setAttr -s 70 ".kit[0:69]"  18 18 18 3 3 3 18 3 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 18 
		18 18 18 18 18 18 18 3 18 18 18;
	setAttr -s 70 ".kot[0:69]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5;
	setAttr -s 70 ".kix[43:69]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1;
	setAttr -s 70 ".kiy[43:69]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTA -n "R_eyeLid_ctrl_rotateX";
	rename -uid "531BBEA3-4436-1429-E224-66A6BCA72B22";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 0 7 0 13 0 202 0 217 0 275 0 485 0 509 0
		 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0
		 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 -0.61302970495536158 630 -0.61302970495536158
		 631 -0.61302970495536158 636 -0.61302970495536158 637 -0.61302970495536158 642 -0.61302970495536158
		 643 -0.61302970495536158 647 -0.61302970495536158 648 -0.61302970495536158 656 -0.61302970495536158
		 657 0 666 0 667 0 681 0 684 -0.61302970495536158 687 -0.61302970495536158 690 -0.61302970495536158
		 697 0 707 -0.61302970495536158 711 -0.61302970495536158 716 -0.61302970495536158
		 720 -0.61302970495536158 725 -0.61302970495536158 729 -0.61302970495536158 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0 888 0;
	setAttr -s 67 ".kit[0:66]"  18 18 18 3 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 1 
		1 1 1 1 1 1 1 1 1 1 1 1 1 18 18 18 18 
		18 18 18 18 3 18 18 18;
	setAttr -s 67 ".kot[0:66]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
	setAttr -s 67 ".kix[40:66]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1;
	setAttr -s 67 ".kiy[40:66]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTA -n "R_eyeLid_ctrl_rotateY";
	rename -uid "D3109769-411F-0197-B4C8-1BBCA0761001";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 0 7 0 13 0 202 0 217 0 275 0 485 0 509 0
		 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0
		 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 -0.61683198361872382 630 -0.61683198361872382
		 631 -0.61683198361872382 636 -0.61683198361872382 637 -0.61683198361872382 642 -0.61683198361872382
		 643 -0.61683198361872382 647 -0.61683198361872382 648 -0.61683198361872382 656 -0.61683198361872382
		 657 0 666 0 667 0 681 0 684 -0.61683198361872382 687 -0.61683198361872382 690 -0.61683198361872382
		 697 0 707 -0.61683198361872382 711 -0.61683198361872382 716 -0.61683198361872382
		 720 -0.61683198361872382 725 -0.61683198361872382 729 -0.61683198361872382 734 0
		 740 0 752 0 759 0 765 0 779 0 806 0 819 0 826 0 841 0 850 0 864 0 888 0;
	setAttr -s 67 ".kit[0:66]"  18 18 18 3 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 1 
		1 1 1 1 1 1 1 1 1 1 1 1 1 18 18 18 18 
		18 18 18 18 3 18 18 18;
	setAttr -s 67 ".kot[0:66]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
	setAttr -s 67 ".kix[40:66]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1;
	setAttr -s 67 ".kiy[40:66]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTA -n "R_eyeLid_ctrl_rotateZ";
	rename -uid "8EB19944-4E76-8C2E-8BB6-9381799A9432";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 0 7 0 13 0 202 0 217 0 275 0 485 0 509 0
		 510 0 515 0 516 0 526 0 527 0 535 0 536 0 544 0 545 0 555 0 556 0 560 0 561 0 567 0
		 568 0 580 0 581 0 596 0 597 0 613 0 614 0 624 0 625 0.29600701608139651 630 0.29600701608139651
		 631 0.29600701608139651 636 0.29600701608139651 637 0.29600701608139651 642 0.29600701608139651
		 643 0.29600701608139651 647 0.29600701608139651 648 0.29600701608139651 656 0.29600701608139651
		 657 0 666 0 667 0 681 0 684 0.29600701608139651 687 0.29600701608139651 690 0.29600701608139651
		 697 0 707 0.29600701608139651 711 0.29600701608139651 716 0.29600701608139651 720 0.29600701608139651
		 725 0.29600701608139651 729 0.29600701608139651 734 0 740 0 752 0 759 0 765 0 779 0
		 806 0 819 0 826 0 841 0 850 0 864 0 888 0;
	setAttr -s 67 ".kit[0:66]"  18 18 18 3 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 1 
		1 1 1 1 1 1 1 1 1 1 1 1 1 18 18 18 18 
		18 18 18 18 3 18 18 18;
	setAttr -s 67 ".kot[0:66]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
	setAttr -s 67 ".kix[40:66]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1;
	setAttr -s 67 ".kiy[40:66]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTU -n "R_eyeLid_ctrl_scaleX";
	rename -uid "42C8606C-4509-4144-4938-CBA88FD2583E";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 0.99999999999999989 7 0.99999999999999989
		 13 0.99999999999999989 202 0.99999999999999989 217 0.99999999999999989 275 0.99999999999999989
		 485 0.99999999999999989 509 0.99999999999999989 510 0.99999999999999989 515 0.99999999999999989
		 516 0.99999999999999989 526 0.99999999999999989 527 0.99999999999999989 535 0.99999999999999989
		 536 0.99999999999999989 544 0.99999999999999989 545 0.99999999999999989 555 0.99999999999999989
		 556 0.99999999999999989 560 0.99999999999999989 561 0.99999999999999989 567 0.99999999999999989
		 568 0.99999999999999989 580 0.99999999999999989 581 0.99999999999999989 596 0.99999999999999989
		 597 0.99999999999999989 613 0.99999999999999989 614 0.99999999999999989 624 0.99999999999999989
		 625 0.99999999999999989 630 0.99999999999999989 631 0.99999999999999989 636 0.99999999999999989
		 637 0.99999999999999989 642 0.99999999999999989 643 0.99999999999999989 647 0.99999999999999989
		 648 0.99999999999999989 656 0.99999999999999989 657 0.99999999999999989 666 0.99999999999999989
		 667 0.99999999999999989 681 0.99999999999999989 684 0.99999999999999989 687 0.99999999999999989
		 690 0.99999999999999989 697 0.99999999999999989 707 0.99999999999999989 711 0.99999999999999989
		 716 0.99999999999999989 720 0.99999999999999989 725 0.99999999999999989 729 0.99999999999999989
		 734 0.99999999999999989 740 0.99999999999999989 752 0.99999999999999989 759 0.99999999999999989
		 765 0.99999999999999989 779 0.99999999999999989 806 0.99999999999999989 819 0.99999999999999989
		 826 0.99999999999999989 841 0.99999999999999989 850 0.99999999999999989 864 0.99999999999999989
		 888 0.99999999999999989;
	setAttr -s 67 ".kit[0:66]"  18 18 18 3 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 1 
		1 1 1 1 1 1 1 1 1 1 1 1 1 18 18 18 18 
		18 18 18 18 3 18 18 18;
	setAttr -s 67 ".kot[0:66]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
	setAttr -s 67 ".kix[40:66]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1;
	setAttr -s 67 ".kiy[40:66]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTU -n "R_eyeLid_ctrl_scaleY";
	rename -uid "E07C660D-4F19-9A26-D1C5-C191B9DFCB77";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 1 7 1 13 1 202 1 217 1 275 1 485 1 509 1
		 510 1 515 1 516 1 526 1 527 1 535 1 536 1 544 1 545 1 555 1 556 1 560 1 561 1 567 1
		 568 1 580 1 581 1 596 1 597 1 613 1 614 1 624 1 625 1 630 1 631 1 636 1 637 1 642 1
		 643 1 647 1 648 1 656 1 657 1 666 1 667 1 681 1 684 1 687 1 690 1 697 1 707 1 711 1
		 716 1 720 1 725 1 729 1 734 1 740 1 752 1 759 1 765 1 779 1 806 1 819 1 826 1 841 1
		 850 1 864 1 888 1;
	setAttr -s 67 ".kit[0:66]"  18 18 18 3 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 1 
		1 1 1 1 1 1 1 1 1 1 1 1 1 18 18 18 18 
		18 18 18 18 3 18 18 18;
	setAttr -s 67 ".kot[0:66]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
	setAttr -s 67 ".kix[40:66]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1;
	setAttr -s 67 ".kiy[40:66]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTU -n "R_eyeLid_ctrl_scaleZ";
	rename -uid "FDA4A2B1-45C0-293F-80D9-20A7CC0465B9";
	setAttr ".tan" 5;
	setAttr ".wgt" no;
	setAttr -s 67 ".ktv[0:66]"  1 0.99999999999999989 7 0.99999999999999989
		 13 0.99999999999999989 202 0.99999999999999989 217 0.99999999999999989 275 0.99999999999999989
		 485 0.99999999999999989 509 0.99999999999999989 510 0.99999999999999989 515 0.99999999999999989
		 516 0.99999999999999989 526 0.99999999999999989 527 0.99999999999999989 535 0.99999999999999989
		 536 0.99999999999999989 544 0.99999999999999989 545 0.99999999999999989 555 0.99999999999999989
		 556 0.99999999999999989 560 0.99999999999999989 561 0.99999999999999989 567 0.99999999999999989
		 568 0.99999999999999989 580 0.99999999999999989 581 0.99999999999999989 596 0.99999999999999989
		 597 0.99999999999999989 613 0.99999999999999989 614 0.99999999999999989 624 0.99999999999999989
		 625 0.99999999999999989 630 0.99999999999999989 631 0.99999999999999989 636 0.99999999999999989
		 637 0.99999999999999989 642 0.99999999999999989 643 0.99999999999999989 647 0.99999999999999989
		 648 0.99999999999999989 656 0.99999999999999989 657 0.99999999999999989 666 0.99999999999999989
		 667 0.99999999999999989 681 0.99999999999999989 684 0.99999999999999989 687 0.99999999999999989
		 690 0.99999999999999989 697 0.99999999999999989 707 0.99999999999999989 711 0.99999999999999989
		 716 0.99999999999999989 720 0.99999999999999989 725 0.99999999999999989 729 0.99999999999999989
		 734 0.99999999999999989 740 0.99999999999999989 752 0.99999999999999989 759 0.99999999999999989
		 765 0.99999999999999989 779 0.99999999999999989 806 0.99999999999999989 819 0.99999999999999989
		 826 0.99999999999999989 841 0.99999999999999989 850 0.99999999999999989 864 0.99999999999999989
		 888 0.99999999999999989;
	setAttr -s 67 ".kit[0:66]"  18 18 18 3 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 
		18 18 18 18 18 18 18 18 18 18 18 18 18 18 18 1 1 
		1 1 1 1 1 1 1 1 1 1 1 1 1 18 18 18 18 
		18 18 18 18 3 18 18 18;
	setAttr -s 67 ".kot[0:66]"  18 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 
		5 5 5 5 5 5 5 5;
	setAttr -s 67 ".kix[40:66]"  1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 
		1 1 1 1 1 1 1;
	setAttr -s 67 ".kiy[40:66]"  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
		0 0 0 0 0 0 0;
createNode animCurveTU -n "M_Head_offset_pointConstraint1_nodeState";
	rename -uid "FA34FAED-4972-4E65-88A7-4D838547463F";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  5 0 596 0 819 0 826 0 850 0;
createNode animCurveTL -n "M_Head_offset_pointConstraint1_offsetX";
	rename -uid "504C5175-4FF5-955B-BA43-51A96F420E73";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  5 0 596 0 819 0 826 0 850 0;
createNode animCurveTL -n "M_Head_offset_pointConstraint1_offsetY";
	rename -uid "A3D0080C-466B-F0EE-1A4E-C0AD823050DC";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  5 0 596 0 819 0 826 0 850 0;
createNode animCurveTL -n "M_Head_offset_pointConstraint1_offsetZ";
	rename -uid "94B30FEF-403B-26D1-7781-44850F76EDA9";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  5 0 596 0 819 0 826 0 850 0;
createNode animCurveTU -n "M_Head_offset_pointConstraint1_M_Head_pointConstrain_offsetW0";
	rename -uid "0F0FA9BF-4C23-6D5D-D755-2D93CB0D3633";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 5 ".ktv[0:4]"  5 1 596 1 819 1 826 1 850 1;
createNode audio -n "drunk_squirrel_ringtone";
	rename -uid "B0F5136E-4FC1-FBCB-81AB-9EB1BADCA10D";
	setAttr ".o" 190;
	setAttr ".ef" 887.46938711734697;
	setAttr ".se" 697.46938711734697;
	setAttr ".f" -type "string" "C:/Users/davnkrit/Documents/Audacity/drunk-squirrel-ringtone.wav";
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "57CC6797-4BF3-AECC-8008-C9A82A885A27";
	setAttr ".version" -type "string" "3.1.2";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "FC2F6FBF-4B90-1C18-A187-2D8DA4D7183D";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "C7C5151C-49A4-5590-5D8B-C791D0EAEFAF";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "14B0709F-4B4E-D12E-13E4-44A268F21F42";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode expression -n "expression1";
	rename -uid "8309947E-4F91-312E-C752-1395DB7920F0";
	setAttr -k on ".nds";
	setAttr ".ixp" -type "string" ".O[0]=frame";
createNode animCurveTA -n "R_Wrist_ctrl_offset_rotateX";
	rename -uid "F7F1C3F3-4E6A-176A-5D58-519EBC24FD6A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  819 0 826 0 850 0;
createNode animCurveTA -n "R_Wrist_ctrl_offset_rotateY";
	rename -uid "E5982E54-448B-1190-2ACF-1F9DCC0F1E6D";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  819 0 826 0 850 0;
createNode animCurveTA -n "R_Wrist_ctrl_offset_rotateZ";
	rename -uid "C68FF78F-46A6-A4CD-2AB9-1184A80769D8";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  819 0 826 0 850 0;
createNode animCurveTU -n "R_Wrist_ctrl_offset_visibility";
	rename -uid "69328125-4D1C-5600-E1B7-38BBCED424BB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  819 1 826 1 850 1;
createNode animCurveTL -n "R_Wrist_ctrl_offset_translateX";
	rename -uid "6DD1FC78-4B3E-7F4B-5FC2-77A3117F23BB";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  819 11.474157865482777 826 11.474157865482777
		 850 11.474157865482777;
createNode animCurveTL -n "R_Wrist_ctrl_offset_translateY";
	rename -uid "C361642B-4B20-28F0-ADD6-97B4E5C935B5";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  819 -7.1054273576010019e-15 826 -7.1054273576010019e-15
		 850 -7.1054273576010019e-15;
createNode animCurveTL -n "R_Wrist_ctrl_offset_translateZ";
	rename -uid "3C6393C1-4538-E9AD-8122-4E9063331271";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  819 -1.3947176746853531e-15 826 -1.3947176746853531e-15
		 850 -1.3947176746853531e-15;
createNode animCurveTU -n "R_Wrist_ctrl_offset_scaleX";
	rename -uid "CBFFB2C5-425C-B5B5-13BF-72A52AAD0F7E";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  819 1 826 1 850 1;
createNode animCurveTU -n "R_Wrist_ctrl_offset_scaleY";
	rename -uid "8FF9EC5E-4A3E-4F48-E7D1-03B0EF39865A";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  819 0.99999999999999989 826 0.99999999999999989
		 850 0.99999999999999989;
createNode animCurveTU -n "R_Wrist_ctrl_offset_scaleZ";
	rename -uid "A7BE872A-4501-12BE-F59C-28B3360E4F05";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 3 ".ktv[0:2]"  819 1 826 1 850 1;
createNode animCurveTA -n "L_Wrist_ctrl_rotateX";
	rename -uid "82718824-4A18-7A94-B57D-98A0381A1930";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  397 0 398 2.8679407332376026 436 -20.561225335265192
		 485 -20.561225335265192 568 -92.67699180584944 637 -86.155321571460789 677 -86.155321571460789
		 681 -86.155321571460789 684 -86.155321571460789;
	setAttr -s 9 ".kot[0:8]"  5 5 5 5 5 5 5 5 
		5;
createNode animCurveTA -n "L_Wrist_ctrl_rotateY";
	rename -uid "8C598C31-48F1-F134-75F6-86BBE98FE894";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  397 0 398 -12.102680638086571 436 -38.009941916867639
		 485 -38.009941916867639 568 12.00546349641818 637 20.554807195124177 677 20.554807195124177
		 681 20.554807195124177 684 20.554807195124177;
	setAttr -s 9 ".kot[0:8]"  5 5 5 5 5 5 5 5 
		5;
createNode animCurveTA -n "L_Wrist_ctrl_rotateZ";
	rename -uid "340F36D5-42B4-C6F0-FE10-818743F8C95B";
	setAttr ".tan" 3;
	setAttr ".wgt" no;
	setAttr -s 9 ".ktv[0:8]"  397 0 398 -10.030363011022576 436 -17.330977617786509
		 485 -17.330977617786509 568 -82.222199485905705 637 9.7108623653928188 677 9.7108623653928188
		 681 9.7108623653928188 684 9.7108623653928188;
	setAttr -s 9 ".kot[0:8]"  5 5 5 5 5 5 5 5 
		5;
createNode mute -n "tUtilities";
	rename -uid "85B9C219-440C-2A52-2D5E-DDB531759FF3";
	addAttr -ci true -sn "cameraSelected" -ln "cameraSelected" -dt "string";
	setAttr ".cameraSelected" -type "string" "rig:leftShape";
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 739;
	setAttr ".unw" 739;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 3 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 5 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".u";
select -ne :defaultRenderingList1;
	setAttr -s 2 ".r";
select -ne :defaultTextureList1;
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 24 ".dsm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".mcfr" 30;
	setAttr ".ren" -type "string" "arnold";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av -k on ".w";
	setAttr -av -k on ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av -k on ".dar";
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off ".ctrs" 256;
	setAttr -k off ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr" 30;
select -ne :ikSystem;
	setAttr -s 2 ".sol";
connectAttr "M_Root_ctrl_L_Arm_FKIK.o" "rigRN.phl[123]";
connectAttr "M_Root_ctrl_R_Arm_FKIK.o" "rigRN.phl[124]";
connectAttr "M_Root_ctrl_visibility.o" "rigRN.phl[125]";
connectAttr "M_Root_ctrl_translateX.o" "rigRN.phl[126]";
connectAttr "M_Root_ctrl_translateY.o" "rigRN.phl[127]";
connectAttr "M_Root_ctrl_translateZ.o" "rigRN.phl[128]";
connectAttr "M_Root_ctrl_rotateX.o" "rigRN.phl[129]";
connectAttr "M_Root_ctrl_rotateY.o" "rigRN.phl[130]";
connectAttr "M_Root_ctrl_rotateZ.o" "rigRN.phl[131]";
connectAttr "M_Root_ctrl_scaleX.o" "rigRN.phl[132]";
connectAttr "M_Root_ctrl_scaleY.o" "rigRN.phl[133]";
connectAttr "M_Root_ctrl_scaleZ.o" "rigRN.phl[134]";
connectAttr "L_foot_ctrl_toeTap.o" "rigRN.phl[135]";
connectAttr "L_foot_ctrl_footPeel.o" "rigRN.phl[136]";
connectAttr "L_foot_ctrl_footSwivel.o" "rigRN.phl[137]";
connectAttr "L_foot_ctrl_toeTip.o" "rigRN.phl[138]";
connectAttr "L_foot_ctrl_ankle.o" "rigRN.phl[139]";
connectAttr "L_foot_ctrl_visibility.o" "rigRN.phl[140]";
connectAttr "L_foot_ctrl_translateX.o" "rigRN.phl[141]";
connectAttr "L_foot_ctrl_translateY.o" "rigRN.phl[142]";
connectAttr "L_foot_ctrl_translateZ.o" "rigRN.phl[143]";
connectAttr "L_foot_ctrl_rotateX.o" "rigRN.phl[144]";
connectAttr "L_foot_ctrl_rotateY.o" "rigRN.phl[145]";
connectAttr "L_foot_ctrl_rotateZ.o" "rigRN.phl[146]";
connectAttr "L_foot_ctrl_scaleX.o" "rigRN.phl[147]";
connectAttr "L_foot_ctrl_scaleY.o" "rigRN.phl[148]";
connectAttr "L_foot_ctrl_scaleZ.o" "rigRN.phl[149]";
connectAttr "L_knee_ctrl_translateX.o" "rigRN.phl[150]";
connectAttr "L_knee_ctrl_translateY.o" "rigRN.phl[151]";
connectAttr "L_knee_ctrl_translateZ.o" "rigRN.phl[152]";
connectAttr "L_knee_ctrl_visibility.o" "rigRN.phl[153]";
connectAttr "L_knee_ctrl_rotateX.o" "rigRN.phl[154]";
connectAttr "L_knee_ctrl_rotateY.o" "rigRN.phl[155]";
connectAttr "L_knee_ctrl_rotateZ.o" "rigRN.phl[156]";
connectAttr "L_knee_ctrl_scaleX.o" "rigRN.phl[157]";
connectAttr "L_knee_ctrl_scaleY.o" "rigRN.phl[158]";
connectAttr "L_knee_ctrl_scaleZ.o" "rigRN.phl[159]";
connectAttr "R_foot_ctrl_toeTap.o" "rigRN.phl[160]";
connectAttr "R_foot_ctrl_footPeel.o" "rigRN.phl[161]";
connectAttr "R_foot_ctrl_footSwivel.o" "rigRN.phl[162]";
connectAttr "R_foot_ctrl_toeTip.o" "rigRN.phl[163]";
connectAttr "R_foot_ctrl_ankle.o" "rigRN.phl[164]";
connectAttr "R_foot_ctrl_visibility.o" "rigRN.phl[165]";
connectAttr "R_foot_ctrl_translateX.o" "rigRN.phl[166]";
connectAttr "R_foot_ctrl_translateY.o" "rigRN.phl[167]";
connectAttr "R_foot_ctrl_translateZ.o" "rigRN.phl[168]";
connectAttr "R_foot_ctrl_rotateX.o" "rigRN.phl[169]";
connectAttr "R_foot_ctrl_rotateY.o" "rigRN.phl[170]";
connectAttr "R_foot_ctrl_rotateZ.o" "rigRN.phl[171]";
connectAttr "R_foot_ctrl_scaleX.o" "rigRN.phl[172]";
connectAttr "R_foot_ctrl_scaleY.o" "rigRN.phl[173]";
connectAttr "R_foot_ctrl_scaleZ.o" "rigRN.phl[174]";
connectAttr "R_knee_ctrl_translateX.o" "rigRN.phl[175]";
connectAttr "R_knee_ctrl_translateY.o" "rigRN.phl[176]";
connectAttr "R_knee_ctrl_translateZ.o" "rigRN.phl[177]";
connectAttr "R_knee_ctrl_visibility.o" "rigRN.phl[178]";
connectAttr "R_knee_ctrl_rotateX.o" "rigRN.phl[179]";
connectAttr "R_knee_ctrl_rotateY.o" "rigRN.phl[180]";
connectAttr "R_knee_ctrl_rotateZ.o" "rigRN.phl[181]";
connectAttr "R_knee_ctrl_scaleX.o" "rigRN.phl[182]";
connectAttr "R_knee_ctrl_scaleY.o" "rigRN.phl[183]";
connectAttr "R_knee_ctrl_scaleZ.o" "rigRN.phl[184]";
connectAttr "M_COG_ctrl_translateX.o" "rigRN.phl[185]";
connectAttr "M_COG_ctrl_translateY.o" "rigRN.phl[186]";
connectAttr "M_COG_ctrl_translateZ.o" "rigRN.phl[187]";
connectAttr "M_COG_ctrl_rotateX.o" "rigRN.phl[188]";
connectAttr "M_COG_ctrl_rotateY.o" "rigRN.phl[189]";
connectAttr "M_COG_ctrl_rotateZ.o" "rigRN.phl[190]";
connectAttr "M_COG_ctrl_scaleX.o" "rigRN.phl[191]";
connectAttr "M_COG_ctrl_scaleY.o" "rigRN.phl[192]";
connectAttr "M_COG_ctrl_scaleZ.o" "rigRN.phl[193]";
connectAttr "M_COG_ctrl_visibility.o" "rigRN.phl[194]";
connectAttr "L_IK_Elbow_ctrl_translateX.o" "rigRN.phl[195]";
connectAttr "L_IK_Elbow_ctrl_translateY.o" "rigRN.phl[196]";
connectAttr "L_IK_Elbow_ctrl_translateZ.o" "rigRN.phl[197]";
connectAttr "L_IK_Elbow_ctrl_rotateX.o" "rigRN.phl[198]";
connectAttr "L_IK_Elbow_ctrl_rotateY.o" "rigRN.phl[199]";
connectAttr "L_IK_Elbow_ctrl_rotateZ.o" "rigRN.phl[200]";
connectAttr "L_IK_Elbow_ctrl_scaleX.o" "rigRN.phl[201]";
connectAttr "L_IK_Elbow_ctrl_scaleY.o" "rigRN.phl[202]";
connectAttr "L_IK_Elbow_ctrl_scaleZ.o" "rigRN.phl[203]";
connectAttr "R_IK_Elbow_ctrl_translateX.o" "rigRN.phl[204]";
connectAttr "R_IK_Elbow_ctrl_translateY.o" "rigRN.phl[205]";
connectAttr "R_IK_Elbow_ctrl_translateZ.o" "rigRN.phl[206]";
connectAttr "R_IK_Elbow_ctrl_rotateX.o" "rigRN.phl[207]";
connectAttr "R_IK_Elbow_ctrl_rotateY.o" "rigRN.phl[208]";
connectAttr "R_IK_Elbow_ctrl_rotateZ.o" "rigRN.phl[209]";
connectAttr "R_IK_Elbow_ctrl_scaleX.o" "rigRN.phl[210]";
connectAttr "R_IK_Elbow_ctrl_scaleY.o" "rigRN.phl[211]";
connectAttr "R_IK_Elbow_ctrl_scaleZ.o" "rigRN.phl[212]";
connectAttr "L_UpArm_ctrl_rotateX.o" "rigRN.phl[213]";
connectAttr "L_UpArm_ctrl_rotateY.o" "rigRN.phl[214]";
connectAttr "L_UpArm_ctrl_rotateZ.o" "rigRN.phl[215]";
connectAttr "L_UpArm_ctrl_translateX.o" "rigRN.phl[216]";
connectAttr "L_UpArm_ctrl_translateY.o" "rigRN.phl[217]";
connectAttr "L_UpArm_ctrl_translateZ.o" "rigRN.phl[218]";
connectAttr "L_UpArm_ctrl_scaleX.o" "rigRN.phl[219]";
connectAttr "L_UpArm_ctrl_scaleY.o" "rigRN.phl[220]";
connectAttr "L_UpArm_ctrl_scaleZ.o" "rigRN.phl[221]";
connectAttr "L_LoArm_ctrl_rotateX.o" "rigRN.phl[222]";
connectAttr "L_LoArm_ctrl_rotateY.o" "rigRN.phl[223]";
connectAttr "L_LoArm_ctrl_rotateZ.o" "rigRN.phl[224]";
connectAttr "L_LoArm_ctrl_translateX.o" "rigRN.phl[225]";
connectAttr "L_LoArm_ctrl_translateY.o" "rigRN.phl[226]";
connectAttr "L_LoArm_ctrl_translateZ.o" "rigRN.phl[227]";
connectAttr "L_LoArm_ctrl_scaleX.o" "rigRN.phl[228]";
connectAttr "L_LoArm_ctrl_scaleY.o" "rigRN.phl[229]";
connectAttr "L_LoArm_ctrl_scaleZ.o" "rigRN.phl[230]";
connectAttr "L_Wrist_ctrl_rotateX.o" "rigRN.phl[231]";
connectAttr "L_Wrist_ctrl_rotateY.o" "rigRN.phl[232]";
connectAttr "L_Wrist_ctrl_rotateZ.o" "rigRN.phl[233]";
connectAttr "rigRN.phl[234]" "pairBlend1.w";
connectAttr "L_Wrist_ctrl_blendOrient1.o" "rigRN.phl[235]";
connectAttr "L_Wrist_ctrl_translateX.o" "rigRN.phl[236]";
connectAttr "L_Wrist_ctrl_translateY.o" "rigRN.phl[237]";
connectAttr "L_Wrist_ctrl_translateZ.o" "rigRN.phl[238]";
connectAttr "L_Wrist_ctrl_scaleX.o" "rigRN.phl[239]";
connectAttr "L_Wrist_ctrl_scaleY.o" "rigRN.phl[240]";
connectAttr "L_Wrist_ctrl_scaleZ.o" "rigRN.phl[241]";
connectAttr "R_UpArm_ctrl_rotateX.o" "rigRN.phl[242]";
connectAttr "R_UpArm_ctrl_rotateY.o" "rigRN.phl[243]";
connectAttr "R_UpArm_ctrl_rotateZ.o" "rigRN.phl[244]";
connectAttr "R_UpArm_ctrl_translateX.o" "rigRN.phl[245]";
connectAttr "R_UpArm_ctrl_translateY.o" "rigRN.phl[246]";
connectAttr "R_UpArm_ctrl_translateZ.o" "rigRN.phl[247]";
connectAttr "R_UpArm_ctrl_scaleX.o" "rigRN.phl[248]";
connectAttr "R_UpArm_ctrl_scaleY.o" "rigRN.phl[249]";
connectAttr "R_UpArm_ctrl_scaleZ.o" "rigRN.phl[250]";
connectAttr "R_LoArm_ctrl_rotateX.o" "rigRN.phl[251]";
connectAttr "R_LoArm_ctrl_rotateY.o" "rigRN.phl[252]";
connectAttr "R_LoArm_ctrl_rotateZ.o" "rigRN.phl[253]";
connectAttr "R_LoArm_ctrl_translateX.o" "rigRN.phl[254]";
connectAttr "R_LoArm_ctrl_translateY.o" "rigRN.phl[255]";
connectAttr "R_LoArm_ctrl_translateZ.o" "rigRN.phl[256]";
connectAttr "R_LoArm_ctrl_scaleX.o" "rigRN.phl[257]";
connectAttr "R_LoArm_ctrl_scaleY.o" "rigRN.phl[258]";
connectAttr "R_LoArm_ctrl_scaleZ.o" "rigRN.phl[259]";
connectAttr "R_Wrist_ctrl_offset_rotateX.o" "rigRN.phl[260]";
connectAttr "R_Wrist_ctrl_offset_rotateY.o" "rigRN.phl[261]";
connectAttr "R_Wrist_ctrl_offset_rotateZ.o" "rigRN.phl[262]";
connectAttr "R_Wrist_ctrl_offset_visibility.o" "rigRN.phl[263]";
connectAttr "R_Wrist_ctrl_offset_translateX.o" "rigRN.phl[264]";
connectAttr "R_Wrist_ctrl_offset_translateY.o" "rigRN.phl[265]";
connectAttr "R_Wrist_ctrl_offset_translateZ.o" "rigRN.phl[266]";
connectAttr "R_Wrist_ctrl_offset_scaleX.o" "rigRN.phl[267]";
connectAttr "R_Wrist_ctrl_offset_scaleY.o" "rigRN.phl[268]";
connectAttr "R_Wrist_ctrl_offset_scaleZ.o" "rigRN.phl[269]";
connectAttr "R_Wrist_ctrl_rotateX.o" "rigRN.phl[270]";
connectAttr "R_Wrist_ctrl_rotateY.o" "rigRN.phl[271]";
connectAttr "R_Wrist_ctrl_rotateZ.o" "rigRN.phl[272]";
connectAttr "R_Wrist_ctrl_translateX.o" "rigRN.phl[273]";
connectAttr "R_Wrist_ctrl_translateY.o" "rigRN.phl[274]";
connectAttr "R_Wrist_ctrl_translateZ.o" "rigRN.phl[275]";
connectAttr "R_Wrist_ctrl_scaleX.o" "rigRN.phl[276]";
connectAttr "R_Wrist_ctrl_scaleY.o" "rigRN.phl[277]";
connectAttr "R_Wrist_ctrl_scaleZ.o" "rigRN.phl[278]";
connectAttr "M_Hips_ctrl_rotateX.o" "rigRN.phl[279]";
connectAttr "M_Hips_ctrl_rotateY.o" "rigRN.phl[280]";
connectAttr "M_Hips_ctrl_rotateZ.o" "rigRN.phl[281]";
connectAttr "M_Hips_ctrl_translateX.o" "rigRN.phl[282]";
connectAttr "M_Hips_ctrl_translateY.o" "rigRN.phl[283]";
connectAttr "M_Hips_ctrl_translateZ.o" "rigRN.phl[284]";
connectAttr "M_Hips_ctrl_visibility.o" "rigRN.phl[285]";
connectAttr "M_Hips_ctrl_scaleX.o" "rigRN.phl[286]";
connectAttr "M_Hips_ctrl_scaleY.o" "rigRN.phl[287]";
connectAttr "M_Hips_ctrl_scaleZ.o" "rigRN.phl[288]";
connectAttr "M_eye_ctrl_L_Uplid.o" "rigRN.phl[289]";
connectAttr "M_eye_ctrl_L_Lolid.o" "rigRN.phl[290]";
connectAttr "M_eye_ctrl_R_Uplid.o" "rigRN.phl[291]";
connectAttr "M_eye_ctrl_R_Lolid.o" "rigRN.phl[292]";
connectAttr "M_eye_ctrl_L_Lid_Twist.o" "rigRN.phl[293]";
connectAttr "M_eye_ctrl_R_Lid_Twist.o" "rigRN.phl[294]";
connectAttr "M_eye_ctrl_L_Lid_Bend.o" "rigRN.phl[295]";
connectAttr "M_eye_ctrl_R_Lid_Bend.o" "rigRN.phl[296]";
connectAttr "M_eye_ctrl_visibility.o" "rigRN.phl[297]";
connectAttr "M_eye_ctrl_translateX.o" "rigRN.phl[298]";
connectAttr "M_eye_ctrl_translateY.o" "rigRN.phl[299]";
connectAttr "M_eye_ctrl_translateZ.o" "rigRN.phl[300]";
connectAttr "M_eye_ctrl_rotateX.o" "rigRN.phl[301]";
connectAttr "M_eye_ctrl_rotateY.o" "rigRN.phl[302]";
connectAttr "M_eye_ctrl_rotateZ.o" "rigRN.phl[303]";
connectAttr "M_eye_ctrl_scaleX.o" "rigRN.phl[304]";
connectAttr "M_eye_ctrl_scaleY.o" "rigRN.phl[305]";
connectAttr "M_eye_ctrl_scaleZ.o" "rigRN.phl[306]";
connectAttr "L_eye_ctrl_translateX.o" "rigRN.phl[307]";
connectAttr "L_eye_ctrl_translateY.o" "rigRN.phl[308]";
connectAttr "L_eye_ctrl_translateZ.o" "rigRN.phl[309]";
connectAttr "L_eye_ctrl_visibility.o" "rigRN.phl[310]";
connectAttr "L_eye_ctrl_rotateX.o" "rigRN.phl[311]";
connectAttr "L_eye_ctrl_rotateY.o" "rigRN.phl[312]";
connectAttr "L_eye_ctrl_rotateZ.o" "rigRN.phl[313]";
connectAttr "L_eye_ctrl_scaleX.o" "rigRN.phl[314]";
connectAttr "L_eye_ctrl_scaleY.o" "rigRN.phl[315]";
connectAttr "L_eye_ctrl_scaleZ.o" "rigRN.phl[316]";
connectAttr "R_eye_ctrl_translateX.o" "rigRN.phl[317]";
connectAttr "R_eye_ctrl_translateY.o" "rigRN.phl[318]";
connectAttr "R_eye_ctrl_translateZ.o" "rigRN.phl[319]";
connectAttr "R_eye_ctrl_visibility.o" "rigRN.phl[320]";
connectAttr "R_eye_ctrl_rotateX.o" "rigRN.phl[321]";
connectAttr "R_eye_ctrl_rotateY.o" "rigRN.phl[322]";
connectAttr "R_eye_ctrl_rotateZ.o" "rigRN.phl[323]";
connectAttr "R_eye_ctrl_scaleX.o" "rigRN.phl[324]";
connectAttr "R_eye_ctrl_scaleY.o" "rigRN.phl[325]";
connectAttr "R_eye_ctrl_scaleZ.o" "rigRN.phl[326]";
connectAttr "L_IK_Hand_ctrl_translateX.o" "rigRN.phl[327]";
connectAttr "L_IK_Hand_ctrl_translateY.o" "rigRN.phl[328]";
connectAttr "L_IK_Hand_ctrl_translateZ.o" "rigRN.phl[329]";
connectAttr "L_IK_Hand_ctrl_rotateX.o" "rigRN.phl[330]";
connectAttr "L_IK_Hand_ctrl_rotateY.o" "rigRN.phl[331]";
connectAttr "L_IK_Hand_ctrl_rotateZ.o" "rigRN.phl[332]";
connectAttr "L_IK_Hand_ctrl_scaleX.o" "rigRN.phl[333]";
connectAttr "L_IK_Hand_ctrl_scaleY.o" "rigRN.phl[334]";
connectAttr "L_IK_Hand_ctrl_scaleZ.o" "rigRN.phl[335]";
connectAttr "R_IK_Hand_ctrl_translateX.o" "rigRN.phl[336]";
connectAttr "R_IK_Hand_ctrl_translateY.o" "rigRN.phl[337]";
connectAttr "R_IK_Hand_ctrl_translateZ.o" "rigRN.phl[338]";
connectAttr "R_IK_Hand_ctrl_rotateX.o" "rigRN.phl[339]";
connectAttr "R_IK_Hand_ctrl_rotateY.o" "rigRN.phl[340]";
connectAttr "R_IK_Hand_ctrl_rotateZ.o" "rigRN.phl[341]";
connectAttr "R_IK_Hand_ctrl_scaleX.o" "rigRN.phl[342]";
connectAttr "R_IK_Hand_ctrl_scaleY.o" "rigRN.phl[343]";
connectAttr "R_IK_Hand_ctrl_scaleZ.o" "rigRN.phl[344]";
connectAttr "M_Head_ctrl_translateX.o" "rigRN.phl[345]";
connectAttr "M_Head_ctrl_translateY.o" "rigRN.phl[346]";
connectAttr "M_Head_ctrl_translateZ.o" "rigRN.phl[347]";
connectAttr "M_Head_ctrl_rotateX.o" "rigRN.phl[348]";
connectAttr "M_Head_ctrl_rotateY.o" "rigRN.phl[349]";
connectAttr "M_Head_ctrl_rotateZ.o" "rigRN.phl[350]";
connectAttr "M_Head_ctrl_scaleX.o" "rigRN.phl[351]";
connectAttr "M_Head_ctrl_scaleY.o" "rigRN.phl[352]";
connectAttr "M_Head_ctrl_scaleZ.o" "rigRN.phl[353]";
connectAttr "M_Head_ctrl_visibility.o" "rigRN.phl[354]";
connectAttr "M_mouth_main_ctrl_translateX.o" "rigRN.phl[355]";
connectAttr "M_mouth_main_ctrl_translateY.o" "rigRN.phl[356]";
connectAttr "M_mouth_main_ctrl_translateZ.o" "rigRN.phl[357]";
connectAttr "M_mouth_main_ctrl_rotateX.o" "rigRN.phl[358]";
connectAttr "M_mouth_main_ctrl_rotateY.o" "rigRN.phl[359]";
connectAttr "M_mouth_main_ctrl_rotateZ.o" "rigRN.phl[360]";
connectAttr "M_mouth_main_ctrl_scaleX.o" "rigRN.phl[361]";
connectAttr "M_mouth_main_ctrl_scaleY.o" "rigRN.phl[362]";
connectAttr "M_mouth_main_ctrl_scaleZ.o" "rigRN.phl[363]";
connectAttr "M_mouth_main_ctrl_visibility.o" "rigRN.phl[364]";
connectAttr "M_mouth_ctrl_translateX.o" "rigRN.phl[365]";
connectAttr "M_mouth_ctrl_translateY.o" "rigRN.phl[366]";
connectAttr "M_mouth_ctrl_translateZ.o" "rigRN.phl[367]";
connectAttr "M_mouth_ctrl_rotateX.o" "rigRN.phl[368]";
connectAttr "M_mouth_ctrl_rotateY.o" "rigRN.phl[369]";
connectAttr "M_mouth_ctrl_rotateZ.o" "rigRN.phl[370]";
connectAttr "M_mouth_ctrl_scaleX.o" "rigRN.phl[371]";
connectAttr "M_mouth_ctrl_scaleY.o" "rigRN.phl[372]";
connectAttr "M_mouth_ctrl_scaleZ.o" "rigRN.phl[373]";
connectAttr "M_mouth_ctrl_visibility.o" "rigRN.phl[374]";
connectAttr "R_mouth_ctrl_translateX.o" "rigRN.phl[375]";
connectAttr "R_mouth_ctrl_translateY.o" "rigRN.phl[376]";
connectAttr "R_mouth_ctrl_translateZ.o" "rigRN.phl[377]";
connectAttr "R_mouth_ctrl_rotateX.o" "rigRN.phl[378]";
connectAttr "R_mouth_ctrl_rotateY.o" "rigRN.phl[379]";
connectAttr "R_mouth_ctrl_rotateZ.o" "rigRN.phl[380]";
connectAttr "R_mouth_ctrl_scaleX.o" "rigRN.phl[381]";
connectAttr "R_mouth_ctrl_scaleY.o" "rigRN.phl[382]";
connectAttr "R_mouth_ctrl_scaleZ.o" "rigRN.phl[383]";
connectAttr "R_mouth_ctrl_visibility.o" "rigRN.phl[384]";
connectAttr "L_mouth_ctrl_translateX.o" "rigRN.phl[385]";
connectAttr "L_mouth_ctrl_translateY.o" "rigRN.phl[386]";
connectAttr "L_mouth_ctrl_translateZ.o" "rigRN.phl[387]";
connectAttr "L_mouth_ctrl_rotateX.o" "rigRN.phl[388]";
connectAttr "L_mouth_ctrl_rotateY.o" "rigRN.phl[389]";
connectAttr "L_mouth_ctrl_rotateZ.o" "rigRN.phl[390]";
connectAttr "L_mouth_ctrl_scaleX.o" "rigRN.phl[391]";
connectAttr "L_mouth_ctrl_scaleY.o" "rigRN.phl[392]";
connectAttr "L_mouth_ctrl_scaleZ.o" "rigRN.phl[393]";
connectAttr "L_mouth_ctrl_visibility.o" "rigRN.phl[394]";
connectAttr "L_eyeBall_ctrl_translateX.o" "rigRN.phl[395]";
connectAttr "L_eyeBall_ctrl_translateY.o" "rigRN.phl[396]";
connectAttr "L_eyeBall_ctrl_translateZ.o" "rigRN.phl[397]";
connectAttr "L_eyeBall_ctrl_rotateX.o" "rigRN.phl[398]";
connectAttr "L_eyeBall_ctrl_rotateY.o" "rigRN.phl[399]";
connectAttr "L_eyeBall_ctrl_rotateZ.o" "rigRN.phl[400]";
connectAttr "L_eyeBall_ctrl_scaleX.o" "rigRN.phl[401]";
connectAttr "L_eyeBall_ctrl_scaleY.o" "rigRN.phl[402]";
connectAttr "L_eyeBall_ctrl_scaleZ.o" "rigRN.phl[403]";
connectAttr "L_eyeBall_ctrl_visibility.o" "rigRN.phl[404]";
connectAttr "R_eyeBall_ctrl_translateX.o" "rigRN.phl[405]";
connectAttr "R_eyeBall_ctrl_translateY.o" "rigRN.phl[406]";
connectAttr "R_eyeBall_ctrl_translateZ.o" "rigRN.phl[407]";
connectAttr "R_eyeBall_ctrl_rotateX.o" "rigRN.phl[408]";
connectAttr "R_eyeBall_ctrl_rotateY.o" "rigRN.phl[409]";
connectAttr "R_eyeBall_ctrl_rotateZ.o" "rigRN.phl[410]";
connectAttr "R_eyeBall_ctrl_scaleX.o" "rigRN.phl[411]";
connectAttr "R_eyeBall_ctrl_scaleY.o" "rigRN.phl[412]";
connectAttr "R_eyeBall_ctrl_scaleZ.o" "rigRN.phl[413]";
connectAttr "R_eyeBall_ctrl_visibility.o" "rigRN.phl[414]";
connectAttr "R_eyeLid_ctrl_rotateX.o" "rigRN.phl[415]";
connectAttr "R_eyeLid_ctrl_rotateY.o" "rigRN.phl[416]";
connectAttr "R_eyeLid_ctrl_rotateZ.o" "rigRN.phl[417]";
connectAttr "R_eyeLid_ctrl_translateX.o" "rigRN.phl[418]";
connectAttr "R_eyeLid_ctrl_translateY.o" "rigRN.phl[419]";
connectAttr "R_eyeLid_ctrl_translateZ.o" "rigRN.phl[420]";
connectAttr "R_eyeLid_ctrl_visibility.o" "rigRN.phl[421]";
connectAttr "R_eyeLid_ctrl_scaleX.o" "rigRN.phl[422]";
connectAttr "R_eyeLid_ctrl_scaleY.o" "rigRN.phl[423]";
connectAttr "R_eyeLid_ctrl_scaleZ.o" "rigRN.phl[424]";
connectAttr "L_eyeLid_ctrl_rotateX.o" "rigRN.phl[425]";
connectAttr "L_eyeLid_ctrl_rotateY.o" "rigRN.phl[426]";
connectAttr "L_eyeLid_ctrl_rotateZ.o" "rigRN.phl[427]";
connectAttr "L_eyeLid_ctrl_translateX.o" "rigRN.phl[428]";
connectAttr "L_eyeLid_ctrl_translateY.o" "rigRN.phl[429]";
connectAttr "L_eyeLid_ctrl_translateZ.o" "rigRN.phl[430]";
connectAttr "L_eyeLid_ctrl_visibility.o" "rigRN.phl[431]";
connectAttr "L_eyeLid_ctrl_scaleX.o" "rigRN.phl[432]";
connectAttr "L_eyeLid_ctrl_scaleY.o" "rigRN.phl[433]";
connectAttr "L_eyeLid_ctrl_scaleZ.o" "rigRN.phl[434]";
connectAttr "M_Head_offset_pointConstraint1_nodeState.o" "rigRN.phl[435]";
connectAttr "M_Head_offset_pointConstraint1_M_Head_pointConstrain_offsetW0.o" "rigRN.phl[436]"
		;
connectAttr "M_Head_offset_pointConstraint1_offsetX.o" "rigRN.phl[437]";
connectAttr "M_Head_offset_pointConstraint1_offsetY.o" "rigRN.phl[438]";
connectAttr "M_Head_offset_pointConstraint1_offsetZ.o" "rigRN.phl[439]";
connectAttr "imagePlaneShape1.msg" ":perspShape.ip" -na;
connectAttr ":defaultColorMgtGlobals.cme" "imagePlaneShape1.cme";
connectAttr ":defaultColorMgtGlobals.cfe" "imagePlaneShape1.cmcf";
connectAttr ":defaultColorMgtGlobals.cfp" "imagePlaneShape1.cmcp";
connectAttr ":defaultColorMgtGlobals.wsn" "imagePlaneShape1.ws";
connectAttr "expression1.out[0]" "imagePlaneShape1.fe";
connectAttr "polyCube1.out" "pCubeShape1.i";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "aTools_StoreNode.o" "scene.m";
connectAttr "rigRN.phl[120]" "pairBlend1.iry2";
connectAttr "rigRN.phl[121]" "pairBlend1.irx2";
connectAttr "rigRN.phl[122]" "pairBlend1.irz2";
connectAttr "pairBlend1_inRotateX1.o" "pairBlend1.irx1";
connectAttr "pairBlend1_inRotateY1.o" "pairBlend1.iry1";
connectAttr "pairBlend1_inRotateZ1.o" "pairBlend1.irz1";
connectAttr ":defaultArnoldDisplayDriver.msg" ":defaultArnoldRenderOptions.drivers"
		 -na;
connectAttr ":defaultArnoldFilter.msg" ":defaultArnoldRenderOptions.filt";
connectAttr ":defaultArnoldDriver.msg" ":defaultArnoldRenderOptions.drvr";
connectAttr ":time1.o" "expression1.tim";
connectAttr "aTools_StoreNode.o" "tUtilities.m";
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "pCubeShape1.iog" ":initialShadingGroup.dsm" -na;
// End of Creep2_Anim_Shock2Laugh.7.ma
